<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
ini_set('memory_limit','2048M');
ini_set('max_execution_time', 999);
require_once("thumb.php");

$path = dirname(__FILE__).'/../viewer/panoramas/';
$dir = new DirectoryIterator($path);
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot() && ($fileinfo->isFile())) {
        $file_path = $fileinfo->getRealPath();
        $file_name = $fileinfo->getBasename();
        $thumb_path = str_replace(DIRECTORY_SEPARATOR."panoramas".DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR."panoramas".DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR,$file_path);
        if(!file_exists($thumb_path)) {
            $tg = new thumbnailGenerator;
            $tg->generate($file_path, 213, 120, $thumb_path);
        }
    }
}

$path = dirname(__FILE__).'/../viewer/gallery/';
$dir = new DirectoryIterator($path);
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot() && ($fileinfo->isFile())) {
        $file_path = $fileinfo->getRealPath();
        $file_name = $fileinfo->getBasename();
        $thumb_path = str_replace(DIRECTORY_SEPARATOR."gallery".DIRECTORY_SEPARATOR,DIRECTORY_SEPARATOR."gallery".DIRECTORY_SEPARATOR."thumb".DIRECTORY_SEPARATOR,$file_path);
        if(!file_exists($thumb_path)) {
            $tg = new thumbnailGenerator;
            $tg->generate($file_path, 200, 200, $thumb_path);
        }
    }
}