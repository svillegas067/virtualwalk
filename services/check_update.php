<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../db/connection.php");

//UPDATE 1.4
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'allow_pitch';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `allow_pitch` BOOL NOT NULL DEFAULT '1';");
    }
}

//UPDATE 1.5
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'song';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `song` varchar(50) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'song_autoplay';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `song_autoplay` tinyint(1) NOT NULL DEFAULT '0';");
    }
}

//UPDATE 1.6
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'role';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `role` varchar(50) DEFAULT 'customer';");
        $result2 = $mysqli->query("SELECT * FROM svt_users WHERE role='administrator';");
        if ($result2->num_rows==0) {
            $mysqli->query("UPDATE svt_users SET role='administrator' LIMIT 1;");
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'id_plan';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `id_plan` bigint(20) DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'active';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `active` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW TABLES LIKE 'svt_plans';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_plans` (
                                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                  `name` varchar(50) DEFAULT NULL,
                                  `n_virtual_tours` int(11) DEFAULT NULL,
                                  `n_rooms` int(11) DEFAULT NULL,
                                  `n_markers` int(11) DEFAULT NULL,
                                  `n_pois` int(11) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $mysqli->query("INSERT INTO `svt_plans` (`id`, `name`, `n_virtual_tours`, `n_rooms`, `n_markers`, `n_pois`) VALUES(1, 'Unlimited', -1, -1, -1, -1);");
    }
}

//UPDATE 1.7
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'logo';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `logo` varchar(50) DEFAULT NULL;");
    }
}

//UPDATE 1.8
$result = $mysqli->query("SHOW TABLES LIKE 'svt_plans';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_presentations` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
                              `id_room` bigint(20) unsigned DEFAULT NULL,
                              `action` varchar(50) DEFAULT NULL,
                              `params` text,
                              `sleep` int(11) NOT NULL DEFAULT '0',
                              `priority_1` int(11) DEFAULT NULL,
                              `priority_2` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `id_virtual_tour` (`id_virtualtour`),
                              KEY `id_room` (`id_room`),
                              CONSTRAINT `svt_presentations_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                              CONSTRAINT `svt_presentations_ibfk_2` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }
}

//UPDATE 1.9
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'nadir_logo';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `nadir_logo` varchar(50) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'nadir_size';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `nadir_size` varchar(25) NOT NULL DEFAULT 'small';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'autorotate_inactivity';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `autorotate_inactivity` int(11) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'autorotate_speed';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `autorotate_speed` int(11) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'markers_icon';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `markers_icon` varchar(50) NOT NULL DEFAULT 'fas fa-chevron-circle-up';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'markers_show_room';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `markers_show_room` tinyint(1) NOT NULL DEFAULT '1';");
    }
}

//UPDATE 2.0
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'html') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','html') DEFAULT NULL;");
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'arrows_nav';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `arrows_nav` tinyint(1) NOT NULL DEFAULT '1';");
    }
}

$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'info_box';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `info_box` longtext;");
    }
}

$result = $mysqli->query("SHOW TABLES LIKE 'svt_gallery';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_gallery` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
                              `image` varchar(50) DEFAULT NULL,
                              `priority` int(11) NOT NULL DEFAULT '0',
                              PRIMARY KEY (`id`),
                              KEY `id_virtualtour` (`id_virtualtour`),
                              CONSTRAINT `svt_gallery_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }
}

//UPDATE 2.1
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'priority';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `priority` int(11) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'password';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `password` varchar(200) DEFAULT NULL;");
    }
}

//UPDATE 2.2
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'id_map';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `id_map` bigint(20) unsigned DEFAULT NULL AFTER `yaw`;");
    }
}
$result = $mysqli->query("SHOW TABLES LIKE 'svt_maps';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_maps` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_virtualtour` bigint(20) unsigned NOT NULL,
                              `map` varchar(200) DEFAULT NULL,
                              `point_color` varchar(25) NOT NULL DEFAULT '#005eff',
                              `name` varchar(200) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `id_virtualtour` (`id_virtualtour`),
                              CONSTRAINT `svt_maps_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
        $query_m = "SELECT id,map FROM svt_virtualtours WHERE map <> '';";
        $result_m = $mysqli->query($query_m);
        if($result_m) {
            if($result_m->num_rows>0) {
                while($row_m = $result_m->fetch_array(MYSQLI_ASSOC)) {
                    $id_vt = $row_m['id'];
                    $map = $row_m['map'];
                    $result_i = $mysqli->query("INSERT INTO svt_maps(id_virtualtour,map,name) VALUES($id_vt,'$map','Main');");
                    if($result_i) {
                        $id_map = $mysqli->insert_id;
                        $mysqli->query("UPDATE svt_rooms SET id_map=$id_map WHERE id_virtualtour=$id_vt AND map_top IS NOT NULL;");
                    }
                }
            }
        }
        $mysqli->query("ALTER TABLE svt_virtualtours DROP COLUMN `map`;");
    }
}

//UPDATE 2.5
$result = $mysqli->query("SHOW TABLES LIKE 'svt_voice_commands';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_voice_commands` (
                              `id` int(11) NOT NULL DEFAULT '0',
                              `language` varchar(10) NOT NULL DEFAULT 'en-US',
                              `initial_msg` varchar(200) NOT NULL DEFAULT 'Listening ... Say HELP for command list',
                              `listening_msg` varchar(200) NOT NULL DEFAULT 'Listening ...',
                              `next_cmd` varchar(200) NOT NULL DEFAULT 'next',
                              `next_msg` varchar(200) NOT NULL DEFAULT 'Ok, going to next room',
                              `prev_cmd` varchar(200) NOT NULL DEFAULT 'prev',
                              `prev_msg` varchar(200) NOT NULL DEFAULT 'Ok, going to previous room',
                              `left_cmd` varchar(200) NOT NULL DEFAULT 'left',
                              `left_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking left',
                              `right_cmd` varchar(200) NOT NULL DEFAULT 'right',
                              `right_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking right',
                              `up_cmd` varchar(200) NOT NULL DEFAULT 'up',
                              `up_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking up',
                              `down_cmd` varchar(200) NOT NULL DEFAULT 'down',
                              `down_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking down',
                              `help_cmd` varchar(200) NOT NULL DEFAULT 'help',
                              `help_msg_1` varchar(200) NOT NULL DEFAULT 'Say NEXT / PREVIOUS to navigate between rooms',
                              `help_msg_2` varchar(200) NOT NULL DEFAULT 'Say LEFT / RIGHT / UP / DOWN to look around',
                              `error_msg` varchar(200) NOT NULL DEFAULT 'I do not understand, repeat please...',
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $mysqli->query("INSERT IGNORE INTO `svt_voice_commands` (`id`) VALUES(1);");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'voice_commands';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `voice_commands` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'icon';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `icon` varchar(50) DEFAULT NULL AFTER `type`;");
        $mysqli->query("UPDATE svt_pois SET icon='fas fa-image' WHERE `type`='image';");
        $mysqli->query("UPDATE svt_pois SET icon='fas fa-video' WHERE `type`='video';");
        $mysqli->query("UPDATE svt_pois SET icon='fas fa-link' WHERE `type`='link';");
        $mysqli->query("UPDATE svt_pois SET icon='fas fa-info-circle' WHERE `type`='html';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'html_sc') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','html','html_sc','download') DEFAULT NULL;");
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'markers_color';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `markers_color` varchar(25) NOT NULL DEFAULT '#000000' AFTER `markers_icon`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'markers_background';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `markers_background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)' AFTER `markers_icon`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'color';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `color` varchar(25) NOT NULL DEFAULT '#000000' AFTER `icon`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'background';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)' AFTER `icon`;");
    }
}

//UPDATE 2.6
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'compass';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `compass` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'background_image';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `background_image` varchar(50) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'auto_start';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `auto_start` tinyint(1) NOT NULL DEFAULT '1';");
    }
}

//UPDATE 2.7
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'description';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `description` text DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'ga_tracking_id';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `ga_tracking_id` varchar(50) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'friendly_url';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `friendly_url` varchar(200) DEFAULT NULL;");
    }
}

//UPDATE 2.8
$result = $mysqli->query("SHOW COLUMNS FROM svt_maps LIKE 'point_size';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_maps ADD `point_size` int(11) NOT NULL DEFAULT '20';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'compress_jpg';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `compress_jpg` int(11) NOT NULL DEFAULT '90';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'active';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `active` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'max_pitch';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `max_pitch` int(11) NOT NULL DEFAULT '90' AFTER `allow_pitch`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'min_pitch';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `min_pitch` int(11) NOT NULL DEFAULT '-90' AFTER `allow_pitch`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_markers LIKE 'rotateZ';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_markers ADD `rotateZ` int(11) NOT NULL DEFAULT '0' AFTER `yaw`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_markers LIKE 'rotateX';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_markers ADD `rotateX` int(11) NOT NULL DEFAULT '0' AFTER `yaw`;");
    }
}
$result = $mysqli->query("SHOW TABLES LIKE 'svt_settings';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_settings` (
                              `id` int(11) NOT NULL DEFAULT '0',
                              `purchase_code` varchar(250) DEFAULT NULL,
                              `license` varchar(250) DEFAULT NULL,
                              `name` varchar(200) DEFAULT 'Simple Virtual Tour',
                              `logo` varchar(50) DEFAULT NULL,
                              `background` varchar(50) DEFAULT NULL,
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $mysqli->query("INSERT IGNORE INTO `svt_settings` (`id`) VALUES(1);");
    }
}

//UPDATE 2.9
$result = $mysqli->query("SHOW TABLES LIKE 'svt_icons';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_icons` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
                              `image` varchar(50) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `id_virtualtour` (`id_virtualtour`),
                              CONSTRAINT `svt_icon_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'pois_color';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `pois_color` varchar(25) NOT NULL DEFAULT '#000000' AFTER `markers_show_room`;");
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `pois_background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)' AFTER `markers_show_room`;");
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `pois_icon` varchar(50) NOT NULL DEFAULT 'fas fa-info-circle' AFTER `markers_show_room`;");
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `pois_style` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'style';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `style` tinyint(1) NOT NULL DEFAULT '0' AFTER `type`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_markers LIKE 'icon';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_markers ADD `icon` varchar(50) NOT NULL DEFAULT 'fas fa-chevron-circle-up';");
        $mysqli->query("ALTER TABLE svt_markers ADD `color` varchar(25) NOT NULL DEFAULT '#000000';");
        $mysqli->query("ALTER TABLE svt_markers ADD `background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)';");
        $mysqli->query("ALTER TABLE svt_markers ADD `show_room` tinyint(1) NOT NULL DEFAULT '1';");
        $query_v = "SELECT id,markers_icon,markers_color,markers_background,markers_show_room FROM svt_virtualtours;";
        $result_v = $mysqli->query($query_v);
        if($result_v) {
            if($result_v->num_rows>0) {
                while($row_v = $result_v->fetch_array(MYSQLI_ASSOC)) {
                    $id_vt = $row_v['id'];
                    $markers_icon = $row_v['markers_icon'];
                    $markers_color = $row_v['markers_color'];
                    $markers_background = $row_v['markers_background'];
                    $markers_show_room = $row_v['markers_show_room'];
                    $mysqli->query("UPDATE svt_markers SET icon='$markers_icon',color='$markers_color',background='$markers_background',show_room=$markers_show_room 
                                            WHERE id_room IN (SELECT id FROM svt_rooms WHERE id_virtualtour=$id_vt);");
                }
            }
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'size_scale';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `size_scale` float NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_markers LIKE 'size_scale';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_markers ADD `size_scale` float NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'id_icon_library';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `id_icon_library` bigint(20) unsigned NOT NULL DEFAULT '0' AFTER `icon`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_markers LIKE 'id_icon_library';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_markers ADD `id_icon_library` bigint(20) unsigned NOT NULL DEFAULT '0' AFTER `icon`;");
    }
}

//UPDATE 2.9.1
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'link_ext') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','link_ext','html','html_sc','download') DEFAULT NULL;");
        }
    }
}

//UPDATE 2.9.2
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'link_logo';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `link_logo` varchar(250) DEFAULT NULL AFTER `logo`;");
    }
}

//UPDATE 3.0
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'max_width_compress';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `max_width_compress` int(11) NOT NULL DEFAULT '8192' AFTER `compress_jpg`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'sameAzimuth';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `sameAzimuth` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'access_count';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `access_count` bigint(20) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'access_count';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `access_count` bigint(20) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW TABLES LIKE 'svt_rooms_access_log';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE `svt_rooms_access_log` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_room` bigint(20) unsigned DEFAULT NULL,
                              `time` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `id_room` (`id_room`),
                              CONSTRAINT `svt_rooms_access_log_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'form') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','link_ext','html','html_sc','download','form') DEFAULT NULL;");
        }
    }
}
$result = $mysqli->query("SHOW TABLES LIKE 'svt_forms_data';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE `svt_forms_data` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_virtualtour` bigint(20) unsigned NOT NULL,
                              `id_room` bigint(20) unsigned DEFAULT NULL,
                              `title` varchar(250) DEFAULT NULL,
                              `field1` text,
                              `field2` text,
                              `field3` text,
                              `field4` text,
                              `field5` text,
                              PRIMARY KEY (`id`),
                              KEY `id_virtualtour` (`id_virtualtour`),
                              CONSTRAINT `svt_forms_data_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;");
    }
}

//UPDATE 3.1
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'auto_show_slider';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `auto_show_slider` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'form_enable';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `form_enable` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'form_icon';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `form_icon` varchar(50) NOT NULL DEFAULT 'fas fa-file-signature';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'form_content';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `form_content` text DEFAULT NULL;");
    }
}

//UPDATE 3.2
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'email';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `email` varchar(100) DEFAULT NULL AFTER `username`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'forgot_code';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `forgot_code` varchar(16) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_settings LIKE 'smtp_server';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_server` varchar(100) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_auth` tinyint(1) NOT NULL DEFAULT '0';");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_username` varchar(100) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_password` varchar(100) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_secure` enum('none','ssl','tls') DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_port` int(11) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_from_email` varchar(100) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_from_name` varchar(100) DEFAULT NULL;");
        $mysqli->query("ALTER TABLE svt_settings ADD `smtp_valid` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'label';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `label` varchar(100) DEFAULT NULL AFTER `icon`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'title';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `title` varchar(100) DEFAULT NULL AFTER `color`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'description';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_pois ADD `description` text AFTER `title`;");
    }
}

//UPDATE 3.3
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'visible_list';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `visible_list` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'html_landing';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `html_landing` longtext;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'fb_messenger';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `fb_messenger` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'fb_page_id';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `fb_page_id` varchar(100) DEFAULT NULL;");
    }
}

//UPDATE 3.4
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'video360') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','link_ext','html','html_sc','download','form','video360') DEFAULT NULL;");
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_settings LIKE 'enable_registration';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_settings ADD `enable_registration` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_settings LIKE 'default_id_plan';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_settings ADD `default_id_plan` bigint(20) unsigned DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_settings LIKE 'furl_blacklist';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_settings ADD `furl_blacklist` text;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_plans LIKE 'days';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_plans ADD `days` int(11) NOT NULL DEFAULT '-1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'registration_date';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `registration_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_users LIKE 'expire_plan_date';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_users ADD `expire_plan_date` DATETIME DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'type';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `type` enum('image','video') DEFAULT 'image' AFTER `name`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'panorama_video';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `panorama_video` varchar(100) DEFAULT NULL AFTER `panorama_image`;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_plans LIKE 'create_landing';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_plans ADD `create_landing` tinyint(1) NOT NULL DEFAULT '1';");
    }
}

//UPDATE 3.5
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_info';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_info` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_gallery';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_gallery` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_icons_toggle';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_icons_toggle` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_presentation';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_presentation` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_main_form';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_main_form` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_share';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_share` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_device_orientation';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_device_orientation` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_webvr';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_webvr` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_map';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_map` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_fullscreen';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_fullscreen` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_audio';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_audio` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'live_session';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `live_session` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'song';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `song` varchar(50) DEFAULT NULL;");
    }
}

//UPDATE 3.6
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'annotation_title';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `annotation_title` varchar(100) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_rooms LIKE 'annotation_description';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_rooms ADD `annotation_description` text;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_annotations';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_annotations` tinyint(1) NOT NULL DEFAULT '1';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'show_list_alt';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `show_list_alt` tinyint(1) NOT NULL DEFAULT '0';");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'list_alt';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `list_alt` text;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'audio') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','link_ext','html','html_sc','download','form','video360','audio') DEFAULT NULL;");
        }
    }
}

//UPDATE 3.7
$result = $mysqli->query("SHOW TABLES LIKE 'svt_poi_gallery';");
if($result) {
    if ($result->num_rows == 0) {
        $mysqli->query("CREATE TABLE IF NOT EXISTS `svt_poi_gallery` (
                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                              `id_poi` bigint(20) unsigned DEFAULT NULL,
                              `image` varchar(50) DEFAULT NULL,
                              `priority` int(11) NOT NULL DEFAULT '0',
                              PRIMARY KEY (`id`),
                              KEY `id_poi` (`id_poi`),
                              CONSTRAINT `svt_poi_gallery_ibfk_1` FOREIGN KEY (`id_poi`) REFERENCES `svt_pois` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_pois LIKE 'type';");
if($result) {
    if ($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $type = $row['Type'];
        if (strpos($type, 'gallery') === false) {
            $mysqli->query("ALTER TABLE svt_pois MODIFY COLUMN `type` enum('image','video','link','link_ext','html','html_sc','download','form','video360','audio','gallery') DEFAULT NULL;");
        }
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'intro_desktop';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `intro_desktop` varchar(50) DEFAULT NULL;");
    }
}
$result = $mysqli->query("SHOW COLUMNS FROM svt_virtualtours LIKE 'intro_mobile';");
if($result) {
    if ($result->num_rows==0) {
        $mysqli->query("ALTER TABLE svt_virtualtours ADD `intro_mobile` varchar(50) DEFAULT NULL;");
    }
}