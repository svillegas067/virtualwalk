<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once(__DIR__."/../db/connection.php");

$path = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;

$files = glob($path."backend".DIRECTORY_SEPARATOR."tmp_panoramas".DIRECTORY_SEPARATOR."*");
foreach($files as $file){
    if(is_file($file)) {
        unlink($file);
    }
}

$array_content_files = array();
$array_map_files = array();
$array_rooms_files = array();
$array_rooms_v_files = array();
$array_gallery_files = array();
$array_icon_files = array();
$array_assets_files = array();

$query = "SELECT song,logo,nadir_logo,background_image,intro_desktop,intro_mobile FROM svt_virtualtours;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $song = $row['song'];
            $logo = $row['logo'];
            $nadir_logo = $row['nadir_logo'];
            $background_image = $row['background_image'];
            $intro_desktop = $row['intro_desktop'];
            $intro_mobile = $row['intro_mobile'];
            if($song!='') {
                if(!in_array($song,$array_content_files)) {
                    array_push($array_content_files,$song);
                }
            }
            if($logo!='') {
                if(!in_array($logo,$array_content_files)) {
                    array_push($array_content_files,$logo);
                }
            }
            if($nadir_logo!='') {
                if(!in_array($nadir_logo,$array_content_files)) {
                    array_push($array_content_files,$nadir_logo);
                }
            }
            if($background_image!='') {
                if(!in_array($background_image,$array_content_files)) {
                    array_push($array_content_files,$background_image);
                }
            }
            if($intro_desktop!='') {
                if(!in_array($intro_desktop,$array_content_files)) {
                    array_push($array_content_files,$intro_desktop);
                }
            }
            if($intro_mobile!='') {
                if(!in_array($intro_mobile,$array_content_files)) {
                    array_push($array_content_files,$intro_mobile);
                }
            }
        }
    }
}

$query = "SELECT map FROM svt_maps;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $map = $row['map'];
            if($map!='') {
                if(!in_array($map,$array_map_files)) {
                    array_push($array_map_files,$map);
                }
            }
        }
    }
}

$query = "SELECT content FROM svt_pois WHERE type IN ('image','download','video','video360','audio') AND content LIKE '%viewer/content%';";
$result = $mysqli->query($query);
if($result) {
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $content = basename($row['content']);
            if($content!='') {
                if(!in_array($content,$array_content_files)) {
                    array_push($array_content_files,$content);
                }
            }
        }
    }
}

$query = "SELECT panorama_image,panorama_video,song FROM svt_rooms;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $panorama_image = $row['panorama_image'];
            if($panorama_image!='') {
                if(!in_array($panorama_image,$array_rooms_files)) {
                    array_push($array_rooms_files,$panorama_image);
                }
            }
            $panorama_video = $row['panorama_video'];
            if($panorama_video!='') {
                if(!in_array($panorama_video,$array_rooms_v_files)) {
                    array_push($array_rooms_v_files,$panorama_video);
                }
            }
            $song = $row['song'];
            if($song!='') {
                if(!in_array($song,$array_content_files)) {
                    array_push($array_content_files,$song);
                }
            }
        }
    }
}

$query = "SELECT image FROM svt_gallery;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $image = $row['image'];
            if($image!='') {
                if(!in_array($image,$array_gallery_files)) {
                    array_push($array_gallery_files,$image);
                }
            }
        }
    }
}
$query = "SELECT image FROM svt_poi_gallery;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $image = $row['image'];
            if($image!='') {
                if(!in_array($image,$array_gallery_files)) {
                    array_push($array_gallery_files,$image);
                }
            }
        }
    }
}

$query = "SELECT image FROM svt_icons;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $image = $row['image'];
            if($image!='') {
                if(!in_array($image,$array_icon_files)) {
                    array_push($array_icon_files,$image);
                }
            }
        }
    }
}

$query = "SELECT logo,background FROM svt_settings;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $logo = $row['logo'];
            if($logo!='') {
                if(!in_array($logo,$array_assets_files)) {
                    array_push($array_assets_files,$logo);
                }
            }
            $background = $row['background'];
            if($background!='') {
                if(!in_array($background,$array_assets_files)) {
                    array_push($array_assets_files,$background);
                }
            }
        }
    }
}

if(count($array_content_files)>0) {
    $files = glob($path."viewer".DIRECTORY_SEPARATOR."content".DIRECTORY_SEPARATOR."*");
    foreach($files as $file){
        if(is_file($file)) {
            $filename = basename($file);
            if(!in_array($filename,$array_content_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_map_files)>0) {
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "maps" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_map_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_rooms_files)>0) {
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "panoramas" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_rooms_files)) {
                unlink($file);
            }
        }
    }
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "panoramas" . DIRECTORY_SEPARATOR . "mobile" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_rooms_files)) {
                unlink($file);
            }
        }
    }
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "panoramas" . DIRECTORY_SEPARATOR . "thumb" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_rooms_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_rooms_v_files)>0) {
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_rooms_v_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_gallery_files)>0) {
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "gallery" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_gallery_files)) {
                unlink($file);
            }
        }
    }
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "gallery" . DIRECTORY_SEPARATOR . "thumb" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_gallery_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_icon_files)>0) {
    $files = glob($path . "viewer" . DIRECTORY_SEPARATOR . "icons" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_icon_files)) {
                unlink($file);
            }
        }
    }
}

if(count($array_assets_files)>0) {
    $files = glob($path . "backend" . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR . "*");
    foreach ($files as $file) {
        if (is_file($file)) {
            $filename = basename($file);
            if (!in_array($filename, $array_assets_files)) {
                unlink($file);
            }
        }
    }
}