ABOUT
Simple Virtual Tour has been developed entirely in php, html and jquery.
It’s simple and intuitive: just use the backend to create as many virtual tour you want!

FEATURES VIEWER
- preload of all rooms
- nav marker for move from one room to another
- pois marker for show image, gallery, video, video 360, audio, text, html, link, form or download file
- multi maps with the position where you are
- compass to indicate north
- controls (zoom, fullscreen, map, room list, icons, song, share)
- custom logo
- nadir logo (to hide tripod)
- autorotate on inactivity
- presentation
- info box
- image's gallery
- facebook messenger chat
- live session (shared virtual tour - video call - chat)
- webvr support
- 360 video support
- voice commands support
- google analytics integration
- responsive

FEATURES BACKEND
- simple installation
- create / edit customers and administrators (require Extendend license)
- create / edit plans to associate to users with limitations (require Extendend license)
- dashboard
- create / edit unlimited virtual tours
- upload panoramas and create / edit unlimited rooms
- add / edit markers
- add / edit pois with custom contents
- create / edit maps (assign room's position map points)
- create / edit presentations
- create / edit image's gallery
- custom icons library
- statistics
- landing page
- preview
- publish (link or embed code generator)
- protect virtual tour with a password
- customize voice commands
- white label settings
- responsive

REQUIREMENTS
- PHP and MYSQL

INSTALLATION
1) unzip simple_virtual_tour.zip
2) copy all files and directories into your hosting server root directory
3) access to http://yourserver/root_dir/install/start.php
4) follow the wizard and complete the installation

UPDATE
1) take backup of file /config/config.inc.php
2) take backup of directories /viewer/panoramas/, /viewer/videos/, /viewer/content/, /viewer/maps/, /viewer/gallery/, /viewer/icons/, /backend/assets/
3) upload new file versions into your hosting server root directory
4) logout from backend and re-login
P.S. if config file (1) and directories(2) missings, restore from your backup copy

Changelog
3.7
- added POI type images gallery
- added keyboard's navigation support
- added intro images for displaying instruction or something else
- when editing the room, the point on the map and its viewing angle are shown to better set the north
- detect hyperlinks in live session chat
- when uploading an image with the bulk function, the file name is retained as the map name
- fixed a bug with room's menu list creation
- fixed audio autoplay
3.6
- added Room's Annotations
- added Room's List Menu editable to show an organized textual list of rooms
- added POI type audio
- moved info box creation in a separated menu with a better content editor
- when uploading an image with the bulk function, the file name is retained as the room name
3.5
- added live sessions to invite people to join a shared virtual tour session with video call and chat
- added upload of audio files in the rooms that are played when entering them
- added more style settings to virtual tours for hide/show some viewer components
- fixed device orientation that was deactivated sometimes
- fixed some backend issues for iphone / ipad
3.4
- added support for 360 videos as room's panorama
- added POI type to play 360 videos
- added registration for customers with default plan assignment (useful for trial)
- added expires day for plans
- added landing page creation toggle for plans
- added friendly urls blacklist into settings to limit their use from customers
- fixed a bug with the room list slider
- fixed a bug that background sound not stop after open a video
3.3
- modified angle of view's direction to the map with the color of the pointer
- added an option to rooms to show or not into the slider list
- added editor for create landing page
- added facebook messenger chat
- cleaned viewer interface and redesigned the menu
3.2
- added angle of view's direction to the map
- added email field to users
- added edit profile for current logged-in user (change username, email, password)
- added mail server settings
- added forgot password to login
- added more POI styles
- added title and description to POI type image
- added possibility to upload local mp4 video to POI type video
3.1
- added POI form fields's type for better validation
- added settings to auto show room's list slider after virtual tour load
- added customizable main form for entire virtual tours
- added bulk upload map images (fast create multiple maps)
3.0
- added same azimuth in virtual tour settings to maintain the same direction with regard to north while navigate between rooms
- accept also PNG panorama files
- added maximum width setting in pixels of panoramic images. if they exceed this width the images will be resized
- added more detailed statistics
- added POI type form to create simple form and store collected data on database
- fixed a bug in editing presentation elements order
2.9.2
- added virtual tour's hyperlink setting for logo
- reusable content (logos and song) for new virtual tours creation
2.9.1
- added a POI type link (external), that open link in a new page instead of embed it
- cleaned up code that was detected as malware
2.9
- redesigned poi and markers backend sections
- added poi and markers individual style settings
- added possibility to resize poi and markers
- added icons library - custom images to use as poi and markers
2.8
- added possibility to change map points size
- added a compression settings for uploaded panorama images
- added possibility to limit upper and lower pitch degree of rooms
- added placement of markers in perspective
- added possibility to activate / deactivate virtual tours
- added white label settings
2.7
- added friendly url to publish link with a custom url
- added google analytics integration
- fixed meta tag for better share preview
2.6
- added possibility to auto start or not the virtual tour at loading
- added customizable background loading image
- added possibility to hide the compass
- added some shortcut on backend
- minor bugfix / improvements
2.5
- introduced voice commands support
- customizable poi's icon
- added poi types 'html' and 'download'
- added possibility to change markers color and background
- added possibility to change pois color and background
2.4
- introduced webvr support
2.3
- added bulk upload panorama images (fast create multiple rooms)
- sync room's list position when change room
2.2.1
- fixed a bug with whatsapp share
2.2
- added multi maps functionality
- added name of map and possibility to change color of points
- improved map visualization
2.1
- added possibility to change room's order
- added possibility to protect the virtual tour with a password
- fixed a bug in the gallery
2.0
- added POIs type content as custom html/text
- added navigation by next / prev arrows
- added a info box customizable via backend
- added an image's gallery customizable via backend
1.9
- added nadir logo (to hide tripod)
- added possibility to set autorotate on inactivity
- customizable marker's style / icon
1.8
- added a presentation feature customizable via backend
1.7
- added custom logo for each virtual tour
- cleaned loading screen
- speed up the first load with background preload of rooms
1.6.1
- improved compatibility / bugfix
1.6
- added users menu backend (only for administrator): manage customers and administrators who can use the application
- added plans menu backend (only for administrator): manage plans with limitation of creation of virtual tours, rooms, markers, pois
- added script that automatically clean unused images
1.5
- added control to show/hide map
- added control to show/hide icons
- added control to show/hide rooms list
- added control to share the virtual tour
- added control to play/stop song
1.4
- fixed room upload with resize if image is too large
- added possibility to allow or not vertical movement of rooms
- minor bugfix
1.3.1
- bugfix
1.3
- added a complete and intuitive backend for create virtual tours without editing code
1.2
- added pois for show image, video or external link
1.1
- correct some bug
- improved mobile resolution
1.0
- initial release