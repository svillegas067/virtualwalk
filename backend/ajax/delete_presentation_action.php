<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$id = $_POST['id'];

$result = $mysqli->query("DELETE FROM svt_presentations WHERE id_virtualtour=$id_virtualtour AND id=$id;");
if($result) {
    $mysqli->query("ALTER TABLE svt_presentations AUTO_INCREMENT = 1;");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}
