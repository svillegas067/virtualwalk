<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_poi = $_POST['id_poi'];

$query = "DELETE FROM svt_pois WHERE id=$id_poi; ";
$result = $mysqli->query($query);

if($result) {
    $mysqli->query("ALTER TABLE svt_pois AUTO_INCREMENT = 1;");
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}
