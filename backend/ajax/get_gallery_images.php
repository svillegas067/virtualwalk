<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];

$array = array();
$query = "SELECT * FROM svt_gallery WHERE id_virtualtour=$id_virtualtour ORDER BY priority;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $array[]=$row;
        }
    }
}
echo json_encode($array);