<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$query = "SELECT v.code,IFNULL(COUNT(r.id),0) as count_rooms FROM svt_virtualtours as v
LEFT JOIN svt_rooms as r ON r.id_virtualtour=v.id
WHERE v.id = $id_virtualtour
GROUP BY v.id";
$result = $mysqli->query($query);
$code = "";
if($result) {
    if($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $code = $row['code'];
        $count_rooms = $row['count_rooms'];
    }
}
echo json_encode(array("code"=>$code,"count_rooms"=>$count_rooms));