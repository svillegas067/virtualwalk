<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

require(__DIR__.'/ssp.class.php');
require(__DIR__.'/../../config/config.inc.php');

$id_map = $_GET['id_map'];
$query = "SELECT * FROM svt_plans";
$table = "( $query ) t";
$primaryKey = 'id';

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'name',  'dt' =>0 ),
    array( 'db' => 'n_virtual_tours',  'dt' =>1, 'formatter' => function( $d, $row ) {
        if($d<0) {
            return "<i class=\"fas fa-infinity\"></i>";
        } else {
            return $d;
        }
    }),
    array( 'db' => 'n_rooms',  'dt' =>2, 'formatter' => function( $d, $row ) {
        if($d<0) {
            return "<i class=\"fas fa-infinity\"></i>";
        } else {
            return $d;
        }
    }),
    array( 'db' => 'n_markers',  'dt' =>3, 'formatter' => function( $d, $row ) {
        if($d<0) {
            return "<i class=\"fas fa-infinity\"></i>";
        } else {
            return $d;
        }
    }),
    array( 'db' => 'n_pois',  'dt' =>4, 'formatter' => function( $d, $row ) {
        if($d<0) {
            return "<i class=\"fas fa-infinity\"></i>";
        } else {
            return $d;
        }
    }),
    array( 'db' => 'create_landing',  'dt' =>5, 'formatter' => function( $d, $row ) {
        if($d==1) {
            return "<i class=\"fas fa-check\"></i>";
        } else {
            return "<i class=\"fas fa-times\"></i>";
        }
    }),
    array( 'db' => 'days',  'dt' =>6, 'formatter' => function( $d, $row ) {
        if($d<0) {
            return "<i class=\"fas fa-infinity\"></i>";
        } else {
            return $d;
        }
    }),

);

$sql_details = array(
    'user' => DATABASE_USERNAME,
    'pass' => DATABASE_PASSWORD,
    'db' => DATABASE_NAME,
    'host' => DATABASE_HOST);

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);