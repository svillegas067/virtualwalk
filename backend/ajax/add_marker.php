<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$id_room = $_POST['id_room'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
$id_room_target = $_POST['id_room_target'];
$rotateX = 0;
$rotateZ = 0;

$query_v = "SELECT markers_icon,markers_color,markers_background,markers_show_room FROM svt_virtualtours WHERE id=$id_virtualtour LIMIT 1;";
$result_v = $mysqli->query($query_v);
if($result_v) {
    if ($result_v->num_rows == 1) {
        $row_v = $result_v->fetch_array(MYSQLI_ASSOC);
        $markers_icon = $row_v['markers_icon'];
        $markers_color = $row_v['markers_color'];
        $markers_background = $row_v['markers_background'];
        $markers_show_room = $row_v['markers_show_room'];
        $query = "INSERT INTO svt_markers(id_room,yaw,pitch,id_room_target,rotateX,rotateZ,icon,color,background,show_room) VALUES($id_room,$yaw,$pitch,$id_room_target,$rotateX,$rotateZ,'$markers_icon','$markers_color','$markers_background',$markers_show_room);";
        $result = $mysqli->query($query);
        if($result) {
            $insert_id = $mysqli->insert_id;
            echo json_encode(array("status"=>"ok","id"=>$insert_id));
        } else {
            echo json_encode(array("status"=>"error"));
        }
    } else {
        echo json_encode(array("status"=>"error"));
    }
} else {
    echo json_encode(array("status"=>"error"));
}

