<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require(__DIR__.'/ssp.class.php');
require(__DIR__.'/../../config/config.inc.php');

$id_vt = $_GET['id_vt'];

$query = "SELECT f.*,r.name as room FROM svt_forms_data as f
LEFT JOIN svt_rooms as r ON r.id=f.id_room
WHERE f.id_virtualtour=$id_vt";
$table = "( $query ) t";
$primaryKey = 'id';

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'room',  'dt' =>0 ),
    array( 'db' => 'title',  'dt' =>1 ),
    array( 'db' => 'field1',  'dt' =>2 ),
    array( 'db' => 'field2',  'dt' =>3 ),
    array( 'db' => 'field3',  'dt' =>4 ),
    array( 'db' => 'field4',  'dt' =>5 ),
    array( 'db' => 'field5',  'dt' =>6 )
);

$sql_details = array(
    'user' => DATABASE_USERNAME,
    'pass' => DATABASE_PASSWORD,
    'db' => DATABASE_NAME,
    'host' => DATABASE_HOST);

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);