<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
ini_set('memory_limit','2048M');
ini_set('max_execution_time', 9999);
ini_set('max_input_time', 9999);
ini_set('post_max_size', '99MB');
ini_set('upload_max_filesize', '99MB');
require_once(dirname(__FILE__).'/ImageResize.php');
use \Gumlet\ImageResize;
if (!file_exists(dirname(__FILE__).'/../tmp_panoramas/')) {
    mkdir(dirname(__FILE__).'/../tmp_panoramas/', 0775);
}
$compress_jpg = $_SESSION['compress_jpg'];
$max_width_compress = $_SESSION['max_width_compress'];
if($compress_jpg=="") $compress_jpg=90;
if($max_width_compress=="") $max_width_compress=0;
if(isset($_FILES) && !empty($_FILES['file']['name'])){
    $allowed_ext = array('jpg','JPG','jpeg','JPEG','png','PNG');
    $filename = $_FILES['file']['name'];
    $ext = explode('.',$filename);
    $ext = end($ext);
    if(in_array($ext,$allowed_ext)){
        if(strtolower($ext)=='png') {
            png2jpg($_FILES['file']['tmp_name'],$_FILES['file']['tmp_name'],100);
        }
        $name = "pano_".time().".jpg";
        $moved = move_uploaded_file($_FILES['file']['tmp_name'],dirname(__FILE__).'/../tmp_panoramas/'.$name);
        if($moved) {
            list($width, $height) = getimagesize(dirname(__FILE__).'/../tmp_panoramas/'.$name);
            if($compress_jpg<100) {
                try {
                    $image = new ImageResize(dirname(__FILE__).'/../tmp_panoramas/'.$name);
                    $image->quality_jpg = $compress_jpg;
                    if($max_width_compress>0) {
                        $image->resizeToWidth($max_width_compress,false);
                    }
                    $image->gamma(false);
                    $image->save(dirname(__FILE__).'/../tmp_panoramas/'.$name);
                } catch (Exception $e) {}
            }
            $ratio = $width / $height;
            if($ratio>2) {
                $new_height = $width/2;
                $src = imagecreatefromjpeg(dirname(__FILE__).'/../tmp_panoramas/'.$name);
                $dst = imagecreatetruecolor($width, $new_height);
                imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
                scale_image($src,$dst);
                imagejpeg($dst, dirname(__FILE__).'/../tmp_panoramas/'.$name);
            }
            echo 'tmp_panoramas/'.$name;
        } else {
            echo 'ERROR: code:'.$_FILES["file"]["error"];
        }
    }else{
        echo 'ERROR: Only jpg and png files are supported';
    }
}else{
    echo 'ERROR: file not provided';
}

function scale_image($src_image, $dst_image, $op = 'fit') {
    $src_width = imagesx($src_image);
    $src_height = imagesy($src_image);

    $dst_width = imagesx($dst_image);
    $dst_height = imagesy($dst_image);

    // Try to match destination image by width
    $new_width = $dst_width;
    $new_height = round($new_width*($src_height/$src_width));
    $new_x = 0;
    $new_y = round(($dst_height-$new_height)/2);

    // FILL and FIT mode are mutually exclusive
    if ($op =='fill')
        $next = $new_height < $dst_height; else $next = $new_height > $dst_height;

    // If match by width failed and destination image does not fit, try by height
    if ($next) {
        $new_height = $dst_height;
        $new_width = round($new_height*($src_width/$src_height));
        $new_x = round(($dst_width - $new_width)/2);
        $new_y = 0;
    }

    // Copy image on right place
    imagecopyresampled($dst_image, $src_image , $new_x, $new_y, 0, 0, $new_width, $new_height, $src_width, $src_height);
}

function png2jpg($originalFile, $outputFile, $quality) {
    $image = imagecreatefrompng($originalFile);
    imagejpeg($image, $outputFile, $quality);
    imagedestroy($image);
}

exit;
