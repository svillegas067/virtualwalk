<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
require_once("../functions.php");
$id_user = $_POST['id_user'];
$id_virtualtour = $_POST['id_virtualtour'];

if(get_user_role($id_user)=='administrator') {
    $where_user = "";
} else {
    $where_user = " AND v.id_user = $id_user ";
}

$array = array();
$query = "SELECT r.id,r.name,r.type,r.panorama_image,(SELECT COUNT(*) FROM svt_markers WHERE id_room=r.id) as count_markers,(SELECT COUNT(*) FROM svt_pois WHERE id_room=r.id) as count_pois FROM svt_rooms as r 
JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
WHERE v.id = $id_virtualtour $where_user
GROUP BY r.id
ORDER BY r.priority ASC, r.id ASC";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $array[]=$row;
        }
    }
}
echo json_encode($array);