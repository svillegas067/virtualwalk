<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$name = str_replace("'","\'",strip_tags($_POST['name']));
$author = str_replace("'","\'",strip_tags($_POST['author']));
$hfov = $_POST['hfov'];
$min_hfov = $_POST['min_hfov'];
$max_hfov = $_POST['max_hfov'];
$song = $_POST['song'];
$song_autoplay = $_POST['song_autoplay'];
$logo = $_POST['logo'];
$link_logo = str_replace("'","\'",$_POST['link_logo']);
$background_image = $_POST['background_image'];
$nadir_logo = $_POST['nadir_logo'];
$nadir_size = $_POST['nadir_size'];
$intro_desktop = $_POST['intro_desktop'];
$intro_mobile = $_POST['intro_mobile'];
$autorotate_speed = $_POST['autorotate_speed'];
if($autorotate_speed=="") $autorotate_speed=0;
if($autorotate_speed>=10) $autorotate_speed=10;
if($autorotate_speed<=-10) $autorotate_speed=-10;
$autorotate_inactivity = $_POST['autorotate_inactivity'];
if($autorotate_inactivity=="") $autorotate_inactivity=0;
$markers_icon = $_POST['markers_icon'];
$markers_color = $_POST['markers_color'];
$markers_background = $_POST['markers_background'];
$markers_show_room = $_POST['markers_show_room'];
$pois_icon = $_POST['pois_icon'];
$pois_color = $_POST['pois_color'];
$pois_background = $_POST['pois_background'];
$pois_style = $_POST['pois_style'];
$arrows_nav = $_POST['arrows_nav'];
$voice_commands = $_POST['voice_commands'];
$compass = $_POST['compass'];
$auto_start = $_POST['auto_start'];
$sameAzimuth = $_POST['sameAzimuth'];
$auto_show_slider = $_POST['auto_show_slider'];
$show_list_alt = $_POST['show_list_alt'];
$description = str_replace("'","\'",strip_tags($_POST['description']));
$ga_tracking_id = $_POST['ga_tracking_id'];
$compress_jpg = $_POST['compress_jpg'];
if($compress_jpg=="") { $compress_jpg=90; }
$max_width_compress = $_POST['max_width_compress'];
if($max_width_compress=="") $max_width_compress=0;
$form_enable = $_POST['form_enable'];
$form_icon = $_POST['form_icon'];
$form_content = $_POST['form_content'];
$fb_messenger = $_POST['fb_messenger'];
$fb_page_id = strip_tags($_POST['fb_page_id']);
$show_info = $_POST['show_info'];
$show_gallery = $_POST['show_gallery'];
$show_icons_toggle = $_POST['show_icons_toggle'];
$show_presentation = $_POST['show_presentation'];
$show_main_form = $_POST['show_main_form'];
$show_share = $_POST['show_share'];
$show_device_orientation = $_POST['show_device_orientation'];
$show_webvr = $_POST['show_webvr'];
$show_audio = $_POST['show_audio'];
$show_fullscreen = $_POST['show_fullscreen'];
$show_map = $_POST['show_map'];
$live_session = $_POST['live_session'];
$show_annotations = $_POST['show_annotations'];

$query = "UPDATE svt_virtualtours SET name='$name',author='$author',hfov=$hfov,min_hfov=$min_hfov,max_hfov=$max_hfov,song='$song',song_autoplay=$song_autoplay,logo='$logo',background_image='$background_image',nadir_logo='$nadir_logo',nadir_size='$nadir_size',autorotate_speed=$autorotate_speed,autorotate_inactivity=$autorotate_inactivity,markers_icon='$markers_icon',markers_color='$markers_color',markers_background='$markers_background',markers_show_room=$markers_show_room,pois_icon='$pois_icon',pois_color='$pois_color',pois_background='$pois_background',pois_style=$pois_style,arrows_nav=$arrows_nav,voice_commands=$voice_commands,compass=$compass,auto_start=$auto_start,sameAzimuth=$sameAzimuth,auto_show_slider=$auto_show_slider,description='$description',ga_tracking_id='$ga_tracking_id',compress_jpg=$compress_jpg,link_logo='$link_logo',max_width_compress=$max_width_compress,form_enable=$form_enable,form_icon='$form_icon',form_content='$form_content',fb_messenger=$fb_messenger,fb_page_id='$fb_page_id', 
show_info=$show_info,show_gallery=$show_gallery,show_icons_toggle=$show_icons_toggle,show_presentation=$show_presentation,show_main_form=$show_main_form,show_share=$show_share,show_device_orientation=$show_device_orientation,show_webvr=$show_webvr,show_audio=$show_audio,show_fullscreen=$show_fullscreen,show_map=$show_map,live_session=$live_session,show_annotations=$show_annotations,show_list_alt=$show_list_alt,intro_desktop='$intro_desktop',intro_mobile='$intro_mobile' 
WHERE id=$id_virtualtour;";
$result = $mysqli->query($query);

if($result) {
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

