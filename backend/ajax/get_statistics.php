<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$elem = $_POST['elem'];

$stats = array();


switch ($elem) {
    case 'chart_visitor_vt':
        $stats['labels'] = array();
        $stats['data'] = array();
        $query = "SELECT date_time,COUNT(*) as num FROM svt_access_log 
                    WHERE id_virtualtour=$id_virtualtour
                    GROUP BY MONTH(date_time),YEAR(date_time)
                    ORDER BY date_time;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    array_push($stats['labels'],date('M Y',strtotime($row['date_time'])));
                    array_push($stats['data'],$row['num']);
                }
            }
        }
        break;
    case 'chart_rooms_access':
        $stats['labels'] = array();
        $stats['data'] = array();
        $query = "SELECT r.name,r.access_count as num 
                    FROM svt_rooms as r
                    WHERE r.id_virtualtour=$id_virtualtour 
                    GROUP BY r.id
                    ORDER BY r.priority;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    array_push($stats['labels'],strtoupper($row['name']));
                    array_push($stats['data'],$row['num']);
                }
            }
        }
        break;
    case 'chart_rooms_time':
        $stats['labels'] = array();
        $stats['data'] = array();
        $query = "SELECT r.name,AVG(time) as num 
                    FROM svt_rooms_access_log as l
                    JOIN svt_rooms as r on r.id=l.id_room
                    WHERE r.id_virtualtour=$id_virtualtour 
                    GROUP BY l.id_room
                    ORDER BY r.priority;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    array_push($stats['labels'],strtoupper($row['name']));
                    array_push($stats['data'],round($row['num']));
                }
            }
        }
        break;
    case 'chart_poi_views':
        $stats['pois'] = array();
        $stats['total_poi'] = 0;
        $query = "SELECT r.name as room,p.type,p.content,p.access_count
                    FROM svt_pois as p
                    JOIN svt_rooms as r ON r.id=p.id_room
                    WHERE r.id_virtualtour=$id_virtualtour 
                    GROUP BY p.id
                    ORDER BY r.id,p.id;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    $stats['total_poi'] = $stats['total_poi'] + $row['access_count'];
                    array_push($stats['pois'],$row);
                }
            }
        }
        break;
}

echo json_encode($stats);