<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_poi = $_POST['id_poi'];

$array = array();
$query = "SELECT * FROM svt_poi_gallery WHERE id_poi=$id_poi ORDER BY priority;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $array[]=$row;
        }
    }
}
echo json_encode($array);