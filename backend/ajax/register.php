<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
require_once("../../db/connection.php");
require_once("../functions.php");

$settings = get_settings();
$id_plan = $settings['default_id_plan'];

$username = str_replace("'","\'",$_POST['username']);
$email = str_replace("'","\'",$_POST['email']);
$password = str_replace("'","\'",$_POST['password']);

$query_check = "SELECT * FROM svt_users WHERE username = '$username';";
$result_check = $mysqli->query($query_check);
if($result_check->num_rows>0) {
    echo json_encode(array("status"=>"error","msg"=>"Username already registered!"));
    exit;
}
$query_check = "SELECT * FROM svt_users WHERE email = '$email';";
$result_check = $mysqli->query($query_check);
if($result_check->num_rows>0) {
    echo json_encode(array("status"=>"error","msg"=>"E-mail already registered!"));
    exit;
}

$query = "INSERT INTO svt_users(username,email,password,role,id_plan) VALUES('$username','$email',MD5('$password'),'customer',$id_plan); ";
$result = $mysqli->query($query);

if($result) {
    $user_id = $mysqli->insert_id;
    $_SESSION['id_user'] = $user_id;
    update_plans_expires_date();
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

