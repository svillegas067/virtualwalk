<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once(__DIR__."/../functions.php");
$id_user = $_POST['id_user'];
$object = $_POST['object'];

$can_create = check_plan($object, $id_user);
if($can_create) {
    echo json_encode(array("can_create"=>1));
} else {
    echo json_encode(array("can_create"=>0));
}