<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_poi = $_POST['id'];
$type = $_POST['type'];
$content = str_replace("'","\'",$_POST['content']);
$title = str_replace("'","\'",strip_tags($_POST['title']));
$description = str_replace("'","\'",strip_tags($_POST['description']));
if($type=='html_sc') {
    $content = htmlspecialchars_decode($content);
}

$query = "UPDATE svt_pois SET content='$content',title='$title',description='$description' WHERE id=$id_poi;";
$result = $mysqli->query($query);

if($result) {
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

