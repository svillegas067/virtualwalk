<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$id_map = $_POST['id_map'];

$query = "UPDATE svt_rooms SET id_map=NULL,map_left=NULL,map_top=NULL WHERE id_virtualtour=$id_virtualtour AND id_map=$id_map;";
$result = $mysqli->query($query);

$query = "DELETE FROM svt_maps WHERE id=$id_map;";
$result = $mysqli->query($query);

if($result) {
    $mysqli->query("ALTER TABLE svt_maps AUTO_INCREMENT = 1;");
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

