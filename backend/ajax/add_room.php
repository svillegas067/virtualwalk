<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$name = str_replace("'","\'",strip_tags($_POST['name']));
$name = ucfirst($name);
$panorama_image = $_POST['panorama_image'];
if(isset($_POST['panorama_video'])) {
    $panorama_video = $_POST['panorama_video'];
} else {
    $panorama_video = '';
}
if(isset($_POST['type'])) {
    $type = $_POST['type'];
} else {
    $type = 'image';
}

switch($type) {
    case 'image':
        $name_image = str_replace("tmp_panoramas/","",$panorama_image);
        $name_video = '';
        $path_source = dirname(__FILE__).'/../tmp_panoramas/'.$name_image;
        $path_dest = dirname(__FILE__).'/../../viewer/panoramas/'.$name_image;
        if (!file_exists(dirname(__FILE__).'/../../viewer/panoramas/')) {
            mkdir(dirname(__FILE__).'/../../viewer/panoramas/', 0775);
            mkdir(dirname(__FILE__).'/../../viewer/panoramas/thumb/', 0775);
        }
        if(copy($path_source,$path_dest)) {
            unlink($path_source);
            include("../../services/generate_thumb.php");
            include("../../services/generate_pano_mobile.php");
        } else {
            echo json_encode(array("status"=>"error image"));
            die();
        }
        break;
    case 'video':
        $name_image = "pano_".time().".jpg";
        $name_video = str_replace("../viewer/videos/","",$panorama_video);
        $path_dest = dirname(__FILE__).'/../../viewer/panoramas/'.$name_image;
        $ifp = fopen($path_dest,'wb');
        $data = explode(',', $panorama_image);
        fwrite($ifp,base64_decode($data[1]));
        fclose( $ifp );
        include("../../services/generate_thumb.php");
        include("../../services/generate_pano_mobile.php");
        break;
}


$priority = 0;
$query = "SELECT MAX(priority)+1 as priority FROM svt_rooms WHERE id_virtualtour=$id_virtualtour LIMIT 1;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $priority = $row['priority'];
        if(empty($priority)) $priority=0;
    }
}

$query = "INSERT INTO svt_rooms(id_virtualtour,name,type,panorama_image,panorama_video,priority)
            VALUES($id_virtualtour,'$name','$type','$name_image','$name_video',$priority); ";
$result = $mysqli->query($query);

if($result) {
    $id_room = $mysqli->insert_id;
    echo json_encode(array("status"=>"ok","id"=>$id_room));
} else {
    echo json_encode(array("status"=>"error"));
}

