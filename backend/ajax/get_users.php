<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

require(__DIR__.'/ssp.class.php');
require(__DIR__.'/../../config/config.inc.php');

$query = "SELECT u.*,COALESCE(p.name, '--') as plan_name FROM svt_users as u LEFT JOIN svt_plans as p ON p.id = u.id_plan";
$table = "( $query ) t";
$primaryKey = 'id';

$columns = array(
    array(
        'db' => 'id',
        'dt' => 'DT_RowId',
        'formatter' => function( $d, $row ) {
            return $d;
        }
    ),
    array( 'db' => 'username',  'dt' =>0 ),
    array( 'db' => 'email',  'dt' =>1 ),
    array( 'db' => 'role',  'dt' =>2, 'formatter' => function( $d, $row ) {
        return ucfirst($d);
    }),
    array( 'db' => 'plan_name',  'dt' =>3, 'formatter' => function( $d, $row ) {
        if($row['expire_plan_date']==null) {
            return "<i class='fa fa-circle' style='color: green'></i> " . $d;
        } else {
            if (new DateTime() > new DateTime($row['expire_plan_date'])) {
                return "<i class='fa fa-circle' style='color: red'></i> " . $d;
            } else{
                return "<i class='fa fa-circle' style='color: green'></i> " . $d;
            }
        }
    }),
    array( 'db' => 'active',  'dt' =>4, 'formatter' => function( $d, $row ) {
        if($d) {
            return "<i class='fa fa-check'></i>";
        } else {
            return "<i class='fa fa-times'></i>";
        }
    }),
);

$sql_details = array(
    'user' => DATABASE_USERNAME,
    'pass' => DATABASE_PASSWORD,
    'db' => DATABASE_NAME,
    'host' => DATABASE_HOST);

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);