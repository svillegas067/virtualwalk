<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
require_once("../functions.php");
$id_user = $_POST['id_user'];
$id_virtualtour = $_POST['id_virtualtour'];

if(get_user_role($id_user)=='administrator') {
    $query = "DELETE FROM svt_virtualtours WHERE id=$id_virtualtour; ";
} else {
    $query = "DELETE FROM svt_virtualtours WHERE id_user=$id_user AND id=$id_virtualtour; ";
}
$result = $mysqli->query($query);

if($result) {
    $mysqli->query("ALTER TABLE svt_virtualtours AUTO_INCREMENT = 1;");
    if(isset($_SESSION['id_virtualtour_sel'])) {
        if($_SESSION['id_virtualtour_sel']==$id_virtualtour) {
            unset($_SESSION['id_virtualtour_sel']);
            unset($_SESSION['name_virtualtour_sel']);
        }
    }
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}
