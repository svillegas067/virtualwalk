<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$id_room = $_POST['id_room'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
$type = $_POST['type'];

$query_v = "SELECT pois_icon,pois_color,pois_background,pois_style FROM svt_virtualtours WHERE id=$id_virtualtour LIMIT 1;";
$result_v = $mysqli->query($query_v);
if($result_v) {
    if ($result_v->num_rows == 1) {
        $row_v = $result_v->fetch_array(MYSQLI_ASSOC);
        $pois_icon = $row_v['pois_icon'];
        $pois_color = $row_v['pois_color'];
        $pois_background = $row_v['pois_background'];
        $pois_style = $row_v['pois_style'];
        $query = "INSERT INTO svt_pois(id_room,yaw,pitch,type,icon,color,background,style) VALUES($id_room,$yaw,$pitch,'$type','$pois_icon','$pois_color','$pois_background',$pois_style);";
        $result = $mysqli->query($query);
        if($result) {
            $insert_id = $mysqli->insert_id;
            echo json_encode(array("status"=>"ok","id"=>$insert_id));
        } else {
            echo json_encode(array("status"=>"error"));
        }
    } else {
        echo json_encode(array("status"=>"error"));
    }
} else {
    echo json_encode(array("status"=>"error"));
}

