<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id = $_POST['id'];

$query = "DELETE FROM svt_icons WHERE id=$id;";
$result = $mysqli->query($query);

if($result) {
    $mysqli->query("UPDATE svt_markers SET show_room=0,id_icon_library=0 WHERE id_icon_library=$id;");
    $mysqli->query("UPDATE svt_pois SET style=0,id_icon_library=0 WHERE id_icon_library=$id;");
    $mysqli->query("ALTER TABLE svt_icons AUTO_INCREMENT = 1;");
    require_once("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

