<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$id_map = $_POST['id_map'];
$array = array();
$map = "";
$query = "SELECT id,map,point_color FROM svt_maps WHERE id_virtualtour = $id_virtualtour AND id=$id_map LIMIT 1;";
$result = $mysqli->query($query);
$all_points = true;
if($result) {
    if($result->num_rows==1) {
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $id_map = $row['id'];
        $map = $row['map'];
        $point_color = $row['point_color'];
        if($map==null) $map="";
        $query = "SELECT id,id_map,name,map_top,map_left FROM svt_rooms WHERE id_virtualtour = $id_virtualtour;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                    if($row['map_top']==null) {
                        $all_points = false;
                    }
                    $row['point_color'] = $point_color;
                    $array[]=$row;
                }
            }
        }
    }
}
echo json_encode(array("id_map"=>$id_map,"map"=>$map,"map_points"=>$array,"all_points"=>$all_points));