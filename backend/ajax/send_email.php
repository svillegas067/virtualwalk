<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once('../vendor/PHPMailer/Exception.php');
require_once('../vendor/PHPMailer/PHPMailer.php');
require_once('../vendor/PHPMailer/SMTP.php');
require_once('../functions.php');
require_once("../../db/connection.php");

$settings = get_settings();

$smtp_server = $settings['smtp_server'];
$smtp_auth = $settings['smtp_auth'];
$smtp_username = $settings['smtp_username'];
$smtp_password = $settings['smtp_password'];
$smtp_secure = $settings['smtp_secure'];
$smtp_port = $settings['smtp_port'];
$smtp_from_email = $settings['smtp_from_email'];
$smtp_from_name = $settings['smtp_from_name'];

$email = $_POST['email'];

switch ($_POST['type']) {
    case 'validate':
        $subject = 'Test e-mail';
        $body = 'This is a test e-mail for validating mail server settings.';
        break;
    case 'forgot':
        $query = "SELECT id FROM svt_users WHERE email='$email' LIMIT 1;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows==1) {
                $row = $result->fetch_array(MYSQLI_ASSOC);
                $id_user = $row['id'];
            } else {
                echo json_encode(array("status"=>"error","msg"=>"Invalid e-mail"));
                exit;
            }
        }
        $verification_code = generateRandomString(16);
        $subject = 'Verification code';
        $body = "This is your verification code: <b>$verification_code</b>";
        break;
}

$mail = new PHPMailer(true);
try {
    $mail->isSMTP();
    $mail->Timeout = 10;
    $mail->Host = $smtp_server;
    $mail->SMTPAuth = $smtp_auth;
    $mail->Username = $smtp_username;
    $mail->Password = $smtp_password;
    switch($smtp_secure) {
        case 'ssl':
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            break;
        case 'tls':
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            break;
    }
    $mail->Port = $smtp_port;
    $mail->setFrom($smtp_from_email, $smtp_from_name);
    $mail->addAddress($email);
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->send();
    switch ($_POST['type']) {
        case 'validate':
            $mysqli->query("UPDATE svt_settings SET smtp_valid=1;");
            break;
        case 'forgot':
            $mysqli->query("UPDATE svt_users SET forgot_code='$verification_code' WHERE id=$id_user;");
            break;
    }
    echo json_encode(array("status"=>"ok"));
} catch (Exception $e) {
    switch ($_POST['type']) {
        case 'validate':
            $mysqli->query("UPDATE svt_settings SET smtp_valid=0;");
            break;
    }
    echo json_encode(array("status"=>"error","msg"=>$mail->ErrorInfo));
}