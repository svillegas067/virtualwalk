<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_poi = $_POST['id'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
$size_scale = $_POST['size_scale'];

$query = "UPDATE svt_pois SET yaw=$yaw,pitch=$pitch,size_scale=$size_scale WHERE id=$id_poi;";
$result = $mysqli->query($query);

if($result) {
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

