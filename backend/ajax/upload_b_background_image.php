<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
if (!file_exists(dirname(__FILE__).'/../assets/')) {
    mkdir(dirname(__FILE__).'/../assets/', 0775);
}
if(isset($_FILES) && !empty($_FILES['file']['name'])){
    $allowed_ext = array('png','PNG','jpg','JPG','jpeg','JPEG');
    $filename = $_FILES['file']['name'];
    $ext = explode('.',$filename);
    $ext = end($ext);
    if(in_array($ext,$allowed_ext)){
        $name = "bg_".time().".$ext";
        $moved = move_uploaded_file($_FILES['file']['tmp_name'],dirname(__FILE__).'/../assets/'.$name);
        if($moved) {
            echo $name;
        } else {
            echo 'ERROR: code:'.$_FILES["file"]["error"];
        }
    }else{
        echo 'ERROR: Only jpg,png files are supported';
    }
}else{
    echo 'ERROR: file not provided';
}
exit;
