<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_room = $_POST['id_room'];
$array = array();
$query = "SELECT 'marker' as what,m.*,r.name as name_room_target,r.id as id_room_target,IFNULL(i.id,0) as id_icon_library, i.image as img_icon_library FROM svt_markers AS m
          JOIN svt_rooms AS r ON m.id_room_target = r.id 
          JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
          LEFT JOIN svt_icons as i ON i.id=m.id_icon_library
          WHERE m.id_room=$id_room;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $array[]=$row;
        }
    }
}
$query = "SELECT 'poi' as what,p.*,IFNULL(i.id,0) as id_icon_library, i.image as img_icon_library FROM svt_pois as p
            LEFT JOIN svt_icons as i ON i.id=p.id_icon_library
            WHERE p.id_room=$id_room;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            unset($row['id']);
            if($row['type']=='html_sc') {
                $row['content'] = htmlspecialchars($row['content']);
            }
            if($row['label']==null) $row['label']='';
            $array[]=$row;
        }
    }
}
echo json_encode($array);