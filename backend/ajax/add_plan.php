<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");

$name = str_replace("'","\'",strip_tags($_POST['name']));
$n_virtual_tours = ($_POST['n_virtual_tours']=="") ? -1 : $_POST['n_virtual_tours'];
$n_rooms = ($_POST['n_rooms']=="") ? -1 : $_POST['n_rooms'];
$n_markers = ($_POST['n_markers']=="") ? -1 : $_POST['n_markers'];
$n_pois = ($_POST['n_pois']=="") ? -1 : $_POST['n_pois'];
$days = ($_POST['days']=="") ? -1 : $_POST['days'];
$create_landing = $_POST['create_landing'];

$query = "INSERT INTO svt_plans(name,n_virtual_tours,n_rooms,n_markers,n_pois,days,create_landing) VALUES('$name',$n_virtual_tours,$n_rooms,$n_markers,$n_pois,$days,$create_landing); ";
$result = $mysqli->query($query);

if($result) {
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

