<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];

$array = array();
$query = "SELECT m.*,(SELECT COUNT(*) FROM svt_rooms WHERE id_map=m.id) as count_rooms FROM svt_maps as m WHERE m.id_virtualtour=$id_virtualtour";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $array[]=$row;
        }
    }
}
echo json_encode($array);