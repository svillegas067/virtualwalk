<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
ini_set('memory_limit','2048M');
ini_set('max_execution_time', 9999);
ini_set('max_input_time', 9999);
ini_set('post_max_size', '999MB');
ini_set('upload_max_filesize', '999MB');
if (!file_exists(dirname(__FILE__).'/../../viewer/videos/')) {
    mkdir(dirname(__FILE__).'/../../viewer/videos/', 0775);
}
if(isset($_FILES) && !empty($_FILES['file']['name'])){
    $allowed_ext = array('mp4','MP4');
    $filename = $_FILES['file']['name'];
    $ext = explode('.',$filename);
    $ext = end($ext);
    if(in_array($ext,$allowed_ext)){
        $video = "pano_".time().".mp4";
        $moved = move_uploaded_file($_FILES['file']['tmp_name'],dirname(__FILE__).'/../../viewer/videos/'.$video);
        if($moved) {
            echo "../viewer/videos/".$video;
        } else {
            echo 'ERROR: code:'.$_FILES["file"]["error"];
        }
    }else{
        echo 'ERROR: Only mp4 files are supported';
    }
}else{
    echo 'ERROR: file not provided';
}
exit;