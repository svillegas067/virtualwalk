<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
require_once("../functions.php");
$id_user = $_POST['id_user'];

if(get_user_role($id_user)=='administrator') {
    $where_user = "";
} else {
    $where_user = " WHERE v.id_user = $id_user ";
}

$array = array();
$query = "SELECT v.id,UPPER(v.name) as name,v.date_created,v.author,u.username FROM svt_virtualtours as v 
            LEFT JOIN svt_users as u ON u.id=v.id_user
            $where_user 
            ORDER BY v.date_created DESC";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $row['date_created'] = date("d F Y",strtotime($row['date_created']));
            if($row['author']!=$row['username']) {
                $row['author'] = $row['username']." (".$row['author'].")";
            }
            $array[]=$row;
        }
    }
}
echo json_encode($array);