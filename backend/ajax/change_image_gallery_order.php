<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$array_images_priority = json_decode($_POST['array_images_priority'],true);

foreach ($array_images_priority as $priority=>$id) {
    $mysqli->query("UPDATE svt_gallery SET priority=$priority WHERE id=$id AND id_virtualtour=$id_virtualtour;");
}