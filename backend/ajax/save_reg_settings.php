<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");

$enable_registration = $_POST['enable_registration'];
$default_id_plan = $_POST['default_id_plan'];

$query = "UPDATE svt_settings SET enable_registration=$enable_registration,default_id_plan=$default_id_plan;";
$result = $mysqli->query($query);

if($result) {
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

