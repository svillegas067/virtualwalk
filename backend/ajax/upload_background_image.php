<?php
ini_set('memory_limit','256M');
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once(dirname(__FILE__).'/ImageResize.php');
use \Gumlet\ImageResize;
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
if (!file_exists(dirname(__FILE__).'/../../viewer/content/')) {
    mkdir(dirname(__FILE__).'/../../viewer/content/', 0775);
}
if(isset($_FILES) && !empty($_FILES['file']['name'])){
    $allowed_ext = array('png','PNG','jpg','JPG','jpeg','JPEG');
    $filename = $_FILES['file']['name'];
    $ext = explode('.',$filename);
    $ext = end($ext);
    if(in_array($ext,$allowed_ext)){
        $name = "logo_".time().".$ext";
        $moved = move_uploaded_file($_FILES['file']['tmp_name'],dirname(__FILE__).'/../../viewer/content/'.$name);
        if($moved) {
            try {
                $image = new ImageResize(dirname(__FILE__).'/../../viewer/content/'.$name);
                $image->quality_jpg = 60;
                $image->gamma(false);
                $image->resizeToWidth(1280);
                $image->save(dirname(__FILE__).'/../../viewer/content/'.$name);
                echo $name;
            } catch (Exception $e) {
                echo 'ERROR: retry';
            }
        } else {
            echo 'ERROR: code:'.$_FILES["file"]["error"];
        }
    }else{
        echo 'ERROR: Only jpg,png files are supported';
    }
}else{
    echo 'ERROR: file not provided';
}
exit;
