<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_room = $_POST['id_room'];
$name = str_replace("'","\'",strip_tags($_POST['name']));
$yaw_pitch = $_POST['yaw_pitch'];
$northOffset = $_POST['northOffset'];
$change_image = $_POST['change_image'];
$change_video = $_POST['change_video'];
$panorama_image = $_POST['panorama_image'];
$panorama_video = $_POST['panorama_video'];
$song = $_POST['song'];
$annotation_title = str_replace("'","\'",strip_tags($_POST['annotation_title']));
$annotation_description = str_replace("'","\'",strip_tags($_POST['annotation_description']));
$allow_pitch = $_POST['allow_pitch'];
$visible_list = $_POST['visible_list'];
$min_pitch = $_POST['min_pitch'];
$max_pitch = $_POST['max_pitch'];
$tmp = explode(",",$yaw_pitch);
$yaw = $tmp[0];
$pitch = $tmp[1];

if($min_pitch=="") {
    $min_pitch=90;
}
if($max_pitch=="") {
    $max_pitch=90;
}
$min_pitch = $min_pitch*-1;

if($change_image==1) {
    $name_image = str_replace("tmp_panoramas/","",$panorama_image);
    $path_source = dirname(__FILE__).'/../tmp_panoramas/'.$name_image;
    $path_dest = dirname(__FILE__).'/../../viewer/panoramas/'.$name_image;
    if(copy($path_source,$path_dest)) {
        unlink($path_source);
        $q_add = ",panorama_image='$name_image'";
        include("../../services/generate_thumb.php");
        include("../../services/generate_pano_mobile.php");
    } else {
        echo json_encode(array("status"=>"error image"));
        die();
    }
} else if($change_video==1) {
    $name_image = "pano_".time().".jpg";
    $name_video = str_replace("../viewer/videos/","",$panorama_video);
    $path_dest = dirname(__FILE__).'/../../viewer/panoramas/'.$name_image;
    $ifp = fopen($path_dest,'wb');
    $data = explode(',', $panorama_image);
    fwrite($ifp,base64_decode($data[1]));
    fclose( $ifp );
    $q_add = ",panorama_image='$name_image',panorama_video='$name_video'";
    include("../../services/generate_thumb.php");
    include("../../services/generate_pano_mobile.php");
} else {
    $q_add = "";
}

$query = "UPDATE svt_rooms SET name='$name',yaw=$yaw,pitch=$pitch,allow_pitch=$allow_pitch,visible_list=$visible_list,min_pitch=$min_pitch,max_pitch=$max_pitch,northOffset=$northOffset,song='$song',annotation_title='$annotation_title',annotation_description='$annotation_description' $q_add WHERE id=$id_room;";
$result = $mysqli->query($query);

if($result) {
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

