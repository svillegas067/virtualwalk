<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");

$purchase_code = str_replace("'","\'",$_POST['purchase_code']);
$name = str_replace("'","\'",strip_tags($_POST['name']));
$furl_blacklist = str_replace("'","\'",strip_tags($_POST['furl_blacklist']));
$furl_blacklist = strtolower($furl_blacklist);
$furl_blacklist = str_replace(" ","",$furl_blacklist);
$logo = $_POST['logo'];
$background = $_POST['background'];
$smtp_server = $_POST['smtp_server'];
$smtp_port = $_POST['smtp_port'];
if($smtp_port=='') $smtp_port=0;
$smtp_secure = $_POST['smtp_secure'];
$smtp_auth = $_POST['smtp_auth'];
$smtp_username = str_replace("'","\'",$_POST['smtp_username']);
$smtp_password = str_replace("'","\'",$_POST['smtp_password']);
$smtp_from_email = str_replace("'","\'",$_POST['smtp_from_email']);
$smtp_from_name = str_replace("'","\'",$_POST['smtp_from_name']);

$query_add = "";
if($smtp_password!='keep_password') {
    $query_add .= ",smtp_password='$smtp_password'";
}

$query = "UPDATE svt_settings SET purchase_code='$purchase_code',name='$name',logo='$logo',background='$background',smtp_server='$smtp_server',smtp_port=$smtp_port,smtp_secure='$smtp_secure',smtp_auth=$smtp_auth,smtp_username='$smtp_username',smtp_from_email='$smtp_from_email',smtp_from_name='$smtp_from_name',furl_blacklist='$furl_blacklist' $query_add;";
$result = $mysqli->query($query);

if($result) {
    include("../../services/clean_images.php");
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}

