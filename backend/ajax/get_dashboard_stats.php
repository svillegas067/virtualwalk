<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
require_once("../functions.php");
$id_user = $_POST['id_user'];

if(get_user_role($id_user)=='administrator') {
    $where_user = "";
} else {
    $where_user = " WHERE id_user = $id_user ";
}

$stats = array();
$stats['count_virtual_tours'] = 0;
$stats['count_rooms'] = 0;
$stats['count_markers'] = 0;
$stats['count_pois'] = 0;
$stats['total_visitors'] = 0;
$stats['visitors'] = array();

$query = "SELECT COUNT(*) as num FROM svt_virtualtours $where_user LIMIT 1";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows==1) {
        $row=$result->fetch_array(MYSQLI_ASSOC);
        $num = $row['num'];
        $stats['count_virtual_tours'] = $num;
    }
}

$query = "SELECT COUNT(*) as num FROM svt_rooms as r
JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
$where_user LIMIT 1;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows==1) {
        $row=$result->fetch_array(MYSQLI_ASSOC);
        $num = $row['num'];
        $stats['count_rooms'] = $num;
    }
}

$query = "SELECT COUNT(*) as num FROM svt_markers as m
JOIN svt_rooms as r ON m.id_room = r.id
JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
$where_user LIMIT 1;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows==1) {
        $row=$result->fetch_array(MYSQLI_ASSOC);
        $num = $row['num'];
        $stats['count_markers'] = $num;
    }
}

$query = "SELECT COUNT(*) as num FROM svt_pois as m
JOIN svt_rooms as r ON m.id_room = r.id
JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
$where_user LIMIT 1;";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows==1) {
        $row=$result->fetch_array(MYSQLI_ASSOC);
        $num = $row['num'];
        $stats['count_pois'] = $num;
    }
}

$total_visitors = 0;
$query = "SELECT v.id,UPPER(v.name) as name,COUNT(a.id) as count FROM svt_virtualtours as v
LEFT JOIN svt_access_log as a ON v.id = a.id_virtualtour
$where_user
GROUP BY v.id";
$result = $mysqli->query($query);
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $count = $row['count'];
            $total_visitors = $total_visitors + $count;
            $stats['visitors'][] = $row;
        }
        $stats['total_visitors'] = $total_visitors;
    }
}

echo json_encode($stats);