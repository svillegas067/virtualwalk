<?php
session_start();
require_once("functions.php");
$role = get_user_role($_SESSION['id_user']);
$settings = get_settings();
$r0='';if(array_key_exists(base64_decode('U0VSVkVSX0FERFI='),$_SERVER))$r0=$_SERVER[base64_decode('U0VSVkVSX0FERFI=')];elseif(array_key_exists(base64_decode('TE9DQUxfQUREUg=='),$_SERVER))$r0=$_SERVER[base64_decode('TE9DQUxfQUREUg==')];elseif(array_key_exists(base64_decode('U0VSVkVSX05BTUU='),$_SERVER))$r0=gethostbyname($_SERVER[base64_decode('U0VSVkVSX05BTUU=')]);else{if(stristr(PHP_OS,base64_decode('V0lO'))){$r0=gethostbyname(php_uname(base64_decode('bg==')));}else{$u1=shell_exec(base64_decode('L3NiaW4vaWZjb25maWcgZXRoMA=='));preg_match(base64_decode('L2FkZHI6KFtcZFwuXSspLw=='),$u1,$a2);$r0=$a2[1];}}echo base64_decode('PGlucHV0IHR5cGU9J2hpZGRlbicgaWQ9J3ZsZmMnIC8+');$v3=get_settings();$o5=$r0.base64_decode('UlI=').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$v6=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);$o5=$r0.base64_decode('UkU=').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$w7=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);$o5=$r0.base64_decode('RQ==').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$r8=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);if($v6){include(base64_decode('bGljZW5zZS5waHA='));exit;}else if(($r8)||($w7)){}else{include(base64_decode('bGljZW5zZS5waHA='));exit;}
?>

<?php if($role!='administrator'): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
    <?php die(); endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-crown text-gray-700"></i> PLANS</h1>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Notice</h6>
            </div>
            <div class="card-body">
                <p>Different plans let you limit your customers to create a certain number of Virtual Tours, Rooms, Markers and POIs. The default Unlimited's plan has no limits.</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Registration</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="enable_registration">Enable <i title="enables registration form" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="enable_registration" <?php echo ($settings['enable_registration'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="default_plan">Default Plan <i title="default plan assigned to new registered users" class="help_t fas fa-question-circle"></i></label>
                            <select class="form-control" id="default_plan">
                                <?php echo get_plans_options($settings['default_id_plan']); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="color: white">.</label>
                        <button id="btn_save_reg_settings" onclick="save_reg_settings();" class="btn btn-primary btn-block">SAVE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button <?php echo ($demo) ? 'disabled':''; ?> data-toggle="modal" data-target="#modal_new_plan" class="btn btn-block btn-success"><i class="fa fa-plus"></i> ADD PLAN</button>
    </div>
</div>

<div class="row mt-2">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-body">
                <table class="table table-bordered table-hover" id="plans_table" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>N. VT</th>
                        <th>N. Rooms</th>
                        <th>N. Markers</th>
                        <th>N. POIs</th>
                        <th>Landing</th>
                        <th>Exp. Days</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modal_new_plan" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Plan</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter plan name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_virtual_tours">N. Virtual Tours</label>
                            <input type="number" min="-1" class="form-control" id="n_virtual_tours" value="-1" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_rooms">N. Rooms</label>
                            <input type="number" min="-1" class="form-control" id="n_rooms" value="-1" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_markers">N. Markers</label>
                            <input type="number" min="-1" class="form-control" id="n_markers" value="-1" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_pois">N. POIs</label>
                            <input type="number" min="-1" class="form-control" id="n_pois" value="-1" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="create_landing">Create Landing</label><br>
                            <input type="checkbox" id="create_landing" checked />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="days">Expires Days</label>
                            <input type="number" min="-1" class="form-control" id="days" value="-1" />
                        </div>
                    </div>
                </div>
                <p class="text-right mb-0">* -1 = unlimited</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> onclick="add_plan();" type="button" class="btn btn-success">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_edit_plan" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Plan</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name_edit">Name</label>
                            <input type="text" class="form-control" id="name_edit" placeholder="Enter plan name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_virtual_tours_edit">N. Virtual Tours</label>
                            <input type="number" min="-1" class="form-control" id="n_virtual_tours_edit" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_rooms_edit">N. Rooms</label>
                            <input type="number" min="-1" class="form-control" id="n_rooms_edit" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_markers_edit">N. Markers</label>
                            <input type="number" min="-1" class="form-control" id="n_markers_edit" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="n_pois_edit">N. POIs</label>
                            <input type="number" min="-1" class="form-control" id="n_pois_edit" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="create_landing_edit">Create Landing</label><br>
                            <input type="checkbox" id="create_landing_edit" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="days_edit">Expires Days</label>
                            <input type="number" min="-1" class="form-control" id="days_edit" value="" />
                        </div>
                    </div>
                </div>
                <p class="text-right mb-0">* -1 = unlimited</p>
            </div>
            <div class="modal-footer">
                <button id="btn_delete_plan" <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_plan();" type="button" class="btn btn-danger">Delete</button>
                <button <?php echo ($demo) ? 'disabled':''; ?> onclick="save_plan();" type="button" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict";
        window.id_plan_sel = null;
        window.plan_need_save = false;
        $(document).ready(function () {
            $('.help_t').tooltip();
            $('#plans_table').DataTable({
                "order": [[ 1, "asc" ]],
                "responsive": true,
                "scrollX": true,
                "processing": true,
                "searching": false,
                "serverSide": true,
                "ajax": "ajax/get_plans.php"
            });
            $('#plans_table tbody').on('click', 'td', function () {
                var plan_id = $(this).parent().attr("id");
                window.id_plan_sel = plan_id;
                open_modal_plan_edit(plan_id);
            });
        });
        $("input").change(function(){
            window.plan_need_save = true;
        });

        $("select").change(function(){
            window.plan_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.plan_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });
    })(jQuery);
</script>