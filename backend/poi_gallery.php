<?php
session_start();
$id_poi = $_GET['id_poi'];
?>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="ajax/upload_gallery_image.php" class="dropzone" id="gallery-dropzone"></form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List Images <i style="font-size:12px">(drag images to change order)</i></h6>
            </div>
            <div class="card-body">
                <div id="list_images">
                    <p>Loading ...</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_poi = '<?php echo $id_poi; ?>';
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            get_poi_gallery_images(id_poi);
            var gallery_dropzone = new Dropzone("#gallery-dropzone", {
                    url: "ajax/upload_gallery_image.php",
                    parallelUploads: 1,
                    maxFilesize: 20,
                    timeout: 120000,
                    acceptedFiles: 'image/*'
                });
            gallery_dropzone.on("addedfile", function(file) {
                $('#list_images').addClass('disabled');
            });
            gallery_dropzone.on("success", function(file,rsp) {
                add_image_to_poi_gallery(id_poi,rsp);
            });
            gallery_dropzone.on("queuecomplete", function() {
                $('#list_images').removeClass('disabled');
                gallery_dropzone.removeAllFiles();
            });
        });
    })(jQuery); // End of use strict
</script>