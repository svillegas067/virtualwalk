(function($) {
    "use strict"; // Start of use strict

    window.login = function () {
        var username = $('#username_l').val();
        var password = $('#password_l').val();
        $('#btn_login').addClass("disabled");
        $.ajax({
            url: "ajax/login.php",
            type: "POST",
            data: {
                username: username,
                password: password
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=='ok') {
                    if(rsp.email=='set') {
                        $('#btn_assoc_email').attr('onclick','associate_email('+rsp.id+')');
                        $('#modal_email').modal('show');
                    } else {
                        location.href="index.php";
                    }
                } else {
                    $('#username_l').addClass("error-highlight");
                    $('#password_l').addClass("error-highlight");
                    $('#btn_login').removeClass("disabled");
                }
            }
        });
    }

    window.register = function () {
        var complete = true;
        var username = $('#username_r').val();
        var email = $('#email_r').val();
        var password = $('#password_r').val();
        var password2 = $('#password2_r').val();
        if(username=='') {
            complete = false;
            $('#username_r').addClass("error-highlight");
        } else {
            $('#username_r').removeClass("error-highlight");
        }
        if(email=='') {
            complete = false;
            $('#email_r').addClass("error-highlight");
        } else {
            $('#email_r').removeClass("error-highlight");
        }
        if(password=='') {
            complete = false;
            $('#password_r').addClass("error-highlight");
        } else {
            $('#password_r').removeClass("error-highlight");
        }
        if(password2=='') {
            complete = false;
            $('#password2_r').addClass("error-highlight");
        } else {
            $('#password2_r').removeClass("error-highlight");
        }
        if((password!='') && (password2!='')) {
            if(password!=password2) {
                complete = false;
                $('#password_r').addClass("error-highlight");
                $('#password2_r').addClass("error-highlight");
            } else {
                $('#password_r').removeClass("error-highlight");
                $('#password2_r').removeClass("error-highlight");
            }
        }
        if(complete) {
            $('#btn_register').addClass("disabled");
            $.ajax({
                url: "ajax/register.php",
                type: "POST",
                data: {
                    username: username,
                    email: email,
                    password: password
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=='ok') {
                        location.href="index.php";
                    } else {
                        alert(rsp.msg);
                        $('#btn_register').removeClass("disabled");
                    }
                },
                error: function () {
                    alert('Error, retry later.');
                    $('#btn_register').removeClass("disabled");
                }
            });
        }
    }

    window.send_verification_code = function() {
        var email = $('#email_f').val();
        if(email=='') {
            $('#email_f').addClass("error-highlight");
        } else {
            $('#btn_forgot_code').addClass('disabled');
            $.ajax({
                url: "ajax/send_email.php",
                type: "POST",
                data: {
                    type: 'forgot',
                    email: email
                },
                timeout: 15000,
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        alert("Check your inbox for the verification code.")
                    } else {
                        alert(rsp.msg);
                        $('#btn_forgot_code').removeClass('disabled');
                    }
                },
                error: function(){
                    alert('Error, retry later.');
                    $('#btn_forgot_code').removeClass('disabled');
                },
            });
        }
    }

    window.change_password_forgot = function() {
        var complete = true;
        var forgot_code = $('#forgot_code').val();
        var password = $('#password_f').val();
        var password2 = $('#repeat_password_f').val();

        if(forgot_code=='') {
            complete = false;
            $('#forgot_code').addClass("error-highlight");
        } else {
            $('#forgot_code').removeClass("error-highlight");
        }
        if(password=='') {
            complete = false;
            $('#password_f').addClass("error-highlight");
        } else {
            $('#password_f').removeClass("error-highlight");
        }
        if(password2=='') {
            complete = false;
            $('#repeat_password_f').addClass("error-highlight");
        } else {
            $('#repeat_password_f').removeClass("error-highlight");
        }
        if((password!='') && (password2!='')) {
            if(password!=password2) {
                complete = false;
                $('#password_f').addClass("error-highlight");
                $('#repeat_password_f').addClass("error-highlight");
            } else {
                $('#password_f').removeClass("error-highlight");
                $('#repeat_password_f').removeClass("error-highlight");
            }
        }

        if(complete) {
            $.ajax({
                url: "ajax/change_password_forgot.php",
                type: "POST",
                data: {
                    forgot_code: forgot_code,
                    password: password,
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        alert('Password successfully changed!');
                        $('#modal_forgot').modal('hide');
                    } else {
                        alert(rsp.msg);
                    }
                }
            });
        }
    }

    window.associate_email = function(user_id) {
        var email = $('#email').val();
        if(email=='') {
            $('#email').addClass("error-highlight");
        } else {
            $('#btn_assoc_email').addClass('disabled');
            $('#email').removeClass("error-highlight");
            $.ajax({
                url: "ajax/associate_email.php",
                type: "POST",
                data: {
                    user_id: user_id,
                    email: email
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=='ok') {
                        $('#btn_assoc_email').removeClass('disabled');
                        $('#modal_email').modal('hide');
                        location.href="index.php";
                    } else {
                        $('#email').addClass("error-highlight");
                        $('#btn_assoc_email').removeClass('disabled');
                        alert(rsp.msg);
                    }
                }
            });
        }
    }

    window.logout = function () {
        $.ajax({
            url: "ajax/logout.php",
            type: "POST",
            async: true,
            success: function (json) {
                location.href="login.php";
            }
        });
    }

    window.get_dashboard_stats = function () {
        $.ajax({
            url: "ajax/get_dashboard_stats.php",
            type: "POST",
            data: {
                id_user: window.id_user
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#num_virtual_tours').html(rsp.count_virtual_tours);
                $('#num_rooms').html(rsp.count_rooms);
                $('#num_markers').html(rsp.count_markers);
                $('#num_pois').html(rsp.count_pois);
                var total_visitors = rsp.total_visitors;
                var visitors = rsp.visitors;
                var html_visitors = "";
                jQuery.each(visitors, function(index, visitor) {
                    var vt_name = visitor.name;
                    var count = visitor.count;
                    var perc = count / total_visitors * 100;
                    perc = Math.round(perc);
                    html_visitors += '<h4 class="small font-weight-bold">'+vt_name+' <span class="float-right">'+count+'/<b>'+total_visitors+'</b></span></h4>\n' +
                        '                <div class="progress mb-4">\n' +
                        '                    <div class="progress-bar bg-primary" role="progressbar" style="width: '+perc+'%" aria-valuenow="'+perc+'" aria-valuemin="0" aria-valuemax="100"></div>\n' +
                        '                </div>';
                });
                if(html_visitors!='') {
                    $('#list_visitors').html(html_visitors);
                }
            }
        });
    };

    window.get_virtual_tours = function () {
        $.ajax({
            url: "ajax/get_virtual_tours.php",
            type: "POST",
            data: {
                id_user: window.id_user
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                parse_virtual_tour_list(rsp);
            }
        });
    };

    window.get_maps = function () {
        $.ajax({
            url: "ajax/get_maps.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                parse_map_list(rsp);
            }
        });
    };

    window.get_rooms = function (id_virtualtour,p) {
        $.ajax({
            url: "ajax/get_rooms.php",
            type: "POST",
            data: {
                id_user: window.id_user,
                id_virtualtour: id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                window.rooms = rsp;
                switch(p) {
                    case 'list':
                        parse_room_list(rsp);
                        break;
                    case 'marker':
                        parse_room_marker(rsp);
                        break;
                    case 'poi':
                        parse_room_poi(rsp);
                        break;
                }
            }
        });
    };
    
    window.get_rooms_menu_list  = function (id_virtualtour) {
        $.ajax({
            url: "ajax/get_rooms_menu_list.php",
            type: "POST",
            data: {
                id_user: window.id_user,
                id_virtualtour: id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                parse_rooms_menu_list(rsp);
            }
        });
    };

    window.get_presentation = function (id_virtualtour) {
        $.ajax({
            url: "ajax/get_presentation.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                array_presentation = rsp;
                parse_presentation_list();
            }
        });
    };


    window.get_map = function (id_virtualtour,id_map) {
        $('#msg_load_map').show();
        $.ajax({
            url: "ajax/get_map.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                id_map: id_map
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.map!="") {
                    window.map_points = rsp.map_points;
                    window.id_map_sel = rsp.id_map;
                    var tmpImg = new Image();
                    tmpImg.src = '../viewer/maps/'+rsp.map;
                    tmpImg.onload = function() {
                        $('#msg_load_map').hide();
                        $('#map_image').attr('src','../viewer/maps/'+rsp.map);
                        parse_map_points(window.map_points);
                        if(rsp.all_points) {
                            $('#btn_add_point').prop("disabled",true);
                            $('#btn_add_point').css('opacity',0.3);
                        } else {
                            $('#btn_add_point').prop("disabled",false);
                            $('#btn_add_point').css('opacity',1);
                        }
                    };
                }
            }
        });
    };

    window.preview = function(id,container_h) {
        $.ajax({
            url: "ajax/get_code.php",
            type: "POST",
            data: {
                id_virtualtour: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                var code = rsp.code;
                if(code!="") {
                    if(rsp.count_rooms>0) {
                        $('#msg_no_room').hide();
                        $('#iframe_div').show();
                        $("#iframe_div").html("<iframe allowfullscreen " +
                            "width=\"100%\" height=\""+container_h+"px\" frameborder=\"0\" scrolling=\"no\" " +
                            "marginheight=\"0\" marginwidth=\"0\" " +
                            "src=\"../viewer/index.php?code="+code+"\"" +
                            "></iframe>");
                    } else {
                        $('#msg_no_room').show();
                        $('#iframe_div').hide();
                    }
                }
            }
        });
    }

    window.add_map_point = function () {
        var id_room = $('#room_select option:selected').attr('id');
        $.ajax({
            url: "ajax/add_map_point.php",
            type: "POST",
            data: {
                id_room: id_room,
                id_map: window.id_map_sel
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    location.reload();
                }
            }
        });
    }

    window.save_map_settings = function () {
        var map_name = $('#map_name').val();
        var point_color = $('#point_color').val();
        var point_size = $('#point_size').val();
        if(map_name!='') {
            $.ajax({
                url: "ajax/save_map_settings.php",
                type: "POST",
                data: {
                    id_map: window.id_map_sel,
                    map_name: map_name,
                    point_color: point_color,
                    point_size: point_size
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=='ok') {
                        window.map_need_save = false;
                    }
                }
            });
        }
    }

    window.save_map_point = function () {
        var position = $('#point_pos').val();
        $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
        $.ajax({
            url: "ajax/save_map_point.php",
            type: "POST",
            data: {
                id_room: window.id_room_sel,
                position: position
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=="ok") {
                    $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                    setTimeout(function () {
                        $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                    },1000);
                } else {
                    $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                    $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                    setTimeout(function () {
                        $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                        $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                    },1000);
                }
            }
        });
    }

    function parse_map_points(points) {
        var html_points = '',html_map_select = '';
        jQuery.each(points, function(index, point) {
            var map_id = point.id;
            var map_top = point.map_top;
            var map_left = point.map_left;
            var map_name = point.name;
            var point_color = point.point_color;
            if(map_top!=null) {
                if(point.id_map == window.id_map_sel) {
                    html_points += '<div id="pointer_'+map_id+'" style="top: '+map_top+'px; left: '+map_left+'px; opacity: 0.3;background-color: '+point_color+';" class="pointer"></div>';
                }
            } else {
                html_map_select += '<option id="'+map_id+'">'+map_name+'</option>';
            }
        });
        $('#room_select').html(html_map_select);
        $('#pointers_div').html(html_points).promise().done(function () {
            $('.pointer').css('width',window.point_size+'px');
            $('.pointer').css('height',window.point_size+'px');
            adjust_points_position();
            $( ".pointer" ).draggable({
                containment: '#pointers_div',
                drag: function(event) {
                    var top = $(this).position().top;
                    var left = $(this).position().left;
                    var id = $(this).attr('id').replace('pointer_','');
                    set_point_map_pos(id,top,left);
                    click_map_point(id);
                },
                start: function (event, ui) {
                    $(this).addClass('dragging');
                    $(".pointer").css('opacity',0.3);
                    $(this).css('opacity',1);
                    var id = $(this).attr('id').replace('pointer_','');
                    click_map_point(id);
                },
                stop: function (event, ui) {
                    $(this).removeClass('dragging');
                    var id = $(this).attr('id').replace('pointer_','');
                    click_map_point(id);
                    save_map_point();
                }
            });
            $('.pointer').click(function (event) {
                if (!$(this).hasClass('dragging')) {
                    var top = $(this).position().top;
                    var left = $(this).position().left;
                    $(".pointer").css('opacity',0.3);
                    $(this).css('opacity',1);
                    var id = $(this).attr('id').replace('pointer_','');
                    set_point_map_pos(id,top,left);
                    click_map_point(id);
                }
            });
            if(window.id_room_point_sel!='') {
                $('#pointer_'+window.id_room_point_sel).trigger('click');
            }
        });
    }

    function set_point_map_pos(id,top,left) {
        top = Math.round(top / ratio_w);
        left = Math.round(left / ratio_h);
        $('#point_pos').val(top+","+left);
        jQuery.each(window.map_points, function(index, point) {
            var id_point = point.id;
            if(id==id_point) {
                point.map_left = left;
                point.map_top = top;
            }
        });
    }

    function click_map_point(id) {
        if(window.id_room_sel!=id) {
            set_room_target_map(id);
            $('#btn_delete_point').attr('onclick','modal_delete_map_point('+id+');');
            $('#msg_select_point').hide();
            $('.point_settings').show();
        }
        window.id_room_sel = id;
    }

    function parse_presentation_list() {
        var html = '';
        var first_row = true;
        var priority_1_old = 0;
        window.array_id_rooms = [];
        jQuery.each(array_presentation, function(index, presentation) {
            var id = presentation.id;
            var action = presentation.action;
            var room_id = presentation.id_room;
            var priority_1 = presentation.priority_1;
            if(priority_1_old==0) {
                priority_1_old = priority_1;
            }
            if(!window.array_id_rooms.includes(room_id)) {
                window.array_id_rooms.push(room_id);
            }
            var room_image = presentation.panorama_image;
            var room_name = presentation.room_name;
            var params = presentation.params;
            var sleep_ms = presentation.sleep;
            switch (action) {
                case'goto':
                    if(first_row) {
                        var mt = 0;
                        first_row = false;
                    } else {
                        var mt = 4;
                    }
                    var ml = 0;
                    var py = 2;
                    var b_color = 'warning';
                    var classn = 'p_room';
                    var priority_click_up = 'change_presentation_priority(1,\'up\','+id+');';
                    var priority_click_down = 'change_presentation_priority(1,\'down\','+id+');';
                    var text = '<i class="far fa-arrow-alt-circle-up"></i> <b>'+room_name+'</b>&nbsp;&nbsp;&nbsp;<i class="far fa-pause-circle"></i> '+sleep_ms+'ms';
                    break;
                case 'lookAt':
                    var mt = 1;
                    var ml = 4;
                    var py = 1;
                    var b_color = 'primary';
                    var classn = 'p_subs p_sub_'+room_id;
                    var priority_click_up = 'change_presentation_priority(2,\'up\','+id+');';
                    var priority_click_down = 'change_presentation_priority(2,\'down\','+id+');';
                    room_image = '';
                    var text = '<i class="fas fa-bullseye"></i> '+params+'&nbsp;&nbsp;&nbsp;<i class="far fa-pause-circle"></i> '+sleep_ms+'ms';
                    break;
                case 'type':
                    var mt = 1;
                    var ml = 4;
                    var py = 1;
                    var b_color = 'info';
                    var classn = 'p_subs p_sub_'+room_id;
                    var priority_click_up = 'change_presentation_priority(2,\'up\','+id+');';
                    var priority_click_down = 'change_presentation_priority(2,\'down\','+id+');';
                    room_image = '';
                    var text = '<i class="far fa-comment-dots"></i> '+params+ '&nbsp;&nbsp;&nbsp;<i class="far fa-pause-circle"></i> '+sleep_ms+'ms';
                    break;
            }

            if(priority_1!=priority_1_old) {
                html += '<div onclick="open_modal_p_action('+array_presentation[index-1].id_room+');" style="cursor: pointer" class="card ml-4 mt-1 py-1 bg-info text-white">\n' +
                    '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                    '                <div class="row">\n' +
                    '                    <div class="col-md-12 text-center">\n' +
                    '                        <i class="fas fa-plus-circle"></i> <span>ADD ACTION</span>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </div>';
                priority_1_old = priority_1;
            }

            html += '<div class="'+classn+' noselect card mt-'+mt+' ml-'+ml+' py-'+py+' border-left-'+b_color+'">\n' +
                '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                '                <div class="row">\n' +
                '                    <div onclick="presentation_elem_edit('+index+');" style="cursor: pointer" class="col-md-10 text-center text-sm-center text-md-left text-lg-left">\n';
            if(room_image!='') {
                html += '                        <div class="d-inline-block align-middle"><img style="height: 40px;" src="../viewer/panoramas/thumb/'+room_image+'" /></div>\n';
            }
            html += '                        <div class="noselect d-inline-block ml-2 align-middle text-left">'+text+'</div>\n' +
                '                    </div>\n'+
                '                    <div class="col-md-2 align-self-center text-center text-sm-center text-md-right text-lg-right">\n';
            html += '                        <i onclick="'+priority_click_up+'" class="icon_order fas fa-caret-up"></i>&nbsp;&nbsp;<i onclick="'+priority_click_down+'" class="icon_order fas fa-caret-down"></i>';
            html += '                 </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            if((index==(array_presentation.length-1))) {
                html += '<div onclick="open_modal_p_action('+room_id+');" style="cursor: pointer" class="card ml-4 mt-1 py-1 bg-info text-white">\n' +
                    '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                    '                <div class="row">\n' +
                    '                    <div class="col-md-12 text-center">\n' +
                    '                        <i class="fas fa-plus-circle"></i> <span>ADD ACTION</span>\n' +
                    '                    </div>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '        </div>';
            }
        });

        html += '<div onclick="open_modal_p_room(null)" style="cursor: pointer" class="card mt-4 py-2 bg-warning text-white">\n' +
            '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
            '                <div class="row">\n' +
            '                    <div class="col-md-12 text-center">\n' +
            '                        <i class="fas fa-plus-circle"></i> <span>ADD ROOM</span>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>';

        $('#presentation_list').html(html).promise().done(function () {
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) {
                window.scrollTo(0, parseInt(scrollpos));
                localStorage.removeItem('scrollpos');
            }
            $('.p_room:first .fa-caret-up').addClass('disabled');
            $('.p_room:last .fa-caret-down').addClass('disabled');
            jQuery.each(window.array_id_rooms, function(index, id_room) {
                $('.p_sub_'+id_room+':first .fa-caret-up').addClass('disabled');
                $('.p_sub_'+id_room+':last .fa-caret-down').addClass('disabled');
            });
        });
    }

    window.change_presentation_priority = function (priority,direction,id) {
        $.ajax({
            url: "ajax/change_presentation_priority.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id: id,
                priority: priority,
                direction: direction
            },
            async: true,
            success: function (rsp) {
                localStorage.setItem('scrollpos', parseInt(window.scrollY));
                location.reload();
            }
        });
    }

    window.change_p_action = function() {
        var p_action = $('#p_action option:selected').attr('id');
        switch (p_action) {
            case '0':
                $('#div_type').hide();
                $('#div_lookAt').hide();
                $('#btn_add_p_action').addClass('disabled');
                break;
            case 'type':
                $('#div_type').show();
                $('#div_lookAt').hide();
                $('#btn_add_p_action').removeClass('disabled');
                break;
            case 'lookAt':
                $('#div_type').hide();
                $('#div_lookAt').show();
                $('#btn_add_p_action').removeClass('disabled');
                init_p_viewer(null,null,null);
                break;
        }
    }

    function init_p_viewer(yaw,pitch,hfov) {
        $.ajax({
            url: "ajax/get_room.php",
            type: "POST",
            data: {
                id: window.id_p_room,
            },
            async: true,
            success: function (rsp) {
                var room = JSON.parse(rsp);
                if(yaw==null) yaw = room.yaw;
                if(pitch==null) pitch = room.pitch;
                if(hfov==null) hfov = window.p_hfov;
                p_viewer = pannellum.viewer('p_lookAt', {
                    "type": "equirectangular",
                    "panorama": '../viewer/panoramas/'+room.panorama_image,
                    "autoLoad": true,
                    "showFullscreenCtrl": false,
                    "showControls": false,
                    "hfov": parseInt(hfov),
                    "minHfov": parseInt(window.p_min_hfov),
                    "maxHfov": parseInt(window.p_max_hfov),
                    "yaw": parseInt(yaw),
                    "pitch": parseInt(pitch),
                    "compass": false,
                    "friction": 1
                });
                p_viewer.on('load', function () {
                    p_viewer_initialized = true;
                    adjust_ratio_hfov('p_lookAt',p_viewer,hfov,window.p_min_hfov,window.p_max_hfov);
                });
                p_viewer.on('animatefinished',function () {
                    var yaw = parseInt(p_viewer.getYaw());
                    var pitch = parseInt(p_viewer.getPitch());
                    var hfov = parseInt(p_viewer.getHfov());
                    var c_w = parseFloat($('#p_lookAt').css('width').replace('px',''));
                    var c_h = parseFloat($('#p_lookAt').css('height').replace('px',''));
                    var ratio_panorama = c_w / c_h;
                    var ratio_hfov = 1.7771428571428571 / ratio_panorama;
                    hfov = hfov * ratio_hfov;
                    hfov = Math.round(hfov)+1;
                    $('#div_lookAt p').html(yaw+','+pitch+' ('+hfov+')');
                    window.p_params = pitch+','+yaw+','+hfov;
                });
            }
        });
    }

    window.open_modal_p_room = function (index) {
        if(index==null) {
            $('#p_sleep_r').val(0);
            jQuery.each(window.array_id_rooms, function(index, id_room) {
                $("#p_room option[id='"+id_room+"']").prop("disabled", true);
            });
            $("#p_room").val($("#p_room :not([disabled]):first").val());
            $("#p_room").prop('disabled',false);
            $('#btn_add_p_room').html('Add');
            $('#btn_add_p_room').attr('onclick','add_presentation_room();');
            $('#btn_delete_p_room').hide();
        } else {
            $('#p_sleep_r').val(array_presentation[index].sleep);
            $('#btn_remove_p_room').attr('onclick','delete_p_room('+array_presentation[index].id_room+')');
            $('#btn_delete_p_room').show();
        }
        $('#modal_presentation_room').modal('show');
    }

    window.open_modal_p_action = function (id_room) {
        window.id_p_room = id_room;
        try {
            p_viewer.destroy();
        } catch (e) {}
        $('#btn_add_p_action').html('Add');
        $('#btn_add_p_action').attr('onclick','add_presentation_action();');
        $("#p_action").prop('disabled',false);
        $('#btn_add_p_action').addClass('disabled');
        $('#div_type').hide();
        $('#div_lookAt').hide();
        $("#p_action option").prop("selected", false);
        $("#p_action").val($("#p_action option:first").val());
        $('#p_animation').val(1000);
        $('#p_sleep_l').val(0);
        $('#p_sleep_t').val(0);
        $('#p_text').val('');
        $('#btn_delete_p_action').hide();
        $('#modal_presentation_action').modal('show');
    }

    window.presentation_elem_edit = function (index) {
        window.id_p_room = array_presentation[index].id_room;
        if(array_presentation[index].action=='goto') {
            $("#p_room option").prop("selected", false);
            $("#p_room option[id='"+window.id_p_room+"']").prop("selected", true);
            $("#p_room").prop('disabled',true);
            $('#btn_add_p_room').html('Save');
            $('#btn_add_p_room').attr('onclick','edit_presentation_room('+array_presentation[index].id+');');
            open_modal_p_room(index);
        } else {
            try {
                p_viewer.destroy();
            } catch (e) {}
            $('#btn_add_p_action').html('Save');
            $('#btn_add_p_action').attr('onclick','edit_presentation_action('+array_presentation[index].id+');');
            $('#btn_add_p_action').removeClass('disabled');
            $("#p_action").prop('disabled',true);
            $("#p_action option").prop("selected", false);
            switch (array_presentation[index].action) {
                case "type":
                    $('#div_type').show();
                    $('#div_lookAt').hide();
                    $("#p_action option[id='type']").prop("selected", true);
                    $('#p_sleep_t').val(array_presentation[index].sleep);
                    $('#p_text').val(array_presentation[index].text);
                    break;
                case 'lookAt':
                    $('#div_type').hide();
                    $('#div_lookAt').show();
                    $("#p_action option[id='lookAt']").prop("selected", true);
                    $('#p_animation').val(array_presentation[index].animation);
                    $('#p_sleep_l').val(array_presentation[index].sleep);
                    init_p_viewer(array_presentation[index].yaw,array_presentation[index].pitch,array_presentation[index].hfov);
                    break;
            }
            $('#btn_remove_p_action').attr('onclick','delete_p_action('+array_presentation[index].id+')');
            $('#btn_delete_p_action').show();
            $('#modal_presentation_action').modal('show');
        }
    }

    window.add_presentation_action = function () {
        $('#modal_presentation_action button').addClass("disabled");
        var action = $('#p_action option:selected').attr('id');
        switch (action) {
            case "type":
                var sleep = $('#p_sleep_t').val();
                var params = $('#p_text').val();
                break;
            case 'lookAt':
                var sleep = $('#p_sleep_l').val();
                var animation = $('#p_animation').val();
                if(animation=='') animation=0;
                var params = window.p_params+','+animation;
                break;
        }
        $.ajax({
            url: "ajax/add_presentation_action.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_room: window.id_p_room,
                sleep: sleep,
                action: action,
                params: params
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_presentation_action button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_presentation_action').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    window.edit_presentation_action = function (id) {
        $('#modal_presentation_action button').addClass("disabled");
        var action = $('#p_action option:selected').attr('id');
        switch (action) {
            case "type":
                var sleep = $('#p_sleep_t').val();
                var params = $('#p_text').val();
                break;
            case 'lookAt':
                var sleep = $('#p_sleep_l').val();
                var animation = $('#p_animation').val();
                if(animation=='') animation=0;
                var params = window.p_params+','+animation;
                break;
        }
        $.ajax({
            url: "ajax/save_presentation_action.php",
            type: "POST",
            data: {
                id: id,
                sleep: sleep,
                params: params
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_presentation_action button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_presentation_action').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    window.add_presentation_room = function () {
        $('#modal_presentation_room button').addClass("disabled");
        var id_room = $('#p_room option:selected').attr('id');
        var sleep = $('#p_sleep_r').val();
        $.ajax({
            url: "ajax/add_presentation_room.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_room: id_room,
                sleep: sleep
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_presentation_room button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_presentation_room').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    window.edit_presentation_room = function (id) {
        $('#modal_presentation_room button').addClass("disabled");
        var sleep = $('#p_sleep_r').val();
        $.ajax({
            url: "ajax/save_presentation_room.php",
            type: "POST",
            data: {
                id: id,
                sleep: sleep
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_presentation_room button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_presentation_room').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    window.delete_p_action = function (id) {
        $('#modal_delete_p_action button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_presentation_action.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_delete_p_action button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_delete_p_action').modal("hide");
                    $('#modal_presentation_action').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    window.delete_p_room = function (id_room) {
        $('#modal_delete_p_room button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_presentation_room.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_room: id_room
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                $('#modal_delete_p_room button').removeClass("disabled");
                if(rsp.status=='ok') {
                    $('#modal_delete_p_room').modal("hide");
                    $('#modal_presentation_room').modal("hide");
                    localStorage.setItem('scrollpos', parseInt(window.scrollY));
                    location.reload();
                }
            }
        });
    }

    function parse_virtual_tour_list(virtual_tours) {
        var html = '';
        jQuery.each(virtual_tours, function(index, virtual_tour) {
            var id = virtual_tour.id;
            var name = virtual_tour.name;
            var author = virtual_tour.author;
            var date_created = virtual_tour.date_created;
            html += '<div class="card mb-2 py-2 border-left-primary">\n' +
                '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                '                <div class="row">\n' +
                '                    <div class="col text-center text-sm-center text-md-left text-lg-left">\n' +
                '                        <b>'+name+'</b><br><span style="font-size:12px">'+date_created+' by '+author+'</span>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-auto pt-1 text-center text-sm-center text-md-right text-lg-right">\n' +
                '                        <a title="EDIT" href="index.php?p=edit_virtual_tour&id='+id+'" class="btn btn-warning btn-circle">\n' +
                '                            <i class="fas fa-edit"></i>\n' +
                '                        </a>\n' +
                '                        <a title="ROOMS" href="index.php?p=rooms&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-vector-square"></i>\n' +
                '                        </a>\n' +
                '                        <a title="MAPS" href="index.php?p=maps&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-map-marked-alt"></i>\n' +
                '                        </a>\n' +
                '                        <a title="GALLERY" href="index.php?p=gallery&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-images"></i>\n' +
                '                        </a>\n' +
                '                        <a title="INFO BOX" href="index.php?p=info&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-info-circle"></i>\n' +
                '                        </a>\n' +
                '                        <a title="PREVIEW" href="index.php?p=preview&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-eye"></i>\n' +
                '                        </a>\n' +
                '                        <a title="PUBLISH" href="index.php?p=publish&id_vt='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-paper-plane"></i>\n' +
                '                        </a>\n' +
                '                        <a title="DELETE" href="#" onclick="modal_delete_virtualtour('+id+');return false;" class="btn btn-danger btn-circle">\n' +
                '                            <i class="fas fa-trash"></i>\n' +
                '                        </a>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        });
        $('#virtual_tours_list').html(html).promise().done(function () {
            $('#virtual_tours_list .btn').tooltipster({
                delay: 10,
                hideOnClick: true
            });
        });
    }

    function parse_map_list(maps) {
        var html = '';
        jQuery.each(maps, function(index, map) {
            var id = map.id;
            var name = map.name;
            var count = map.count_rooms;
            html += '<div class="card mb-2 py-2 border-left-primary">\n' +
                '            <div class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                '                <div class="row">\n' +
                '                    <div class="col-md-8 text-center text-sm-center text-md-left text-lg-left">\n' +
                '                        <b>'+name+'</b><br><span style="font-size:12px">'+count+' Rooms assigned</span>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-4 pt-1 text-center text-sm-center text-md-right text-lg-right">\n' +
                '                        <a href="index.php?p=edit_map&id='+id+'" class="btn btn-warning btn-circle">\n' +
                '                            <i class="fas fa-edit"></i>\n' +
                '                        </a>\n' +
                '                        <a href="#" onclick="modal_delete_map('+id+');return false;" class="btn btn-danger btn-circle">\n' +
                '                            <i class="fas fa-trash"></i>\n' +
                '                        </a>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        });
        $('#maps_list').html(html);
    }

    function parse_room_marker(rooms) {
        var html = '';
        if(rooms.length==0) {
            $('.rooms_slider').html("<p>No rooms created for this virtual tour. Go to <a href='index.php?p=rooms'>Rooms</a> and create a new one!</p>");
        } else {
            window.rooms_count = rooms.length;
            jQuery.each(rooms, function(index, room) {
                var id = room.id;
                var name = room.name;
                var image = room.panorama_image;
                var count_markers = room.count_markers;
                html += '<div onclick="select_room_marker('+id+',\''+image+'\',null);return false;" class="text-center pt-1 room_'+id+'">\n' +
                    '   <img class="m-auto" style="height: 40px;" src="../viewer/panoramas/thumb/'+image+'">\n' +
                    '   <div class="d-block">'+name+'</div>\n' +
                    '   <div class="d-block"><i style="font-size:12px"><b id="count_marker_'+id+'">'+count_markers+'</b> markers</i></div>\n' +
                    '</div>';
            });
            $('.rooms_slider').html(html).promise().done(function () {
                $('.rooms_slider').on('init', function(event, slick){
                    if(window.id_room_marker!=0) {
                        jQuery.each(window.rooms, function(index, room) {
                            var id = room.id;
                            var image = room.panorama_image;
                            if(id==window.id_room_marker) {
                                select_room_marker(id,image,null);
                            }
                        });
                    }
                });
                $('.rooms_slider').slick({
                    infinite: false,
                    draggable: false,
                    swipe: false,
                    touchMove: false,
                    dots: true,
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    responsive: [
                        {
                            breakpoint: 1530,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 4
                            }
                        },
                        {
                            breakpoint: 1050,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 830,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            });
        }
    }

    function parse_room_poi(rooms) {
        var html = '';
        if(rooms.length==0) {
            $('.rooms_slider').html("<p>No rooms created for this virtual tour. Go to <a href='index.php?p=rooms'>Rooms</a> and create a new one!</p>");
        } else {
            jQuery.each(rooms, function(index, room) {
                var id = room.id;
                var name = room.name;
                var image = room.panorama_image;
                var count_markers = room.count_pois;
                html += '<div onclick="select_room_poi('+id+',\''+image+'\',null);return false;" class="text-center pt-1 room_'+id+'">\n' +
                    '   <img class="m-auto" style="height: 40px;" src="../viewer/panoramas/thumb/'+image+'">\n' +
                    '   <div class="d-block">'+name+'</div>\n' +
                    '   <div class="d-block"><i style="font-size:12px"><b id="count_poi_'+id+'">'+count_markers+'</b> pois</i></div>\n' +
                    '</div>';
            });
            $('.rooms_slider').html(html).promise().done(function () {
                $('.rooms_slider').on('init', function(event, slick){
                    if(window.id_room_poi!=0) {
                        jQuery.each(window.rooms, function(index, room) {
                            var id = room.id;
                            var image = room.panorama_image;
                            if(id==window.id_room_poi) {
                                select_room_poi(id,image,null);
                            }
                        });
                    }
                });
                $('.rooms_slider').slick({
                    infinite: false,
                    draggable: false,
                    swipe: false,
                    touchMove: false,
                    dots: true,
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    responsive: [
                        {
                            breakpoint: 1530,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 4
                            }
                        },
                        {
                            breakpoint: 1050,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 830,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            });
        }
    }

    window.select_room_marker = function (id_room,image,id_marker_add) {
        if(id_room!=window.id_room_sel) {
            window.currentPitch = 0;
            window.currentYaw = 0;
        }
        window.id_room_sel = id_room;
        $('.slick-slide').removeClass("selected_room");
        $('.room_'+id_room).addClass("selected_room");
        $('#btn_add_marker').attr('onclick','add_marker('+id_room+',\''+image+'\')');
        $('#btn_add_marker').css('opacity',0);
        $('#msg_sel_room').hide();
        $("html, body").animate({ scrollTop: $(document).height() }, 200);
        get_markers(id_room,image,id_marker_add);
    }

    window.select_room_poi = function (id_room,image,id_poi_add) {
        if(id_room!=window.id_room_sel) {
            window.currentPitch = 0;
            window.currentYaw = 0;
        }
        window.id_room_sel = id_room;
        $('.slick-slide').removeClass("selected_room");
        $('.room_'+id_room).addClass("selected_room");
        $('#btn_add_poi').attr('onclick','add_poi('+id_room+',\''+image+'\');');
        $('#btn_add_poi').css('opacity',0);
        $('#msg_sel_room').hide();
        $("html, body").animate({ scrollTop: $(document).height() }, 200);
        get_pois(id_room,image,id_poi_add);
    }

    function get_markers(id_room,image,id_marker_add) {
        $.ajax({
            url: "ajax/get_markers.php",
            type: "POST",
            data: {
                id_room: id_room
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                window.markers = rsp;
                initialize_pano_markers(id_room,image,id_marker_add);
            }
        });
    }

    window.add_marker = function (id_room,image) {
        get_option_rooms_target('room_target_add',id_room,0,null,null);
        $('#btn_new_marker').attr('onclick','new_marker('+id_room+',\''+image+'\')');
        $('#modal_add_marker').modal('show');
    }

    window.add_poi = function (id_room,image) {
        $('#btn_new_poi').attr('onclick','new_poi('+id_room+',\''+image+'\')');
        $('#modal_add_poi').modal('show');
    }

    function initialize_pano_pois(id_room,image,id_poi_add) {
        window.panorama_image = image;
        $('#panorama_pois').show();
        var hotSpots = [];
        var index_poi_add = null;
        jQuery.each(window.pois, function(index, poi) {
            if(id_poi_add!=null) {
                if(id_poi_add==poi.id) {
                    index_poi_add = index;
                }
            }
            if(poi.what=='poi') {
                hotSpots.push({
                    "id": poi.id,
                    "type": 'poi',
                    "pitch": parseInt(poi.pitch),
                    "yaw": parseInt(poi.yaw),
                    "rotateX": 0,
                    "rotateZ": 0,
                    "size_scale": parseFloat(poi.size_scale),
                    "cssClass": "custom-hotspot-content",
                    "createTooltipFunc": hotspot_p,
                    "createTooltipArgs": poi,
                    "clickHandlerFunc": click_edit_poi,
                    "clickHandlerArgs": poi.id
                });
            } else {
                hotSpots.push({
                    "type": 'marker',
                    "pitch": parseInt(poi.pitch),
                    "yaw": parseInt(poi.yaw),
                    "rotateX": parseInt(poi.rotateX),
                    "rotateZ": parseInt(poi.rotateZ),
                    "size_scale": parseFloat(poi.size_scale),
                    "cssClass": "custom-hotspot",
                    "createTooltipFunc": hotspot_m,
                    "createTooltipArgs": poi
                });
            }
        });
        try {
            window.viewer.destroy();
        } catch (e) {}
        window.viewer = pannellum.viewer('panorama_pois', {
            "type": "equirectangular",
            "panorama": "../viewer/panoramas/"+image,
            "autoLoad": true,
            "showFullscreenCtrl": false,
            "showControls": false,
            "hfov": 100,
            "minHfov": 100,
            "maxHfov": 100,
            "yaw": window.currentYaw,
            "pitch": window.currentPitch,
            "friction": 1,
            "hotSpots": hotSpots
        });
        window.viewer.on("load",function () {
            window.viewer_initialized = true;
            adjust_ratio_hfov('panorama_pois',window.viewer,100,100,100);
            check_plan(window.id_user,'poi');
            if(window.can_create) {
                $('#plan_poi_msg').addClass('d-none');
                $('#btn_add_poi').css('opacity',1);
                $('#btn_add_poi').css('pointer-events','initial');
            } else {
                $('#plan_poi_msg').removeClass('d-none');
                $('#btn_add_poi').css('opacity',0.3);
                $('#btn_add_poi').css('pointer-events','none');
            }
            if(id_poi_add!=null) {
                move_p(id_poi_add,index_poi_add,id_poi_add);
            }
        });
        window.viewer.on('animatefinished',function () {
            var yaw = parseInt(viewer.getYaw());
            var pitch = parseInt(viewer.getPitch());
            window.currentYaw = yaw;
            window.currentPitch = pitch;
        });
    }

    function initialize_pano_markers(id_room,image,id_marker_add) {
        window.panorama_image = image;
        $('#panorama_markers').show();
        var hotSpots = [];
        var index_marker_add = null;
        jQuery.each(window.markers, function(index_m, marker_m) {
            if(id_marker_add!=null) {
                if(id_marker_add==marker_m.id) {
                    index_marker_add = index_m;
                }
            }
            if(marker_m.what=='marker') {
                hotSpots.push({
                    "id": marker_m.id,
                    "type": 'marker',
                    "pitch": parseInt(marker_m.pitch),
                    "yaw": parseInt(marker_m.yaw),
                    "rotateX": parseInt(marker_m.rotateX),
                    "rotateZ": parseInt(marker_m.rotateZ),
                    "size_scale": parseFloat(marker_m.size_scale),
                    "cssClass": "custom-hotspot",
                    "createTooltipFunc": hotspot_m,
                    "createTooltipArgs": marker_m,
                    "clickHandlerFunc": click_edit_marker,
                    "clickHandlerArgs": marker_m.id
                });
            } else {
                hotSpots.push({
                    "type": 'poi',
                    "pitch": parseInt(marker_m.pitch),
                    "yaw": parseInt(marker_m.yaw),
                    "rotateX": 0,
                    "rotateZ": 0,
                    "size_scale": parseFloat(marker_m.size_scale),
                    "cssClass": "custom-hotspot-content",
                    "createTooltipFunc": hotspot_p,
                    "createTooltipArgs": marker_m,
                });
            }
        });
        try {
            window.viewer.destroy();
        } catch (e) {}
        window.viewer = pannellum.viewer('panorama_markers', {
            "type": "equirectangular",
            "panorama": "../viewer/panoramas/"+image,
            "autoLoad": true,
            "showFullscreenCtrl": false,
            "showControls": false,
            "hfov": 100,
            "minHfov": 100,
            "maxHfov": 100,
            "yaw": window.currentYaw,
            "pitch": window.currentPitch,
            "friction": 1,
            "hotSpots": hotSpots
        });
        window.viewer.on("load",function () {
            window.viewer_initialized = true;
            adjust_ratio_hfov('panorama_markers',window.viewer,100,100,100);
            if(window.rooms_count>1) {
                check_plan(window.id_user,'marker');
                if(window.can_create) {
                    $('#plan_marker_msg').addClass('d-none');
                    $('#btn_add_marker').css('opacity',1);
                    $('#btn_add_marker').css('pointer-events','initial');
                } else {
                    $('#plan_marker_msg').removeClass('d-none');
                    $('#btn_add_marker').css('opacity',0.3);
                    $('#btn_add_marker').css('pointer-events','none');
                }
            } else {
                $('#btn_add_marker').css('opacity',0.3);
                $('#btn_add_marker').css('pointer-events','none');
            }
            if(id_marker_add!=null) {
                move_m(id_marker_add,index_marker_add);
            }
        });
        window.viewer.on('animatefinished',function () {
            var yaw = parseInt(viewer.getYaw());
            var pitch = parseInt(viewer.getPitch());
            window.currentYaw = yaw;
            window.currentPitch = pitch;
        });
    }

    window.click_edit_marker = function(hotSpotDiv,args) {
        if(!window.is_editing) {
            $('.custom-hotspot').css('opacity',0.5);
            $('.hotspot_'+args).css('opacity',1);
            jQuery.each(window.markers, function(index_m, marker_m) {
                if(parseInt(marker_m.id)==parseInt(args)) {
                    var yaw = parseInt(marker_m.yaw);
                    var pitch = parseInt(marker_m.pitch);
                    window.viewer.lookAt(pitch,yaw,100,200,function () {
                        $('.move_action').attr('onclick','move_m('+args+','+index_m+')');
                        $('.edit_action').attr('onclick','edit_m('+args+','+index_m+')');
                        $('.style_action').attr('onclick','style_m('+args+','+index_m+')');
                        $('.delete_action').attr('onclick','modal_delete_marker('+args+','+window.id_room_sel+',\''+window.panorama_image+'\')');
                        $('#action_box').show();
                    });
                    return;
                }
            });
        }
    }

    window.click_edit_poi = function(hotSpotDiv,args) {
        if(!window.is_editing) {
            $('.custom-hotspot-content').css('opacity',0.5);
            $('.hotspot_'+args).css('opacity',1);
            jQuery.each(window.pois, function(index, poi) {
                if(poi.what=='poi') {
                    if(parseInt(poi.id)==parseInt(args)) {
                        var yaw = parseInt(poi.yaw);
                        var pitch = parseInt(poi.pitch);
                        window.viewer.lookAt(pitch,yaw,100,200,function () {
                            $('.move_action').attr('onclick','move_p('+args+','+index+',null)');
                            $('.edit_action').attr('onclick','edit_p('+args+','+index+')');
                            $('.style_action').attr('onclick','style_p('+args+','+index+')');
                            $('.delete_action').attr('onclick','modal_delete_poi('+args+','+window.id_room_sel+',\''+window.panorama_image+'\')');
                            $('#action_box').show();
                        });
                        return;
                    }
                }
            });
        }
    }

    window.style_m = function(id,index) {
        window.marker_index_edit = index;
        window.marker_id_edit = id;
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_marker').addClass('disabled');
        $('#confirm_style i').attr('onclick','confirm_style_marker('+id+','+index+')');
        var yaw = parseInt(window.markers[index].yaw);
        var pitch = parseInt(window.markers[index].pitch)+20;
        window.viewer.lookAt(pitch,yaw,100,200);
        $('#marker_color').val(window.markers[index].color);
        $("#marker_color").spectrum("set", window.markers[index].color);
        $('#marker_background').val(window.markers[index].background);
        $("#marker_background").spectrum("set", window.markers[index].background);
        $('#marker_icon_preview')[0].className = window.markers[index].icon;
        $('#marker_icon').val(window.markers[index].icon);
        $("#marker_style").prop("selected", false);
        $("#marker_style option[id='"+window.markers[index].show_room+"']").prop("selected", true);
        $('#marker_library_icon').val(window.markers[index].id_icon_library);
        if(window.markers[index].show_room==4) {
            $('#marker_library_icon_preview').attr('src','../viewer/icons/'+window.markers[index].img_icon_library);
            $('#marker_library_icon_preview').show();
        }
        change_marker_style();
        $('#confirm_style').show();
        window.is_editing = true;
    }

    window.style_p = function(id,index) {
        window.poi_index_edit = index;
        window.poi_id_edit = id;
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_poi').addClass('disabled');
        $('#confirm_style i').attr('onclick','confirm_style_poi('+id+','+index+')');
        var yaw = parseInt(window.pois[index].yaw);
        var pitch = parseInt(window.pois[index].pitch)+20;
        window.viewer.lookAt(pitch,yaw,100,200);
        $('#poi_color').val(window.pois[index].color);
        $("#poi_color").spectrum("set", window.pois[index].color);
        $('#poi_background').val(window.pois[index].background);
        $("#poi_background").spectrum("set", window.pois[index].background);
        $('#poi_icon_preview')[0].className = window.pois[index].icon;
        $('#poi_icon').val(window.pois[index].icon);
        $('#poi_label').val(window.pois[index].label);
        $("#poi_style option").prop("selected", false);
        $("#poi_style option[id='"+window.pois[index].style+"']").prop("selected", true);
        $('#poi_library_icon').val(window.pois[index].id_icon_library);
        if(window.pois[index].style==1) {
            $('#poi_library_icon_preview').attr('src','../viewer/icons/'+window.pois[index].img_icon_library);
            $('#poi_library_icon_preview').show();
        }
        change_poi_style();
        $('#confirm_style').show();
        window.is_editing = true;
    }

    window.edit_p = function(id,index) {
        window.poi_index_edit = index;
        window.poi_id_edit = id;
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_poi').addClass('disabled');
        $('#confirm_edit i').attr('onclick','confirm_edit_poi('+id+','+index+')');
        var yaw = parseInt(window.pois[index].yaw);
        var pitch = parseInt(window.pois[index].pitch)+28;
        window.viewer.lookAt(pitch,yaw,100,200);
        switch(window.pois[index].type) {
            case 'image':
                $('#div_form_edit').hide();
                $('#frm_edit').show();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().show();
                $('#poi_description').parent().show();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Image Link");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val(window.pois[index].title);
                $('#poi_description').val(window.pois[index].description);
                break;
            case 'gallery':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().hide();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().show();
                break;
            case 'video':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').show();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Youtube/Vimeo Link or upload Video MP4");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'audio':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').show();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Audio MP3 Link or upload Audio MP3");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'video360':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').show();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Video 360 MP4");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'link':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Link (embed)");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'link_ext':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - Link (external)");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'html':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().hide();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#poi_content_html').parent().show();
                $('#poi_content_html').parent().find('.ql-toolbar').remove();
                $('#poi_content_html').html(window.pois[index].content).promise().done(function () {
                    var toolbarOptions = [
                        ['bold', 'italic', 'underline', 'strike'],
                        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                        [{ 'size': ['small', false, 'large', 'huge'] }],
                        [{ 'color': [] }, { 'background': [] }],
                        [{ 'align': [] }],
                        ['clean']
                    ];
                    window.html_editor = new Quill('#poi_content_html', {
                        modules: {
                            toolbar: toolbarOptions
                        },
                        theme: 'snow'
                    });
                });
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'html_sc':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().hide();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().show();
                $('#btn_poi_gallery').parent().hide();
                $('#poi_content_html_sc').html(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'download':
                $('#div_form_edit').hide();
                $('#frm_edit').hide();
                $('#frm_d_edit').show();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().show();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#content_label').html("Content - File Link");
                $('#poi_content').val(window.pois[index].content);
                $('#poi_title').val('');
                $('#poi_description').val('');
                break;
            case 'form':
                var content = window.pois[index].content;
                try {
                    content = JSON.parse(content);
                    $('#form_title').val(content[0].title);
                    $('#form_button').val(content[0].button);
                    $('#form_description').val(content[0].description);
                    for(var i=1;i<=5;i++) {
                        if(content[i]['enabled']) {
                            $('#form_field_'+i).prop('checked',true);
                        } else {
                            $('#form_field_'+i).prop('checked',false);
                        }
                        if(content[i]['required']) {
                            $('#form_field_required_'+i).prop('checked',true);
                        } else {
                            $('#form_field_required_'+i).prop('checked',false);
                        }
                        $('#form_field_type_'+i+' option[id=\''+content[i]['type']+'\']').prop('selected',true);
                        $('#form_field_label_'+i).val(content[i]['label']);
                    }
                } catch (e) {}
                $('#poi_title').val('');
                $('#poi_description').val('');
                $('#frm_edit').hide();
                $('#frm_d_edit').hide();
                $('#frm_v_edit').hide();
                $('#frm_a_edit').hide();
                $('#poi_content').parent().hide();
                $('#poi_title').parent().hide();
                $('#poi_description').parent().hide();
                $('#poi_content_html').parent().hide();
                $('#poi_content_html_sc').parent().hide();
                $('#btn_poi_gallery').parent().hide();
                $('#div_form_edit').show();
                break;
        }
        $('#confirm_edit').show();
        window.is_editing = true;
    }

    window.edit_poi_gallery = function() {
        $("#modal_images_gallery .modal-body").html('').promise().done(function () {
            $("#modal_images_gallery .modal-body").load('poi_gallery.php?id_poi='+window.poi_id_edit);
            $('#modal_images_gallery').modal('show');
        });
    }

    window.edit_m = function(id,index) {
        window.marker_index_edit = index;
        window.marker_id_edit = id;
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_marker').addClass('disabled');
        $('#confirm_edit i').attr('onclick','confirm_edit_marker('+id+','+index+')');
        get_option_rooms_target('room_target',window.id_room_sel,window.markers[index].id_room_target,id,index);
        $('#confirm_edit').show();
        window.is_editing = true;
    }

    window.move_m = function(id,index) {
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_marker').addClass('disabled');
        $('#confirm_move i').attr('onclick','confirm_move_marker('+id+','+index+')');
        $('#rotateX').val(window.markers[index].rotateX);
        $('#rotateZ').val(window.markers[index].rotateZ);
        $('#size_scale').val(window.markers[index].size_scale);
        $('#rotateX').attr('oninput','adjust_perspective('+id+','+index+')');
        $('#rotateZ').attr('oninput','adjust_perspective('+id+','+index+')');
        $('#size_scale').attr('oninput','adjust_size_scale_m('+id+','+index+')');
        $('#confirm_move').show();
        var md = new MobileDetect(window.navigator.userAgent);
        if(md.mobile()==null) {
            var interval_sec = 10;
        } else {
            var interval_sec = 500;
        }
        window.is_editing = true;
        window.interval_move = setInterval(function () {
            window.viewer.removeHotSpot(id.toString());
            var yaw = parseInt(window.viewer.getYaw());
            var pitch = parseInt(window.viewer.getPitch());
            window.viewer.addHotSpot({
                "id": window.markers[index].id,
                "type": 'marker',
                "pitch": pitch,
                "yaw": yaw,
                "rotateX": parseInt(window.markers[index].rotateX),
                "rotateZ": parseInt(window.markers[index].rotateZ),
                "size_scale": parseFloat(window.markers[index].size_scale),
                "cssClass": "custom-hotspot",
                "createTooltipFunc": hotspot_m,
                "createTooltipArgs": window.markers[index],
                "clickHandlerFunc": click_edit_marker,
                "clickHandlerArgs": parseInt(window.markers[index].id)
            });
        },interval_sec);
    }

    window.move_p = function(id,index,index_poi_add) {
        $('#action_box').hide();
        $('.rooms_slider').addClass('disabled');
        $('#btn_add_poi').addClass('disabled');
        $('#size_scale').val(window.pois[index].size_scale);
        $('#confirm_move i').attr('onclick','confirm_move_poi('+id+','+index+','+index_poi_add+')');
        $('#size_scale').attr('oninput','adjust_size_scale_p('+id+','+index+')');
        $('#confirm_move').show();
        var md = new MobileDetect(window.navigator.userAgent);
        if(md.mobile()==null) {
            var interval_sec = 10;
        } else {
            var interval_sec = 500;
        }
        window.interval_move = setInterval(function () {
            window.viewer.removeHotSpot(id.toString());
            var yaw = parseInt(window.viewer.getYaw());
            var pitch = parseInt(window.viewer.getPitch());
            window.viewer.addHotSpot({
                "id": window.pois[index].id,
                "type": 'poi',
                "pitch": pitch,
                "yaw": yaw,
                "rotateX": 0,
                "rotateZ": 0,
                "size_scale": parseFloat(window.pois[index].size_scale),
                "cssClass": "custom-hotspot-content",
                "createTooltipFunc": hotspot_p,
                "createTooltipArgs": window.pois[index],
                "clickHandlerFunc": click_edit_poi,
                "clickHandlerArgs": window.pois[index].id
            });
        },interval_sec);
    }

    window.adjust_perspective = function(id,index) {
        var rotateX = $('#rotateX').val();
        var rotateZ = $('#rotateZ').val();
        window.markers[index].rotateX = rotateX;
        window.markers[index].rotateZ = rotateZ;
    }

    window.adjust_size_scale_m = function(id,index) {
        var size_scale = $('#size_scale').val();
        window.markers[index].size_scale = size_scale;
    }

    window.adjust_size_scale_p = function(id,index) {
        var size_scale = $('#size_scale').val();
        window.pois[index].size_scale = size_scale;
    }

    window.confirm_move_marker = function (id,index) {
        clearInterval(window.interval_move);
        var yaw = parseInt(window.viewer.getYaw());
        var pitch = parseInt(window.viewer.getPitch());
        var rotateX = $('#rotateX').val();
        var rotateZ = $('#rotateZ').val();
        var size_scale = $('#size_scale').val();
        window.markers[index].yaw = yaw;
        window.markers[index].pitch = pitch;
        window.markers[index].rotateX = rotateX;
        window.markers[index].rotateZ = rotateZ;
        window.markers[index].size_scale = size_scale;
        window.is_editing = false;
        save_marker_pos(id,yaw,pitch,rotateX,rotateZ,size_scale);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_marker').removeClass('disabled');
        $('.custom-hotspot').css('opacity',1);
        $('#confirm_move').hide();
    }

    window.confirm_move_poi = function (id,index,index_poi_add) {
        clearInterval(window.interval_move);
        var yaw = parseInt(window.viewer.getYaw());
        var pitch = parseInt(window.viewer.getPitch());
        var size_scale = $('#size_scale').val();
        window.pois[index].size_scale = size_scale;
        window.pois[index].yaw = yaw;
        window.pois[index].pitch = pitch;
        window.is_editing = false;
        save_poi_pos(id,yaw,pitch,size_scale);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_poi').removeClass('disabled');
        $('.custom-hotspot-content').css('opacity',1);
        $('#confirm_move').hide();
        if(index_poi_add!=null) {
            edit_p(id,index);
        }
    }

    window.confirm_edit_marker = function (id,index) {
        window.is_editing = false;
        var id_room_target = window.markers[index].id_room_target;
        save_marker_edit(id,id_room_target);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_marker').removeClass('disabled');
        $('.custom-hotspot').css('opacity',1);
        $('#confirm_edit').hide();
    }

    window.confirm_edit_poi = function (id,index) {
        window.is_editing = false;
        var content = '';
        var c_title = '';
        var c_description = '';
        var type = window.pois[index].type;
        switch (type) {
            case 'image':
                c_title = $('#poi_title').val();
                c_description = $('#poi_description').val();
                content = $('#poi_content').val();
                break;
            case 'gallery':
                content = '';
                break;
            case 'html':
                content = window.html_editor.root.innerHTML;
                break;
            case 'html_sc':
                content = $('#poi_content_html_sc')[0].innerHTML;
                break;
            case 'form':
                content = [{},{},{},{},{},{}];
                var title = $('#form_title').val();
                var button = $('#form_button').val();
                var description = $('#form_description').val();
                content[0]['title'] = title;
                content[0]['button'] = button;
                content[0]['description'] = description;
                for(var i=1;i<=5;i++) {
                    content[i]['enabled'] = $('#form_field_'+i).is(':checked');
                    content[i]['required'] = $('#form_field_required_'+i).is(':checked');
                    content[i]['type'] = $('#form_field_type_'+i+' option:selected').attr('id');
                    content[i]['label'] = $('#form_field_label_'+i).val();
                }
                content = JSON.stringify(content);
                break;
            default:
                content = $('#poi_content').val();
                break;
        }
        window.pois[index].content = content;
        window.pois[index].title = c_title;
        window.pois[index].description = c_description;
        save_poi_edit(id,type,content,c_title,c_description);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_poi').removeClass('disabled');
        var yaw = parseInt(window.pois[index].yaw);
        var pitch = parseInt(window.pois[index].pitch);
        window.viewer.lookAt(pitch,yaw,100,200);
        $('.custom-hotspot-content').css('opacity',1);
        $('#confirm_edit').hide();
    }

    window.confirm_style_marker = function (id,index) {
        var show_room = $('#marker_style option:selected').attr('id');
        var color = $('#marker_color').val();
        var background = $('#marker_background').val();
        var icon = $('#marker_icon').val();
        if(show_room==4) {
            var id_icon_library = $('#marker_library_icon').val();
            if(id_icon_library==0) {
                alert('Please select an icon from library');
                return;
            }
        } else {
            var id_icon_library = 0;
        }
        window.is_editing = false;
        window.markers[index].id_icon_library = id_icon_library;
        window.markers[index].show_room = show_room;
        save_marker_style(id,show_room,color,background,icon,id_icon_library);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_marker').removeClass('disabled');
        var yaw = parseInt(window.markers[index].yaw);
        var pitch = parseInt(window.markers[index].pitch);
        window.viewer.lookAt(pitch,yaw,100,200);
        $('.custom-hotspot').css('opacity',1);
        $('#confirm_style').hide();
    }

    window.confirm_style_poi = function (id,index) {
        var color = $('#poi_color').val();
        var background = $('#poi_background').val();
        var icon = $('#poi_icon').val();
        var label = $('#poi_label').val();
        var style = $('#poi_style option:selected').attr('id');
        if(style==1) {
            var id_icon_library = $('#poi_library_icon').val();
            if(id_icon_library==0) {
                alert('Please select an icon from library');
                return;
            }
        } else {
            var id_icon_library = 0;
        }
        window.is_editing = false;
        window.pois[index].id_icon_library = id_icon_library;
        window.pois[index].style = style;
        window.pois[index].label = label;
        save_poi_style(id,color,background,icon,style,id_icon_library,label);
        $('.rooms_slider').removeClass('disabled');
        $('#btn_add_poi').removeClass('disabled');
        var yaw = parseInt(window.pois[index].yaw);
        var pitch = parseInt(window.pois[index].pitch);
        window.viewer.lookAt(pitch,yaw,100,200);
        $('.custom-hotspot-content').css('opacity',1);
        $('#confirm_style').hide();
    }

    function hotspot_p(hotSpotDiv, args) {
        hotSpotDiv.classList.add('hotspot_'+args.id);
        hotSpotDiv.classList.add('noselect');

        if(args.what=='marker') {
            hotSpotDiv.style.opacity = 0.2;
        }

        var style_t = parseInt(args.style);
        switch (style_t) {
            case 0:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                break;
            case 1:
                var img = document.createElement('img');
                img.src = '../viewer/icons/'+args.img_icon_library;
                img.style = "width:50px";
                hotSpotDiv.appendChild(img);
                break;
            case 2:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                var span = document.createElement('span');
                span.innerHTML = '&nbsp;'+args.label.toUpperCase();
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
            case 3:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.label.toUpperCase()+'&nbsp;';
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                break;
            case 4:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.label.toUpperCase();
                span.style = "font-size:14px;vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
        }
    }

    function hotspot_m(hotSpotDiv, args) {
        hotSpotDiv.classList.add('custom-tooltip');
        hotSpotDiv.classList.add('hotspot_'+args.id);
        hotSpotDiv.classList.add('noselect');
        var show_room_t = parseInt(args.show_room);
        switch (show_room_t) {
            case 0:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon+" marker_img_"+args.id_room_target;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                break;
            case 1:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon+" marker_img_"+args.id_room_target;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                var span = document.createElement('span');
                span.innerHTML = '&nbsp;'+args.name_room_target.toUpperCase();
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
            case 2:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.name_room_target.toUpperCase()+'&nbsp;';
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                var i = document.createElement('i');
                i.className = args.icon+" marker_img_"+args.name_room_target;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                break;
            case 3:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.name_room_target.toUpperCase();
                span.style = "font-size:14px;vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
            case 4:
                var img = document.createElement('img');
                img.src = '../viewer/icons/'+args.img_icon_library;
                img.style = "width:50px";
                hotSpotDiv.appendChild(img);
                break;
        }
    }

    function get_pois(id_room,image,id_poi_add) {
        $.ajax({
            url: "ajax/get_pois.php",
            type: "POST",
            data: {
                id_room: id_room
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                window.pois = rsp;
                initialize_pano_pois(id_room,image,id_poi_add);
            }
        });
    }

    window.change_marker_room_target = function (id,index,id_select) {
        var name_room_target = $('#'+id_select).val().toUpperCase();
        var id_room_target = $('#'+id_select+' option:selected').attr('id');
        if(id_select=='room_target') {
            window.viewer.removeHotSpot(id.toString());
            window.markers[index].name_room_target = name_room_target;
            window.markers[index].id_room_target = id_room_target;
            window.viewer.addHotSpot({
                "id": window.markers[index].id,
                "type": 'marker',
                "pitch": parseInt(window.markers[index].pitch),
                "yaw": parseInt(window.markers[index].yaw),
                "rotateX": parseInt(window.markers[index].rotateX),
                "rotateZ": parseInt(window.markers[index].rotateZ),
                "size_scale": parseFloat(window.markers[index].size_scale),
                "cssClass": "custom-hotspot",
                "createTooltipFunc": hotspot_m,
                "createTooltipArgs": window.markers[index],
                "clickHandlerFunc": click_edit_marker,
                "clickHandlerArgs": parseInt(window.markers[index].id)
            });
        }
        change_preview_room_image(id_room_target);
    }

    function change_preview_room_image(id_room_target) {
        jQuery.each(window.rooms, function(index, room) {
            var id_room = room.id;
            if(id_room==id_room_target) {
                var room_image = room.panorama_image;
                $('.preview_room_target').attr('src','../viewer/panoramas/thumb/'+room_image);
                $('.preview_room_target').show();
                return;
            }
        });
    }

    window.change_marker_style = function() {
        var show_room = $('#marker_style option:selected').attr('id');
        if(show_room==4) {
            $('#marker_icon').parent().parent().hide();
            $('#marker_color').parent().parent().hide();
            $('#marker_background').parent().parent().hide();
            $('#marker_library_icon').parent().parent().show();
            var id_icon_library = parseInt($('#marker_library_icon').val());
            if(id_icon_library!=0) {
                window.markers[window.marker_index_edit].show_room = show_room;
                window.markers[window.marker_index_edit].id_icon_library = id_icon_library;
                window.markers[window.marker_index_edit].img_icon_library = $('#marker_library_icon_preview').attr('src').replace('../viewer/icons/','');
                render_marker(window.marker_id_edit,window.marker_index_edit);
            } else {
                $('#marker_library_icon_preview').hide();
            }
        } else {
            $('#marker_library_icon').parent().parent().hide();
            $('#marker_icon').parent().parent().show();
            $('#marker_color').parent().parent().show();
            $('#marker_background').parent().parent().show();
            window.markers[window.marker_index_edit].show_room = show_room;
            render_marker(window.marker_id_edit,window.marker_index_edit);
        }
    }

    window.change_poi_style = function() {
        var style = $('#poi_style option:selected').attr('id');
        if(style==1) {
            $('#poi_icon').parent().parent().hide();
            $('#poi_color').parent().parent().hide();
            $('#poi_background').parent().parent().hide();
            $('#poi_library_icon').parent().parent().show();
            var id_icon_library = parseInt($('#poi_library_icon').val());
            if(id_icon_library!=0) {
                window.pois[poi_index_edit].style = style;
                window.pois[poi_index_edit].id_icon_library = id_icon_library;
                window.pois[poi_index_edit].img_icon_library = $('#poi_library_icon_preview').attr('src').replace('../viewer/icons/','');
                render_poi(window.poi_id_edit,window.poi_index_edit);
            } else {
                $('#poi_library_icon_preview').hide();
            }
        } else {
            $('#poi_library_icon').parent().parent().hide();
            $('#poi_icon').parent().parent().show();
            $('#poi_color').parent().parent().show();
            $('#poi_background').parent().parent().show();
            window.pois[window.poi_index_edit].style = style;
            render_poi(window.poi_id_edit,window.poi_index_edit);
        }
    }

    window.select_icon_library = function(p,id,image) {
        switch(p){
            case 'marker':
                window.markers[window.marker_index_edit].show_room = 4;
                window.markers[window.marker_index_edit].img_icon_library = image;
                $('#marker_library_icon').val(id);
                $('#marker_library_icon_preview').attr('src','../viewer/icons/'+image);
                $('#marker_library_icon_preview').show();
                render_marker(window.marker_id_edit,window.marker_index_edit);
                break;
            case 'poi':
                window.pois[window.poi_index_edit].style = 1;
                window.pois[window.poi_index_edit].img_icon_library = image;
                $('#poi_library_icon').val(id);
                $('#poi_library_icon_preview').attr('src','../viewer/icons/'+image);
                $('#poi_library_icon_preview').show();
                render_poi(window.poi_id_edit,window.poi_index_edit);
                break;
        }
        $('#modal_library_icons').modal('hide');
    }

    window.render_marker = function(id,index) {
        window.viewer.removeHotSpot(id.toString());
        window.viewer.addHotSpot({
            "id": window.markers[index].id,
            "type": 'marker',
            "pitch": parseInt(window.markers[index].pitch),
            "yaw": parseInt(window.markers[index].yaw),
            "rotateX": parseInt(window.markers[index].rotateX),
            "rotateZ": parseInt(window.markers[index].rotateZ),
            "size_scale": parseFloat(window.markers[index].size_scale),
            "cssClass": "custom-hotspot",
            "createTooltipFunc": hotspot_m,
            "createTooltipArgs": window.markers[index],
            "clickHandlerFunc": click_edit_marker,
            "clickHandlerArgs": parseInt(window.markers[index].id)
        });
    }

    window.render_poi = function(id,index) {
        window.viewer.removeHotSpot(id.toString());
        window.viewer.addHotSpot({
            "id": window.pois[index].id,
            "type": 'poi',
            "pitch": parseInt(window.pois[index].pitch),
            "yaw": parseInt(window.pois[index].yaw),
            "rotateX": 0,
            "rotateZ": 0,
            "size_scale": parseFloat(window.pois[index].size_scale),
            "cssClass": "custom-hotspot-content",
            "createTooltipFunc": hotspot_p,
            "createTooltipArgs": window.pois[index],
            "clickHandlerFunc": click_edit_poi,
            "clickHandlerArgs": window.pois[index].id
        });
    }

    window.get_option_rooms_target = function(id_select,id,id_room_target,id_marker,index_marker) {
        var html_option_rooms_target = "";
        var first_id_room = null;
        jQuery.each(window.rooms, function(index, room) {
            var id_room = room.id;
            var name = room.name;
            if(id!=id_room) {
                if(first_id_room==null) {
                    first_id_room = id_room;
                }
                if(id_room==id_room_target) {
                    html_option_rooms_target += '<option selected id="'+id_room+'">'+name+'</option>';
                } else {
                    html_option_rooms_target += '<option id="'+id_room+'">'+name+'</option>';
                }
                $('#'+id_select).html(html_option_rooms_target).promise().done(function () {
                    $('#'+id_select).attr('onchange','change_marker_room_target('+id_marker+','+index_marker+',\''+id_select+'\')');
                    if(id_select=='room_target_add') {
                        change_preview_room_image(first_id_room);
                    } else {
                        change_preview_room_image(id_room_target);
                    }
                });
                return;
            }
        });
    }

    window.set_room_target_map = function(id) {
        jQuery.each(window.map_points, function(index, point) {
            var id_room = point.id;
            var name = point.name;
            if(id_room==id) {
                $('#room_target').val(name);
            }
        });
    }

    window.adjust_points_size = function () {
        var point_size = $('#point_size').val();
        if((point_size=='') || (point_size<=0)) {
            point_size=20;
        }
        $('.pointer').css('width',point_size+'px');
        $('.pointer').css('height',point_size+'px');
    }

    window.adjust_points_position = function () {
        var image_w = $('#map_image').width();
        var image_h = $('#map_image').height();
        $('#pointers_div').css('width',image_w+'px');
        $('#pointers_div').css('height',image_h+'px');
        var ratio = image_w / image_h;
        var ratio_w = image_w / 300;
        var ratio_h = image_h / ((image_w / ratio_w) / ratio);
        window.ratio_w = ratio_w;
        window.ratio_h = ratio_h
        jQuery.each(window.map_points, function(index, point) {
            var id_point = point.id;
            var map_left = point.map_left;
            var map_top = point.map_top;
            var pos_left = map_left * ratio_w;
            var pos_top = map_top * ratio_h;
            $('#pointer_'+id_point).css('transform','scale('+ratio_w+')');
            $('#pointer_'+id_point).css('top',pos_top+'px');
            $('#pointer_'+id_point).css('left',pos_left+'px');
        });
    }

    window.adjust_marker_position = function () {
        var image_w = $('#panorama_image').width();
        var image_h = $('#panorama_image').height();
        $('#markers_div').css('width',image_w+'px');
        $('#markers_div').css('height',image_h+'px');
        var half_x = image_w / 2;
        var half_y = image_h / 2;
        var ratio_x = half_x / 180;
        var ratio_y = half_y / 90;
        if(image_w>=1100) {
            var scale = 0.7;
        } else if((image_w<1100) & (image_w>=500)) {
            var scale = 0.5;
        } else {
            var scale = 0.3;
        }
        jQuery.each(window.markers, function(index, marker) {
            var id_marker = marker.id;
            var yaw = marker.yaw;
            var pitch = marker.pitch * -1;
            var marker_w = $('#marker_'+id_marker).width();
            var marker_h = $('#marker_'+id_marker).height();
            var pos_left = ((yaw*ratio_x)+half_x)-(marker_w/1.77);
            var pos_top = ((pitch*ratio_y)+half_y)-(marker_h/1.77);
            $('#marker_'+id_marker).css('transform','translate('+pos_left+'px,'+pos_top+'px) scale('+scale+')');
        });
    }

    window.adjust_poi_position = function () {
        var image_w = $('#panorama_image').width();
        var image_h = $('#panorama_image').height();
        $('#markers_div').css('width',image_w+'px');
        $('#markers_div').css('height',image_h+'px');
        var half_x = image_w / 2;
        var half_y = image_h / 2;
        var ratio_x = half_x / 180;
        var ratio_y = half_y / 90;
        if(image_w>=1100) {
            var scale = 1;
        } else if((image_w<1100) & (image_w>=500)) {
            var scale = 0.6;
        } else {
            var scale = 0.3;
        }
        jQuery.each(window.pois, function(index, poi) {
            var id_poi = poi.id;
            var yaw = poi.yaw;
            var pitch = poi.pitch * -1;
            var poi_w = $('#poi_'+id_poi).width();
            var poi_h = $('#poi_'+id_poi).height();
            var pos_left = ((yaw*ratio_x)+half_x)-(poi_w/1.3);
            var pos_top = ((pitch*ratio_y)+half_y)-(poi_h/1.3);
            $('#poi_'+id_poi).css('transform','translate('+pos_left+'px,'+pos_top+'px) scale('+scale+')');
        });
    }

    function parse_room_list(rooms) {
        var html = '';
        jQuery.each(rooms, function(index, room) {
            var id = room.id;
            var name = room.name;
            var image = room.panorama_image;
            var count_markers = room.count_markers;
            var count_pois = room.count_pois;
            var type = room.type;
            switch (type) {
                case 'image':
                    type = '<i style="font-size: 14px" class="fas fa-image"></i>';
                    break;
                case 'video':
                    type = '<i style="font-size: 14px" class="fas fa-video"></i>';
                    break;
            }
            html += '<div class="card mb-2 py-2 border-left-primary">\n' +
                '            <div id="room_list" class="card-body" style="padding-top: 0;padding-bottom: 0;">\n' +
                '                <div data-id="'+id+'" class="row room">\n' +
                '                    <div class="col text-center text-sm-center text-md-left text-lg-left">\n' +
                '                        <div class="d-inline-block align-middle"><img style="height: 40px;" src="../viewer/panoramas/thumb/'+image+'" /></div>\n' +
                '                        <div class="d-inline-block ml-2 align-middle text-left">'+type+' <b>'+name+'</b><br><i style="font-size:12px">'+count_markers+' markers - '+count_pois+' pois</i></div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-auto pt-1 text-center text-sm-center text-md-right text-lg-right">\n' +
                '                        <a title="DRAG TO CHANGE POSITION" style="background-color: white; border: 1px solid black; cursor: pointer" class="handle btn btn-primary btn-circle">\n' +
                '                           <i style="color: black" class="fas fa-arrows-alt"></i>\n' +
                '                        </a>\n' +
                '                        <a title="EDIT" href="index.php?p=edit_room&id='+id+'" class="btn btn-warning btn-circle">\n' +
                '                            <i class="fas fa-edit"></i>\n' +
                '                        </a>\n' +
                '                        <a title="MARKERS" href="index.php?p=markers&id_room='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-caret-square-up"></i>\n' +
                '                        </a>\n' +
                '                        <a title="POIS" href="index.php?p=pois&id_room='+id+'" class="btn btn-info btn-circle">\n' +
                '                            <i class="fas fa-bullseye"></i>\n' +
                '                        </a>\n' +
                '                        <a title="DELETE" href="#" onclick="modal_delete_room('+id+');return false;" class="btn btn-danger btn-circle">\n' +
                '                            <i class="fas fa-trash"></i>\n' +
                '                        </a>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        });
        $('#rooms_list').html(html).promise().done(function () {
            $('#rooms_list .btn').tooltipster({
                delay: 10,
                hideOnClick: true
            });
            var el = document.getElementById('rooms_list');
            Sortable.create(el,{
                handle: '.handle',
                onEnd: function (evt) {
                    var array_rooms_priority = [];
                    $('#rooms_list .room').each(function () {
                        var id = $(this).attr('data-id');
                        array_rooms_priority.push(id);
                    });
                    change_rooms_order(array_rooms_priority);
                },
            });
        });
    }

    function parse_rooms_menu_list(menu_list) {
        var html = '<div class="col-md-12"><div class="dd"><ol class="dd-list">';
        jQuery.each(menu_list, function(index, item) {
            var id = item.id;
            var name = item.name;
            var type = item.type;
            var hide = item.hide;
            switch (type) {
                case 'room':
                    if(hide==0) {
                        html += '<li class="dd-item dd-nochildren" data-hide="0" data-type="room" data-id="'+id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><span style="opacity: 1.0"><i class="fas fa-fw fa-vector-square"></i> '+name+'</span> <i onclick="hide_menu_item(\''+id+'\')" style="float: right;cursor: pointer" class="fas fa-eye"></i></div></li>';
                    } else {
                        html += '<li class="dd-item dd-nochildren" data-hide="1" data-type="room" data-id="'+id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><span style="opacity: 0.5"><i class="fas fa-fw fa-vector-square"></i> '+name+'</span> <i onclick="show_menu_item(\''+id+'\')" style="float: right;cursor: pointer" class="fas fa-eye-slash"></i></div></li>';
                    }
                    break;
                case 'category':
                    html += '<li class="dd-item" data-cat="'+name+'" data-type="category" data-id="'+id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><i class="fas fa-stream"></i> <div style="width: 70%;padding-left: 5px;display: inline-block;background-color: white;border: 1px solid lightgray;" contenteditable="true">'+name+'</div> <i onclick="remove_menu_item(\''+id+'\')" style="float: right;cursor: pointer" class="fas fa-trash"></i></div>';
                    var childrens = item.childrens;
                    if(childrens.length > 0) {
                        html += '<ol class="dd-list">';
                        for(var i=0;i<childrens.length;i++) {
                            if(childrens[i]['hide']==0) {
                                html += '<li class="dd-item dd-nochildren" data-hide="0" data-type="room" data-id="'+childrens[i]['id']+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><span style="opacity: 1.0"><i class="fas fa-fw fa-vector-square"></i> '+childrens[i]['name']+'</span> <i onclick="hide_menu_item(\''+childrens[i]['id']+'\')" style="float: right;cursor: pointer" class="fas fa-eye"></i></div></li>';
                            } else {
                                html += '<li class="dd-item dd-nochildren" data-hide="1" data-type="room" data-id="'+childrens[i]['id']+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><span style="opacity: 0.5"><i class="fas fa-fw fa-vector-square"></i> '+childrens[i]['name']+'</span> <i onclick="show_menu_item(\''+childrens[i]['id']+'\')" style="float: right;cursor: pointer" class="fas fa-eye-slash"></i></div></li>';
                            }
                        }
                        html += '</ol>';
                    }
                    html += '</li>';
                    break;
            }
        });
        html += '</ol></div></div>';
        $('.list_div').html(html).promise().done(function () {
            $('.dd3-content div').on('input',function () {
                var cat = $(this).html();
                $(this).parent().parent().attr('data-cat',cat);
                $(this).parent().parent().data('cat',cat);
                save_list_alt();
            });
            $('.dd').nestable({
                scroll: true,
                maxDepth: 2,
                callback: function(l,e){
                    check_category_deletable();
                    save_list_alt();
                }
            });
            check_category_deletable();
            $('.add_cat_div').show();
        });
    }

    function check_category_deletable() {
        $(".dd-item[data-type='category']").each(function () {
            if($(this).find('ol').hasClass('dd-list')) {
                $(this).find('i.fa-trash').addClass('disabled');
            } else {
                $(this).find('i.fa-trash').removeClass('disabled');
            }
        });
    }

    function save_list_alt() {
        var list_alt = $('.dd').nestable('serialize');
        list_alt = JSON.stringify(list_alt);
        $.ajax({
            url: "ajax/save_list_alt.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                list_alt: list_alt,
            },
            async: true,
            success: function (json) {

            }
        });
    }

    window.add_menu_list_cat = function () {
        var last_id_cat = $(".dd-item[data-type='category']").length;
        if(last_id_cat==0) {
            last_id_cat = 1;
        } else {
            last_id_cat++;
        }
        var add_cat = $('#add_cat').val();
        if(add_cat != '') {
            $(".dd > ol").append("<li class='dd-item' data-cat='"+add_cat+"' data-type='category' data-id='c"+last_id_cat+"'><div class='dd-handle dd3-handle'></div><div class='dd3-content'><i class=\"fas fa-stream\"></i> <div style=\"width: 70%;padding-left: 5px;display: inline-block;background-color: white;border: 1px solid lightgray;\" contenteditable=\"true\">"+add_cat+"</div> <i onclick=\"remove_menu_item('c"+last_id_cat+"')\" style=\"float: right;cursor: pointer\" class=\"fas fa-trash\"></i></div></li>");
            $('.dd3-content div').off('input');
            $('.dd3-content div').on('input',function () {
                var cat = $(this).html();
                $(this).parent().parent().attr('data-cat',cat);
                $(this).parent().parent().data('cat',cat);
                save_list_alt();
            });
        }
        $('#add_cat').val('');
    }

    window.remove_menu_item = function(id) {
        $('.dd').nestable('remove', id);
        save_list_alt();
    }

    window.hide_menu_item = function(id) {
        $(".dd-item[data-id='"+id+"']").find('i.fa-eye').first().removeClass('fa-eye').addClass('fa-eye-slash');
        $(".dd-item[data-id='"+id+"']").find('span').first().css('opacity',0.5);
        $(".dd-item[data-id='"+id+"']").attr('data-hide','1');
        $(".dd-item[data-id='"+id+"']").data('hide','1');
        $(".dd-item[data-id='"+id+"']").find('i.fa-eye-slash').first().attr('onclick','show_menu_item(\''+id+'\')');
        save_list_alt();
    }

    window.show_menu_item = function(id) {
        $(".dd-item[data-id='"+id+"']").find('i.fa-eye-slash').first().removeClass('fa-eye-slash').addClass('fa-eye');
        $(".dd-item[data-id='"+id+"']").find('span').first().css('opacity',1.0);
        $(".dd-item[data-id='"+id+"']").attr('data-hide','0');
        $(".dd-item[data-id='"+id+"']").data('hide','0');
        $(".dd-item[data-id='"+id+"']").find('i.fa-eye').first().attr('onclick','hide_menu_item(\''+id+'\')');
        save_list_alt();
    }

    window.change_room_type = function() {
        var type = $('#type option:selected').attr('id');
        switch (type) {
            case 'image':
                $('#room_upload_div label').html('Panorama image');
                $('#room_upload_div p').html('<i>Accepted only 360 degree images in JPG/PNG format.</i>');
                $('#frm').attr('action', 'ajax/upload_room_image.php');
                break;
            case 'video':
                $('#room_upload_div label').html('Panorama video');
                $('#room_upload_div p').html('<i>Accepted only 360 degree videos in MP4 format.</i>');
                $('#frm').attr('action', 'ajax/upload_room_video.php');
                break;
        }
        $("#txtFile").val(null);
        $('#preview_image img').attr('src','');
        $('#preview_image').hide();
        $('#btn_create_room').prop("disabled",true);
    }

    window.add_user = function () {
        var complete = true;
        var username = $('#username').val();
        var role = $('#role option:selected').attr('id');
        var password = $('#password').val();
        var password2 = $('#repeat_password').val();
        if(username=='') {
            complete = false;
            $('#username').addClass("error-highlight");
        } else {
            $('#username').removeClass("error-highlight");
        }
        if(password=='') {
            complete = false;
            $('#password').addClass("error-highlight");
        } else {
            $('#password').removeClass("error-highlight");
        }
        if(password2=='') {
            complete = false;
            $('#repeat_password').addClass("error-highlight");
        } else {
            $('#repeat_password').removeClass("error-highlight");
        }
        if((password!='') && (password2!='')) {
            if(password!=password2) {
                complete = false;
                $('#password').addClass("error-highlight");
                $('#repeat_password').addClass("error-highlight");
            } else {
                $('#password').removeClass("error-highlight");
                $('#repeat_password').removeClass("error-highlight");
            }
        }
        if(complete) {
            $('#modal_new_user button').addClass("disabled");
            $.ajax({
                url: "ajax/add_user.php",
                type: "POST",
                data: {
                    username: username,
                    password: password,
                    role: role
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_new_user button').removeClass("disabled");
                        $('#modal_new_user').modal("hide");
                        location.reload();
                    } else {
                        $('#modal_new_user button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.change_password = function () {
        var complete = true;
        var password = $('#password').val();
        var password2 = $('#repeat_password').val();
        if(password=='') {
            complete = false;
            $('#password').addClass("error-highlight");
        } else {
            $('#password').removeClass("error-highlight");
        }
        if(password2=='') {
            complete = false;
            $('#repeat_password').addClass("error-highlight");
        } else {
            $('#repeat_password').removeClass("error-highlight");
        }
        if((password!='') && (password2!='')) {
            if(password!=password2) {
                complete = false;
                $('#password').addClass("error-highlight");
                $('#repeat_password').addClass("error-highlight");
            } else {
                $('#password').removeClass("error-highlight");
                $('#repeat_password').removeClass("error-highlight");
            }
        }
        if(complete) {
            $('#modal_change_password button').addClass("disabled");
            $.ajax({
                url: "ajax/change_password.php",
                type: "POST",
                data: {
                    id: window.id_user_edit,
                    password: password,
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        window.user_need_save = false;
                        $('#modal_change_password button').removeClass("disabled");
                        $('#modal_change_password').modal("hide");
                        $('#password').val('');
                        $('#repeat_password').val('');
                    } else {
                        $('#modal_change_password button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.open_modal_plan_edit = function(id) {
        $.ajax({
            url: "ajax/get_plan.php",
            type: "POST",
            data: {
                id: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                console.log(rsp);
                $('#name_edit').val(rsp.name);
                $('#n_virtual_tours_edit').val(rsp.n_virtual_tours);
                $('#n_rooms_edit').val(rsp.n_rooms);
                $('#n_markers_edit').val(rsp.n_markers);
                $('#n_pois_edit').val(rsp.n_pois);
                $('#days_edit').val(rsp.days);
                if(rsp.create_landing==1) {
                    $('#create_landing_edit').prop('checked',true);
                } else {
                    $('#create_landing_edit').prop('checked',false);
                }
                var count_usage = rsp.count_usage;
                if(count_usage>0) {
                    $('#btn_delete_plan').addClass("disabled");
                } else {
                    $('#btn_delete_plan').removeClass("disabled");
                }
                $('#modal_edit_plan').modal("show");
            }
        });
    };

    window.save_plan = function () {
        var complete = true;
        var name = $('#name_edit').val();
        var n_virtual_tours = $('#n_virtual_tours_edit').val();
        var n_rooms = $('#n_rooms_edit').val();
        var n_markers = $('#n_markers_edit').val();
        var n_pois = $('#n_pois_edit').val();
        var days = $('#days_edit').val();
        var create_landing = $('#create_landing_edit').is(':checked');
        if(name=='') {
            complete = false;
            $('#name_edit').addClass("error-highlight");
        } else {
            $('#name_edit').removeClass("error-highlight");
        }
        if(complete) {
            $('#modal_edit_plan button').addClass("disabled");
            $.ajax({
                url: "ajax/save_plan.php",
                type: "POST",
                data: {
                    id: window.id_plan_sel,
                    name: name,
                    n_virtual_tours: n_virtual_tours,
                    n_rooms: n_rooms,
                    n_markers: n_markers,
                    n_pois: n_pois,
                    days: days,
                    create_landing: create_landing
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_edit_plan button').removeClass("disabled");
                        $('#modal_edit_plan').modal("hide");
                        window.plan_need_save = false;
                        location.reload();
                    } else {
                        $('#modal_edit_plan button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.delete_plan = function () {
        $('#modal_edit_plan button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_plan.php",
            type: "POST",
            data: {
                id: window.id_plan_sel
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_edit_plan button').removeClass("disabled");
                    $('#modal_edit_plan').modal("hide");
                    window.plan_need_save = false;
                    location.reload();
                } else {
                    alert(rsp.msg);
                    $('#modal_edit_plan button').removeClass("disabled");
                }
            }
        });

    };

    window.add_plan = function () {
        var complete = true;
        var name = $('#name').val();
        var n_virtual_tours = $('#n_virtual_tours').val();
        var n_rooms = $('#n_rooms').val();
        var n_markers = $('#n_markers').val();
        var n_pois = $('#n_pois').val();
        var create_landing = $('#create_landing').is(':checked');
        var days = $('#days').val();
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            $('#modal_new_plan button').addClass("disabled");
            $.ajax({
                url: "ajax/add_plan.php",
                type: "POST",
                data: {
                    name: name,
                    n_virtual_tours: n_virtual_tours,
                    n_rooms: n_rooms,
                    n_markers: n_markers,
                    n_pois: n_pois,
                    days: days,
                    create_landing: create_landing
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_new_plan button').removeClass("disabled");
                        $('#modal_new_plan').modal("hide");
                        window.plan_need_save = false;
                        location.reload();
                    } else {
                        $('#modal_new_plan button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.save_reg_settings = function() {
        var enable_registration = $('#enable_registration').is(':checked');
        var default_id_plan = $('#default_plan option:selected').attr('id');
        $('#btn_save_reg_settings').addClass('disabled');
        $.ajax({
            url: "ajax/save_reg_settings.php",
            type: "POST",
            data: {
                enable_registration: enable_registration,
                default_id_plan: default_id_plan
            },
            async: true,
            success: function (json) {
                window.plan_need_save = false;
                $('#btn_save_reg_settings').removeClass('disabled');
            }
        });
    }

    window.save_virtualtour = function() {
        var complete = true;
        var name = $('#name').val();
        var author = $('#author').val();
        var hfov = parseInt($('#hfov').val());
        var min_hfov = parseInt($('#min_hfov').val());
        var max_hfov = parseInt($('#max_hfov').val());
        var song_autoplay = $('#song_autoplay').is(':checked');
        var nadir_size = $('#size_nadir_logo option:selected').attr('id');
        var autorotate_inactivity = $('#autorotate_inactivity').val();
        var autorotate_speed = $('#autorotate_speed').val();
        var markers_icon = $('#markers_icon').val();
        var markers_color = $('#markers_color').val();
        var markers_background = $('#markers_background').val();
        var markers_style = $('#markers_style option:selected').attr('id');
        var pois_icon = $('#pois_icon').val();
        var pois_color = $('#pois_color').val();
        var pois_background = $('#pois_background').val();
        var pois_style = $('#pois_style option:selected').attr('id');
        var arrows_nav = $('#arrows_nav option:selected').attr('id');
        var voice_commands = $('#voice_commands option:selected').attr('id');
        var compass = $('#compass').is(':checked');
        var fb_messenger = $('#fb_messenger').is(':checked');
        var auto_start = $('#auto_start').is(':checked');
        var sameAzimuth = $('#sameAzimuth').is(':checked');
        var auto_show_slider = $('#auto_show_slider option:selected').attr('id');
        var show_list_alt = $('#show_list_alt option:selected').attr('id');
        var description = $('#description').val();
        var ga_tracking_id = $('#ga_tracking_id').val();
        var fb_page_id = $('#fb_page_id').val();
        var compress_jpg = $('#compress_jpg').val();
        var max_width_compress = $('#max_width_compress').val();
        var link_logo = $('#link_logo').val();
        var show_info = $('#show_info').is(':checked');
        var show_gallery = $('#show_gallery').is(':checked');
        var show_icons_toggle = $('#show_icons_toggle').is(':checked');
        var show_presentation = $('#show_presentation').is(':checked');
        var show_main_form = $('#show_main_form').is(':checked');
        var show_share = $('#show_share').is(':checked');
        var show_device_orientation = $('#show_device_orientation').is(':checked');
        var show_webvr = $('#show_webvr').is(':checked');
        var show_audio = $('#show_audio').is(':checked');
        var show_fullscreen = $('#show_fullscreen').is(':checked');
        var show_map = $('#show_map option:selected').attr('id');
        var live_session = $('#live_session').is(':checked');
        var show_annotations = $('#show_annotations').is(':checked');
        switch(markers_style) {
            case 'room_and_icon':
                var markers_show_room = 2;
                break;
            case 'icon_and_room':
                var markers_show_room = 1;
                break;
            case 'only_icon':
                var markers_show_room = 0;
                break;
            case 'only_room':
                var markers_show_room = 3;
                break;
        }
        switch(pois_style) {
            case 'label_and_icon':
                var pois_style_t = 3;
                break;
            case 'icon_and_label':
                var pois_style_t = 2;
                break;
            case 'only_icon':
                var pois_style_t = 0;
                break;
            case 'only_label':
                var pois_style_t = 4;
                break;
        }
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        var content = [{},{},{},{},{},{}];
        content[0]['title'] = $('#form_title').val();
        content[0]['button'] = $('#form_button').val();
        content[0]['description'] = $('#form_description').val();
        for(var i=1;i<=5;i++) {
            content[i]['enabled'] = $('#form_field_'+i).is(':checked');
            content[i]['required'] = $('#form_field_required_'+i).is(':checked');
            content[i]['type'] = $('#form_field_type_'+i+' option:selected').attr('id');
            content[i]['label'] = $('#form_field_label_'+i).val();
        }
        var form_content = JSON.stringify(content);
        var form_enable = $('#form_enable').is(':checked');
        var form_icon = $('#form_icon').val();
        if(complete) {
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_virtual_tour.php",
                type: "POST",
                data: {
                    id_virtualtour: id_virtualtour,
                    name: name,
                    author: author,
                    hfov: hfov,
                    min_hfov: min_hfov,
                    max_hfov: max_hfov,
                    song: window.song,
                    song_autoplay: song_autoplay,
                    background_image: window.background_image,
                    logo: window.logo,
                    link_logo: link_logo,
                    nadir_logo: window.nadir_logo,
                    nadir_size: nadir_size,
                    intro_mobile: window.intro_mobile,
                    intro_desktop: window.intro_desktop,
                    autorotate_speed: autorotate_speed,
                    autorotate_inactivity: autorotate_inactivity,
                    markers_icon: markers_icon,
                    markers_color: markers_color,
                    markers_background: markers_background,
                    markers_show_room: markers_show_room,
                    pois_icon: pois_icon,
                    pois_color: pois_color,
                    pois_background: pois_background,
                    pois_style: pois_style_t,
                    arrows_nav: arrows_nav,
                    voice_commands: voice_commands,
                    compass: compass,
                    auto_start: auto_start,
                    sameAzimuth: sameAzimuth,
                    auto_show_slider: auto_show_slider,
                    show_list_alt: show_list_alt,
                    description: description,
                    ga_tracking_id: ga_tracking_id,
                    compress_jpg: compress_jpg,
                    max_width_compress: max_width_compress,
                    form_enable: form_enable,
                    form_icon: form_icon,
                    form_content: form_content,
                    fb_messenger: fb_messenger,
                    fb_page_id: fb_page_id,
                    show_info: show_info,
                    show_gallery: show_gallery,
                    show_icons_toggle: show_icons_toggle,
                    show_presentation: show_presentation,
                    show_main_form: show_main_form,
                    show_share: show_share,
                    show_device_orientation: show_device_orientation,
                    show_webvr: show_webvr,
                    show_audio: show_audio,
                    show_fullscreen: show_fullscreen,
                    show_map: show_map,
                    live_session: live_session,
                    show_annotations: show_annotations
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=="ok") {
                        window.vt_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    }
                }
            });
        }
    }

    window.save_settings = function (validate_mail) {
        var complete = true;
        var purchase_code = $('#purchase_code').val();
        var name = $('#name').val();
        var furl_blacklist = $('#furl_blacklist').val();
        var smtp_server = $('#smtp_server').val();
        var smtp_port = $('#smtp_port').val();
        var smtp_auth = $('#smtp_auth').is(':checked');
        var smtp_secure = $('#smtp_secure option:selected').attr('id');
        var smtp_username = $('#smtp_username').val();
        var smtp_password = $('#smtp_password').val();
        var smtp_from_email = $('#smtp_from_email').val();
        var smtp_from_name = $('#smtp_from_name').val();
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            if(validate_mail) {
                $('#btn_validate_mail').addClass("disabled");
            }
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_settings.php",
                type: "POST",
                data: {
                    purchase_code: purchase_code,
                    name: name,
                    logo: window.b_logo_image,
                    background: window.b_background_image,
                    smtp_server: smtp_server,
                    smtp_port: smtp_port,
                    smtp_secure: smtp_secure,
                    smtp_auth: smtp_auth,
                    smtp_username: smtp_username,
                    smtp_password: smtp_password,
                    smtp_from_email: smtp_from_email,
                    smtp_from_name: smtp_from_name,
                    furl_blacklist: furl_blacklist
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        window.settings_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                        }, 1000);
                        if(validate_mail) {
                            $.ajax({
                                url: "ajax/send_email.php",
                                type: "POST",
                                data: {
                                    type: 'validate',
                                    email: smtp_from_email
                                },
                                timeout: 15000,
                                async: true,
                                success: function (json) {
                                    var rsp = JSON.parse(json);
                                    if (rsp.status == "ok") {
                                        $('#validate_mail').html('<i style="color: green" class="fas fa-circle"></i> Valid');
                                    } else {
                                        alert(rsp.msg);
                                        $('#validate_mail').html('<i style="color: red" class="fas fa-circle"></i> Invalid');
                                    }
                                    $('#btn_validate_mail').removeClass("disabled");
                                },
                                error: function(){
                                    alert('Timeout');
                                    $('#validate_mail').html('<i style="color: red" class="fas fa-circle"></i> Invalid');
                                    $('#btn_validate_mail').removeClass("disabled");
                                },
                            });
                        }
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        }, 1000);
                        if(validate_mail) {
                            $('#btn_validate_mail').removeClass("disabled");
                        }
                    }
                }
            });
        }
    }

    window.add_virtualtour = function () {
        var complete = true;
        var name = $('#name').val();
        var author = $('#author').val();
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            $('#modal_new_virtualtour button').addClass("disabled");
            $.ajax({
                url: "ajax/add_virtual_tour.php",
                type: "POST",
                data: {
                    id_user: window.id_user,
                    name: name,
                    author: author
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_new_virtualtour button').removeClass("disabled");
                        $('#modal_new_virtualtour').modal("hide");
                        location.href="index.php?p=edit_virtual_tour&id="+rsp.id;
                    } else {
                        $('#modal_new_virtualtour button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.add_map = function () {
        var complete = true;
        var name = $('#name').val();
        var map_image = $('#preview_image img').attr('src');
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            $('#modal_new_map button').addClass("disabled");
            $.ajax({
                url: "ajax/add_map.php",
                type: "POST",
                data: {
                    id_virtualtour: window.id_virtualtour,
                    name: name,
                    map_image: map_image
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_new_map button').removeClass("disabled");
                        $('#modal_new_map').modal("hide");
                        var id_map = rsp.id;
                        location.href = "index.php?p=edit_map&id="+id_map;
                    } else {
                        $('#modal_new_map button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.add_room = function () {
        var complete = true;
        var name = $('#name').val();
        var panorama_image = $('#preview_image img').attr('src');
        var type = $('#type option:selected').attr('id');
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            $('#modal_new_room button').addClass("disabled");
            $.ajax({
                url: "ajax/add_room.php",
                type: "POST",
                data: {
                    id_virtualtour: window.id_virtualtour,
                    name: name,
                    type: type,
                    panorama_image: panorama_image,
                    panorama_video: window.panorama_video
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        $('#modal_new_room button').removeClass("disabled");
                        $('#modal_new_room').modal("hide");
                        var id_room = rsp.id;
                        location.href = "index.php?p=edit_room&id="+id_room;
                    } else {
                        $('#modal_new_room button').removeClass("disabled");
                    }
                }
            });
        }
    };

    window.add_bulk_room = function (id_virtualtour,panorama_image,name) {
        $.ajax({
            url: "ajax/add_room.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                name: name,
                panorama_image: panorama_image
            },
            async: true,
            success: function (json) {
                window.rooms_created++;
                $('#rooms_created').html(window.rooms_created);
            }
        });
    };

    window.add_bulk_map = function (id_virtualtour,map_image,name) {
        $.ajax({
            url: "ajax/add_map.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                name: name,
                map_image: map_image
            },
            async: true,
            success: function (json) {
                window.maps_created++;
                $('#maps_created').html(window.maps_created);
            }
        });
    };

    window.save_user = function(id_user_edit) {
        var complete = true;
        var username = $('#username').val();
        var email = $('#email').val();
        var role = $('#role option:selected').attr('id');
        var plan = $('#plan option:selected').attr('id');
        var active = $('#active').is(':checked');
        if(username=='') {
            complete = false;
            $('#username').addClass("error-highlight");
        } else {
            $('#username').removeClass("error-highlight");
        }
        if(email=='') {
            complete = false;
            $('#email').addClass("error-highlight");
        } else {
            $('#email').removeClass("error-highlight");
        }
        if(complete) {
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_user.php",
                type: "POST",
                data: {
                    id: id_user_edit,
                    username: username,
                    email: email,
                    role: role,
                    plan: plan,
                    active: active,
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=="ok") {
                        window.user_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    }
                }
            });
        }
    }

    window.save_profile = function(id_user_edit) {
        var complete = true;
        var username = $('#username').val();
        var email = $('#email').val();
        if(username=='') {
            complete = false;
            $('#username').addClass("error-highlight");
        } else {
            $('#username').removeClass("error-highlight");
        }
        if(email=='') {
            complete = false;
            $('#email').addClass("error-highlight");
        } else {
            $('#email').removeClass("error-highlight");
        }
        if(complete) {
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_profile.php",
                type: "POST",
                data: {
                    id: id_user_edit,
                    username: username,
                    email: email,
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=="ok") {
                        window.user_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                            location.href = 'index.php?p=dashboard';
                        },200);
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    }
                }
            });
        }
    }

    window.save_voice_commands = function() {
        var complete = true;
        var language = $('#language').val();
        var initial_msg = $('#initial_msg').val();
        var listening_msg = $('#listening_msg').val();
        var error_msg = $('#error_msg').val();
        var help_cmd = $('#help_cmd').val();
        var help_msg_1 = $('#help_msg_1').val();
        var help_msg_2 = $('#help_msg_2').val();
        var next_cmd = $('#next_cmd').val();
        var next_msg = $('#next_msg').val();
        var prev_cmd = $('#prev_cmd').val();
        var prev_msg = $('#prev_msg').val();
        var left_cmd = $('#left_cmd').val();
        var left_msg = $('#left_msg').val();
        var right_cmd = $('#right_cmd').val();
        var right_msg = $('#right_msg').val();
        var up_cmd = $('#up_cmd').val();
        var up_msg = $('#up_msg').val();
        var down_cmd = $('#down_cmd').val();
        var down_msg = $('#down_msg').val();
        $('input').each(function () {
            if($(this).val()=='') {
                complete = false;
                $(this).addClass("error-highlight");
            } else {
                $(this).removeClass("error-highlight");
            }
        });
        if(complete) {
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_voice_commands.php",
                type: "POST",
                data: {
                    language: language,
                    initial_msg: initial_msg,
                    listening_msg: listening_msg,
                    error_msg: error_msg,
                    help_cmd: help_cmd,
                    help_msg_1: help_msg_1,
                    help_msg_2: help_msg_2,
                    next_cmd: next_cmd,
                    next_msg: next_msg,
                    prev_cmd: prev_cmd,
                    prev_msg: prev_msg,
                    left_cmd: left_cmd,
                    left_msg: left_msg,
                    right_cmd: right_cmd,
                    right_msg: right_msg,
                    up_cmd: up_cmd,
                    up_msg: up_msg,
                    down_cmd: down_cmd,
                    down_msg: down_msg
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        window.voice_commands_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                        }, 1000);
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        }, 1000);
                    }
                }
            });
        }
    }

    window.save_room = function() {
        var complete = true;
        var name = $('#name').val();
        var yaw_pitch = $('#yaw_pitch').val();
        var northOffset = $('#northOffset').val();
        var panorama_image = $('#panorama_image').attr('src');
        var allow_pitch = $('#allow_pitch').is(':checked');
        var visible_list = $('#visible_list').is(':checked');
        var annotation_title = $('#annotation_title').val();
        var annotation_description = $('#annotation_description').val();
        var min_pitch = $('#min_pitch').val();
        var max_pitch = $('#max_pitch').val();
        if(name=='') {
            complete = false;
            $('#name').addClass("error-highlight");
        } else {
            $('#name').removeClass("error-highlight");
        }
        if(complete) {
            $('#save_btn .icon i').removeClass('far fa-circle').addClass('fas fa-circle-notch fa-spin');
            $('#save_btn').addClass("disabled");
            $.ajax({
                url: "ajax/save_room.php",
                type: "POST",
                data: {
                    id_room: id_room,
                    name: name,
                    yaw_pitch: yaw_pitch,
                    northOffset: northOffset,
                    change_image: change_image,
                    change_video: change_video,
                    panorama_image: panorama_image,
                    panorama_video: panorama_video,
                    allow_pitch: allow_pitch,
                    visible_list: visible_list,
                    min_pitch: min_pitch,
                    max_pitch: max_pitch,
                    song: window.song,
                    annotation_title: annotation_title,
                    annotation_description: annotation_description
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=="ok") {
                        window.room_need_save = false;
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-check');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-check').addClass('far fa-circle');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    } else {
                        $('#save_btn .icon i').removeClass('fas fa-circle-notch fa-spin').addClass('fas fa-times');
                        $('#save_btn').removeClass('btn-success').addClass('btn-danger');
                        setTimeout(function () {
                            $('#save_btn .icon i').removeClass('fas fa-times').addClass('far fa-circle');
                            $('#save_btn').removeClass('btn-danger').addClass('btn-success');
                            $('#save_btn').removeClass("disabled");
                        },1000);
                    }
                }
            });
        }
    }

    window.save_marker_pos = function(id_marker,yaw,pitch,rotateX,rotateZ,size_scale) {
        $.ajax({
            url: "ajax/save_marker_pos.php",
            type: "POST",
            data: {
                id: id_marker,
                yaw: yaw,
                pitch: pitch,
                rotateX: rotateX,
                rotateZ: rotateZ,
                size_scale: size_scale
            },
            async: true
        });
    }

    window.save_marker_style = function(id_marker,show_room,color,background,icon,id_icon_library) {
        $.ajax({
            url: "ajax/save_marker_style.php",
            type: "POST",
            data: {
                id: id_marker,
                show_room: show_room,
                color: color,
                background: background,
                icon: icon,
                id_icon_library: id_icon_library
            },
            async: true
        });
    }

    window.save_poi_pos = function(id_poi,yaw,pitch,size_scale) {
        $.ajax({
            url: "ajax/save_poi_pos.php",
            type: "POST",
            data: {
                id: id_poi,
                yaw: yaw,
                pitch: pitch,
                size_scale: size_scale
            },
            async: true
        });
    }

    window.save_poi_style = function(id_poi,color,background,icon,style,id_icon_library,label) {
        $.ajax({
            url: "ajax/save_poi_style.php",
            type: "POST",
            data: {
                id: id_poi,
                color: color,
                background: background,
                icon: icon,
                label: label,
                style: style,
                id_icon_library: id_icon_library
            },
            async: true
        });
    }

    window.save_poi_edit = function(id_poi,type,content,title,description) {
        $.ajax({
            url: "ajax/save_poi_edit.php",
            type: "POST",
            data: {
                id: id_poi,
                type: type,
                content: content,
                title: title,
                description: description
            },
            async: true
        });
    }

    window.save_marker_edit = function(id_marker,id_room_target) {
        $.ajax({
            url: "ajax/save_marker_edit.php",
            type: "POST",
            data: {
                id: id_marker,
                id_room_target: id_room_target
            },
            async: true
        });
    }

    window.new_marker = function(id_room,image) {
        var id_room_target = $('#room_target_add option:selected').attr('id');
        $('#modal_add_marker button').prop("disabled",true);
        try {
            var yaw = parseInt(window.viewer.getYaw());
            var pitch = parseInt(window.viewer.getPitch());
        } catch (e) {
            var yaw = 0;
            var pitch = 0;
        }
        $.ajax({
            url: "ajax/add_marker.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_room: id_room,
                id_room_target: id_room_target,
                yaw: yaw,
                pitch: pitch
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=="ok") {
                    $('#modal_add_marker').modal("hide");
                    $('#modal_add_marker button').prop("disabled", false);
                    var count_marker = parseInt($('#count_marker_'+id_room).html());
                    count_marker = count_marker + 1;
                    $('#count_marker_'+id_room).html(count_marker);
                    select_room_marker(id_room,image,rsp.id);
                } else {
                    $('#modal_add_marker button').prop("disabled", false);
                }
            }
        });
    }

    window.new_poi = function(id_room,image) {
        $('#modal_add_poi button').prop("disabled",true);
        try {
            var yaw = parseInt(window.viewer.getYaw());
            var pitch = parseInt(window.viewer.getPitch());
        } catch (e) {
            var yaw = 0;
            var pitch = 0;
        }
        var type = $('#poi_type_add option:selected').attr('id');
        $.ajax({
            url: "ajax/add_poi.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_room: id_room,
                yaw: yaw,
                pitch: pitch,
                type: type
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=="ok") {
                    $('#modal_add_poi').modal("hide");
                    $('#modal_add_poi button').prop("disabled", false);
                    var count_poi = parseInt($('#count_poi_'+id_room).html());
                    count_poi = count_poi + 1;
                    $('#count_poi_'+id_room).html(count_poi);
                    select_room_poi(id_room,image,rsp.id);
                } else {
                    $('#modal_add_poi button').prop("disabled", false);
                }
            }
        });
    }

    window.modal_delete_map = function(id) {
        $('#btn_delete_map').attr('onclick','delete_map('+id+');');
        $('#modal_delete_map').modal("show");
    }

    window.modal_delete_virtualtour = function(id) {
        $('#btn_delete_virtualtour').attr('onclick','delete_virtualtour('+id+');');
        $('#modal_delete_virtualtour').modal("show");
    }

    window.modal_delete_room = function(id) {
        $('#btn_delete_room').attr('onclick','delete_room('+id+');');
        $('#modal_delete_room').modal("show");
    }

    window.modal_delete_marker = function(id,id_room,image) {
        $('#btn_delete_marker').attr('onclick','delete_marker('+id+','+id_room+',\''+image+'\')');
        $('#modal_delete_marker').modal("show");
    }

    window.modal_delete_poi = function(id,id_room,image) {
        $('#btn_delete_poi').attr('onclick','delete_poi('+id+','+id_room+',\''+image+'\')');
        $('#modal_edit_poi').modal('hide');
        $('#modal_delete_poi').modal("show");
    }

    window.modal_delete_map_point = function(id) {
        $('#btn_delete_map_point').attr('onclick','delete_map_point('+id+');');
        $('#modal_delete_map_point').modal("show");
    }

    window.delete_virtualtour = function (id) {
        $('#modal_delete_virtualtour button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_virtual_tour.php",
            type: "POST",
            data: {
                id_user: window.id_user,
                id_virtualtour: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_virtualtour button').removeClass("disabled");
                    $('#modal_delete_virtualtour').modal("hide");
                    location.reload();
                } else {
                    $('#modal_delete_virtualtour button').removeClass("disabled");
                }
            }
        });
    };

    window.delete_room = function (id) {
        $('#modal_delete_room button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_room.php",
            type: "POST",
            data: {
                id_user: window.id_user,
                id_room: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_room button').removeClass("disabled");
                    $('#modal_delete_room').modal("hide");
                    location.reload();
                } else {
                    $('#modal_delete_room button').removeClass("disabled");
                }
            }
        });
    };

    window.delete_marker = function (id,id_room,image) {
        $('#modal_delete_marker button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_marker.php",
            type: "POST",
            data: {
                id_marker: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_marker button').removeClass("disabled");
                    $('#modal_delete_marker').modal("hide");
                    var count_marker = parseInt($('#count_marker_'+id_room).html());
                    count_marker = count_marker - 1;
                    $('#count_marker_'+id_room).html(count_marker);
                    select_room_marker(id_room,image,null);
                } else {
                    $('#modal_delete_marker button').removeClass("disabled");
                }
            }
        });
    };

    window.delete_poi = function (id,id_room,image) {
        $('#modal_delete_poi button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_poi.php",
            type: "POST",
            data: {
                id_poi: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_poi button').removeClass("disabled");
                    $('#modal_delete_poi').modal("hide");
                    var count_poi = parseInt($('#count_poi_'+id_room).html());
                    count_poi = count_poi - 1;
                    $('#count_poi_'+id_room).html(count_poi);
                    select_room_poi(id_room,image,null);
                } else {
                    $('#modal_delete_poi button').removeClass("disabled");
                }
            }
        });
    };

    window.delete_map_point = function (id) {
        $('#modal_delete_map_point button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_map_point.php",
            type: "POST",
            data: {
                id_room: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_map_point button').removeClass("disabled");
                    $('#modal_delete_map_point').modal("hide");
                    location.reload();
                } else {
                    $('#modal_delete_map_point button').removeClass("disabled");
                }
            }
        });
    };

    window.delete_map = function (id_map) {
        $('#modal_delete_map button').addClass("disabled");
        $.ajax({
            url: "ajax/delete_map.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                id_map: id_map
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#modal_delete_map button').removeClass("disabled");
                    $('#modal_delete_map').modal("hide");
                    location.reload();
                } else {
                    $('#modal_delete_map button').removeClass("disabled");
                }
            }
        });
    };

    window.change_exist_logo = function() {
        var logo = $('#exist_logo option:selected').attr('id');
        if(logo==0) {
            window.logo = '';
            window.vt_need_save = true;
            $('#div_delete_logo').hide();
            $('#div_image_logo').hide();
            $('#div_upload_logo').show();
            $('#div_exist_logo').show();
            $('#div_link_logo').hide();
            $('#div_image_logo img').attr('src','');
        } else {
            window.logo = logo;
            window.vt_need_save = true;
            $('#div_delete_logo').hide();
            $('#div_image_logo').show();
            $('#div_upload_logo').hide();
            $('#div_exist_logo').show();
            $('#div_link_logo').show();
            $('#div_image_logo img').attr('src','../viewer/content/'+logo);
        }
    }

    window.change_exist_introd = function() {
        var introd = $('#exist_introd option:selected').attr('id');
        if(introd==0) {
            window.intro_desktop = '';
            window.vt_need_save = true;
            $('#div_delete_introd').hide();
            $('#div_image_introd').hide();
            $('#div_upload_introd').show();
            $('#div_exist_introd').show();
            $('#div_image_introd img').attr('src','');
        } else {
            window.intro_desktop = introd;
            window.vt_need_save = true;
            $('#div_delete_introd').hide();
            $('#div_image_introd').show();
            $('#div_upload_introd').hide();
            $('#div_exist_introd').show();
            $('#div_image_introd img').attr('src','../viewer/content/'+introd);
        }
    }

    window.change_exist_introm = function() {
        var introm = $('#exist_introm option:selected').attr('id');
        if(introm==0) {
            window.intro_mobile = '';
            window.vt_need_save = true;
            $('#div_delete_introm').hide();
            $('#div_image_introm').hide();
            $('#div_upload_introm').show();
            $('#div_exist_introm').show();
            $('#div_image_introm img').attr('src','');
        } else {
            window.intro_mobile = introm;
            window.vt_need_save = true;
            $('#div_delete_introm').hide();
            $('#div_image_introm').show();
            $('#div_upload_introm').hide();
            $('#div_exist_introm').show();
            $('#div_image_introm img').attr('src','../viewer/content/'+introm);
        }
    }

    window.change_exist_bg = function() {
        var logo = $('#exist_bg option:selected').attr('id');
        if(logo==0) {
            window.background_image = '';
            window.vt_need_save = true;
            $('#div_delete_bg').hide();
            $('#div_image_bg').hide();
            $('#div_upload_bg').show();
            $('#div_exist_bg').show();
            $('#div_image_bg img').attr('src','');
        } else {
            window.background_image = logo;
            window.vt_need_save = true;
            $('#div_delete_bg').hide();
            $('#div_image_bg').show();
            $('#div_upload_bg').hide();
            $('#div_exist_bg').show();
            $('#div_image_bg img').attr('src','../viewer/content/'+logo);
        }
    }

    window.change_exist_nadir_logo = function() {
        var logo = $('#exist_nadir_logo option:selected').attr('id');
        if(logo==0) {
            window.nadir_logo = '';
            window.vt_need_save = true;
            $('#div_delete_nadir_logo').hide();
            $('#div_image_nadir_logo').hide();
            $('#div_upload_nadir_logo').show();
            $('#div_exist_nadir_logo').show();
            $('#div_size_nadir_logo').hide();
            $('#div_image_nadir_logo img').attr('src','');
        } else {
            window.nadir_logo = logo;
            window.vt_need_save = true;
            $('#div_delete_nadir_logo').hide();
            $('#div_image_nadir_logo').show();
            $('#div_upload_nadir_logo').hide();
            $('#div_exist_nadir_logo').show();
            $('#div_size_nadir_logo').show();
            $('#div_image_nadir_logo img').attr('src','../viewer/content/'+logo);
        }
    }

    window.change_exist_song = function() {
        var song = $('#exist_song option:selected').attr('id');
        if(song==0) {
            window.song = '';
            window.vt_need_save = true;
            $('#div_delete_song').hide();
            $('#div_player_song').hide();
            $('#div_upload_song').show();
            $('#div_exist_song').show();
            $('#div_autoplay_song').hide();
            $('#div_player_song audio').attr('src','');
        } else {
            window.song = song;
            window.vt_need_save = true;
            $('#div_delete_song').hide();
            $('#div_player_song').show();
            $('#div_upload_song').hide();
            $('#div_exist_song').show();
            $('#div_autoplay_song').show();
            $('#div_player_song audio').attr('src','../viewer/content/'+song);
        }
    }

    window.delete_song = function() {
        window.song = '';
        window.vt_need_save = true;
        $('#div_delete_song').hide();
        $('#div_player_song').hide();
        $('#div_upload_song').show();
        $('#div_exist_song').show();
        $('#div_autoplay_song').hide();
        $('#div_player_song audio').attr('src','');
    }

    window.delete_room_song = function() {
        window.song = '';
        window.room_need_save = true;
        $('#div_delete_song').hide();
        $('#div_player_song').hide();
        $('#div_upload_song').show();
        $('#div_player_song audio').attr('src','');
    }

    window.delete_logo = function () {
        window.logo = '';
        window.vt_need_save = true;
        $('#div_delete_logo').hide();
        $('#div_image_logo').hide();
        $('#div_upload_logo').show();
        $('#div_exist_logo').show();
        $('#div_link_logo').hide();
        $('#div_image_logo img').attr('src','');
    }

    window.delete_introd = function () {
        window.intro_desktop = '';
        window.vt_need_save = true;
        $('#div_delete_introd').hide();
        $('#div_image_introd').hide();
        $('#div_upload_introd').show();
        $('#div_exist_introd').show();
        $('#div_image_introd img').attr('src','');
    }

    window.delete_introm = function () {
        window.intro_mobile = '';
        window.vt_need_save = true;
        $('#div_delete_introm').hide();
        $('#div_image_introm').hide();
        $('#div_upload_introm').show();
        $('#div_exist_introm').show();
        $('#div_image_introm img').attr('src','');
    }

    window.delete_bg = function () {
        window.background_image = '';
        window.vt_need_save = true;
        $('#div_delete_bg').hide();
        $('#div_image_bg').hide();
        $('#div_upload_bg').show();
        $('#div_exist_bg').show();
        $('#div_image_bg img').attr('src','');
    }

    window.delete_b_bg = function () {
        window.b_background_image = '';
        window.settings_need_save = true;
        $('#div_delete_bg').hide();
        $('#div_image_bg').hide();
        $('#div_upload_bg').show();
        $('#div_image_bg img').attr('src','');
    }

    window.delete_b_logo = function () {
        window.b_logo_image = '';
        window.settings_need_save = true;
        $('#div_delete_logo').hide();
        $('#div_image_logo').hide();
        $('#div_upload_logo').show();
        $('#div_image_logo img').attr('src','');
    }

    window.delete_nadir_logo = function () {
        window.nadir_logo = '';
        window.vt_need_save = true;
        $('#div_delete_nadir_logo').hide();
        $('#div_image_nadir_logo').hide();
        $('#div_size_nadir_logo').hide();
        $('#div_upload_nadir_logo').show();
        $('#div_exist_nadir_logo').show();
        $('#div_image_nadir_logo img').attr('src','');
    }

    window.get_gallery_images = function(id_virtualtour) {
        $.ajax({
            url: "ajax/get_gallery_images.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.length>0) {
                    parse_gallery_images(rsp);
                } else {
                    $('#list_images').html("<p>No images in this gallery.</p>")
                }
            }
        });
    }

    window.get_poi_gallery_images = function(id_poi) {
        $.ajax({
            url: "ajax/get_poi_gallery_images.php",
            type: "POST",
            data: {
                id_poi: id_poi
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.length>0) {
                    parse_poi_gallery_images(rsp);
                } else {
                    $('#list_images').html("<p>No images in this gallery.</p>")
                }
            }
        });
    }

    window.get_icon_images = function(id_virtualtour) {
        $.ajax({
            url: "ajax/get_icon_images.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.length>0) {
                    parse_icon_images(rsp);
                } else {
                    $('#list_images').html("<p>No icons in that library.</p>")
                }
            }
        });
    }

    function parse_gallery_images(images_array) {
        var html = '';
        jQuery.each(images_array, function(index, image) {
            var id = image.id;
            var image = image.image;
            html += '<div data-id="'+id+'" style="position: relative" class="image_gallery float-left mb-2 ml-1 mr-1"><div class="remove_image_gallery"><i onclick="remove_image_gallery('+id+');" style="font-size: 24px;color: white" class="fas fa-trash-alt"></i></div><img style="height: 100px;" src="../viewer/gallery/thumb/'+image+'" /></div>';
        });
        $('#list_images').html(html).promise().done(function () {
            var el = document.getElementById('list_images');
            Sortable.create(el,{
                onEnd: function (evt) {
                    var array_images_priority = [];
                   $('#list_images .image_gallery').each(function () {
                        var id = $(this).attr('data-id');
                       array_images_priority.push(id);
                   });
                    change_image_gallery_order(array_images_priority);
                },
            });
        });
    }

    function parse_poi_gallery_images(images_array) {
        var html = '';
        jQuery.each(images_array, function(index, image) {
            var id = image.id;
            var image = image.image;
            html += '<div data-id="'+id+'" style="position: relative" class="image_gallery float-left mb-2 ml-1 mr-1"><div class="remove_image_gallery"><i onclick="remove_image_poi_gallery('+id+');" style="font-size: 24px;color: white" class="fas fa-trash-alt"></i></div><img style="height: 100px;" src="../viewer/gallery/thumb/'+image+'" /></div>';
        });
        $('#list_images').html(html).promise().done(function () {
            var el = document.getElementById('list_images');
            Sortable.create(el,{
                onEnd: function (evt) {
                    var array_images_priority = [];
                    $('#list_images .image_gallery').each(function () {
                        var id = $(this).attr('data-id');
                        array_images_priority.push(id);
                    });
                    change_poi_image_gallery_order(array_images_priority);
                },
            });
        });
    }

    function parse_icon_images(images_array) {
        var html = '';
        jQuery.each(images_array, function(index, image) {
            var id = image.id;
            var image = image.image;
            html += '<div data-id="'+id+'" style="position: relative" class="image_icon float-left mb-2 ml-1 mr-1"><div class="remove_image_icon"><i onclick="remove_image_icon('+id+');" style="font-size: 24px;color: white" class="fas fa-trash-alt"></i></div><img style="height: 100px;" src="../viewer/icons/'+image+'" /></div>';
        });
        $('#list_images').html(html).promise().done(function () {

        });
    }

    function change_rooms_order(array_rooms_priority) {
        var array_rooms_priority = JSON.stringify(array_rooms_priority);
        $.ajax({
            url: "ajax/change_rooms_order.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                array_rooms_priority: array_rooms_priority
            },
            async: false,
            success: function (json) {

            }
        });
    }

    function change_image_gallery_order(array_images_priority) {
        var array_images_priority = JSON.stringify(array_images_priority);
        $.ajax({
            url: "ajax/change_image_gallery_order.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                array_images_priority: array_images_priority
            },
            async: false,
            success: function (json) {

            }
        });
    }

    function change_poi_image_gallery_order(array_images_priority) {
        var array_images_priority = JSON.stringify(array_images_priority);
        $.ajax({
            url: "ajax/change_poi_image_gallery_order.php",
            type: "POST",
            data: {
                id_poi: window.id_poi,
                array_images_priority: array_images_priority
            },
            async: false,
            success: function (json) {

            }
        });
    }

    window.add_image_to_gallery = function(id_virtualtour,image) {
        $.ajax({
            url: "ajax/add_image_to_gallery.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                image: image
            },
            async: true,
            success: function (json) {
                get_gallery_images(id_virtualtour);
            }
        });
    }

    window.add_image_to_poi_gallery = function(id_poi,image) {
        $.ajax({
            url: "ajax/add_image_to_poi_gallery.php",
            type: "POST",
            data: {
                id_poi: id_poi,
                image: image
            },
            async: true,
            success: function (json) {
                get_poi_gallery_images(id_poi);
            }
        });
    }

    window.add_image_to_icon = function(id_virtualtour,image) {
        $.ajax({
            url: "ajax/add_image_to_icon.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtualtour,
                image: image
            },
            async: true,
            success: function (json) {
                get_icon_images(id_virtualtour);
            }
        });
    }

    window.remove_image_gallery = function(id) {
        var retVal = confirm("Are you sure you want to delete image?");
        if( retVal == true ) {
            $.ajax({
                url: "ajax/delete_image_to_gallery.php",
                type: "POST",
                data: {
                    id: id
                },
                async: false,
                success: function (json) {
                    get_gallery_images(window.id_virtualtour);
                }
            });
        } else {
            return false;
        }
    }

    window.remove_image_poi_gallery = function(id) {
        var retVal = confirm("Are you sure you want to delete image?");
        if( retVal == true ) {
            $.ajax({
                url: "ajax/delete_image_to_poi_gallery.php",
                type: "POST",
                data: {
                    id: id
                },
                async: false,
                success: function (json) {
                    get_poi_gallery_images(window.id_poi);
                }
            });
        } else {
            return false;
        }
    }

    window.remove_image_icon = function(id) {
        var retVal = confirm("Are you sure you want to delete icon?");
        if( retVal == true ) {
            $.ajax({
                url: "ajax/delete_image_to_icon.php",
                type: "POST",
                data: {
                    id: id
                },
                async: false,
                success: function (json) {
                    get_icon_images(window.id_virtualtour);
                }
            });
        } else {
            return false;
        }
    }

    window.check_plan = function(id_user,object) {
        $.ajax({
            url: "ajax/check_plan.php",
            type: "POST",
            data: {
                id_user: id_user,
                object: object
            },
            async: false,
            success: function (json) {
                var rsp = JSON.parse(json);
                window.can_create = rsp.can_create;
            }
        });
    }

    window.set_friendly_url = function() {
        var friendly_url = $('#friendly_url').val();
        $('#btn_friendly_url').addClass('disabled');
        $.ajax({
            url: "ajax/set_friendly_url.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                friendly_url: friendly_url
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#friendly_url').removeClass('error-highlight');
                    location.reload();
                } else {
                    $('#btn_friendly_url').removeClass('disabled');
                    $('#friendly_url').addClass('error-highlight');
                }
            }
        });
    }

    window.set_status_vt = function(status) {
        $('#btn_status').addClass('disabled');
        $.ajax({
            url: "ajax/set_status_vt.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                status: status
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    location.reload();
                } else {
                    $('#btn_status').removeClass('disabled');
                }
            }
        });
    }

    window.set_password_vt = function() {
        var password = $('#vt_password').val();
        if(password!='') {
            $('#btn_protect').addClass('disabled');
            $.ajax({
                url: "ajax/set_password_vt.php",
                type: "POST",
                data: {
                    id_virtualtour: window.id_virtualtour,
                    password: password
                },
                async: true,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if (rsp.status == "ok") {
                        location.reload();
                    } else {
                        $('#btn_protect').removeClass('disabled');
                    }
                }
            });
        }
    }

    window.remove_password_vt = function() {
        $('#btn_unprotect').addClass('disabled');
        $.ajax({
            url: "ajax/remove_password_vt.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    location.reload();
                } else {
                    $('#btn_unprotect').removeClass('disabled');
                }
            }
        });
    }

    window.check_license = function() {
        $('#btn_check_license').addClass("disabled");
        var purchase_code = $('#purchase_code').val();
        $.ajax({
            url: "https://simpledemo.it/check_license_svt.php",
            type: "POST",
            data: {
                server_name: window.server_name,
                server_ip: window.server_ip,
                purchase_code: purchase_code
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                switch (rsp.status) {
                    case 'ok':
                        $('#license_status').html("<i style='color: green;' class=\"fas fa-circle\"></i> "+rsp.msg);
                        var license = rsp.license;
                        break;
                    case 'error':
                        $('#license_status').html("<i style='color: red;' class=\"fas fa-circle\"></i> "+rsp.msg);
                        var license = '';
                        break;
                }
                $('#btn_check_license').removeClass("disabled");
                $.ajax({
                    url: "ajax/save_license.php",
                    type: "POST",
                    data: {
                        purchase_code: purchase_code,
                        license: license
                    },
                    async: true
                });
            }
        });
    }

    window.set_session_vt = function(id) {
        $.ajax({
            url: "ajax/set_session_vt.php",
            type: "POST",
            data: {
                id_virtualtour: id
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    location.reload();
                }
            }
        });
    }

    $(document).ready(function(){$("#users_table").length&&!$("#vlfc").length&&(location.href="index.php?p=license");$("#plans_table").length&&!$("#vlfc").length&&(location.href="index.php?p=license");$("#plan").length&&!$("#vlfc").length&&(location.href="index.php?p=license")});

    window.adjust_ratio_hfov = function (id_panorama,viewer,hfov,min_hfov,max_hfov) {
        var c_w = parseFloat($('#'+id_panorama).css('width').replace('px',''));
        var c_h = parseFloat($('#'+id_panorama).css('height').replace('px',''));
        var ratio_panorama = c_w / c_h;
        var ratio_hfov = 1.7771428571428571 / ratio_panorama;
        var min_hfov_t = min_hfov / ratio_hfov;
        var max_hfov_t = max_hfov / ratio_hfov;
        var hfov_t = hfov / ratio_hfov;
        viewer.setHfovBounds([min_hfov_t,max_hfov_t]);
        viewer.setHfov(hfov_t,false);
    }

    window.get_statistics = function (elem) {
        $.ajax({
            url: "ajax/get_statistics.php",
            type: "POST",
            data: {
                id_virtualtour: window.id_virtualtour,
                elem: elem
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                switch (elem) {
                    case 'chart_visitor_vt':
                        new Chart(document.getElementById("chart_visitor_vt"), {
                            type: 'line',
                            data: {
                                labels: rsp.labels,
                                datasets: [{
                                    data: rsp.data,
                                    backgroundColor: "#4e73df",
                                    borderColor: "#4e73df",
                                    fill: false
                                }]
                            },
                            options: {
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                        break;
                    case 'chart_rooms_access':
                        new Chart(document.getElementById("chart_rooms_access"), {
                            type: 'horizontalBar',
                            data: {
                                labels: rsp.labels,
                                datasets: [{
                                    data: rsp.data,
                                    backgroundColor: "#4e73df",
                                    borderColor: "#4e73df",
                                    fill: false
                                }]
                            },
                            options: {
                                legend: {
                                    display: false
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                        break;
                    case 'chart_rooms_time':
                        new Chart(document.getElementById("chart_rooms_time"), {
                            type: 'horizontalBar',
                            data: {
                                labels: rsp.labels,
                                datasets: [{
                                    data: rsp.data,
                                    backgroundColor: "#4e73df",
                                    borderColor: "#4e73df",
                                    fill: false
                                }]
                            },
                            options: {
                                legend: {
                                    display: false
                                },
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                        break;
                    case 'chart_poi_views':
                        var total_access = rsp.total_poi;
                        var html_pois = '';
                        jQuery.each(rsp.pois, function(index, poi) {
                            var room = poi.room;
                            var type = poi.type;
                            var content = poi.content;
                            content = $($.parseHTML(content)).text();
                            switch (type) {
                                case 'link':
                                    type = "link (embed)";
                                    break;
                                case 'link_ext':
                                    type = "link (external)";
                                    break;
                                case 'html':
                                    type = "text";
                                    if(content.length>100) {
                                        content = content.substring(0, 100)+"...";
                                    }
                                    break;
                                case 'html_sc':
                                    type = "html";
                                    if(content.length>100) {
                                        content = content.substring(0, 100)+"...";
                                    }
                                    break;
                                case 'form':
                                    content = JSON.parse(content)[0].title;
                                    break;
                            }
                            type = type.substr(0,1).toUpperCase()+type.substr(1);
                            var name_poi = room+" - <b>"+type+"</b> - <i>"+content+"</i>";
                            var count = poi.access_count;
                            var perc = count / total_access * 100;
                            perc = Math.round(perc);
                            html_pois += '<h4 style="margin-bottom:1px;" class="small font-weight-bold">'+name_poi+' <span class="float-right"><b>'+count+'</b>/'+total_access+'</span></h4>\n' +
                                '                <div class="progress mb-2">\n' +
                                '                    <div class="progress-bar bg-primary" role="progressbar" style="width: '+perc+'%" aria-valuenow="'+perc+'" aria-valuemin="0" aria-valuemax="100"></div>\n' +
                                '                </div>';
                        });
                        $('#chart_poi_views').html(html_pois);
                        break;
                }
            }
        });
    }

    window.save_landing = function(id,html) {
        $.ajax({
            url: "../../ajax/save_landing.php",
            type: "POST",
            data: {
                id_virtualtour: id,
                html: html
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#landing_saving').hide();
                    $('#landing_editor').show();
                }
            }
        });
    }

    window.save_info = function(id,html) {
        $.ajax({
            url: "../../ajax/save_info.php",
            type: "POST",
            data: {
                id_virtualtour: id,
                html: html
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == "ok") {
                    $('#info_saving').hide();
                    $('#info_editor').show();
                }
            }
        });
    }

})(jQuery); // End of use strict