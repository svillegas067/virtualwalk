<?php
session_start();
require_once("functions.php");
$id_room = $_GET['id'];
$room = get_room($id_room,$_SESSION['id_user']);
$virtual_tour = get_virtual_tour($room['id_virtualtour'],$_SESSION['id_user']);
?>

<?php if(!$room): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
<?php die(); endif; ?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-md-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-2 text-gray-800"><i class="fas fa-fw fa-route text-gray-700"></i> EDIT ROOM</span></h1>
    <a id="save_btn" href="#" onclick="save_room();return false;" class="btn btn-success btn-icon-split mb-2 <?php echo ($demo) ? 'disabled':''; ?>">
    <span class="icon text-white-50">
      <i class="far fa-circle"></i>
    </span>
        <span class="text">SAVE</span>
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapsePI" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapsePI">
                <h6 class="m-0 font-weight-bold text-primary">Panorama <?php echo ucfirst($room['type']); ?> <i style="font-size: 12px">(click to view / change)</i></h6>
            </a>
            <div class="collapse" id="collapsePI">
                <div class="card-body">
                    <img id="panorama_image" style="width: 100%" src="../viewer/panoramas/<?php echo $room['panorama_image']; ?>">
                    <form class="mt-4" id="frm" action="<?php echo ($room['type']=='video') ? 'ajax/upload_room_video.php' : 'ajax/upload_room_image.php'; ?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="file" class="form-control" id="txtFile" name="txtFile" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload" value="Upload <?php echo ucfirst($room['type']); ?>" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="preview text-center">
                                    <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                        <div class="progress-bar" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                            0%
                                        </div>
                                    </div>
                                    <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Settings</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter room name" value="<?php echo $room['name']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="allow_pitch">Allow Pitch <i title="enables vertical inclination" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="allow_pitch" <?php echo ($room['allow_pitch'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="visible_list">Visible List <i title="show room on list slider" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="visible_list" <?php echo ($room['visible_list'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="min_pitch">Lower Pitch ° <i title="maximum vertical inclination in degrees down (min 0 - max 90)" class="help_t fas fa-question-circle"></i></label><br>
                            <input <?php echo ($room['allow_pitch'])?'':'disabled'; ?> min="0" max="90" type="number" class="form-control" id="min_pitch" value="<?php echo $room['min_pitch']*-1; ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="max_pitch">Upper Pitch ° <i title="maximum vertical inclination in degrees up (min 0 - max 90)" class="help_t fas fa-question-circle"></i></label><br>
                            <input <?php echo ($room['allow_pitch'])?'':'disabled'; ?> min="0" max="90" type="number" class="form-control" id="max_pitch" value="<?php echo $room['max_pitch']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="annotation_title">Annotation Title</label>
                            <input type="text" class="form-control" id="annotation_title" placeholder="Enter title" value="<?php echo $room['annotation_title']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="annotation_description">Annotation Description</label>
                            <input type="text" class="form-control" id="annotation_description" placeholder="Enter description" value="<?php echo $room['annotation_description']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="display: none" id="div_player_song" class="col-md-6">
                        <label>Audio</label><br>
                        <audio style="width: 100%" controls>
                            <source src="../viewer/content/<?php echo $room['song']; ?>" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                    <div style="display: none" id="div_delete_song" class="col-md-6">
                        <label style="color: white">.</label><br>
                        <button onclick="delete_room_song();return false;" id="btn_delete_song" class="btn btn-block btn-danger">REMOVE AUDIO</button>
                    </div>
                    <div style="display: none" id="div_upload_song" class="col-md-12">
                        <label>Audio</label>
                        <form id="frm_s" action="ajax/upload_song.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_s" name="txtFile" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_s" value="Upload Audio (MP3)" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="preview text-center">
                                        <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_s" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_s"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Preview</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;max-width:622px;height: 350px;margin: 0 auto;" id="panorama"></div>
                <div id="panorama_video"></div>
                <div class="mt-2 text-center " style="width: 100%;">
                    Initial Position <b><span style="color: #36b9cc" id="yaw_pitch_debug"><?php echo $room['yaw'].",".$room['pitch']; ?></span></b> -
                    Compass North <b><span style="color: #f6c23e" id="northOffset_debug">--</span></b><br>
                    <i>hold click and move the mouse to change the position</i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Positions</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="color: #36b9cc" for="yaw_pitch">Initial Position <i title="initial position when you enter in this room" class="help_t fas fa-question-circle"></i></label>
                            <input readonly style="color: #36b9cc" type="text" class="form-control bg-white" id="yaw_pitch" value="<?php echo $room['yaw'].",".$room['pitch']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <a onclick="set_yaw_pitch();return false;" href="#" class="btn btn-block btn-info">
                            <span class="text">Set Initial Position</span>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="color: #f6c23e" for="northOffset">Compass North <i title="indication of the north position of this room" class="help_t fas fa-question-circle"></i></label>
                            <input readonly style="color: #f6c23e" type="number" class="form-control bg-white" id="northOffset" value="<?php echo $room['northOffset']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a onclick="set_northOffset();return false;" href="#" class="btn btn-block btn-warning">
                            <span class="text">Set North</span>
                        </a>
                    </div>
                    <?php if(!empty($room['map'])) : ?>
                    <div class="col-md-12 mt-4">
                        <div class="map">
                            <img style="width: 100%" class='map_image' draggable='false' src='../viewer/maps/<?php echo $room['map']; ?>'>
                            <div data-scale='1.0' style='display:none;transform: rotate(0deg) scale(1.0);top:<?php echo $room['map_top']; ?>px;left:<?php echo $room['map_left']; ?>px;' class='pointer_view pointer_<?php echo $room['id']; ?>'>
                                <div class="view_direction__arrow"></div>
                                <div class="view_direction__center"></div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_room = <?php echo $id_room; ?>;
        var hfov = '<?php echo $virtual_tour['hfov']; ?>';
        var min_hfov = '<?php echo $virtual_tour['min_hfov']; ?>';
        var max_hfov = '<?php echo $virtual_tour['max_hfov']; ?>';
        var yaw = '<?php echo $room['yaw']; ?>';
        var pitch = '<?php echo $room['pitch']; ?>';
        var northOffset = '<?php echo $room['northOffset']; ?>';
        var allow_pitch = '<?php echo $room['allow_pitch']; ?>';
        var min_pitch = '<?php echo $room['min_pitch']; ?>';
        var max_pitch = '<?php echo $room['max_pitch']; ?>';
        var room_type = '<?php echo $room['type']; ?>';
        var viewer,viewer_video;
        var ratio_hfov = 1;
        var viewer_initialized = false;
        var video = document.createElement("video");
        var canvas = document.createElement("canvas");
        var video_preview;
        var point_size = '<?php echo $room['point_size']; ?>';
        var map_top = '<?php echo $room['map_top']; ?>';
        var map_left = '<?php echo $room['map_left']; ?>';
        window.change_image = 0;
        window.change_video = 0;
        window.panorama_video = "../viewer/videos/<?php echo $room['panorama_video']; ?>";
        window.song = '<?php echo $room['song']; ?>';
        window.room_need_save = false;
        $(document).ready(function () {
             $('.help_t').tooltip();
            if(song=='') {
                $('#div_delete_song').hide();
                $('#div_player_song').hide();
                $('#div_upload_song').show();
            } else {
                $('#div_delete_song').show();
                $('#div_player_song').show();
                $('#div_upload_song').hide();
            }
            load_viewer(room_type,"../viewer/panoramas/<?php echo $room['panorama_image']; ?>","../viewer/videos/<?php echo $room['panorama_video']; ?>",yaw,pitch);
        });

        $('#allow_pitch').click(function(){
            window.room_need_save = true;
            if($(this).is(':checked')){
                $('#min_pitch').prop('disabled',false);
                $('#max_pitch').prop('disabled',false);
                min_pitch = (parseInt($('#min_pitch').val())*-1)-34;
                max_pitch = parseInt($('#max_pitch').val())+34;
                if(room_type=='video') {
                    viewer_video.pnlmViewer.setPitchBounds([min_pitch,max_pitch]);
                } else {
                    viewer.setPitchBounds([min_pitch,max_pitch]);
                }
            } else {
                $('#min_pitch').prop('disabled',true);
                $('#max_pitch').prop('disabled',true);
                if(room_type=='video') {
                    viewer_video.pnlmViewer.setPitchBounds([0,0]);
                    viewer_video.pnlmViewer.setPitch(0);
                } else {
                    viewer.setPitchBounds([0,0]);
                    viewer.setPitch(0);
                }
            }
        });

        $('#min_pitch, #max_pitch').on('change',function(){
            window.room_need_save = true;
            min_pitch = (parseInt($('#min_pitch').val())*-1)-34;
            max_pitch = parseInt($('#max_pitch').val())+34;
            if(room_type=='video') {
                viewer_video.pnlmViewer.setPitchBounds([min_pitch,max_pitch]);
            } else {
                viewer.setPitchBounds([min_pitch,max_pitch]);
            }
        });

        function load_viewer(room_type,panorama_image,panorama_video,yaw,pitch) {
            if(allow_pitch=='1') {
                min_pitch = (parseInt($('#min_pitch').val())*-1)-34;
                max_pitch = parseInt($('#max_pitch').val())+34;
            } else {
                min_pitch = 0;
                max_pitch = 0;
                pitch = 0;
            }
            if(room_type=='video') {
                $('#panorama').hide();
                $('#panorama_video').append('<video id="video_viewer" class="video-js vjs-default-skin vjs-big-play-centered" style="width: 100%;max-width:622px;height: 350px;margin: 0 auto;" muted preload="none" crossorigin="anonymous"><source src="'+panorama_video+'" type="video/mp4"/></video>');
                viewer_video = videojs('video_viewer', {
                    loop: true,
                    autoload: true,
                    plugins: {
                        pannellum: {
                            "id_room": window.id_room,
                            "autoLoad": true,
                            "showFullscreenCtrl": false,
                            "showControls": false,
                            "hfov": parseInt(hfov),
                            "minHfov": parseInt(min_hfov),
                            "maxHfov": parseInt(max_hfov),
                            "yaw": parseInt(yaw),
                            "pitch": parseInt(pitch),
                            "minPitch": min_pitch,
                            "maxPitch" : max_pitch,
                            "compass": true,
                            "northOffset": parseInt(northOffset),
                            "friction": 1
                        }
                    }
                });
                viewer_video.load();
                viewer_video.on('ready', function() {
                    viewer_video.play();
                    viewer_video.pnlmViewer.on('load',function () {
                        viewer_initialized = true;
                        var yaw = parseInt(viewer_video.pnlmViewer.getYaw());
                        if(yaw<0) {
                            var northOffset = Math.abs(yaw);
                        } else {
                            var northOffset =  360 - yaw;
                        }
                        $('#northOffset_debug').html(northOffset);
                        adjust_ratio_hfov('panorama',viewer_video.pnlmViewer,hfov,min_hfov,max_hfov);
                        adjust_point_position();
                    });
                    viewer_video.pnlmViewer.on('mouseup',function () {
                        var yaw = parseInt(viewer_video.pnlmViewer.getYaw());
                        var pitch = parseInt(viewer_video.pnlmViewer.getPitch());
                        if(yaw<0) {
                            var northOffset = Math.abs(yaw);
                        } else {
                            var northOffset =  360 - yaw;
                        }
                        $('#yaw_pitch_debug').html(yaw+','+pitch);
                        $('#northOffset_debug').html(northOffset);
                    });
                });
            } else {
                viewer = pannellum.viewer('panorama', {
                    "id_room": window.id_room,
                    "type": "equirectangular",
                    "panorama": panorama_image,
                    "autoLoad": true,
                    "showFullscreenCtrl": false,
                    "showControls": false,
                    "hfov": parseInt(hfov),
                    "minHfov": parseInt(min_hfov),
                    "maxHfov": parseInt(max_hfov),
                    "yaw": parseInt(yaw),
                    "pitch": parseInt(pitch),
                    "minPitch": min_pitch,
                    "maxPitch" : max_pitch,
                    "compass": true,
                    "northOffset": parseInt(northOffset),
                    "friction": 1
                });
                viewer.on('load', function () {
                    viewer_initialized = true;
                    var yaw = parseInt(viewer.getYaw());
                    if(yaw<0) {
                        var northOffset = Math.abs(yaw);
                    } else {
                        var northOffset =  360 - yaw;
                    }
                    $('#northOffset_debug').html(northOffset);
                    adjust_ratio_hfov('panorama',viewer,hfov,min_hfov,max_hfov);
                    adjust_point_position();
                });
                viewer.on('animatefinished',function () {
                    var yaw = parseInt(viewer.getYaw());
                    var pitch = parseInt(viewer.getPitch());
                    if(yaw<0) {
                        var northOffset = Math.abs(yaw);
                    } else {
                        var northOffset =  360 - yaw;
                    }
                    $('#yaw_pitch_debug').html(yaw+','+pitch);
                    $('#northOffset_debug').html(northOffset);
                });
            }
        }

        window.set_yaw_pitch = function() {
            if(room_type=='video') {
                var yaw = parseInt(viewer_video.pnlmViewer.getYaw());
                var pitch = parseInt(viewer_video.pnlmViewer.getPitch());
            } else {
                var yaw = parseInt(viewer.getYaw());
                var pitch = parseInt(viewer.getPitch());
            }
            $('#yaw_pitch').val(yaw+","+pitch);
            window.room_need_save = true;
        }

        window.set_northOffset = function() {
            if(room_type=='video') {
                var yaw = parseInt(viewer_video.pnlmViewer.getYaw());
            } else {
                var yaw = parseInt(viewer.getYaw());
            }
            if(yaw<0) {
                var northOffset = Math.abs(yaw);
            } else {
                var northOffset =  360 - yaw;
            }
            $('#northOffset').val(northOffset);
            if(room_type=='video') {
                viewer_video.pnlmViewer.setNorthOffset(northOffset);
            } else {
                viewer.setNorthOffset(northOffset);
            }
            window.room_need_save = true;
        }

        window.adjust_point_position = function () {
            $('.pointer_view').show();
            var image_w = $('.map_image').width();
            var image_h = $('.map_image').height();
            var ratio = image_w / image_h;
            var ratio_w = image_w / 300;
            var ratio_h = image_h / ((image_w / ratio_w) / ratio);
            var pos_left = (parseInt(map_left)+parseInt(point_size)/2) * ratio_w;
            var pos_top = (parseInt(map_top)+parseInt(point_size)/2) * ratio_h;
            $('.pointer_'+window.id_room).css('top',pos_top+'px');
            $('.pointer_'+window.id_room).css('left',pos_left+'px');
        }

        $(window).resize(function() {
            if(viewer_initialized) {
                adjust_point_position();
                if(room_type=='video') {
                    adjust_ratio_hfov('video_viewer',viewer_video.pnlmViewer,hfov,min_hfov,max_hfov);
                } else {
                    adjust_ratio_hfov('panorama',viewer,hfov,min_hfov,max_hfov);
                }
            }
        });

        $('body').on('submit','#frm',function(e){
            e.preventDefault();
            $('#error').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        if(room_type=='image') {
                            change_image(evt.target.responseText);
                        } else {
                            change_video(evt.target.responseText);
                        }
                    }
                }
                upadte_progressbar(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error('upload failed');
                upadte_progressbar(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error('upload aborted');
                upadte_progressbar(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar(value){
            $('#progressBar').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error(error){
            $('.progress').hide();
            $('#error').show();
            $('#error').html(error);
        }

        function change_image(path) {
            var yaw_pitch = $('#yaw_pitch').val();
            var tmp = yaw_pitch.split(",");
            var yaw = tmp[0];
            var pitch = tmp[1];
            $('#panorama_image').attr('src',path);
            load_viewer('image',path,"",yaw,pitch);
            $('#collapsePI').collapse('hide');
            window.change_image = 1;
            window.change_video = 0;
            window.room_need_save = true;
        }

        function change_video(path) {
            window.panorama_video = path;
            var yaw_pitch = $('#yaw_pitch').val();
            var tmp = yaw_pitch.split(",");
            var yaw = tmp[0];
            var pitch = tmp[1];
            try {
                viewer_video.pnlmViewer.destroy();
                viewer_video.dispose();
                viewer_video = null;
            } catch (e) {}
            load_viewer('video',"",panorama_video,yaw,pitch);
            $('#panorama_image').attr('src',video_preview);
            $('#collapsePI').collapse('hide');
            window.change_image = 0;
            window.change_video = 1;
            window.room_need_save = true;
        }

        video.addEventListener('loadeddata', function() {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            video.currentTime = 0;
        }, false);

        video.addEventListener('seeked', function() {
            var context = canvas.getContext('2d');
            context.drawImage(video, 0, 0, canvas.width, canvas.height);
            video_preview = canvas.toDataURL("image/jpg",0.8);
        }, false);

        var playSelectedFile = function(event) {
            if(room_type=='video') {
                var file = this.files[0];
                var fileURL = URL.createObjectURL(file);
                video.src = fileURL;
            }
        }

        var input = document.getElementById('txtFile');
        input.addEventListener('change', playSelectedFile, false);

        $('body').on('submit','#frm_s',function(e){
            e.preventDefault();
            $('#error_s').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_s[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_s' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_s(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_s(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.room_need_save = true;
                        window.song = evt.target.responseText;
                        $('#div_delete_song').show();
                        $('#div_player_song').show();
                        $('#div_upload_song').hide();
                        $('#div_player_song audio').attr('src','../viewer/content/'+window.song);
                    }
                }
                upadte_progressbar_s(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_s('upload failed');
                upadte_progressbar_s(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_s('upload aborted');
                upadte_progressbar_s(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_s(value){
            $('#progressBar_s').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_s(error){
            $('.progress').hide();
            $('#error_s').show();
            $('#error_s').html(error);
        }

        $("input").change(function(){
            window.room_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.room_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });

    })(jQuery); // End of use strict
</script>