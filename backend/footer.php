<?php require_once('functions.php');?>
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; <?php echo $settings['name']; ?> <?php echo date('Y'); ?> - Version <?php echo $version; ?></span>
            <?php if(get_user_role($_SESSION['id_user'])=='administrator') : ?>
                <?php if(version_compare($version,$latest_version)==-1) : ?>
                <span><i style="cursor: pointer;" class="fas fa-exclamation-circle"></i></span>
                <a target="_blank" href="https://codecanyon.net/item/simple-virtual-tour/26950622">New version <?php echo $latest_version; ?> available!</a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</footer>