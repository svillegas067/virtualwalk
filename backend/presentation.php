<?php
session_start();
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_GET['id_vt'])) {
        $id_virtualtour_sel = $_GET['id_vt'];
        $name_virtualtour_sel = get_virtual_tour($_GET['id_vt'],$id_user)['name'];
        $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
        $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
    } else {
        if(isset($_SESSION['id_virtualtour_sel'])) {
            $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
            $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
        } else {
            $id_virtualtour_sel = $virtual_tours[0]['id'];
            $name_virtualtour_sel = $virtual_tours[0]['name'];
        }
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}

$id_user = $_SESSION['id_user'];
require_once("functions.php");
$virtual_tour = get_virtual_tour($id_virtualtour_sel,$_SESSION['id_user']);
?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-directions text-gray-700"></i> PRESENTATION</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 mb-4">
        <div id="presentation_list">
            <div class="card mb-4 py-3 border-left-primary">
                <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                    <div class="row">
                        <div class="col-md-8 text-center text-sm-center text-md-left text-lg-left">
                            LOADING PRESENTATION ...
                        </div>
                        <div class="col-md-4 text-center text-sm-center text-md-right text-lg-right">
                            <a href="#" class="btn btn-primary btn-circle">
                                <i class="fas fa-spin fa-spinner"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_presentation_room" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="p_room">Room</label>
                            <select id="p_room" class="form-control">
                                <?php echo get_rooms_option($id_virtualtour_sel); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="p_sleep_r">Wait after change room (ms)</label>
                            <input type="number" class="form-control" id="p_sleep_r" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> data-toggle="modal" data-target="#modal_delete_p_room" id="btn_delete_p_room" type="button" class="btn btn-danger">Remove</button>
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_add_p_room" onclick="add_presentation_room();" type="button" class="btn btn-success">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_presentation_action" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="p_action">Action</label>
                            <select onchange="change_p_action();" id="p_action" class="form-control">
                                <option id="0">Select an action</option>
                                <option id="lookAt">Change the view</option>
                                <option id="type">Narrate a text</option>
                            </select>
                        </div>
                    </div>
                    <div id="div_type" class="col-md-12" style="display: none">
                        <div class="form-group">
                            <label for="p_text">Narrate text (Carriage Return to split text into sentences)</label>
                            <textarea class="form-control" id="p_text" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="p_sleep_t">Wait time after narration (ms)</label>
                            <input type="number" class="form-control" id="p_sleep_t" value="0">
                        </div>
                    </div>
                    <div id="div_lookAt" class="col-md-12" style="display: none">
                        <label for="p_text">Frame the desired view</label><br>
                        <div style="width:100%;max-width:444px;height:250px;margin:0 auto;" id="p_lookAt"></div>
                        <p style="width:100%;text-align:center"></p>
                        <div class="form-group">
                            <label for="p_animation">Animation duration (ms)</label>
                            <input type="number" class="form-control" id="p_animation" value="1000">
                        </div>
                        <div class="form-group">
                            <label for="p_sleep_l">Wait time after animation (ms)</label>
                            <input type="number" class="form-control" id="p_sleep_l" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> data-toggle="modal" data-target="#modal_delete_p_action" id="btn_delete_p_action" type="button" class="btn btn-danger">Remove</button>
                <button <?php echo ($demo) ? 'disabled':''; ?> onclick="add_presentation_action();" id="btn_add_p_action" type="button" class="btn btn-success disabled">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_p_room" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove Room</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove room and all its actions from presentation?</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_remove_p_room" onclick="" type="button" class="btn btn-danger">Yes, Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_p_action" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove Action</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove action from room's presentation?</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_remove_p_action" onclick="" type="button" class="btn btn-danger">Yes, Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.p_viewer = null;
        window.id_p_room = null;
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        window.p_hfov = '<?php echo $virtual_tour['hfov']; ?>';
        window.p_min_hfov = '<?php echo $virtual_tour['min_hfov']; ?>';
        window.p_max_hfov = '<?php echo $virtual_tour['max_hfov']; ?>';
        window.p_viewer_initialized = false;
        window.array_presentation = null;
        window.p_params = '';
        window.array_id_rooms = [];
        $(document).ready(function () {
            get_presentation(window.id_virtualtour);
        });
    })(jQuery); // End of use strict
</script>