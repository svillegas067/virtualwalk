<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
if(!file_exists("../config/config.inc.php")) {
    header("Location: ../install/start.php");
}
require_once("functions.php");
$settings = get_settings();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $settings['name']; ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link href="../viewer/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>

<body class="bg-gradient-primary">
<div class="container">
    <div class="row">
        <?php if($settings['logo']!='') : ?>
            <div class="col-md-12 text-white mt-4 text-center">
                <img style="max-height:100px;max-width:200px;width:auto;height:auto" src="assets/<?php echo $settings['logo']; ?>" />
            </div>
        <?php endif; ?>
    </div>
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div style="<?php echo ($settings['background']!='') ? 'background-image: url(assets/'.$settings['background'].');'  : '' ; ?>" class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <form class="user user_login">
                                    <div class="form-group">
                                        <input tabindex="1" autofocus type="text" class="form-control form-control-user" id="username_l" aria-describedby="emailHelp" placeholder="Enter Username or E-mail...">
                                    </div>
                                    <div class="form-group">
                                        <input tabindex="2" type="password" class="form-control form-control-user" id="password_l" placeholder="Password">
                                    </div>
                                    <a tabindex="3" href="#" id="btn_login" onclick="login();return false;" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </a>
                                    <hr>
                                    <?php if($settings['smtp_valid']) : ?>
                                    <div class="text-center">
                                        <a class="small" href="#" data-toggle="modal" data-target="#modal_forgot">Forgot Password?</a>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($settings['enable_registration']) : ?>
                                        <div class="text-center">
                                            <a class="small" href="register.php">Create an Account</a>
                                        </div>
                                    <?php endif; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-white mt-0 text-center">
            <h2><?php echo strtoupper($settings['name']); ?></h2>
        </div>
    </div>

    <div id="modal_email" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Action required</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Associate a valid e-mail address to this account.</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter e-mail address" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_assoc_email" onclick="" type="button" class="btn btn-success">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_forgot" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Forgot Password</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email_f">E-mail</label>
                                <input type="email" class="form-control" id="email_f" placeholder="Enter e-mail address" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button id="btn_forgot_code" onclick="send_verification_code();" class="btn btn-block btn-primary">Send Verification Code</button>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="forgot_code">Verification code</label>
                                <input type="text" class="form-control" id="forgot_code" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password_f">New password</label>
                                <input autocomplete="new-password" type="password" class="form-control" id="password_f" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="repeat_password_f">Repeat password</label>
                                <input autocomplete="new-password" type="password" class="form-control" id="repeat_password_f" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button id="btn_change_password" onclick="change_password_forgot()" class="btn btn-block btn-success">Change password</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="js/sb-admin-2.js"></script>
<script src="js/function.js?v=<?php echo time(); ?>"></script>

<script>
    (function($) {
        "use strict"; // Start of use strict
        $(document).keyup(function(event) {
            if(!$('#modal_forgot').hasClass('show')) {
                if (event.key == "Enter") {
                    event.preventDefault();
                    $("#btn_login").trigger('click');
                }
            }
        });
    })(jQuery); // End of use strict
</script>

</body>
</html>
