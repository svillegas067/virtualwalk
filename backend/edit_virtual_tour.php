<?php
session_start();
require_once("functions.php");
$id_virtual_tour = $_GET['id'];
$virtual_tour = get_virtual_tour($id_virtual_tour,$_SESSION['id_user']);
$form_content = json_decode($virtual_tour['form_content'],true);
?>

<?php if(!$virtual_tour): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
<?php die(); endif; ?>

<?php
$_SESSION['id_virtualtour_sel'] = $id_virtual_tour;
$_SESSION['name_virtualtour_sel'] = $virtual_tour['name'];
?>

<link rel="stylesheet" href="../viewer/css/pannellum.css"/>
<script type="text/javascript" src="../viewer/js/libpannellum.js"></script>
<script type="text/javascript" src="../viewer/js/pannellum.js"></script>
<style>
    .pnlm-control {
        opacity: 1;
    }
</style>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-md-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-2 text-gray-800"><i class="fas fa-fw fa-route text-gray-700"></i> EDIT VIRTUAL TOUR</span></h1>
    <a id="save_btn" href="#" onclick="save_virtualtour();return false;" class="btn btn-success btn-icon-split mb-2 <?php echo ($demo) ? 'disabled':''; ?>">
    <span class="icon text-white-50">
      <i class="far fa-circle"></i>
    </span>
        <span class="text">SAVE</span>
    </a>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">General Settings</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter virtual tour name" value="<?php echo $virtual_tour['name']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="author">Author</label>
                            <input type="text" class="form-control" id="author" placeholder="Enter author" value="<?php echo $virtual_tour['author']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description">Description <i title="description used as preview for share" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="text" class="form-control" id="description" value="<?php echo $virtual_tour['description']; ?>" />
                        </div>
                    </div>
					<div class="col-md-3">
                        <div class="form-group">
                            <label for="autorotate_speed">AutoRotate speed <i title="0 to disable autorotate, -1 to -10 speed clockwise, 1 to 10 speed counterclockwise" class="help_t fas fa-question-circle"></i></label>
                            <input min="-10" max="10" type="number" class="form-control" id="autorotate_speed" value="<?php echo $virtual_tour['autorotate_speed']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="autorotate_inactivity">AutoRotate inactivity (ms) <i title="time in milliseconds to wait before starting the autorotation" class="help_t fas fa-question-circle"></i></label>
                            <input type="number" class="form-control" id="autorotate_inactivity" value="<?php echo $virtual_tour['autorotate_inactivity']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="compress_jpg">Compress images quality <i title="10 to 100: lower values means faster loading (poor quality), higher value means slow loading (high quality). 100 to disable compression." class="help_t fas fa-question-circle"></i></label><br>
                            <input min="10" max="100" type="number" class="form-control" id="compress_jpg" value="<?php echo $virtual_tour['compress_jpg']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="max_width_compress">Max width panorama <i title="maximum width in pixels of panoramic images. if they exceed this width the images will be resized. 0 to disable resize." class="help_t fas fa-question-circle"></i></label><br>
                            <input type="number" class="form-control" id="max_width_compress" value="<?php echo $virtual_tour['max_width_compress']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="ga_tracking_id">Google Analytics Tracking ID <i title="Google Analytics Tracking ID (UA-XXXXXXXXX-X). Note: Use the Friendly URL in Google Analytics's property url setting." class="help_t fas fa-question-circle"></i></label><br>
                            <input type="text" class="form-control" id="ga_tracking_id" value="<?php echo $virtual_tour['ga_tracking_id']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fb_page_id">Facebook page ID <i title="Id of Facebook page" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="text" class="form-control" id="fb_page_id" value="<?php echo $virtual_tour['fb_page_id']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="sameAzimuth">Same Azimuth <i title="maintain the same direction with regard to north while navigate between rooms (you must set the north position in all rooms)" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="sameAzimuth" <?php echo ($virtual_tour['sameAzimuth'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="auto_start">Auto start <i title="start the virtual tour automatically on loading" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="auto_start" <?php echo ($virtual_tour['auto_start'])?'checked':''; ?> />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Style Settings</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_map">Show Map <i title="show map on top right" class="help_t fas fa-question-circle"></i></label><br>
                            <select id="show_map" class="form-control">
                                <option <?php echo ($virtual_tour['show_map']==0) ? 'selected':''; ?> id="0">Hidden</option>
                                <option <?php echo ($virtual_tour['show_map']==1) ? 'selected':''; ?> id="1">Visible, minimized on Mobile</option>
                                <option <?php echo ($virtual_tour['show_map']==2) ? 'selected':''; ?> id="2">Visible, minimized on Desktop and Mobile</option>
                                <option <?php echo ($virtual_tour['show_map']==3) ? 'selected':''; ?> id="3">Visible, opened</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="arrows_nav">Arrows Prev/Next <i title="enable navigation with the forward and back arrows" class="help_t fas fa-question-circle"></i></label><br>
                            <select id="arrows_nav" class="form-control">
                                <option <?php echo ($virtual_tour['arrows_nav']==0) ? 'selected':''; ?> id="0">Disabled</option>
                                <option <?php echo ($virtual_tour['arrows_nav']==1) ? 'selected':''; ?> id="1">Enabled, Mobile and Desktop</option>
                                <option <?php echo ($virtual_tour['arrows_nav']==2) ? 'selected':''; ?> id="2">Enabled, only Desktop</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_list_alt">Show Room's List Menu <i title="show rooms's list menu on top left" class="help_t fas fa-question-circle"></i></label><br>
                            <div class="input-group">
                                <select id="show_list_alt" class="form-control">
                                    <option <?php echo ($virtual_tour['show_list_alt']==0) ? 'selected':''; ?> id="0">Hidden</option>
                                    <option <?php echo ($virtual_tour['show_list_alt']==1) ? 'selected':''; ?> id="1">Visible, minimized</option>
                                    <option <?php echo ($virtual_tour['show_list_alt']==2) ? 'selected':''; ?> id="2">Visible, opened</option>
                                </select>
                                <div class="input-group-append">
                                    <button data-toggle="modal" data-target="#modal_list_alt" class="btn btn-primary btn-xs" type="button"><i style="color: white" class="fas fa-cog"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="auto_show_slider">Show Room's List Slider <i title="show rooms's list slider on botom center" class="help_t fas fa-question-circle"></i></label><br>
                            <select id="auto_show_slider" class="form-control">
                                <option <?php echo ($virtual_tour['auto_show_slider']==2) ? 'selected':''; ?> id="2">Hidden</option>
                                <option <?php echo ($virtual_tour['auto_show_slider']==0) ? 'selected':''; ?> id="0">Visible, minimized</option>
                                <option <?php echo ($virtual_tour['auto_show_slider']==1) ? 'selected':''; ?> id="1">Visible, opened</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="voice_commands">Voice Commands <i title="enable navigation with voice commands" class="help_t fas fa-question-circle"></i></label><br>
                            <select id="voice_commands" class="form-control">
                                <option <?php echo ($virtual_tour['voice_commands']==0) ? 'selected':''; ?> id="0">Disabled</option>
                                <option <?php echo ($virtual_tour['voice_commands']==1) ? 'selected':''; ?> id="1">Enabled, paused</option>
                                <option <?php echo ($virtual_tour['voice_commands']==2) ? 'selected':''; ?> id="2">Enabled, autostart</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="compass">Show Compass <i title="show north compass on bottom right" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="compass" <?php echo ($virtual_tour['compass'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fb_messenger">Show Facebook Messenger <i title="show facebook messenger icon. Note: in order to works you need to add the url to facebook page - settings - messenger platform - whitelist domain" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="fb_messenger" <?php echo ($virtual_tour['fb_messenger'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_info">Show Info <i title="show info icon on bottom left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_info" <?php echo ($virtual_tour['show_info'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_gallery">Show Gallery <i title="show gallery icon on bottom left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_gallery" <?php echo ($virtual_tour['show_gallery'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_icons_toggle">Show Icons toggle <i title="show icons toggle menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_icons_toggle" <?php echo ($virtual_tour['show_icons_toggle'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_presentation">Show Presentation <i title="show presentation menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_presentation" <?php echo ($virtual_tour['show_presentation'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_main_form">Show Main Form <i title="show main form menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_main_form" <?php echo ($virtual_tour['show_main_form'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_share">Show Share <i title="show share menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_share" <?php echo ($virtual_tour['show_share'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_device_orientation">Show Device Orientation <i title="show device orientation menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_device_orientation" <?php echo ($virtual_tour['show_device_orientation'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_webvr">Show Web VR <i title="show web vr menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_webvr" <?php echo ($virtual_tour['show_webvr'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_audio">Show Audio <i title="show audio control in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_audio" <?php echo ($virtual_tour['show_audio'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_fullscreen">Show Fullscreen <i title="show fullscreen control in top right" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_fullscreen" <?php echo ($virtual_tour['show_fullscreen'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_annotations">Show Annotations <i title="show room annotations box in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="show_annotations" <?php echo ($virtual_tour['show_annotations'])?'checked':''; ?> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="live_session">Live Session <i title="show live session menu item in top left" class="help_t fas fa-question-circle"></i></label><br>
                            <input type="checkbox" id="live_session" <?php echo ($virtual_tour['live_session'])?'checked':''; ?> />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Default Markers Settings <i title="changes the default style of the markers that allow you to navigate between rooms" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="markers_style">Style</label>
                            <select id="markers_style" class="form-control">
                                <option <?php echo ($virtual_tour['markers_show_room']==1) ? 'selected' : '' ;  ?> id="icon_and_room">Icon + Room's Name</option>
                                <option <?php echo ($virtual_tour['markers_show_room']==2) ? 'selected' : '' ;  ?> id="room_and_icon">Room's Name + Icon</option>
                                <option <?php echo ($virtual_tour['markers_show_room']==0) ? 'selected' : '' ;  ?> id="only_icon">Only Icon</option>
                                <option <?php echo ($virtual_tour['markers_show_room']==3) ? 'selected' : '' ;  ?> id="only_room">Only Room's Name</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="markers_icon">Icon</label><br>
                            <button class="btn btn-sm btn-primary" type="button" id="GetIconPicker_p" data-iconpicker-input="input#markers_icon" data-iconpicker-preview="i#markers_icon_preview">Select Icon</button>
                            <input readonly type="hidden" id="markers_icon" name="Icon" value="<?php echo $virtual_tour['markers_icon']; ?>" required="" placeholder="" autocomplete="off" spellcheck="false">
                            <div style="vertical-align: middle;" class="icon-preview d-inline-block ml-1" data-toggle="tooltip" title="">
                                <i style="font-size: 24px;" id="markers_icon_preview" class="<?php echo $virtual_tour['markers_icon']; ?>"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="markers_color">Color</label>
                            <input type="text" id="markers_color" class="form-control" value="<?php echo $virtual_tour['markers_color']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="markers_background">Background</label>
                            <input type="text" id="markers_background" class="form-control" value="<?php echo $virtual_tour['markers_background']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Default POIs Settings <i title="changes the default style of the pois that allow you to view contents on the virtual tour" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="pois_style">Style</label>
                            <select id="pois_style" class="form-control">
                                <option <?php echo ($virtual_tour['pois_style']==2) ? 'selected' : '' ;  ?> id="icon_and_label">Icon + Label</option>
                                <option <?php echo ($virtual_tour['pois_style']==3) ? 'selected' : '' ;  ?> id="label_and_icon">Label + Icon</option>
                                <option <?php echo ($virtual_tour['pois_style']==0) ? 'selected' : '' ;  ?> id="only_icon">Only Icon</option>
                                <option <?php echo ($virtual_tour['pois_style']==4) ? 'selected' : '' ;  ?> id="only_label">Only Label</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="markers_icon">Icon</label><br>
                            <button class="btn btn-sm btn-primary" type="button" id="GetIconPicker" data-iconpicker-input="input#pois_icon" data-iconpicker-preview="i#pois_icon_preview">Select Icon</button>
                            <input readonly type="hidden" id="pois_icon" name="Icon" value="<?php echo $virtual_tour['pois_icon']; ?>" required="" placeholder="" autocomplete="off" spellcheck="false">
                            <div style="vertical-align: middle;" class="icon-preview d-inline-block ml-1" data-toggle="tooltip" title="">
                                <i style="font-size: 24px;" id="pois_icon_preview" class="<?php echo $virtual_tour['pois_icon']; ?>"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="pois_color">Color</label>
                            <input type="text" id="pois_color" class="form-control" value="<?php echo $virtual_tour['pois_color']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="pois_background">Background</label>
                            <input type="text" id="pois_background" class="form-control" value="<?php echo $virtual_tour['pois_background']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Main Form <i title="customizable form visible in entire virtual tour by pressing a top left action icon" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label style="margin-bottom: 1px">Enable</label><br>
                            <input <?php echo ($virtual_tour['form_enable'])?'checked':''; ?> id="form_enable" type="checkbox">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label style="margin-bottom: 1px" for="form_icon">Icon</label><br>
                        <button class="btn btn-sm btn-primary" type="button" id="GetIconPicker_form" data-iconpicker-input="input#form_icon" data-iconpicker-preview="i#form_icon_preview">Select Icon</button>
                        <input readonly type="hidden" id="form_icon" name="Icon" value="<?php echo $virtual_tour['form_icon']; ?>" required="" placeholder="" autocomplete="off" spellcheck="false">
                        <div style="vertical-align: middle;" class="icon-preview d-inline-block ml-1" data-toggle="tooltip" title="">
                            <i style="font-size: 24px;" id="form_icon_preview" class="<?php echo $virtual_tour['form_icon']; ?>"></i>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div style="margin-bottom: 3px" class="form-group">
                            <label style="margin-bottom: 1px" for="form_title">Title</label>
                            <input id="form_title" type="text" class="form-control" value="<?php echo $form_content[0]['title']; ?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="margin-bottom: 3px" class="form-group">
                            <label style="margin-bottom: 1px" for="form_button">Button</label>
                            <input id="form_button" type="text" class="form-control" value="<?php echo $form_content[0]['button']; ?>">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div style="margin-bottom: 3px" class="form-group">
                            <label style="margin-bottom: 1px" for="form_description">Description</label>
                            <input id="form_description" type="text" class="form-control" value="<?php echo $form_content[0]['description']; ?>">
                        </div>
                    </div>
                </div>
                <hr style="margin: 3px">
                <?php for($i=1;$i<=5;$i++) { ?>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Enable</label><br>
                                <input <?php echo ($form_content[$i]['enabled'])?'checked':''; ?> id="form_field_<?php echo $i; ?>" type="checkbox">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Required</label><br>
                                <input <?php echo ($form_content[$i]['required'])?'checked':''; ?> id="form_field_required_<?php echo $i; ?>" type="checkbox">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="margin-bottom: 3px" class="form-group">
                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Type</label><br>
                                <select id="form_field_type_<?php echo $i; ?>" class="form-control">
                                    <option <?php echo ($form_content[$i]['type']=='text')?'selected':''; ?> id="text" value="text">Text</option>
                                    <option <?php echo ($form_content[$i]['type']=='number')?'selected':''; ?> id="number" value="number">Number</option>
                                    <option <?php echo ($form_content[$i]['type']=='tel')?'selected':''; ?> id="tel" value="tel">Phone</option>
                                    <option <?php echo ($form_content[$i]['type']=='email')?'selected':''; ?> id="email" value="email">E-Mail</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="margin-bottom: 3px" class="form-group">
                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Label</label><br>
                                <input id="form_field_label_<?php echo $i; ?>" type="text" class="form-control" value="<?php echo $form_content[$i]['label']; ?>">
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Background Loading <i title="image displayed as background during initial loading and used as preview image for share" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="div_exist_bg" class="col-md-12">
                        <div class="form-group">
                            <label for="exist_bg">Exist Background Logo</label>
                            <select onchange="change_exist_bg();" class="form-control" id="exist_bg">
                                <option id="0">Upload new Background Logo</option>
                                <?php echo get_option_exist_background_logo($_SESSION['id_user']); ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none" id="div_image_bg" class="col-md-12">
                        <img style="width: 100%" src="../viewer/content/<?php echo $virtual_tour['background_image']; ?>" />
                    </div>
                    <div style="display: none" id="div_delete_bg" class="col-md-12 mt-4">
                        <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_bg();" class="btn btn-block btn-danger">REMOVE IMAGE</button>
                    </div>
                    <div style="display: none" class="col-md-12" id="div_upload_bg">
                        <form id="frm_b" action="ajax/upload_background_image.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_b" name="txtFile_b" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_b" value="Upload Background Image" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="preview text-center">
                                        <div class="progress progress_b mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_b" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_b"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Logo <i title="logo displayed on top right" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="div_exist_logo" class="col-md-12">
                        <div class="form-group">
                            <label for="exist_logo">Exist Logo</label>
                            <select onchange="change_exist_logo();" class="form-control" id="exist_logo">
                                <option id="0">Upload new Logo</option>
                                <?php echo get_option_exist_logo($_SESSION['id_user']); ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none" id="div_image_logo" class="col-md-12">
                        <img style="width: 100%" src="../viewer/content/<?php echo $virtual_tour['logo']; ?>" />
                    </div>
                    <div style="display: none" id="div_link_logo" class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="link_logo">Hyperlink</label>
                            <input id="link_logo" type="text" class="form-control" value="<?php echo $virtual_tour['link_logo']; ?>">
                        </div>
                    </div>
                    <div style="display: none" id="div_delete_logo" class="col-md-12 mt-4">
                        <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_logo();" class="btn btn-block btn-danger">REMOVE LOGO</button>
                    </div>
                    <div style="display: none" class="col-md-12" id="div_upload_logo">
                        <form id="frm_l" action="ajax/upload_logo_image.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_l" name="txtFile_l" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_l" value="Upload Logo Image" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="preview text-center">
                                        <div class="progress progress_l mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_l" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_l"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Nadir Logo <i title="logo used to hide tripod on panorama image" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="div_exist_nadir_logo" class="col-md-12">
                        <div class="form-group">
                            <label for="exist_nadir_logo">Exist Nadir Logo</label>
                            <select onchange="change_exist_nadir_logo();" class="form-control" id="exist_nadir_logo">
                                <option id="0">Upload new Nadir Logo</option>
                                <?php echo get_option_exist_nadir_logo($_SESSION['id_user']); ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none" id="div_image_nadir_logo" class="col-md-12 text-center">
                        <img style="width: 100%;max-width: 150px" src="../viewer/content/<?php echo $virtual_tour['nadir_logo']; ?>" />
                    </div>
                    <div style="display: none" id="div_size_nadir_logo" class="col-md-12 mt-4">
                        <select id="size_nadir_logo" class="form-control">
                            <option <?php echo ($virtual_tour['nadir_size']=='small') ? 'selected':''; ?> id="small">Small</option>
                            <option <?php echo ($virtual_tour['nadir_size']=='medium') ? 'selected':''; ?> id="medium">Medium</option>
                            <option <?php echo ($virtual_tour['nadir_size']=='large') ? 'selected':''; ?> id="large">Large</option>
                        </select>
                    </div>
                    <div style="display: none" id="div_delete_nadir_logo" class="col-md-12 mt-4">
                        <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_nadir_logo();" class="btn btn-block btn-danger">REMOVE LOGO</button>
                    </div>
                    <div style="display: none" class="col-md-12" id="div_upload_nadir_logo">
                        <form id="frm_n" action="ajax/upload_logo_nadir_image.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_n" name="txtFile_n" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_n" value="Upload Logo Image" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="preview text-center">
                                        <div class="progress progress_n mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_n" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_n"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Intro (Desktop) <i title="image displayed on desktop at first load" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="div_exist_introd" class="col-md-12">
                        <div class="form-group">
                            <label for="exist_introd">Exist Image</label>
                            <select onchange="change_exist_introd();" class="form-control" id="exist_introd">
                                <option id="0">Upload new Image</option>
                                <?php echo get_option_exist_introd($_SESSION['id_user']); ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none" id="div_image_introd" class="col-md-12">
                        <img style="width: 100%" src="../viewer/content/<?php echo $virtual_tour['intro_desktop']; ?>" />
                    </div>
                    <div style="display: none" id="div_delete_introd" class="col-md-12 mt-4">
                        <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_introd();" class="btn btn-block btn-danger">REMOVE IMAGE</button>
                    </div>
                    <div style="display: none" class="col-md-12" id="div_upload_introd">
                        <form id="frm_id" action="ajax/upload_intro_image.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_id" name="txtFile_id" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_id" value="Upload Image" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="preview text-center">
                                        <div class="progress progress_id mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_id" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_id"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Intro (Mobile) <i title="image displayed on mobile at first load" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="div_exist_introm" class="col-md-12">
                        <div class="form-group">
                            <label for="exist_introm">Exist Image</label>
                            <select onchange="change_exist_introm();" class="form-control" id="exist_introm">
                                <option id="0">Upload new Image</option>
                                <?php echo get_option_exist_introm($_SESSION['id_user']); ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none" id="div_image_introm" class="col-md-12">
                        <img style="width: 100%" src="../viewer/content/<?php echo $virtual_tour['intro_mobile']; ?>" />
                    </div>
                    <div style="display: none" id="div_delete_introm" class="col-md-12 mt-4">
                        <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_introm();" class="btn btn-block btn-danger">REMOVE IMAGE</button>
                    </div>
                    <div style="display: none" class="col-md-12" id="div_upload_introm">
                        <form id="frm_im" action="ajax/upload_intro_image.php" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="txtFile_im" name="txtFile_im" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_im" value="Upload Image" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="preview text-center">
                                        <div class="progress progress_im mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                            <div class="progress-bar" id="progressBar_im" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                0%
                                            </div>
                                        </div>
                                        <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_im"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Song <i title="background song during navigation of virtual tour" class="help_t fas fa-question-circle"></i></h6>
            </div>
            <div class="card-body">
                <div id="div_exist_song" class="col-md-12">
                    <div class="form-group">
                        <label for="exist_song">Exist Song</label>
                        <select onchange="change_exist_song();" class="form-control" id="exist_song">
                            <option id="0">Upload new Song</option>
                            <?php echo get_option_exist_song($_SESSION['id_user']); ?>
                        </select>
                    </div>
                </div>
                <div style="display: none" id="div_player_song" class="col-md-12 text-center">
                    <audio controls>
                        <source src="../viewer/content/<?php echo $virtual_tour['song']; ?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
                </div>
                <div id="div_autoplay_song" class="col-md-12 mt-2">
                    <div class="form-group">
                        <label for="song_autoplay">Autoplay</label>
                        <input type="checkbox" id="song_autoplay" <?php echo ($virtual_tour['song_autoplay']==1) ? 'checked':''; ?>>
                    </div>
                </div>
                <div style="display: none" id="div_delete_song" class="mt-4">
                    <div class="col-md-12">
                        <button onclick="delete_song();return false;" id="btn_delete_song" class="btn btn-block btn-danger">REMOVE SONG</button>
                    </div>
                </div>
                <div style="display: none" id="div_upload_song" class="col-md-12">
                    <form id="frm" action="ajax/upload_song.php" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="file" class="form-control" id="txtFile" name="txtFile" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload" value="Upload Song (MP3)" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="preview text-center">
                                    <div class="progress progress_s mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                        <div class="progress-bar" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                            0%
                                        </div>
                                    </div>
                                    <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Preview Field of View</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;max-width:622px;height: 350px;margin: 0 auto;" id="panorama"></div>
                <div class="mt-2 text-center" style="width: 100%;">
                    Current FOV <b><span id="hvof_debug"><?php echo $virtual_tour['hfov']; ?></span></b><br>
                    <i>use the mouse wheel or the controls to zoom</i>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Field of View</h6>
            </div>
            <div class="card-body">
                <p>Change these values ​​to see how the field of view's preview changes.</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Default <i title="sets the panorama’s starting horizontal field of view in degrees." class="help_t fas fa-question-circle"></i></label>
                            <input disabled type="number" min="50" max="120" class="form-control" id="hfov" value="<?php echo $virtual_tour['hfov']; ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Min <i title="sets the minimum pitch the viewer edge can be at, in degrees." class="help_t fas fa-question-circle"></i></label>
                            <input disabled type="number" min="50" max="120" class="form-control" id="min_hfov" value="<?php echo $virtual_tour['min_hfov']; ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Max <i title="Sets the maximum pitch the viewer edge can be at, in degrees." class="help_t fas fa-question-circle"></i></label>
                            <input disabled type="number" min="50" max="120" class="form-control" id="max_hfov" value="<?php echo $virtual_tour['max_hfov']; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_list_alt" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?php include("rooms_menu_list.php"); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_virtualtour = <?php echo $id_virtual_tour; ?>;
        var hfov = '<?php echo $virtual_tour['hfov']; ?>';
        var min_hfov = '<?php echo $virtual_tour['min_hfov']; ?>';
        var max_hfov = '<?php echo $virtual_tour['max_hfov']; ?>';
        window.song = '<?php echo $virtual_tour['song']; ?>';
        window.logo = '<?php echo $virtual_tour['logo']; ?>';
        window.nadir_logo = '<?php echo $virtual_tour['nadir_logo']; ?>';
        window.background_image = '<?php echo $virtual_tour['background_image']; ?>';
        window.intro_desktop = '<?php echo $virtual_tour['intro_desktop']; ?>';
        window.intro_mobile = '<?php echo $virtual_tour['intro_mobile']; ?>';
        var viewer;
        var ratio_hfov = 1;
        var viewer_initialized = false;
        window.markers_color_spectrum = null;
        window.markers_background_spectrum = null;
        window.pois_color_spectrum = null;
        window.pois_background_spectrum = null;
        window.vt_need_save = false;

        $(document).ready(function () {
            $('.help_t').tooltip();
            if(logo=='') {
                $('#div_delete_logo').hide();
                $('#div_image_logo').hide();
                $('#div_upload_logo').show();
                $('#div_exist_logo').show();
                $('#div_link_logo').hide();
            } else {
                $('#div_delete_logo').show();
                $('#div_image_logo').show();
                $('#div_upload_logo').hide();
                $('#div_exist_logo').hide();
                $('#div_link_logo').show();
            }
            if(nadir_logo=='') {
                $('#div_delete_nadir_logo').hide();
                $('#div_image_nadir_logo').hide();
                $('#div_size_nadir_logo').hide();
                $('#div_upload_nadir_logo').show();
                $('#div_exist_nadir_logo').show();
            } else {
                $('#div_delete_nadir_logo').show();
                $('#div_image_nadir_logo').show();
                $('#div_size_nadir_logo').show();
                $('#div_upload_nadir_logo').hide();
                $('#div_exist_nadir_logo').hide();
            }
            if(background_image=='') {
                $('#div_delete_bg').hide();
                $('#div_image_bg').hide();
                $('#div_upload_bg').show();
                $('#div_exist_bg').show();
            } else {
                $('#div_delete_bg').show();
                $('#div_image_bg').show();
                $('#div_upload_bg').hide();
                $('#div_exist_bg').hide();
            }
            if(song=='') {
                $('#div_delete_song').hide();
                $('#div_player_song').hide();
                $('#div_upload_song').show();
                $('#div_autoplay_song').hide();
                $('#div_exist_song').show();
            } else {
                $('#div_delete_song').show();
                $('#div_player_song').show();
                $('#div_upload_song').hide();
                $('#div_autoplay_song').show();
                $('#div_exist_song').hide();
            }
            if(intro_desktop=='') {
                $('#div_delete_introd').hide();
                $('#div_image_introd').hide();
                $('#div_upload_introd').show();
                $('#div_exist_introd').show();
            } else {
                $('#div_delete_introd').show();
                $('#div_image_introd').show();
                $('#div_upload_introd').hide();
                $('#div_exist_introd').hide();
            }
            if(intro_mobile=='') {
                $('#div_delete_introm').hide();
                $('#div_image_introm').hide();
                $('#div_upload_introm').show();
                $('#div_exist_introm').show();
            } else {
                $('#div_delete_introm').show();
                $('#div_image_introm').show();
                $('#div_upload_introm').hide();
                $('#div_exist_introm').hide();
            }
            IconPicker.Init({
                jsonUrl: 'vendor/iconpicker/iconpicker-1.5.0.json',
                searchPlaceholder: 'Search Icon',
                showAllButton: 'Show All',
                cancelButton: 'Cancel',
                noResultsFound: 'No results found.',
                borderRadius: '20px',
            });
            IconPicker.Run('#GetIconPicker', function(){
                window.vt_need_save = true;
            });
            IconPicker.Run('#GetIconPicker_p', function(){
                window.vt_need_save = true;
            });
            IconPicker.Run('#GetIconPicker_form', function(){
                window.vt_need_save = true;
            });
            window.markers_color_spectrum = $('#markers_color').spectrum({
                type: "text",
                preferredFormat: "hex",
                showAlpha: false,
                showButtons: false,
                allowEmpty: false
            });
            window.markers_background_spectrum = $('#markers_background').spectrum({
                type: "text",
                preferredFormat: "rgb",
                showAlpha: true,
                showButtons: false,
                allowEmpty: false
            });
            window.pois_color_spectrum = $('#pois_color').spectrum({
                type: "text",
                preferredFormat: "hex",
                showAlpha: false,
                showButtons: false,
                allowEmpty: false
            });
            window.pois_background_spectrum = $('#pois_background').spectrum({
                type: "text",
                preferredFormat: "rgb",
                showAlpha: true,
                showButtons: false,
                allowEmpty: false
            });
             viewer = pannellum.viewer('panorama', {
                "type": "equirectangular",
                "panorama": "img/test.jpg",
                "autoLoad": true,
                "showFullscreenCtrl": false,
                "showControls": true,
                "hfov": parseInt(hfov),
                "minHfov": parseInt(min_hfov),
                "maxHfov": parseInt(max_hfov)
            });
            viewer.on('load', function () {
                viewer_initialized = true;
                $('#hfov').prop("disabled",false);
                $('#min_hfov').prop("disabled",false);
                $('#max_hfov').prop("disabled",false);
                adjust_ratio_hfov_vt();
            });
            viewer.on('zoomchange', function () {
                var hfov = viewer.getHfov();
                var hfov_t = hfov * ratio_hfov;
                hfov_t = Math.round(hfov_t);
                $('#hvof_debug').html(hfov_t);
                var c_hfov = parseInt($('#hfov').val());
                var c_min_hfov = parseInt($('#min_hfov').val());
                var c_max_hfov = parseInt($('#max_hfov').val());
                if(c_hfov==hfov_t) {
                    $('#hfov').addClass("input-highlight");
                } else {
                    $('#hfov').removeClass("input-highlight");
                }
                if(c_min_hfov==hfov_t) {
                    $('#min_hfov').addClass("input-highlight");
                    $("#min_hfov").blur();
                } else {
                    $('#min_hfov').removeClass("input-highlight");
                }
                if(c_max_hfov==hfov_t) {
                    $('#max_hfov').addClass("input-highlight");
                    $("#max_hfov").blur();
                } else {
                    $('#max_hfov').removeClass("input-highlight");
                }
            });
        });
        $('#hfov,#min_hfov,#max_hfov').on('input',function (event) {
            window.vt_need_save = true;
            var hfov = parseInt($('#hfov').val());
            var min_hfov = parseInt($('#min_hfov').val());
            var max_hfov = parseInt($('#max_hfov').val());
            if(hfov<min_hfov) {
                hfov = min_hfov;
                $('#hfov').val(hfov);
            }
            if(hfov>max_hfov) {
                hfov = max_hfov;
                $('#hfov').val(hfov);
            }
            if(min_hfov<50) {
                min_hfov=50;
                $('#min_hfov').val(min_hfov);
            }
            if(max_hfov>120) {
                max_hfov=120;
                $('#max_hfov').val(max_hfov);
            }
            min_hfov = min_hfov / ratio_hfov;
            max_hfov = max_hfov / ratio_hfov;
            viewer.setHfovBounds([min_hfov,max_hfov]);
            hfov = hfov / ratio_hfov;
            switch(event.currentTarget.id) {
                case 'hfov':
                    viewer.setHfov(hfov,false);
                    break;
                case 'min_hfov':
                    viewer.setHfov(min_hfov,false);
                    break;
                case 'max_hfov':
                    viewer.setHfov(max_hfov,false);
                    break;
            }
        });

        function hotspot_nadir(hotSpotDiv, args) {
            hotSpotDiv.classList.add('noselect');
            hotSpotDiv.style = "background-image:url(../viewer/content/"+args+");background-size:cover;";
        }

        function adjust_ratio_hfov_vt() {
            var c_w = parseFloat($('#panorama').css('width').replace('px',''));
            var c_h = parseFloat($('#panorama').css('height').replace('px',''));
            var ratio_panorama = c_w / c_h;
            ratio_hfov = 1.7771428571428571 / ratio_panorama;
            var hfov = parseInt($('#hfov').val());
            var min_hfov = parseInt($('#min_hfov').val());
            var max_hfov = parseInt($('#max_hfov').val());
            min_hfov = min_hfov / ratio_hfov;
            max_hfov = max_hfov / ratio_hfov;
            hfov = hfov / ratio_hfov;
            viewer.setHfovBounds([min_hfov,max_hfov]);
            viewer.setHfov(hfov,false);
        }

        $('body').on('submit','#frm',function(e){
            e.preventDefault();
            $('#error').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.song = evt.target.responseText;
                        $('#div_delete_song').show();
                        $('#div_player_song').show();
                        $('#div_upload_song').hide();
                        $('#div_exist_song').hide();
                        $('#div_player_song audio').attr('src','../viewer/content/'+window.song);
                    }
                }
                update_progressbar(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error('upload failed');
                update_progressbar(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error('upload aborted');
                update_progressbar(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar(value){
            $('#progressBar').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_s').hide();
            }else{
                $('.progress_s').show();
            }
        }

        function show_error(error){
            $('.progress_s').hide();
            $('#error').show();
            $('#error').html(error);
        }

        $('body').on('submit','#frm_l',function(e){
            e.preventDefault();
            $('#error_l').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_l[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_l' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar_l(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_l(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.logo = evt.target.responseText;
                        $('#div_image_logo img').attr('src','../viewer/content/'+window.logo);
                        $('#div_delete_logo').show();
                        $('#div_image_logo').show();
                        $('#div_upload_logo').hide();
                        $('#div_exist_logo').hide();
                    }
                }
                update_progressbar_l(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_l('upload failed');
                update_progressbar_l(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_l('upload aborted');
                update_progressbar_l(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar_l(value){
            $('#progressBar_l').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_l').hide();
            }else{
                $('.progress_l').show();
            }
        }

        function show_error_l(error){
            $('.progress_l').hide();
            $('#error_l').show();
            $('#error_l').html(error);
        }

        $('body').on('submit','#frm_n',function(e){
            e.preventDefault();
            $('#error_n').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_n[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_n' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar_n(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_n(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.nadir_logo = evt.target.responseText;
                        $('#div_image_nadir_logo img').attr('src','../viewer/content/'+window.nadir_logo);
                        $('#div_delete_nadir_logo').show();
                        $('#div_image_nadir_logo').show();
                        $('#div_size_nadir_logo').show();
                        $('#div_upload_nadir_logo').hide();
                        $('#div_exist_nadir_logo').hide();
                    }
                }
                update_progressbar_n(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_n('upload failed');
                update_progressbar_n(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_n('upload aborted');
                update_progressbar_n(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar_n(value){
            $('#progressBar_n').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_n').hide();
            }else{
                $('.progress_n').show();
            }
        }

        function show_error_n(error){
            $('.progress_n').hide();
            $('#error_n').show();
            $('#error_n').html(error);
        }

        $('body').on('submit','#frm_b',function(e){
            e.preventDefault();
            $('#error_b').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_b[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_b' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar_b(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_b(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.background_image = evt.target.responseText;
                        $('#div_image_bg img').attr('src','../viewer/content/'+window.background_image);
                        $('#div_delete_bg').show();
                        $('#div_image_bg').show();
                        $('#div_upload_bg').hide();
                        $('#div_exist_bg').hide();
                    }
                }
                update_progressbar_b(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_b('upload failed');
                update_progressbar_b(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_b('upload aborted');
                update_progressbar_b(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar_b(value){
            $('#progressBar_b').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_b').hide();
            }else{
                $('.progress_b').show();
            }
        }

        function show_error_b(error){
            $('.progress_b').hide();
            $('#error_b').show();
            $('#error_b').html(error);
        }

        $('body').on('submit','#frm_id',function(e){
            e.preventDefault();
            $('#error_id').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_id[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_id' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar_id(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_id(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.intro_desktop = evt.target.responseText;
                        $('#div_image_introd img').attr('src','../viewer/content/'+window.intro_desktop);
                        $('#div_delete_introd').show();
                        $('#div_image_introd').show();
                        $('#div_upload_introd').hide();
                        $('#div_exist_introd').hide();
                    }
                }
                update_progressbar_id(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_id('upload failed');
                update_progressbar_id(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_id('upload aborted');
                update_progressbar_id(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar_id(value){
            $('#progressBar_id').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_id').hide();
            }else{
                $('.progress_id').show();
            }
        }

        function show_error_id(error){
            $('.progress_id').hide();
            $('#error_id').show();
            $('#error_id').html(error);
        }

        $('body').on('submit','#frm_im',function(e){
            e.preventDefault();
            $('#error_im').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_im[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_im' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                update_progressbar_im(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_im(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.vt_need_save = true;
                        window.intro_mobile = evt.target.responseText;
                        $('#div_image_introm img').attr('src','../viewer/content/'+window.intro_mobile);
                        $('#div_delete_introm').show();
                        $('#div_image_introm').show();
                        $('#div_upload_introm').hide();
                        $('#div_exist_introm').hide();
                    }
                }
                update_progressbar_im(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_im('upload failed');
                update_progressbar_im(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_im('upload aborted');
                update_progressbar_im(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function update_progressbar_im(value){
            $('#progressBar_im').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress_im').hide();
            }else{
                $('.progress_im').show();
            }
        }

        function show_error_im(error){
            $('.progress_im').hide();
            $('#error_im').show();
            $('#error_im').html(error);
        }

        $(window).resize(function() {
            if(viewer_initialized) {
                adjust_ratio_hfov_vt();
            }
        });

        $("input").change(function(){
            window.vt_need_save = true;
        });

        $("select").change(function(){
            window.vt_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.vt_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });
    })(jQuery); // End of use strict
</script>