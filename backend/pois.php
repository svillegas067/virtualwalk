<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
$id_user = $_SESSION['id_user'];
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_SESSION['id_virtualtour_sel'])) {
        $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
        $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
    } else {
        $id_virtualtour_sel = $virtual_tours[0]['id'];
        $name_virtualtour_sel = $virtual_tours[0]['name'];
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}

if(isset($_GET['id_room'])) {
    $id_room = $_GET['id_room'];
} else {
    $id_room = 0;
}

?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-bullseye text-gray-700"></i> POIs</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div id="plan_poi_msg" class="card bg-warning text-white shadow mb-4 d-none">
    <div class="card-body">
        You have reached the maximum number of POIs allowed from your plan!
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rooms List <i style="font-size: 12px">(click on room to select)</i></h6>
            </div>
            <div class="card-body ml-3 mr-3">
                <div class="col-md-12">
                    <div class="rooms_slider">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Pois <i style="font-size: 12px">(click on poi to edit)</i></h6>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <p id="msg_sel_room">Select a room first!</p>
                    <div class="ml-3 mr-3" style="position: relative">
                        <div id="panorama_pois"></div>
                        <div id="action_box">
                            <i title="MOVE" onclick="" class="move_action fa fa-arrows-alt"></i>
                            <i title="EDIT" onclick="" class="edit_action fa fa-edit"></i>
                            <i title="STYLE" onclick="" class="style_action fas fa-palette"></i>
                            <i title="DELETE" onclick="" class="delete_action fa fa-trash"></i>
                        </div>
                        <div id="confirm_edit">
                            <div style="display: none" id="div_form_edit">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 3px" class="form-group">
                                            <label style="margin-bottom: 1px" for="form_title">Title</label>
                                            <input id="form_title" type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div style="margin-bottom: 3px" class="form-group">
                                            <label style="margin-bottom: 1px" for="form_button">Button</label>
                                            <input id="form_button" type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div style="margin-bottom: 3px" class="form-group">
                                            <label style="margin-bottom: 1px" for="form_description">Description</label>
                                            <input id="form_description" type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                </div>
                                <hr style="margin: 3px">
                                <?php for($i=1;$i<=5;$i++) { ?>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Enable</label><br>
                                                <input id="form_field_<?php echo $i; ?>" type="checkbox">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Required</label><br>
                                                <input id="form_field_required_<?php echo $i; ?>" type="checkbox">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div style="margin-bottom: 3px" class="form-group">
                                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Type</label><br>
                                                <select id="form_field_type_<?php echo $i; ?>" class="form-control">
                                                    <option id="text" value="text">Text</option>
                                                    <option id="number" value="number">Number</option>
                                                    <option id="tel" value="tel">Phone</option>
                                                    <option id="email" value="email">E-Mail</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div style="margin-bottom: 3px" class="form-group">
                                                <label style="margin-bottom: 1px">F.<?php echo $i; ?> Label</label><br>
                                                <input id="form_field_label_<?php echo $i; ?>" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label id="content_label">Content - Image Link</label>
                                <input id="poi_content" type="text" class="form-control" value="">
                            </div>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label>Title</label>
                                <input id="poi_title" type="text" class="form-control" value="">
                            </div>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label>Description</label>
                                <input id="poi_description" type="text" class="form-control" value="">
                            </div>
                            <div style="margin-bottom: 5px;display: none" class="form-group">
                                <label>Content - Text</label>
                                <div id="poi_content_html"></div>
                            </div>
                            <div style="margin-bottom: 5px;display: none" class="form-group">
                                <label>Content - Html</label>
                                <div contenteditable="true" id="poi_content_html_sc"></div>
                                <script>
                                    document.querySelector('div[contenteditable="true"]').addEventListener("paste", function(e) {
                                        e.preventDefault();
                                        var text = e.clipboardData.getData("text/plain");
                                        document.execCommand("inserttext", false, text);
                                    });
                                </script>
                            </div>
                            <div style="margin-bottom: 5px;display: none" class="form-group">
                                <button onclick="edit_poi_gallery();" id="btn_poi_gallery" class="btn btn-primary">EDIT IMAGES GALLERY</button><br>
                            </div>
                            <form id="frm_edit" action="ajax/upload_content_image.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Image</label>
                                            <input type="file" class="form-control" id="txtFile_edit" name="txtFile_edit" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_edit" value="Upload Image" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar_edit" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_edit"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form style="display: none" id="frm_d_edit" action="ajax/upload_content_file.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>File</label>
                                            <input type="file" class="form-control" id="txtFile_d_edit" name="txtFile_edit" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_d_edit" value="Upload File" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar_d_edit" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_d_edit"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form id="frm_v_edit" action="ajax/upload_content_video.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Video MP4</label>
                                            <input type="file" class="form-control" id="txtFile_v_edit" name="txtFile_v_edit" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_v_edit" value="Upload Video" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar_v_edit" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_v_edit"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form id="frm_a_edit" action="ajax/upload_content_audio.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Audio MP3</label>
                                            <input type="file" class="form-control" id="txtFile_a_edit" name="txtFile_a_edit" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_a_edit" value="Upload Audio" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar_a_edit" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_a_edit"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <div id="confirm_style">
                            <div class="row">
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="poi_style">Style</label>
                                        <select onchange="change_poi_style()" id="poi_style" class="form-control">
                                            <option id="2">Icon + Label</option>
                                            <option id="3">Label + Icon</option>
                                            <option id="0">Only Icon</option>
                                            <option id="4">Only Label</option>
                                            <option id="1">Custom Icons Library</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="poi_icon">Icon</label><br>
                                        <button class="btn btn-sm btn-primary" type="button" id="GetIconPicker" data-iconpicker-input="input#poi_icon" data-iconpicker-preview="i#poi_icon_preview">Select Icon</button>
                                        <input readonly type="hidden" id="poi_icon" name="Icon" value="fas fa-image" required="" placeholder="" autocomplete="off" spellcheck="false">
                                        <div style="vertical-align: middle;" class="icon-preview d-inline-block ml-1" data-toggle="tooltip" title="">
                                            <i style="font-size: 24px;" id="poi_icon_preview" class="fas fa-image"></i>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="marker_library_icon">Library Icon</label><br>
                                        <button data-toggle="modal" data-target="#modal_library_icons" class="btn btn-sm btn-primary" type="button" id="btn_library_icon">Select Library Icon</button>
                                        <input type="hidden" id="poi_library_icon" value="0" />
                                        <img id="poi_library_icon_preview" style="display: none;height: 30px" src="" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="poi_color">Color</label>
                                        <input type="text" id="poi_color" class="form-control" value="#000000" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="poi_background">Background</label>
                                        <input type="text" id="poi_background" class="form-control" value="rgba(255,255,255,0.7)" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="poi_label">Label</label>
                                        <input type="text" id="poi_label" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <div id="confirm_move">
                            <b>drag view to change position</b><br>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label style="margin-bottom: 0;">Size</label>
                                <input oninput="" type="range" step="0.1" min="0.5" max="1.5" class="form-control-range" id="size_scale">
                            </div>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <button id="btn_add_poi" style="opacity:0;position:absolute;top:10px;right:10px;z-index:10;" class="btn btn-circle btn-primary d-inline-block float-right"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_add_poi" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add POI</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Type</label>
                        <select id="poi_type_add" class="form-control">
                            <option selected id="image">Image (single)</option>
                            <option id="gallery">Images (gallery)</option>
                            <option id="video">Video</option>
                            <option id="video360">Video 360</option>
                            <option id="audio">Audio</option>
                            <option id="link">Link (embed)</option>
                            <option id="link_ext">Link (external)</option>
                            <option id="html">Text</option>
                            <option id="html_sc">Html</option>
                            <option id="download">Download</option>
                            <option id="form">Form</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_new_poi" onclick="" type="button" class="btn btn-success">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_poi" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete POI</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the poi?</p>
            </div>
            <div class="modal-footer">
                <button id="btn_delete_poi" onclick="" type="button" class="btn btn-danger">Yes, Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_library_icons" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Library Icons</h5>
            </div>
            <div class="modal-body">
                <?php echo get_library_icons($id_virtualtour_sel,'poi'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_images_gallery" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" style="width: 90% !important; max-width: 90% !important; margin: 0 auto !important;" role="document">
        <div class="modal-content">
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_room_poi = <?php echo $id_room; ?>;
        window.id_room_sel = null;
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        window.pois = null;
        window.can_create = false;
        window.is_editing = false;
        window.poi_index_edit = null;
        window.poi_id_edit = null;
        window.panorama_image = '';
        window.currentYaw = 0;
        window.currentPitch = 0;
        window.viewer = null;
        window.viewer_initialized = false;
        window.interval_move = null;
        window.poi_color_spectrum = null;
        window.poi_background_spectrum = null;
        var DirectionAttribute = Quill.import('attributors/attribute/direction');
        Quill.register(DirectionAttribute,true);
        var AlignClass = Quill.import('attributors/class/align');
        Quill.register(AlignClass,true);
        var BackgroundClass = Quill.import('attributors/class/background');
        Quill.register(BackgroundClass,true);
        var ColorClass = Quill.import('attributors/class/color');
        Quill.register(ColorClass,true);
        var DirectionClass = Quill.import('attributors/class/direction');
        Quill.register(DirectionClass,true);
        var FontClass = Quill.import('attributors/class/font');
        Quill.register(FontClass,true);
        var SizeClass = Quill.import('attributors/class/size');
        Quill.register(SizeClass,true);
        var AlignStyle = Quill.import('attributors/style/align');
        Quill.register(AlignStyle,true);
        var BackgroundStyle = Quill.import('attributors/style/background');
        Quill.register(BackgroundStyle,true);
        var ColorStyle = Quill.import('attributors/style/color');
        Quill.register(ColorStyle,true);
        var DirectionStyle = Quill.import('attributors/style/direction');
        Quill.register(DirectionStyle,true);
        var FontStyle = Quill.import('attributors/style/font');
        Quill.register(FontStyle,true);
        var SizeStyle = Quill.import('attributors/style/size');
        Quill.register(SizeStyle,true);
        $(document).ready(function () {
            $('#action_box i').tooltip();
            check_plan(window.id_user,'poi');
            if(window.can_create) {
                $('#plan_poi_msg').addClass('d-none');
            } else {
                $('#plan_poi_msg').removeClass('d-none');
            }
            get_rooms(window.id_virtualtour,'poi');
            IconPicker.Init({
                jsonUrl: 'vendor/iconpicker/iconpicker-1.5.0.json',
                searchPlaceholder: 'Search Icon',
                showAllButton: 'Show All',
                cancelButton: 'Cancel',
                noResultsFound: 'No results found.',
                borderRadius: '20px',
            });
            IconPicker.Run('#GetIconPicker', function(){
                window.pois[poi_index_edit].icon = $('#poi_icon').val();
                render_poi(window.poi_id_edit,window.poi_index_edit);
            });
            window.poi_color_spectrum = $('#poi_color').spectrum({
                type: "text",
                preferredFormat: "hex",
                showAlpha: false,
                showButtons: false,
                allowEmpty: false,
                move: function(color) {
                    window.pois[poi_index_edit].color = color.toHexString();
                    render_poi(window.poi_id_edit,window.poi_index_edit);
                },
                change: function(color) {
                    window.pois[poi_index_edit].color = color.toHexString();
                    render_poi(window.poi_id_edit,window.poi_index_edit);
                }
            });
            window.poi_background_spectrum = $('#poi_background').spectrum({
                type: "text",
                preferredFormat: "rgb",
                showAlpha: true,
                showButtons: false,
                allowEmpty: false,
                move: function(color) {
                    window.pois[poi_index_edit].background = color.toRgbString();
                    render_poi(window.poi_id_edit,window.poi_index_edit);
                },
                change: function(color) {
                    window.pois[poi_index_edit].background = color.toRgbString();
                    render_poi(window.poi_id_edit,window.poi_index_edit);
                }
            });
        });
        $('#poi_label').on('keydown change input',function () {
            var label = $('#poi_label').val();
            window.pois[window.poi_index_edit].label = label;
            render_poi(window.poi_id_edit,window.poi_index_edit);
        });
        $(window).resize(function () {
            if(window.viewer_initialized) {
                adjust_ratio_hfov('panorama_pois',window.viewer,100,100,100);
            }
        });
        $(document).mousedown(function(e) {
            var container = $("#action_box");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if(!window.is_editing) {
                    $('.custom-hotspot-content').css('opacity',1);
                }
                container.hide();
            }
        });

        $('body').on('submit','#frm_edit',function(e){
            e.preventDefault();
            $('#error_edit').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_edit[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_edit' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_edit(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_edit(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        $('#poi_content').val(evt.target.responseText);
                    }
                }
                upadte_progressbar_edit(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_edit('upload failed');
                upadte_progressbar_edit(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_edit('upload aborted');
                upadte_progressbar_edit(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_edit(value){
            $('#progressBar_edit').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_edit(error){
            $('.progress').hide();
            $('#error_edit').show();
            $('#error_edit').html(error);
        }

        $('body').on('submit','#frm_d_edit',function(e){
            e.preventDefault();
            $('#error_d_edit').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_d_edit[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_d_edit' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_d_edit(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_d_edit(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        $('#poi_content').val(evt.target.responseText);
                    }
                }
                upadte_progressbar_d_edit(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_d_edit('upload failed');
                upadte_progressbar_d_edit(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_d_edit('upload aborted');
                upadte_progressbar_d_edit(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_d_edit(value){
            $('#progressBar_d_edit').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_d_edit(error){
            $('.progress').hide();
            $('#error_d_edit').show();
            $('#error_d_edit').html(error);
        }

        $('body').on('submit','#frm_v_edit',function(e){
            e.preventDefault();
            $('#error_v_edit').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_v_edit[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_v_edit' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_v_edit(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_v_edit(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        $('#poi_content').val(evt.target.responseText);
                    }
                }
                upadte_progressbar_v_edit(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_v_edit('upload failed');
                upadte_progressbar_v_edit(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_v_edit('upload aborted');
                upadte_progressbar_v_edit(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_v_edit(value){
            $('#progressBar_v_edit').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_v_edit(error){
            $('.progress').hide();
            $('#error_v_edit').show();
            $('#error_v_edit').html(error);
        }

        $('body').on('submit','#frm_a_edit',function(e){
            e.preventDefault();
            $('#error_a_edit').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_a_edit[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_a_edit' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_a_edit(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_a_edit(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        $('#poi_content').val(evt.target.responseText);
                    }
                }
                upadte_progressbar_a_edit(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_a_edit('upload failed');
                upadte_progressbar_a_edit(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_a_edit('upload aborted');
                upadte_progressbar_a_edit(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_a_edit(value){
            $('#progressBar_a_edit').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_a_edit(error){
            $('.progress').hide();
            $('#error_a_edit').show();
            $('#error_a_edit').html(error);
        }
    })(jQuery); // End of use strict
</script>