<?php
session_start();
require_once("functions.php");
$id_map = $_GET['id'];
$map = get_map($id_map,$_SESSION['id_user']);
$id_virtualtour = $map['id_virtualtour'];
if(isset($_SESSION['id_room_point_sel'])) {
    $id_room_point_sel = $_SESSION['id_room_point_sel'];
    unset($_SESSION['id_room_point_sel']);
} else {
    $id_room_point_sel = '';
}
?>

<?php if(!$map): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
    <?php die(); endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-map-marked-alt text-gray-700"></i> EDIT MAP</h1>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Map Image <i style="font-size: 12px">(click on point to edit, drag to change position)</i></h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <p style="display: none" id="msg_load_map"><i class="fas fa-spin fa-circle-notch"></i>&nbsp; Loading map image ...</p>
                        <div style="position: relative">
                            <img id="map_image" style="width: 100%" src="" />
                            <div style="position: absolute;top:0;left:0;" id="pointers_div"></div>
                            <button data-toggle="modal" data-target="#modal_add_map_point" id="btn_add_point" style="opacity:0;position:absolute;top:10px;right:10px" class="btn btn-circle btn-primary d-inline-block float-right"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Settings</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="map_name">Name</label>
                                <input type="text" id="map_name" class="form-control" value="<?php echo $map['name']; ?>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="point_color">Point color</label>
                                <input type="text" id="point_color" class="form-control" value="<?php echo $map['point_color']; ?>" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="point_size">Point size</label>
                                <input onchange="adjust_points_size();" type="number" id="point_size" class="form-control" value="<?php echo $map['point_size']; ?>" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_save_map_name" onclick="save_map_settings();" class="btn btn-block btn-primary">SAVE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Map Point</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div id="msg_select_point" class="col-md-12">
                            <p>Select point from map or add a new one</p>
                        </div>
                        <div class="col-md-12 point_settings" style="display: none">
                            <div class="form-group">
                                <label>Room Target</label>
                                <input readonly type="text" id="room_target" class="form-control bg-white" value="" >
                            </div>
                        </div>
                        <div class="col-md-12 point_settings" style="display: none">
                            <div class="form-group">
                                <label>Position</label>
                                <input readonly type="text" id="point_pos" class="form-control bg-white" value="" >
                            </div>
                        </div>
                        <div class="col-md-12 point_settings" style="display: none">
                            <a style="pointer-events: none" id="save_btn" href="#" class="btn btn-block btn-success btn-icon-split mb-2">
                            <span class="icon text-white-50">
                              <i class="far fa-circle"></i>
                            </span>
                                <span class="text">AUTO SAVE</span>
                            </a>
                            <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_delete_point" onclick="" class="btn btn-block btn-danger">DELETE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_add_map_point" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Map Point</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Select Room</label>
                            <select id="room_select" class="form-control"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn_add_map_point" onclick="add_map_point()" type="button" class="btn btn-success">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_map_point" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Map Point</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the map point?</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_delete_map_point" onclick="" type="button" class="btn btn-danger">Yes, Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour; ?>';
        window.id_room_point_sel = '<?php echo $id_room_point_sel; ?>';
        window.map_points = null;
        window.id_room_sel = null;
        window.id_map_sel = '<?php echo $id_map; ?>';
        window.ratio_w = 1;
        window.ratio_h = 1;
        window.point_color_spectrum = null;
        window.point_size = "<?php echo $map['point_size']; ?>";
        window.map_need_save = false;
        $(document).ready(function () {
            window.point_color_spectrum = $('#point_color').spectrum({
                type: "text",
                preferredFormat: "hex",
                showAlpha: false,
                showButtons: false,
                allowEmpty: false,
                move: function(color) {
                    $('.pointer').css('background-color',color.toHexString());
                },
                change: function(color) {
                    $('.pointer').css('background-color',color.toHexString());
                }
            });
            get_map(id_virtualtour,id_map_sel);
        });
        $(window).resize(function () {
            try {
                adjust_points_position();
            } catch(e) {}
        });

        $("input").change(function(){
            window.map_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.map_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });
    })(jQuery); // End of use strict
</script>