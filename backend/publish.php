<?php
session_start();
$id_user = $_SESSION['id_user'];
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_GET['id_vt'])) {
        $id_virtualtour_sel = $_GET['id_vt'];
        $name_virtualtour_sel = get_virtual_tour($_GET['id_vt'],$id_user)['name'];
        $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
        $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
    } else {
        if(isset($_SESSION['id_virtualtour_sel'])) {
            $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
            $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
        } else {
            $id_virtualtour_sel = $virtual_tours[0]['id'];
            $name_virtualtour_sel = $virtual_tours[0]['name'];
            $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
            $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
        }
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}
$virtual_tour = get_virtual_tour($id_virtualtour_sel,$_SESSION['id_user']);
if(isset($_SERVER['REQUEST_SCHEME'])) {
    $protocol = $_SERVER['REQUEST_SCHEME'];
} else {
    $protocol = 'http';
}
$link = $protocol ."://". $_SERVER['SERVER_NAME'] . str_replace("backend/index.php","viewer/index.php?code=",$_SERVER['SCRIPT_NAME']);
$link_f = $protocol ."://". $_SERVER['SERVER_NAME'] . str_replace("backend/index.php","viewer/",$_SERVER['SCRIPT_NAME']);
$linkl = $protocol ."://". $_SERVER['SERVER_NAME'] . str_replace("backend/index.php","landing/index.php?code=",$_SERVER['SCRIPT_NAME']);
$linkl_f = $protocol ."://". $_SERVER['SERVER_NAME'] . str_replace("backend/index.php","landing/",$_SERVER['SCRIPT_NAME']);
?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-paper-plane text-gray-700"></i> PUBLISH</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Password Protection</h6>
            </div>
            <div class="card-body">
                <?php if(empty($virtual_tour['password'])) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <p>If you decide to protect your virtual tour with a password, at the first access it will be required to visit it.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="password" class="form-control bg-white" id="vt_password" value="" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button id="btn_protect" onclick="set_password_vt();" class="btn btn-success btn-block">PROTECT VIRTUAL TOUR</button>
                    </div>
                </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p>Your virtual tour is now protected with a password, click UNPROTECT to remove it.</p>
                        </div>
                        <div class="col-md-12">
                            <button id="btn_unprotect" onclick="remove_password_vt();" class="btn btn-danger btn-block">UNPROTECT VIRTUAL TOUR</button>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Status</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            Current status: <?php echo ($virtual_tour['active'] ? '<span style="color:green">Actived</span>' : '<span style="color:red">Deactivated</span>' ); ?>
                        </div>
                    </div>
                    <?php if($virtual_tour['active']) { ?>
                        <div class="col-md-12 mb-3">
                            <button id="btn_status" onclick="set_status_vt(0);" class="btn btn-danger btn-block">DEACTIVATE</button>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-12 mb-3">
                            <button id="btn_status" onclick="set_status_vt(1);" class="btn btn-success btn-block">ACTIVATE</button>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Friendly URL</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="friendly_url" value="<?php echo $virtual_tour['friendly_url']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button onclick="set_friendly_url();" id="btn_friendly_url" class="btn btn-success btn-block">SET FRIENDLY URL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Share & Embed</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="link"><i class="fas fa-link"></i> Viewer Link</label>
                            <div class="input-group">
                                <input readonly type="text" class="form-control bg-white" id="link" value="<?php echo $link . $virtual_tour['code']; ?>" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-xs" data-clipboard-target="#link">
                                        <i class="far fa-clipboard"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="link_f"><i class="fas fa-link"></i> Viewer Friendly Url Link</label>
                            <div class="input-group <?php echo ($virtual_tour['friendly_url']=='') ? 'disabled' : ''; ?>">
                                <input readonly type="text" class="form-control bg-white" id="link_f" value="<?php echo $link_f . $virtual_tour['friendly_url']; ?>" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-xs" data-clipboard-target="#link_f">
                                        <i class="far fa-clipboard"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="code"><i class="fas fa-code"></i> Viewer Embed Code</label>
                            <div class="input-group">
                                <textarea id="code" class="form-control" rows="2"><iframe allowfullscreen allow="gyroscope; accelerometer; xr; microphone *" width="1280px" height="720px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $link . $virtual_tour['code']; ?>"></iframe></textarea>
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-xs" data-clipboard-target="#code">
                                        <i class="far fa-clipboard"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="linkl"><i class="fas fa-link"></i> Landing Link</label>
                            <div class="input-group">
                                <input readonly type="text" class="form-control bg-white" id="linkl" value="<?php echo $linkl . $virtual_tour['code']; ?>" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-xs" data-clipboard-target="#linkl">
                                        <i class="far fa-clipboard"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="linkl_f"><i class="fas fa-link"></i> Landing Friendly Url Link</label>
                            <div class="input-group <?php echo ($virtual_tour['friendly_url']=='') ? 'disabled' : ''; ?>">
                                <input readonly type="text" class="form-control bg-white" id="linkl_f" value="<?php echo $linkl_f . $virtual_tour['friendly_url']; ?>" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-xs" data-clipboard-target="#linkl_f">
                                        <i class="far fa-clipboard"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        $(document).ready(function () {
            new ClipboardJS('.btn');
        });
    })(jQuery); // End of use strict
</script>