<?php
session_start();
require_once("functions.php");
$voice_commands = get_voice_commands();
?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-microphone text-gray-700"></i> VOICE COMMANDS</h1>
    <a id="save_btn" href="#" onclick="save_voice_commands();return false;" class="btn btn-success btn-icon-split mb-2 <?php echo ($demo) ? 'disabled':''; ?>">
    <span class="icon text-white-50">
      <i class="far fa-circle"></i>
    </span>
        <span class="text">SAVE</span>
    </a>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Notice</h6>
            </div>
            <div class="card-body">
                <p>Voice commands works with all browsers that implement the Speech Recognition interface of the Web Speech API.</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Language</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="language" placeholder="Enter language code" value="<?php echo $voice_commands['language']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <a href="#" data-toggle="modal" data-target="#modal_languages">
                            Languages Supported
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Commands</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="initial_msg">Welcome message</label>
                            <input type="text" class="form-control" id="initial_msg" value="<?php echo $voice_commands['initial_msg']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="listening_msg">Listening message</label>
                            <input type="text" class="form-control" id="listening_msg" value="<?php echo $voice_commands['listening_msg']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="error_msg">Error message</label>
                            <input type="text" class="form-control" id="error_msg" value="<?php echo $voice_commands['error_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="help_cmd"><b>Help command</b> (show help message)</label>
                            <input type="text" class="form-control" id="help_cmd" value="<?php echo $voice_commands['help_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="help_msg_1">Help response 1</label>
                            <input type="text" class="form-control" id="help_msg_1" value="<?php echo $voice_commands['help_msg_1']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="help_msg_2">Help response 2</label>
                            <input type="text" class="form-control" id="help_msg_2" value="<?php echo $voice_commands['help_msg_2']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="next_cmd"><b>Next command</b> (go to next room)</label>
                            <input type="text" class="form-control" id="next_cmd" value="<?php echo $voice_commands['next_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="next_msg">Next response</label>
                            <input type="text" class="form-control" id="next_msg" value="<?php echo $voice_commands['next_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="prev_cmd"><b>Prev command</b> (go to previous room)</label>
                            <input type="text" class="form-control" id="prev_cmd" value="<?php echo $voice_commands['prev_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="prev_msg">Prev response</label>
                            <input type="text" class="form-control" id="prev_msg" value="<?php echo $voice_commands['prev_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="left_cmd"><b>Left command</b> (look left)</label>
                            <input type="text" class="form-control" id="left_cmd" value="<?php echo $voice_commands['left_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="left_msg">Left response</label>
                            <input type="text" class="form-control" id="left_msg" value="<?php echo $voice_commands['left_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="right_cmd"><b>Right command</b> (look right)</label>
                            <input type="text" class="form-control" id="right_cmd" value="<?php echo $voice_commands['right_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="right_msg">Right response</label>
                            <input type="text" class="form-control" id="right_msg" value="<?php echo $voice_commands['right_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="up_cmd"><b>Up command</b> (look up)</label>
                            <input type="text" class="form-control" id="up_cmd" value="<?php echo $voice_commands['up_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="up_msg">Up response</label>
                            <input type="text" class="form-control" id="up_msg" value="<?php echo $voice_commands['up_msg']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="down_cmd"><b>Down command</b> (look down)</label>
                            <input type="text" class="form-control" id="down_cmd" value="<?php echo $voice_commands['down_cmd']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="down_msg">Down response</label>
                            <input type="text" class="form-control" id="down_msg" value="<?php echo $voice_commands['down_msg']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_languages" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Supported Languages</h5>
            </div>
            <div class="modal-body">
                Afrikaans <b>af</b> - 
                Basque <b>eu</b> - 
                Bulgarian <b>bg</b> - 
                Catalan <b>ca</b> - 
                Arabic (Egypt) <b>ar-EG</b> - 
                Arabic (Jordan) <b>ar-JO</b> - 
                Arabic (Kuwait) <b>ar-KW</b> - 
                Arabic (Lebanon) <b>ar-LB</b> - 
                Arabic (Qatar) <b>ar-QA</b> - 
                Arabic (UAE) <b>ar-AE</b> - 
                Arabic (Morocco) <b>ar-MA</b> - 
                Arabic (Iraq) <b>ar-IQ</b> - 
                Arabic (Algeria) <b>ar-DZ</b> - 
                Arabic (Bahrain) <b>ar-BH</b> - 
                Arabic (Lybia) <b>ar-LY</b> - 
                Arabic (Oman) <b>ar-OM</b> - 
                Arabic (Saudi Arabia) <b>ar-SA</b> - 
                Arabic (Tunisia) <b>ar-TN</b> - 
                Arabic (Yemen) <b>ar-YE</b> - 
                Czech <b>cs</b> - 
                Dutch <b>nl-NL</b> - 
                English (Australia) <b>en-AU</b> - 
                English (Canada) <b>en-CA</b> - 
                English (India) <b>en-IN</b> - 
                English (New Zealand) <b>en-NZ</b> - 
                English (South Africa) <b>en-ZA</b> - 
                English(UK) <b>en-GB</b> - 
                English(US) <b>en-US</b> - 
                Finnish <b>fi</b> - 
                French <b>fr-FR</b> - 
                Galician <b>gl</b> - 
                German <b>de-DE</b> - 
                Greek <b>el-GR</b> - 
                Hebrew <b>he</b> - 
                Hungarian <b>hu</b> - 
                Icelandic <b>is</b> - 
                Italian <b>it-IT</b> - 
                Indonesian <b>id</b> - 
                Japanese <b>ja</b> - 
                Korean <b>ko</b> - 
                Latin <b>la</b> - 
                Mandarin Chinese <b>zh-CN</b> - 
                Traditional Taiwan <b>zh-TW</b> - 
                Simplified China <b>zh-CN </b> - 
                Simplified Hong Kong <b>zh-HK</b> - 
                Yue Chinese (Traditional Hong Kong) <b>zh-yue</b> - 
                Malaysian <b>ms-MY</b> - 
                Norwegian <b>no-NO</b> - 
                Polish <b>pl</b> - 
                Portuguese <b>pt-PT</b> - 
                Portuguese (Brasil) <b>pt-br</b> - 
                Romanian <b>ro-RO</b> - 
                Russian <b>ru</b> - 
                Serbian <b>sr-SP</b> - 
                Slovak <b>sk</b> - 
                Spanish (Argentina) <b>es-AR</b> - 
                Spanish (Bolivia) <b>es-BO</b> - 
                Spanish (Chile) <b>es-CL</b> - 
                Spanish (Colombia) <b>es-CO</b> - 
                Spanish (Costa Rica) <b>es-CR</b> - 
                Spanish (Dominican Republic) <b>es-DO</b> - 
                Spanish (Ecuador) <b>es-EC</b> - 
                Spanish (El Salvador) <b>es-SV</b> - 
                Spanish (Guatemala) <b>es-GT</b> - 
                Spanish (Honduras) <b>es-HN</b> - 
                Spanish (Mexico) <b>es-MX</b> - 
                Spanish (Nicaragua) <b>es-NI</b> - 
                Spanish (Panama) <b>es-PA</b> - 
                Spanish (Paraguay) <b>es-PY</b> - 
                Spanish (Peru) <b>es-PE</b> - 
                Spanish (Puerto Rico) <b>es-PR</b> - 
                Spanish (Spain) <b>es-ES</b> - 
                Spanish (US) <b>es-US</b> - 
                Spanish (Uruguay) <b>es-UY</b> - 
                Spanish (Venezuela) <b>es-VE</b> - 
                Swedish <b>sv-SE</b> - 
                Turkish <b>tr</b> - 
                Zulu <b>zu</b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.voice_commands_need_save = false;

        $("input").change(function(){
            window.voice_commands_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.voice_commands_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });

    })(jQuery); // End of use strict
</script>