<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
require_once("functions.php");
$role = get_user_role($_SESSION['id_user']);
$settings = get_settings();
?>

<?php if($role!='administrator'): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
    <?php die(); endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-cogs text-gray-700"></i> SETTINGS</h1>
    <a id="save_btn" href="#" onclick="save_settings(false);return false;" class="btn btn-success btn-icon-split mb-2 <?php echo ($demo) ? 'disabled':''; ?>">
    <span class="icon text-white-50">
      <i class="far fa-circle"></i>
    </span>
        <span class="text">SAVE</span>
    </a>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">License</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="purchase_code">Purchase Code <a target="_blank" href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code-">(Where i can find it?)</a></label>
                            <input type="text" class="form-control" id="purchase_code" placeholder="Enter purchase code" value="<?php echo $settings['purchase_code']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label style="color: white">.</label>
                            <button id="btn_check_license" onclick="check_license()" class="btn btn-primary btn-block">CHECK</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Status</label><br>
                        <div id="license_status" class="mt-2">
                            <?php
                            if($settings['purchase_code']=='') {
                                echo "<i class=\"fas fa-circle\"></i> Unchecked";
                            } else {
                                if($settings['license']=='') {
                                    echo "<i style='color: red' class=\"fas fa-circle\"></i> Invalid License";
                                } else {
                                    $y0 = ''; if (array_key_exists(base64_decode('U0VSVkVSX0FERFI='), $_SERVER)) $y0 = $_SERVER[base64_decode('U0VSVkVSX0FERFI=')]; elseif (array_key_exists(base64_decode('TE9DQUxfQUREUg=='), $_SERVER)) $y0 = $_SERVER[base64_decode('TE9DQUxfQUREUg==')]; elseif (array_key_exists(base64_decode('U0VSVkVSX05BTUU='), $_SERVER)) $y0 = gethostbyname($_SERVER[base64_decode('U0VSVkVSX05BTUU=')]); else { if (stristr(PHP_OS, base64_decode('V0lO'))) { $y0 = gethostbyname(php_uname(base64_decode('bg=='))); } else { $q1 = shell_exec(base64_decode('L3NiaW4vaWZjb25maWcgZXRoMA==')); preg_match(base64_decode('L2FkZHI6KFtcZFwuXSspLw=='), $q1, $f2); $y0 = $f2[1]; } } $k4 = $settings; $q3 = $y0 . base64_decode('UlI=') . $k4[base64_decode('cHVyY2hhc2VfY29kZQ==')]; $w5 = password_verify($q3, $k4[base64_decode('bGljZW5zZQ==')]); $q3 = $y0 . base64_decode('UkU=') . $k4[base64_decode('cHVyY2hhc2VfY29kZQ==')]; $j6 = password_verify($q3, $k4[base64_decode('bGljZW5zZQ==')]); $q3 = $y0 . base64_decode('RQ==') . $k4[base64_decode('cHVyY2hhc2VfY29kZQ==')]; $u7 = password_verify($q3, $k4[base64_decode('bGljZW5zZQ==')]); if ($w5 || $j6) { echo base64_decode('PGkgc3R5bGU9J2NvbG9yOiBncmVlbicgY2xhc3M9ImZhcyBmYS1jaXJjbGUiPjwvaT4gVmFsaWQsIFJlZ3VsYXIgTGljZW5zZQ=='); } else if ($u7) { echo base64_decode('PGkgc3R5bGU9J2NvbG9yOiBncmVlbicgY2xhc3M9ImZhcyBmYS1jaXJjbGUiPjwvaT4gVmFsaWQsIEV4dGVuZGVkIExpY2Vuc2U='); } else { echo base64_decode('PGkgc3R5bGU9J2NvbG9yOiByZWQnIGNsYXNzPSJmYXMgZmEtY2lyY2xlIj48L2k+IEludmFsaWQgTGljZW5zZQ=='); }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">White Label</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Application Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter application name" value="<?php echo $settings['name']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Logo</label>
                        <div style="background-color:#4e73df;display: none" id="div_image_logo" class="col-md-12">
                            <img style="width: 100%" src="assets/<?php echo $settings['logo']; ?>" />
                        </div>
                        <div style="display: none" id="div_delete_logo" class="col-md-12 mt-4">
                            <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_b_logo();" class="btn btn-block btn-danger">DELETE IMAGE</button>
                        </div>
                        <div style="display: none" id="div_upload_logo">
                            <form id="frm" action="ajax/upload_b_logo_image.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" class="form-control" id="txtFile" name="txtFile" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload" value="Upload Logo Image" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Login image</label>
                        <div style="display: none" id="div_image_bg" class="col-md-12">
                            <img style="width: 100%" src="assets/<?php echo $settings['background']; ?>" />
                        </div>
                        <div style="display: none" id="div_delete_bg" class="col-md-12 mt-4">
                            <button <?php echo ($demo) ? 'disabled':''; ?> onclick="delete_b_bg();" class="btn btn-block btn-danger">DELETE IMAGE</button>
                        </div>
                        <div style="display: none" id="div_upload_bg">
                            <form id="frm_b" action="ajax/upload_b_background_image.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" class="form-control" id="txtFile_b" name="txtFile_b" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input <?php echo ($demo) ? 'disabled':''; ?> type="submit" class="btn btn-block btn-success" id="btnUpload_b" value="Upload Login Image" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="preview text-center">
                                            <div class="progress mb-3 mb-sm-3 mb-lg-0 mb-xl-0" style="height: 2.35rem;display: none">
                                                <div class="progress-bar" id="progressBar_b" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                    0%
                                                </div>
                                            </div>
                                            <div style="display: none;padding: .38rem;" class="alert alert-danger" id="error_b"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">General Settings</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="furl_blacklist">Friendly Urls Blacklist</label>
                            <input type="text" class="form-control" id="furl_blacklist" placeholder="Enter friendly urls separated by comma" value="<?php echo $settings['furl_blacklist']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if($settings['smtp_valid']) {
    $smtp_valid = "<i style='color: green' class=\"fas fa-circle\"></i> Valid";
} else {
    $smtp_valid = "<i style='color: red' class=\"fas fa-circle\"></i> Invalid";
}
?>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary float-left">Mail Server Settings</h6> <span id="validate_mail" class="float-right"><?php echo $smtp_valid; ?></span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="smtp_server">SMTP Server</label>
                            <input type="text" class="form-control" id="smtp_server" value="<?php echo $settings['smtp_server']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="smtp_port">SMTP Port</label>
                            <input type="number" class="form-control" id="smtp_port" value="<?php echo $settings['smtp_port']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="smtp_secure">SMTP Secure</label>
                            <select class="form-control" id="smtp_secure">
                                <option <?php echo ($settings['smtp_secure']=='none') ? 'selected':''; ?> id="none">None</option>
                                <option <?php echo ($settings['smtp_secure']=='ssl') ? 'selected':''; ?> id="ssl">SSL</option>
                                <option <?php echo ($settings['smtp_secure']=='tls') ? 'selected':''; ?> id="tls">TLS</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="smtp_auth">SMTP Auth</label><br>
                            <input <?php echo ($settings['smtp_auth']) ? 'checked':''; ?> type="checkbox" id="smtp_auth" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="smtp_username">SMTP Auth - Username</label>
                            <input type="text" class="form-control" id="smtp_username" value="<?php echo $settings['smtp_username']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="smtp_password">SMTP Auth - Password</label>
                            <input type="password" class="form-control" id="smtp_password" value="<?php echo ($settings['smtp_password']!='') ? 'keep_password' : ''; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="smtp_from_email">From E-Mail</label>
                            <input type="text" class="form-control" id="smtp_from_email" value="<?php echo $settings['smtp_from_email']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="smtp_from_name">From Name</label>
                            <input type="text" class="form-control" id="smtp_from_name" value="<?php echo $settings['smtp_from_name']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button id="btn_validate_mail" onclick="save_settings(true);" class="btn btn-primary btn-block">VALIDATE MAIL SETTINGS</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $z0='';if(array_key_exists(base64_decode('U0VSVkVSX0FERFI='),$_SERVER))$z0=$_SERVER[base64_decode('U0VSVkVSX0FERFI=')];elseif(array_key_exists(base64_decode('TE9DQUxfQUREUg=='),$_SERVER))$z0=$_SERVER[base64_decode('TE9DQUxfQUREUg==')];elseif(array_key_exists(base64_decode('U0VSVkVSX05BTUU='),$_SERVER))$z0=gethostbyname($_SERVER[base64_decode('U0VSVkVSX05BTUU=')]);else{if(stristr(PHP_OS,base64_decode('V0lO'))){$z0=gethostbyname(php_uname(base64_decode('bg==')));}else{$e1=shell_exec(base64_decode('L3NiaW4vaWZjb25maWcgZXRoMA=='));preg_match(base64_decode('L2FkZHI6KFtcZFwuXSspLw=='),$e1,$d2);$z0=$d2[1];}}$j3=$_SERVER[base64_decode('U0VSVkVSX05BTUU=')];echo"<script>window.server_name = '$j3'; window.server_ip = '$z0';</script>";?>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.settings_need_save = false;
        window.b_logo_image = '<?php echo $settings['logo']; ?>';
        window.b_background_image = '<?php echo $settings['background']; ?>';

        $(document).ready(function () {
            if(window.b_logo_image=='') {
                $('#div_delete_logo').hide();
                $('#div_image_logo').hide();
                $('#div_upload_logo').show();
            } else {
                $('#div_delete_logo').show();
                $('#div_image_logo').show();
                $('#div_upload_logo').hide();
            }
            if(window.b_background_image=='') {
                $('#div_delete_bg').hide();
                $('#div_image_bg').hide();
                $('#div_upload_bg').show();
            } else {
                $('#div_delete_bg').show();
                $('#div_image_bg').show();
                $('#div_upload_bg').hide();
            }
        });

        $('body').on('submit','#frm',function(e){
            e.preventDefault();
            $('#error').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.settings_need_save = true;
                        window.b_logo_image = evt.target.responseText;
                        $('#div_image_logo img').attr('src','assets/'+window.b_logo_image);
                        $('#div_delete_logo').show();
                        $('#div_image_logo').show();
                        $('#div_upload_logo').hide();
                    }
                }
                upadte_progressbar(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error('upload failed');
                upadte_progressbar(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error('upload aborted');
                upadte_progressbar(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar(value){
            $('#progressBar').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error(error){
            $('.progress').hide();
            $('#error').show();
            $('#error').html(error);
        }

        $('body').on('submit','#frm_b',function(e){
            e.preventDefault();
            $('#error_b').hide();
            var url = $(this).attr('action');
            var frm = $(this);
            var data = new FormData();
            if(frm.find('#txtFile_b[type="file"]').length === 1 ){
                data.append('file', frm.find( '#txtFile_b' )[0].files[0]);
            }
            var ajax  = new XMLHttpRequest();
            ajax.upload.addEventListener('progress',function(evt){
                var percentage = (evt.loaded/evt.total)*100;
                upadte_progressbar_b(Math.round(percentage));
            },false);
            ajax.addEventListener('load',function(evt){
                if(evt.target.responseText.toLowerCase().indexOf('error')>=0){
                    show_error_b(evt.target.responseText);
                } else {
                    if(evt.target.responseText!='') {
                        window.settings_need_save = true;
                        window.b_background_image = evt.target.responseText;
                        $('#div_image_bg img').attr('src','assets/'+window.b_background_image);
                        $('#div_delete_bg').show();
                        $('#div_image_bg').show();
                        $('#div_upload_bg').hide();
                    }
                }
                upadte_progressbar_b(0);
                frm[0].reset();
            },false);
            ajax.addEventListener('error',function(evt){
                show_error_b('upload failed');
                upadte_progressbar_b(0);
            },false);
            ajax.addEventListener('abort',function(evt){
                show_error_b('upload aborted');
                upadte_progressbar_b(0);
            },false);
            ajax.open('POST',url);
            ajax.send(data);
            return false;
        });

        function upadte_progressbar_b(value){
            $('#progressBar_b').css('width',value+'%').html(value+'%');
            if(value==0){
                $('.progress').hide();
            }else{
                $('.progress').show();
            }
        }

        function show_error_b(error){
            $('.progress').hide();
            $('#error_b').show();
            $('#error_b').html(error);
        }

        $("input").change(function(){
            window.settings_need_save = true;
        });

        $(window).on('beforeunload', function(){
            if(window.settings_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });

    })(jQuery); // End of use strict
</script>