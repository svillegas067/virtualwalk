<?php
session_start();
$id_user = $_SESSION['id_user'];
$can_create = check_plan('virtual_tour',$id_user);
?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-route text-gray-700"></i> VIRTUAL TOURS</h1>
</div>
<div class="row">
    <div class="col-md-12">
        <?php if($user_info['plan_status']=='active') { ?>
            <?php if($can_create) { ?>
            <div class="card mb-4 py-3 border-left-success">
                <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                    <div class="row">
                        <div class="col-md-8 text-center text-sm-center text-md-left text-lg-left">
                            <span>CREATE NEW VIRTUAL TOUR</span>
                        </div>
                        <div class="col-md-4 text-center text-sm-center text-md-right text-lg-right">
                            <a href="#" data-toggle="modal" data-target="#modal_new_virtualtour" class="btn btn-success btn-circle">
                                <i class="fas fa-plus-circle"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else { ?>
                <div class="card bg-warning text-white shadow mb-4">
                    <div class="card-body">
                        You have reached the maximum number of Virtual Tours allowed from your plan!
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <div class="card bg-warning text-white shadow mb-4">
                <div class="card-body">
                    Your "<?php echo $user_info['plan'] ?>" plan has expired!
                </div>
            </div>
        <?php } ?>
        <div id="virtual_tours_list">
            <div class="card mb-4 py-3 border-left-primary">
                <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                    <div class="row">
                        <div class="col-md-8 text-center text-sm-center text-md-left text-lg-left">
                            LOADING VIRTUAL TOURS ...
                        </div>
                        <div class="col-md-4 text-center text-sm-center text-md-right text-lg-right">
                            <a href="#" class="btn btn-primary btn-circle">
                                <i class="fas fa-spin fa-spinner"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_new_virtualtour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Virtual Tour</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter virtual tour name" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Author</label>
                            <input type="text" class="form-control" id="author" placeholder="Enter author" value="<?php echo $user_info['username']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> onclick="add_virtualtour();" type="button" class="btn btn-success">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_virtualtour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Virtual Tour</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the entire virtual tour, included rooms, markers, pois and map?</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_delete_virtualtour" onclick="" type="button" class="btn btn-danger">Yes, Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        $(document).ready(function () {
            get_virtual_tours();
        });
    })(jQuery); // End of use strict
</script>