<?php
require_once(__DIR__."/../db/connection.php");

function get_user_info($id_user) {
    global $mysqli;
    $return = array();
    $query = "SELECT u.*,p.name as plan FROM svt_users as u LEFT JOIN svt_plans as p ON p.id=u.id_plan WHERE u.id = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            if($row['expire_plan_date']==null) {
                $row['plan_status']='active';
            } else {
                if (new DateTime() > new DateTime($row['expire_plan_date'])) {
                    $row['plan_status']='expired';
                } else{
                    $row['plan_status']='active';
                }
            }
            $return=$row;
        }
    }
    return $return;
}

function get_user_role($id_user) {
    global $mysqli;
    $return = 'user';
    $query = "SELECT role FROM svt_users WHERE id = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $return=$row['role'];
        }
    }
    return $return;
}

function get_virtual_tour($id_virtual_tour,$id_user) {
    global $mysqli;
    $return = array();
    $query = "SELECT * FROM svt_virtualtours WHERE id = $id_virtual_tour LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $id_user_vt = $row['id_user'];
            if(get_user_role($id_user)!='administrator') {
                if($id_user!=$id_user_vt) return false;
            }
            $return=$row;
        }
    }
    return $return;
}

function get_virtual_tours($id_user) {
    global $mysqli;
    $return = array();
    if(get_user_role($id_user)=='administrator') {
        $where_user = "";
    } else {
        $where_user = " WHERE id_user = $id_user ";
    }
    $query = "SELECT * FROM svt_virtualtours $where_user ORDER BY date_created DESC;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $return[]=$row;
            }
        }
    }
    return $return;
}

function get_room($id_room,$id_user) {
    global $mysqli;
    $return = array();
    $query = "SELECT r.*,v.id_user,m.map,m.point_size FROM svt_rooms as r
            JOIN svt_virtualtours as v ON r.id_virtualtour=v.id
            LEFT JOIN svt_maps as m ON m.id=r.id_map
            WHERE r.id = $id_room LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $id_user_vt = $row['id_user'];
            if(get_user_role($id_user)!='administrator') {
                if($id_user!=$id_user_vt) return false;
            }
            $return=$row;
        }
    }
    return $return;
}

function get_map($id_map,$id_user) {
    global $mysqli;
    $return = array();
    $query = "SELECT m.*,v.id_user FROM svt_maps as m
            JOIN svt_virtualtours as v ON m.id_virtualtour=v.id
            WHERE m.id = $id_map LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $id_user_vt = $row['id_user'];
            if(get_user_role($id_user)!='administrator') {
                if($id_user!=$id_user_vt) return false;
            }
            $return=$row;
        }
    }
    return $return;
}

function get_rooms_option($id_virtualtour) {
    global $mysqli;
    $options = "";
    $query = "SELECT id,name FROM svt_rooms WHERE id_virtualtour=$id_virtualtour;";
    $result = $mysqli->query($query);
    if($result) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $id = $row['id'];
            $name = $row['name'];
            $options .= "<option id='$id'>$name</option>";
        }
    }
    return $options;
}

function get_plans_options($id_plan_sel) {
    global $mysqli;
    $options = "";
    $query = "SELECT * FROM svt_plans;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $id = $row['id'];
                $name = $row['name'];
                if($id_plan_sel==$id) {
                    $options .= "<option selected id='$id'>$name</option>";
                } else {
                    $options .= "<option id='$id'>$name</option>";
                }
            }
        }
    }
    return $options;
}

function check_plan($object,$id_user) {
    global $mysqli;
    switch($object) {
        case 'virtual_tour':
            $count_virtual_tours = 0;
            $plan_virtual_tours = -1;
            $query = "SELECT COUNT(*) as num FROM svt_virtualtours WHERE id_user = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $count_virtual_tours = $row['num'];
                }
            }
            $query = "SELECT n_virtual_tours FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $plan_virtual_tours = $row['n_virtual_tours'];
                }
            }
            $can_create = false;
            if($plan_virtual_tours<0) {
                $can_create = true;
            } else {
                if($count_virtual_tours>=$plan_virtual_tours) {
                    $can_create = false;
                } else {
                    $can_create = true;
                }
            }
            return $can_create;
            break;
        case 'room':
            $count_rooms = 0;
            $plan_rooms = -1;
            $query = "SELECT COUNT(*) as num FROM svt_rooms as r
                        JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                        WHERE id_user = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $count_rooms = $row['num'];
                }
            }
            $query = "SELECT n_rooms FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $plan_rooms = $row['n_rooms'];
                }
            }
            $can_create = false;
            if($plan_rooms<0) {
                $can_create = true;
            } else {
                if($count_rooms>=$plan_rooms) {
                    $can_create = false;
                } else {
                    $can_create = true;
                }
            }
            return $can_create;
            break;
        case 'marker':
            $count_markers = 0;
            $plan_markers = -1;
            $query = "SELECT COUNT(*) as num FROM svt_markers as m
                        JOIN svt_rooms as r ON m.id_room = r.id
                        JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                        WHERE id_user = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $count_markers = $row['num'];
                }
            }
            $query = "SELECT n_markers FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $plan_markers = $row['n_markers'];
                }
            }
            $can_create = false;
            if($plan_markers<0) {
                $can_create = true;
            } else {
                if($count_markers>=$plan_markers) {
                    $can_create = false;
                } else {
                    $can_create = true;
                }
            }
            return $can_create;
            break;
        case 'poi':
            $count_pois = 0;
            $plan_pois = -1;
            $query = "SELECT COUNT(*) as num FROM svt_pois as m
                        JOIN svt_rooms as r ON m.id_room = r.id
                        JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                        WHERE id_user = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $count_pois = $row['num'];
                }
            }
            $query = "SELECT n_pois FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $plan_pois = $row['n_pois'];
                }
            }
            $can_create = false;
            if($plan_pois<0) {
                $can_create = true;
            } else {
                if($count_pois>=$plan_pois) {
                    $can_create = false;
                } else {
                    $can_create = true;
                }
            }
            return $can_create;
            break;
        case 'landing':
            $can_create = false;
            $query = "SELECT create_landing FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
            $result = $mysqli->query($query);
            if($result) {
                if($result->num_rows==1) {
                    $row=$result->fetch_array(MYSQLI_ASSOC);
                    $create_landing = $row['create_landing'];
                    if($create_landing==1) $can_create=true;
                }
            }
            return $can_create;
            break;
        default:
            return false;
            break;
    }
}

function check_plan_rooms_count($id_user) {
    global $mysqli;
    $rooms_count_create = 0;
    $count_rooms = 0;
    $plan_rooms = -1;
    $query = "SELECT COUNT(*) as num FROM svt_rooms as r
                        JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                        WHERE id_user = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $count_rooms = $row['num'];
        }
    }
    $query = "SELECT n_rooms FROM svt_plans as p LEFT JOIN svt_users AS u ON u.id_plan=p.id WHERE u.id = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $plan_rooms = $row['n_rooms'];
        }
    }
    if($plan_rooms<0) {
        $rooms_count_create = -1;
    } else {
        $rooms_count_create = $plan_rooms-$count_rooms;
    }
    return $rooms_count_create;
}

function get_voice_commands() {
    global $mysqli;
    $return = array();
    $query = "SELECT * FROM svt_voice_commands LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $return=$row;
        }
    }
    return $return;
}

function get_settings() {
    global $mysqli;
    $return = array();
    $query = "SELECT * FROM svt_settings LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $return=$row;
        }
    }
    return $return;
}

function get_library_icons($id_virtualtour,$p) {
    global $mysqli;
    $return = "";
    $query = "SELECT * FROM svt_icons WHERE id_virtualtour=$id_virtualtour;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $id = $row['id'];
                $image = $row['image'];
                $return .= "<img onclick='select_icon_library(\"$p\",$id,\"$image\");' style='display: inline-block;width:50px;padding: 3px;' src='../viewer/icons/$image' />";
            }
        } else {
            $return = "No icons in that library.";
        }
    }
    return $return;
}

function get_option_exist_logo($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT logo FROM svt_virtualtours WHERE id_user=$id_user AND logo!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $logo = $row['logo'];
                $return .= "<option id='$logo'>$logo</option>";
            }
        }
    }
    return $return;
}

function get_option_exist_nadir_logo($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT nadir_logo FROM svt_virtualtours WHERE id_user=$id_user AND nadir_logo!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $nadir_logo = $row['nadir_logo'];
                $return .= "<option id='$nadir_logo'>$nadir_logo</option>";
            }
        }
    }
    return $return;
}

function get_option_exist_background_logo($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT background_image FROM svt_virtualtours WHERE id_user=$id_user AND background_image!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $background_image = $row['background_image'];
                $return .= "<option id='$background_image'>$background_image</option>";
            }
        }
    }
    return $return;
}

function get_option_exist_song($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT song FROM svt_virtualtours WHERE id_user=$id_user AND song!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $song = $row['song'];
                $return .= "<option id='$song'>$song</option>";
            }
        }
    }
    return $return;
}

function get_option_exist_introd($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT intro_desktop FROM svt_virtualtours WHERE id_user=$id_user AND intro_desktop!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $intro_desktop = $row['intro_desktop'];
                $return .= "<option id='$intro_desktop'>$intro_desktop</option>";
            }
        }
    }
    return $return;
}

function get_option_exist_introm($id_user) {
    global $mysqli;
    $return = "";
    $query = "SELECT DISTINCT intro_mobile FROM svt_virtualtours WHERE id_user=$id_user AND intro_mobile!='';";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $intro_mobile = $row['intro_mobile'];
                $return .= "<option id='$intro_mobile'>$intro_mobile</option>";
            }
        }
    }
    return $return;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function update_plans_expires_date() {
    global $mysqli;
    $query = "SELECT u.id,u.registration_date,p.days FROM svt_users as u
                JOIN svt_plans as p ON p.id=u.id_plan";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows>0) {
            while($row=$result->fetch_array(MYSQLI_ASSOC)) {
                $id_user = $row['id'];
                $reg_date = $row['registration_date'];
                $days = $row['days'];
                if($days<0) {
                    $mysqli->query("UPDATE svt_users SET expire_plan_date=NULL WHERE id=$id_user;");
                } else {
                    $exp_date = date('Y-m-d H:i:s', strtotime($reg_date. " + $days days"));
                    $mysqli->query("UPDATE svt_users SET expire_plan_date='$exp_date' WHERE id=$id_user;");
                }
            }
        }
    }
}

function get_user_stats($id_user) {
    global $mysqli;
    $stats = array();
    $stats['count_virtual_tours'] = 0;
    $stats['count_rooms'] = 0;
    $stats['count_markers'] = 0;
    $stats['count_pois'] = 0;
    $query = "SELECT COUNT(*) as num FROM svt_virtualtours WHERE id_user = $id_user LIMIT 1";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $num = $row['num'];
            $stats['count_virtual_tours'] = $num;
        }
    }
    $query = "SELECT COUNT(*) as num FROM svt_rooms as r
                JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                WHERE id_user = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $num = $row['num'];
            $stats['count_rooms'] = $num;
        }
    }
    $query = "SELECT COUNT(*) as num FROM svt_markers as m
                JOIN svt_rooms as r ON m.id_room = r.id
                JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                WHERE id_user = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $num = $row['num'];
            $stats['count_markers'] = $num;
        }
    }
    $query = "SELECT COUNT(*) as num FROM svt_pois as m
                JOIN svt_rooms as r ON m.id_room = r.id
                JOIN svt_virtualtours as v ON v.id = r.id_virtualtour
                WHERE id_user = $id_user LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $num = $row['num'];
            $stats['count_pois'] = $num;
        }
    }
    return $stats;
}

function get_plan($id) {
    global $mysqli;
    $return = array();
    $query = "SELECT * FROM svt_plans WHERE id=$id LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $return=$row;
        }
    }
    return $return;
}

function dateDiffInDays($date1, $date2) {
    $diff = strtotime($date2) - strtotime($date1);
    return abs(round($diff / 86400));
}