<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="planDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="nav-link text-gray-600 small"><i style="line-height: unset;margin-bottom: 2px" class="fas fa-crown fa-sm fa-fw mr-1"></i> <span style="vertical-align: middle;"><?php echo $user_info['plan']; ?></span></span>
            </a>
            <div style="cursor: default" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="planDropdown">
                <h6 class="dropdown-header">
                    Current Plan: <?php echo $user_info['plan']; ?>
                </h6>
                <div class="dropdown-item">
                    Status:&nbsp;<?php
                    switch($user_info['plan_status']) {
                        case 'active':
                            $expires_msg = "";
                            if($user_info['expire_plan_date']) {
                                $diff_days = dateDiffInDays($user_info['expire_plan_date'],date('Y-m-d H:i:s'));
                                $expires_msg = "- expires in $diff_days days";
                            }
                            echo "<span style='color:green'><b>Active</b></span> $expires_msg";
                            break;
                        case 'expired':
                            echo "<span style='color:red'><b>Expired</b></span>";
                            break;
                    }
                    ?>
                </div>
                <div class="dropdown-item">
                    Virtual Tours:&nbsp;<?php echo $user_stats['count_virtual_tours']." of&nbsp;".(($plan_info['n_virtual_tours']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_virtual_tours']).'</b>'; ?><br>
                    Rooms:&nbsp;<?php echo $user_stats['count_rooms']." of&nbsp;".(($plan_info['n_rooms']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_rooms']).'</b>'; ?><br>
                    Markers:&nbsp;<?php echo $user_stats['count_markers']." of&nbsp;".(($plan_info['n_markers']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_markers']).'</b>'; ?><br>
                    POIs:&nbsp;<?php echo $user_stats['count_pois']." of&nbsp;".(($plan_info['n_pois']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_pois']).'</b>'; ?><br>
                </div>
            </div>
        </li>
        <div class="topbar-divider d-none d-sm-block"></div>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $user_info['username']; ?></span>&nbsp;
                <img class="img-profile rounded-circle" src="img/avatar1.png">
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?p=edit_profile">
                    <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                    Edit profile
                </a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" onclick="logout();">Logout</button>
            </div>
        </div>
    </div>
</div>