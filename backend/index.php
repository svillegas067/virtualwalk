<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
if(!file_exists("../config/config.inc.php")) {
    header("Location: ../install/start.php");
}
session_start();
$version = "3.7";
$v = time();
if(!isset($_SESSION["check_update_$version"])) {
    require_once("../services/check_update.php");
    require_once("../services/generate_thumb.php");
    require_once("../services/generate_pano_mobile.php");
    $z0='';if(array_key_exists(base64_decode('U0VSVkVSX0FERFI='),$_SERVER))$z0=$_SERVER[base64_decode('U0VSVkVSX0FERFI=')];elseif(array_key_exists(base64_decode('TE9DQUxfQUREUg=='),$_SERVER))$z0=$_SERVER[base64_decode('TE9DQUxfQUREUg==')];elseif(array_key_exists(base64_decode('U0VSVkVSX05BTUU='),$_SERVER))$z0=gethostbyname($_SERVER[base64_decode('U0VSVkVSX05BTUU=')]);else{if(stristr(PHP_OS,base64_decode('V0lO'))){$z0=gethostbyname(php_uname(base64_decode('bg==')));}else{$b1=shell_exec(base64_decode('L3NiaW4vaWZjb25maWcgZXRoMA=='));preg_match(base64_decode('L2FkZHI6KFtcZFwuXSspLw=='),$b1,$e2);$z0=$e2[1];}}$a3=$_SERVER[base64_decode('U0VSVkVSX05BTUU=')];$i4=$_SERVER[base64_decode('UkVRVUVTVF9VUkk=')];$j5=@file_get_contents("https://simpledemo.it/get_latest_svt_version.php?domain=$a3&ip=$z0&version=$version&request_uri=$i4");if($j5){$_SESSION[base64_decode('bGF0ZXN0X3ZlcnNpb24=')]=$j5;}else{$_SESSION[base64_decode('bGF0ZXN0X3ZlcnNpb24=')]=$version;}
    $_SESSION["check_update_$version"] = false;
}
if($_SESSION['latest_version']=="") {
    $_SESSION['latest_version'] = $version;
}
$latest_version = $_SESSION['latest_version'];
require_once("functions.php");
$settings = get_settings();
if(isset($_SESSION['id_user'])) {
    $id_user = $_SESSION['id_user'];
} else {
    header("Location:login.php");
}
$user_info = get_user_info($id_user);
$user_stats = get_user_stats($id_user);
$plan_info = get_plan($user_info['id_plan']);
if(isset($_GET['p'])) {
    $page = $_GET['p'];
} else {
    $page = "dashboard";
}
$_SESSION['ip_developer']='87.8.67.36';
if(($_SERVER['SERVER_ADDR']=='5.9.136.4') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    $demo = true;
    if(!isset($_SESSION['id_virtualtour_sel'])) {
        $_SESSION['id_virtualtour_sel'] = 1;
        $_SESSION['name_virtualtour_sel'] = 'Simple Virtual Tour';
    }
} else {
    $demo = false;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $settings['name']; ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="../viewer/vendor/fontawesome-free/css/all.min.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" type="text/css" href="css/sb-admin-2.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="../viewer/css/pannellum.css">
    <link rel="stylesheet" type="text/css" href="vendor/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/iconpicker/iconpicker-1.5.0.css">
    <link rel="stylesheet" type="text/css" href="vendor/quill/quill.core.css">
    <link rel="stylesheet" type="text/css" href="vendor/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="vendor/quill/quill.bubble.css">
    <link rel="stylesheet" type="text/css" href="vendor/dropzone/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/dropzone/basic.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/spectrum/spectrum.min.css">
    <link rel="stylesheet" type="text/css" href="../viewer/vendor/tooltipster/css/tooltipster.bundle.min.css">
    <link rel="stylesheet" type="text/css" href="../viewer/vendor/tooltipster/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css">
    <link rel="stylesheet" type="text/css" href="../viewer/vendor/videojs/video-js.css">
    <link rel="stylesheet" type="text/css" href="vendor/Nestable2/jquery.nestable.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css?v=<?php echo $v; ?>">
    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script type="text/javascript" src="vendor/slick/slick.min.js"></script>
    <script type="text/javascript" src="../viewer/js/libpannellum.js?v=<?php echo $v; ?>"></script>
    <script type="text/javascript" src="../viewer/js/pannellum.js?v=<?php echo $v; ?>"></script>
    <script type="text/javascript" src="../viewer/vendor/videojs/video.js"></script>
    <script type="text/javascript" src="../viewer/js/videojs-pannellum-plugin.js?v=<?php echo $v; ?>"></script>
    <script type="text/javascript" src="vendor/clipboard.js/clipboard.min.js"></script>
    <script type="text/javascript" src="vendor/iconpicker/iconpicker-1.5.0.js"></script>
    <script type="text/javascript" src="vendor/quill/quill.core.js"></script>
    <script type="text/javascript" src="vendor/quill/quill.js"></script>
    <script type="text/javascript" src="vendor/quill/quill.min.js"></script>
    <script type="text/javascript" src="vendor/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="vendor/Sortable.min.js"></script>
    <script type="text/javascript" src="vendor/spectrum/spectrum.min.js"></script>
    <script type="text/javascript" src="../viewer/vendor/tooltipster/js/tooltipster.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/chart.js/Chart.min.js"></script>
    <script type="text/javascript" src="../viewer/js/mobile-detect.min.js"></script>
    <script type="text/javascript" src="vendor/Nestable2/jquery.nestable.min.js"></script>
    <script type="text/javascript" src="js/function.js?v=<?php echo $v; ?>"></script>
</head>

<body id="page-top">
  <div id="wrapper">
      <?php include_once("sidebar.php"); ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
          <?php include_once("topbar.php"); ?>
        <div class="container-fluid">
            <?php if($demo) : ?>
                <div class="card bg-danger text-white shadow mb-3">
                    <div class="card-body">
                        DEMO MODE: Add / Save functionality are disabled.
                    </div>
                </div>
            <?php endif; ?>
            <?php include_once("$page.php"); ?>
        </div>
      </div>
        <?php include_once("footer.php"); ?>
    </div>
  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="js/sb-admin-2.js?v=4"></script>
</body>
</html>
