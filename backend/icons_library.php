<?php
session_start();
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_GET['id_vt'])) {
        $id_virtualtour_sel = $_GET['id_vt'];
        $name_virtualtour_sel = get_virtual_tour($_GET['id_vt'],$id_user)['name'];
        $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
        $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
    } else {
        if(isset($_SESSION['id_virtualtour_sel'])) {
            $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
            $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
        } else {
            $id_virtualtour_sel = $virtual_tours[0]['id'];
            $name_virtualtour_sel = $virtual_tours[0]['name'];
        }
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}
?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-icons text-gray-700"></i> ICONS LIBRARY</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="ajax/upload_icon_image.php" class="dropzone" id="gallery-dropzone"></form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List Icons</h6>
            </div>
            <div class="card-body">
                <div id="list_images">
                    <p>Loading ...</p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            get_icon_images(id_virtualtour);
            var gallery_dropzone = new Dropzone("#gallery-dropzone", {
                    url: "ajax/upload_icon_image.php",
                    parallelUploads: 1,
                    maxFilesize: 20,
                    timeout: 120000,
                    acceptedFiles: 'image/*'
                });
            gallery_dropzone.on("addedfile", function(file) {
                $('#list_images').addClass('disabled');
            });
            gallery_dropzone.on("success", function(file,rsp) {
                add_image_to_icon(id_virtualtour,rsp);
            });
            gallery_dropzone.on("queuecomplete", function() {
                $('#list_images').removeClass('disabled');
                gallery_dropzone.removeAllFiles();
            });
        });
    })(jQuery); // End of use strict
</script>