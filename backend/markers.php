<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
$id_user = $_SESSION['id_user'];
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_SESSION['id_virtualtour_sel'])) {
        $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
        $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
    } else {
        $id_virtualtour_sel = $virtual_tours[0]['id'];
        $name_virtualtour_sel = $virtual_tours[0]['name'];
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}

if(isset($_GET['id_room'])) {
    $id_room = $_GET['id_room'];
} else {
    $id_room = 0;
}

$can_create = check_plan('marker', $id_user);
$virtual_tour = get_virtual_tour($id_virtualtour_sel,$id_user);
?>

<?php if($user_info['plan_status']=='expired') : ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php exit; endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-caret-square-up text-gray-700"></i> MARKERS</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div id="plan_marker_msg" class="card bg-warning text-white shadow mb-4 d-none">
    <div class="card-body">
        You have reached the maximum number of Markers allowed from your plan!
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rooms List <i style="font-size: 12px">(click on room to select)</i></h6>
            </div>
            <div class="card-body ml-3 mr-3">
                <div class="col-md-12">
                    <div class="rooms_slider">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Markers <i style="font-size: 12px">(click on marker to edit)</i></h6>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <p id="msg_sel_room">Select a room first!</p>
                    <div class="ml-3 mr-3" style="position: relative">
                        <div id="panorama_markers"></div>
                        <div id="action_box">
                            <i title="MOVE" onclick="" class="move_action fa fa-arrows-alt"></i>
                            <i title="EDIT" onclick="" class="edit_action fa fa-edit"></i>
                            <i title="STYLE" onclick="" class="style_action fas fa-palette"></i>
                            <i title="DELETE" onclick="" class="delete_action fa fa-trash"></i>
                        </div>
                        <div id="confirm_style">
                            <div class="row">
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="markers_style">Style</label>
                                        <select onchange="change_marker_style()" id="marker_style" class="form-control">
                                            <option id="1">Icon + Room's Name</option>
                                            <option id="2">Room's Name + Icon</option>
                                            <option id="0">Only Icon</option>
                                            <option id="3">Only Room's Name</option>
                                            <option id="4">Custom Icons Library</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="marker_icon">Icon</label><br>
                                        <button class="btn btn-sm btn-primary" type="button" id="GetIconPicker" data-iconpicker-input="input#marker_icon" data-iconpicker-preview="i#marker_icon_preview">Select Icon</button>
                                        <input readonly type="hidden" id="marker_icon" name="Icon" value="fas fa-image" required="" placeholder="" autocomplete="off" spellcheck="false">
                                        <div style="vertical-align: middle;" class="icon-preview d-inline-block ml-1" data-toggle="tooltip" title="">
                                            <i style="font-size: 24px;" id="marker_icon_preview" class="fas fa-image"></i>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none" class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="marker_library_icon">Library Icon</label><br>
                                        <button data-toggle="modal" data-target="#modal_library_icons" class="btn btn-sm btn-primary" type="button" id="btn_library_icon">Select Library Icon</button>
                                        <input type="hidden" id="marker_library_icon" value="0" />
                                        <img id="marker_library_icon_preview" style="display: none;height: 30px" src="" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="marker_color">Color</label>
                                        <input type="text" id="marker_color" class="form-control" value="#000000" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="margin-bottom: 5px;" class="form-group">
                                        <label for="marker_background">Background</label>
                                        <input type="text" id="marker_background" class="form-control" value="rgba(255,255,255,0.7)" />
                                    </div>
                                </div>
                            </div>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <div id="confirm_edit">
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label>Room Target</label>
                                <select onchange="" id="room_target" class="form-control"></select>
                            </div>
                            <div class="col-md-12 text-center">
                                <img style="display: none" class="preview_room_target" src="" />
                            </div>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <div id="confirm_move">
                            <b>drag view to change position</b><br>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label style="margin-bottom: 0;">Perspective</label>
                                <input oninput="" type="range" min="0" max="70" class="form-control-range" id="rotateX">
                                <input oninput="" type="range" min="-180" max="180" class="form-control-range" id="rotateZ">
                            </div>
                            <div style="margin-bottom: 5px;" class="form-group">
                                <label style="margin-bottom: 0;">Size</label>
                                <input oninput="" type="range" step="0.1" min="0.5" max="1.5" class="form-control-range" id="size_scale">
                            </div>
                            <i class="btn_confirm fas fa-check-circle"></i>
                        </div>
                        <button id="btn_add_marker" style="opacity:0;position:absolute;top:10px;right:10px;z-index:10;" class="btn btn-circle btn-primary d-inline-block float-right"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_add_marker" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Marker</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Room Target</label>
                        <select onchange="" id="room_target_add" class="form-control"></select>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <img style="display: none" class="preview_room_target" src="" />
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_new_marker" onclick="" type="button" class="btn btn-success">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_marker" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Marker</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete the marker?</p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_delete_marker" onclick="" type="button" class="btn btn-danger">Yes, Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_library_icons" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Library Icons</h5>
            </div>
            <div class="modal-body">
                <?php echo get_library_icons($id_virtualtour_sel,'marker'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_room_marker = <?php echo $id_room; ?>;
        window.id_room_sel = null;
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        window.markers = null;
        window.rooms_count = 0;
        window.can_create = false;
        window.viewer_initialized = false;
        window.viewer = null;
        window.interval_move = null;
        window.is_editing = false;
        window.marker_index_edit = null;
        window.marker_id_edit = null;
        window.panorama_image = '';
        window.currentYaw = 0;
        window.currentPitch = 0;
        $(document).ready(function () {
            $('#action_box i').tooltip();
            check_plan(window.id_user,'marker');
            if(window.can_create) {
                $('#plan_marker_msg').addClass('d-none');
            } else {
                $('#plan_marker_msg').removeClass('d-none');
            }
            get_rooms(window.id_virtualtour,'marker');
            IconPicker.Init({
                jsonUrl: 'vendor/iconpicker/iconpicker-1.5.0.json',
                searchPlaceholder: 'Search Icon',
                showAllButton: 'Show All',
                cancelButton: 'Cancel',
                noResultsFound: 'No results found.',
                borderRadius: '20px',
            });
            IconPicker.Run('#GetIconPicker', function(){
                window.markers[marker_index_edit].icon = $('#marker_icon').val();
                render_marker(window.marker_id_edit,window.marker_index_edit);
            });
            window.marker_color_spectrum = $('#marker_color').spectrum({
                type: "text",
                preferredFormat: "hex",
                showAlpha: false,
                showButtons: false,
                allowEmpty: false,
                move: function(color) {
                    window.markers[marker_index_edit].color = color.toHexString();
                    render_marker(window.marker_id_edit,window.marker_index_edit);
                },
                change: function(color) {
                    window.markers[marker_index_edit].color = color.toHexString();
                    render_marker(window.marker_id_edit,window.marker_index_edit);
                }
            });
            window.marker_background_spectrum = $('#marker_background').spectrum({
                type: "text",
                preferredFormat: "rgb",
                showAlpha: true,
                showButtons: false,
                allowEmpty: false,
                move: function(color) {
                    window.markers[marker_index_edit].background = color.toRgbString();
                    render_marker(window.marker_id_edit,window.marker_index_edit);
                },
                change: function(color) {
                    window.markers[marker_index_edit].background = color.toRgbString();
                    render_marker(window.marker_id_edit,window.marker_index_edit);
                }
            });
        });
        $(window).resize(function () {
            if(window.viewer_initialized) {
                adjust_ratio_hfov('panorama_markers',window.viewer,100,100,100);
            }
        });
        $(document).mousedown(function(e) {
            var container = $("#action_box");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if(!window.is_editing) {
                    $('.custom-hotspot').css('opacity',1);
                }
                container.hide();
            }
        });
    })(jQuery); // End of use strict
</script>