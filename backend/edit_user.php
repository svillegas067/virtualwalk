<?php
session_start();
require_once("functions.php");
$role = get_user_role($_SESSION['id_user']);
$id_user_edit = $_GET['id'];
$user_info_edit = get_user_info($id_user_edit);
$user_stats_edit = get_user_stats($id_user_edit);
$r0='';if(array_key_exists(base64_decode('U0VSVkVSX0FERFI='),$_SERVER))$r0=$_SERVER[base64_decode('U0VSVkVSX0FERFI=')];elseif(array_key_exists(base64_decode('TE9DQUxfQUREUg=='),$_SERVER))$r0=$_SERVER[base64_decode('TE9DQUxfQUREUg==')];elseif(array_key_exists(base64_decode('U0VSVkVSX05BTUU='),$_SERVER))$r0=gethostbyname($_SERVER[base64_decode('U0VSVkVSX05BTUU=')]);else{if(stristr(PHP_OS,base64_decode('V0lO'))){$r0=gethostbyname(php_uname(base64_decode('bg==')));}else{$u1=shell_exec(base64_decode('L3NiaW4vaWZjb25maWcgZXRoMA=='));preg_match(base64_decode('L2FkZHI6KFtcZFwuXSspLw=='),$u1,$a2);$r0=$a2[1];}}echo base64_decode('PGlucHV0IHR5cGU9J2hpZGRlbicgaWQ9J3ZsZmMnIC8+');$v3=get_settings();$o5=$r0.base64_decode('UlI=').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$v6=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);$o5=$r0.base64_decode('UkU=').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$w7=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);$o5=$r0.base64_decode('RQ==').$v3[base64_decode('cHVyY2hhc2VfY29kZQ==')];$r8=password_verify($o5,$v3[base64_decode('bGljZW5zZQ==')]);if($v6){include(base64_decode('bGljZW5zZS5waHA='));exit;}else if(($r8)||($w7)){}else{include(base64_decode('bGljZW5zZS5waHA='));exit;}
?>

<?php if(($role!='administrator') || (count($user_info_edit)==0)): ?>
    <div class="text-center">
        <div class="error mx-auto" data-text="401">401</div>
        <p class="lead text-gray-800 mb-5">Permission denied</p>
        <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
        <a href="index.php?p=dashboard">← Back to Dashboard</a>
    </div>
<?php die(); endif; ?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-user text-gray-700"></i> EDIT USER</h1>
    <a id="save_btn" href="#" onclick="save_user(<?php echo $id_user_edit; ?>);return false;" class="btn btn-success btn-icon-split mb-2 <?php echo ($demo) ? 'disabled':''; ?>">
    <span class="icon text-white-50">
      <i class="far fa-circle"></i>
    </span>
        <span class="text">SAVE</span>
    </a>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Account</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username" value="<?php echo $user_info_edit['username']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter e-mail address" value="<?php echo $user_info_edit['email']; ?>" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control" id="role">
                                <option <?php echo ($user_info_edit['role']=='customer') ? 'selected' : '' ; ?> id="customer">Customer</option>
                                <option <?php echo ($user_info_edit['role']=='administrator') ? 'selected' : '' ; ?> id="administrator">Administrator</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="active">Active</label><br>
                            <input <?php echo ($user_info_edit['active']) ? 'checked' : '' ; ?> type="checkbox" id="active" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <br>
                            <button data-toggle="modal" data-target="#modal_change_password" class="btn btn-block btn-primary">CHANGE PASSWORD</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Plan</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="plan">Current plan</label>
                            <select class="form-control" id="plan">
                                <?php echo get_plans_options($user_info_edit['id_plan']); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Current Plan Status</label><br>
                            <?php
                            switch($user_info_edit['plan_status']) {
                                case 'active':
                                    echo "<span style='color:green'><b>Active</b></span>";
                                break;
                                case 'expired':
                                    echo "<span style='color:red'><b>Expired</b></span>";
                                break;
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Expires on</label><br>
                            <b><?php echo ($user_info_edit['expire_plan_date']==null) ? 'Never' : $user_info_edit['expire_plan_date']; ?></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Virtual Tours</div>
                        <div id="num_virtual_tours" class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $user_stats_edit['count_virtual_tours']; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-route fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Rooms</div>
                        <div id="num_rooms" class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $user_stats_edit['count_rooms']; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-vector-square fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Markers</div>
                        <div id="num_markers" class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $user_stats_edit['count_markers']; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-caret-square-up fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">POIs</div>
                        <div id="num_pois" class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $user_stats_edit['count_pois']; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-bullseye fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_change_password" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Enter password" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="repeat_password">Repeat password</label>
                            <input type="password" class="form-control" id="repeat_password" placeholder="Repeat password" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> onclick="change_password('user');" type="button" class="btn btn-success">Change</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.user_need_save = false;
        window.id_user_edit = '<?php echo $id_user_edit; ?>';
        $(document).ready(function () {

        });
        $("input[type='text']").change(function(){
            window.user_need_save = true;
        });
        $("input[type='checkbox']").change(function(){
            window.user_need_save = true;
        });
        $("select").change(function(){
            window.user_need_save = true;
        });
        $(window).on('beforeunload', function(){
            if(window.user_need_save) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });
    })(jQuery); // End of use strict
</script>