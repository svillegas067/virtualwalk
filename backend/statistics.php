<?php
session_start();
$id_user = $_SESSION['id_user'];
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_GET['id_vt'])) {
        $id_virtualtour_sel = $_GET['id_vt'];
        $name_virtualtour_sel = get_virtual_tour($_GET['id_vt'],$id_user)['name'];
        $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
        $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
    } else {
        if(isset($_SESSION['id_virtualtour_sel'])) {
            $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
            $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
        } else {
            $id_virtualtour_sel = $virtual_tours[0]['id'];
            $name_virtualtour_sel = $virtual_tours[0]['name'];
        }
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}

?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-chart-area text-gray-700"></i> STATISTICS</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Virtualtour Accesses</h6>
            </div>
            <div class="card-body">
                <canvas height="250" id="chart_visitor_vt"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rooms Accesses</h6>
            </div>
            <div class="card-body">
                <canvas id="chart_rooms_access"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rooms Permanence (seconds)</h6>
            </div>
            <div class="card-body">
                <canvas id="chart_rooms_time"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">POI views</h6>
            </div>
            <div id="chart_poi_views" class="card-body">

            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        $(document).ready(function () {
            get_statistics('chart_visitor_vt');
            get_statistics('chart_rooms_access');
            get_statistics('chart_rooms_time');
            get_statistics('chart_poi_views');
        });

    })(jQuery); // End of use strict
</script>