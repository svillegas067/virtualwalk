<?php
$dashboard_active = "";
$virtual_tours_active = "";
$rooms_active = "";
$markers_active = "";
$pois_active = "";
$maps_active = "";
$info_active = "";
$gallery_active = "";
$icons_active = "";
$presentation_active = "";
$voice_commands_active = "";
$forms_data_active = "";
$statistics_active = "";
$landing_active = "";
$preview_active = "";
$publish_active = "";
$settings_active = "";
$users_active = "";
$plans_active = "";
switch ($page) {
    case 'dashboard':
        $dashboard_active = "active";
        break;
    case 'edit_virtual_tour':
    case 'virtual_tours':
        $virtual_tours_active = "active";
        break;
    case 'edit_room':
    case 'rooms':
    case 'rooms_bulk':
        $rooms_active = "active";
        break;
    case 'markers':
        $markers_active = "active";
        break;
    case 'pois':
        $pois_active = "active";
        break;
    case 'maps':
    case 'maps_bulk':
    case 'edit_map':
        $maps_active = "active";
        break;
    case 'info':
        $info_active = "active";
        break;
    case 'gallery':
        $gallery_active = "active";
        break;
    case 'icons_library':
        $icons_active = "active";
        break;
    case 'presentation':
        $presentation_active = "active";
        break;
    case 'forms_data':
        $forms_data_active = "active";
        break;
    case 'voice_commands':
        $voice_commands_active = "active";
        break;
    case 'statistics':
        $statistics_active = "active";
        break;
    case 'landing':
        $landing_active = "active";
        break;
    case 'preview':
        $preview_active = "active";
        break;
    case 'publish':
        $publish_active = "active";
        break;
    case 'settings':
        $settings_active = "active";
        break;
    case 'users':
    case "edit_user":
        $users_active = "active";
        break;
    case 'plans':
        $plans_active = "active";
        break;
}
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
if($count_virtual_tours==0) {
    $vt_disabled = "disabled";
} else {
    $vt_disabled = "";
}
?>

<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?p=dashboard">
        <div class="sidebar-brand-icon">
            <?php if($settings['logo']!='') { ?>
                <img style="max-height:50px;max-width:90px;width:auto;height:auto" src="assets/<?php echo $settings['logo']; ?>" />
            <?php } else { ?>
                <?php echo strtoupper($settings['name']); ?>
            <?php } ?>
        </div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item <?php echo $dashboard_active; ?>">
        <a class="nav-link" href="index.php?p=dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item <?php echo $statistics_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=statistics">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Statistics</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item <?php echo $virtual_tours_active; ?>">
        <a class="nav-link" href="index.php?p=virtual_tours">
            <i class="fas fa-fw fa-route"></i>
            <span>Virtual Tours</span></a>
    </li>
    <li class="nav-item <?php echo $rooms_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=rooms">
            <i class="fas fa-fw fa-vector-square"></i>
            <span>Rooms</span></a>
    </li>
    <li class="nav-item <?php echo $markers_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=markers">
            <i class="fas fa-fw fa-caret-square-up"></i>
            <span>Markers</span></a>
    </li>
    <li class="nav-item <?php echo $pois_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=pois">
            <i class="fas fa-fw fa-bullseye"></i>
            <span>POIs</span></a>
    </li>
    <li class="nav-item <?php echo $maps_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=maps">
            <i class="fas fa-fw fa-map-marked-alt"></i>
            <span>Maps</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item <?php echo $info_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=info">
            <i class="fas fa-fw fa-info-circle"></i>
            <span>Info Box</span></a>
    </li>
    <li class="nav-item <?php echo $gallery_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=gallery">
            <i class="fas fa-fw fa-images"></i>
            <span>Gallery</span></a>
    </li>
    <li class="nav-item <?php echo $icons_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=icons_library">
            <i class="fas fa-fw fa-icons"></i>
            <span>Icons Library</span></a>
    </li>
    <li class="nav-item <?php echo $presentation_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=presentation">
            <i class="fas fa-fw fa-directions"></i>
            <span>Presentation</span></a>
    </li>
    <li class="nav-item <?php echo $voice_commands_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=voice_commands">
            <i class="fas fa-fw fa-microphone"></i>
            <span>Voice Commands</span></a>
    </li>
    <li class="nav-item <?php echo $forms_data_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=forms_data">
            <i class="fas fa-fw fa-database"></i>
            <span>Forms Data</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item <?php echo $preview_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=preview">
            <i class="fas fa-fw fa-eye"></i>
            <span>Preview</span></a>
    </li>
    <li class="nav-item <?php echo $landing_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=landing">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Landing</span></a>
    </li>
    <li class="nav-item <?php echo $publish_active; ?>">
        <a class="nav-link <?php echo $vt_disabled; ?>" href="index.php?p=publish">
            <i class="fas fa-fw fa-paper-plane"></i>
            <span>Publish</span></a>
    </li>
    <?php if($user_info['role']=='administrator') : ?>
    <hr class="sidebar-divider d-none d-md-block">
        <li class="nav-item <?php echo $settings_active; ?>">
            <a class="nav-link" href="index.php?p=settings">
                <i class="fas fa-fw fa-cogs"></i>
                <span>Settings</span></a>
        </li>
    <li class="nav-item <?php echo $users_active; ?>">
        <a class="nav-link" href="index.php?p=users">
            <i class="fas fa-fw fa-user"></i>
            <span>Users</span></a>
    </li>
    <li class="nav-item <?php echo $plans_active; ?>">
        <a class="nav-link" href="index.php?p=plans">
            <i class="fas fa-fw fa-crown"></i>
            <span>Plans</span></a>
    </li>
    <?php endif; ?>
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>