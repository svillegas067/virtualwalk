<?php
session_start();
$virtual_tours = get_virtual_tours($id_user);
$count_virtual_tours = count($virtual_tours);
$array_list_vt = array();
if ($count_virtual_tours==1) {
    $id_virtualtour_sel = $virtual_tours[0]['id'];
    $name_virtualtour_sel = $virtual_tours[0]['name'];
    $_SESSION['id_virtualtour_sel'] = $id_virtualtour_sel;
    $_SESSION['name_virtualtour_sel'] = $name_virtualtour_sel;
} else {
    if(isset($_SESSION['id_virtualtour_sel'])) {
        $id_virtualtour_sel = $_SESSION['id_virtualtour_sel'];
        $name_virtualtour_sel = $_SESSION['name_virtualtour_sel'];
    } else {
        $id_virtualtour_sel = $virtual_tours[0]['id'];
        $name_virtualtour_sel = $virtual_tours[0]['name'];
    }
    foreach ($virtual_tours as $virtual_tour) {
        $id_virtualtour = $virtual_tour['id'];
        $name_virtualtour = $virtual_tour['name'];
        if($id_virtualtour!=$id_virtualtour_sel) {
            $array_list_vt[] = array("id"=>$id_virtualtour,"name"=>$name_virtualtour);
        }
    }
}

$id_user = $_SESSION['id_user'];
$count_rooms_create = check_plan_rooms_count($id_user);
$virtual_tour = get_virtual_tour($id_virtualtour_sel,$id_user);
$_SESSION['compress_jpg'] = $virtual_tour['compress_jpg'];
$_SESSION['max_width_compress'] = $virtual_tour['max_width_compress'];
?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-vector-square text-gray-700"></i> ROOMS</h1>
    <div class="dropdown mb-0">
        <button class="btn btn-primary <?php echo ($count_virtual_tours==1?'no-click':'dropdown-toggle'); ?>" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $name_virtualtour_sel; ?>
        </button>
        <?php if($count_virtual_tours>1): ?>
            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                <?php foreach ($array_list_vt as $vt) { ?>
                    <a onclick="set_session_vt(<?php echo $vt['id']; ?>);return false;" class="dropdown-item" href="#"><?php echo $vt['name']; ?></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Bulk room's create</h6>
            </div>
            <div class="card-body">
                <p>Uploading multiple 360-degree panorama JPG/PNG images will automatically create all the rooms.</p>
            </div>
        </div>
    </div>
</div>
<?php if($user_info['plan_status']=='active') { ?>
    <?php if($count_rooms_create>=0) : ?>
        <div class="card bg-warning text-white shadow mb-3">
            <div class="card-body">
            You have <?php echo $count_rooms_create; ?> remaining uploads!
            </div>
        </div>
    <?php endif; ?>
<?php } else { $count_rooms_create=0; ?>
    <div class="card bg-warning text-white shadow mb-4">
        <div class="card-body">
            Your "<?php echo $user_info['plan'] ?>" plan has expired!
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-body">
                <form action="ajax/upload_room_image.php" class="dropzone" id="rooms-dropzone"></form>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" id="loading_create_rooms" class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-body">
                <h3><i class="fa fa-spin fa-circle-notch"></i> Creating Rooms <b id="rooms_created">0</b> / <b id="rooms_to_upload">0</b>. Do not close this window!</h3>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        window.id_virtualtour = '<?php echo $id_virtualtour_sel; ?>';
        window.rooms_to_upload = 0;
        window.rooms_created = 0;
        window.uploading_panorama = false;
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            var rooms_dropzone = new Dropzone("#rooms-dropzone", {
                url: "ajax/upload_room_image.php",
                parallelUploads: 1,
                maxFilesize: 99,
                timeout: 990000,
                acceptedFiles: 'image/jpeg,image/png',
                <?php if($count_rooms_create>=0) : ?>
                maxFiles: <?php echo $count_rooms_create; ?>,
                <?php endif; ?>
            });
            rooms_dropzone.on("sending", function(file) {
                window.uploading_panorama = true;
                $('#loading_create_rooms').show();
                window.rooms_to_upload++;
                $('#rooms_to_upload').html(window.rooms_to_upload);
            });
            rooms_dropzone.on("success", function(file,rsp) {
                var file_name = file.name.split('.').slice(0, -1).join('.');
                add_bulk_room(id_virtualtour,rsp,file_name);
            });
            rooms_dropzone.on("queuecomplete", function() {
                setInterval(function () {
                    if(window.rooms_created==window.rooms_to_upload) {
                        window.uploading_panorama = false;
                        location.href='index.php?p=rooms';
                    }
                },1000);
            });
        });

        $(window).on('beforeunload', function(){
            if(window.uploading_panorama) {
                var c=confirm();
                if(c) return true; else return false;
            }
        });
    })(jQuery); // End of use strict
</script>