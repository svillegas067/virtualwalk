<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$code = $_POST['code'];
$query = "SELECT r.*,v.name as name_virtualtour,v.info_box,v.form_enable,v.form_icon,v.form_content,v.auto_show_slider,v.sameAzimuth,v.arrows_nav,v.autorotate_speed,v.autorotate_inactivity,v.nadir_logo,v.nadir_size,v.song as song_bg,v.song_autoplay,v.voice_commands,v.compass,v.author,v.hfov as hfov_default,v.min_hfov as min_hfov_default,v.max_hfov as max_hfov_default, 
v.show_info,v.show_gallery,v.show_icons_toggle,v.show_presentation,v.show_main_form,v.show_share,v.show_device_orientation,v.show_webvr,v.show_audio,v.show_fullscreen,v.show_map,v.live_session,v.show_annotations,v.show_list_alt,v.list_alt,v.intro_desktop,v.intro_mobile 
FROM svt_rooms AS r 
JOIN svt_virtualtours AS v ON v.id=r.id_virtualtour
WHERE v.code = '$code'
ORDER BY r.priority ASC, r.id ASC";
$result = $mysqli->query($query);
$rooms = array();
$array = array();
$list_alt = '';
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $id_room = $row['id'];
            $id_virtualtour = $row['id_virtualtour'];
            $name_virtualtour = strtoupper($row['name_virtualtour']);
            $author = trim($row['author']);
            $hfov = $row['hfov_default'];
            $min_hfov = $row['min_hfov_default'];
            $max_hfov = $row['max_hfov_default'];
            $show_audio = $row['show_audio'];
            if($show_audio) {
                if($row['song']==null) $row['song']='';
                $song = $row['song_bg'];
                if($song==null) $song='';
                $song_autoplay = $row['song_autoplay'];
            } else {
                $row['song'] = '';
                $song='';
                $song_autoplay = false;
            }
            $nadir_logo = $row['nadir_logo'];
            $nadir_size = $row['nadir_size'];
            $autorotate_speed = $row['autorotate_speed']*2;
            $autorotate_inactivity = $row['autorotate_inactivity'];
            $arrows_nav = $row['arrows_nav'];
            $show_info = $row['show_info'];
            if($show_info) {
                $info_box = $row['info_box'];
            } else {
                $info_box = "";
            }
            $show_gallery = $row['show_gallery'];
            $show_icons_toggle = $row['show_icons_toggle'];
            $show_presentation = $row['show_presentation'];
            $show_share = $row['show_share'];
            $show_device_orientation = $row['show_device_orientation'];
            $show_webvr = $row['show_webvr'];
            $show_fullscreen = $row['show_fullscreen'];
            $show_map = $row['show_map'];
            $show_annotations = $row['show_annotations'];
            $live_session = $row['live_session'];
            $show_list_alt = $row['show_list_alt'];
            $list_alt = $row['list_alt'];
            $intro_desktop = $row['intro_desktop'];
            if(empty($intro_desktop)) $intro_desktop = "";
            $intro_mobile = $row['intro_mobile'];
            if(empty($intro_mobile)) $intro_mobile = "";
            $voice_commands = $row['voice_commands'];
            $compass = $row['compass'];
            $sameAzimuth = $row['sameAzimuth'];
            $auto_show_slider = $row['auto_show_slider'];
            $show_main_form = $row['show_main_form'];
            if($show_main_form) {
                $form_enable = $row['form_enable'];
            } else {
                $form_enable = false;
            }
            $form_icon = $row['form_icon'];
            $form_content = $row['form_content'];
            if(empty($row['annotation_title'])) $row['annotation_title']='';
            if(empty($row['annotation_description'])) $row['annotation_description']='';
            $query_m = "SELECT m.*,r.name as name_room_target, 'marker' as type,i.id as id_icon_library, i.image as img_icon_library FROM svt_markers AS m
                        JOIN svt_rooms AS r ON m.id_room_target = r.id
                        LEFT JOIN svt_icons as i ON i.id=m.id_icon_library
                        WHERE m.id_room = $id_room";
            $result_m = $mysqli->query($query_m);
            $markers = array();
            if($result_m) {
                if ($result_m->num_rows > 0) {
                    while ($row_m = $result_m->fetch_array(MYSQLI_ASSOC)) {
                        $markers[] = $row_m;
                    }
                }
            }
            $query_p = "SELECT p.*,i.id as id_icon_library, i.image as img_icon_library FROM svt_pois AS p 
                        LEFT JOIN svt_icons as i ON i.id=p.id_icon_library
                        WHERE p.id_room = $id_room";
            $result_p = $mysqli->query($query_p);
            if($result_p) {
                if ($result_p->num_rows > 0) {
                    while ($row_p = $result_p->fetch_array(MYSQLI_ASSOC)) {
                        if($row_p['label']==null) $row_p['label']='';
                        if($row_p['type']=='gallery') {
                            $id_poi = $row_p['id'];
                            $array_images = array();
                            $query_g = "SELECT image FROM svt_poi_gallery WHERE id_poi=$id_poi ORDER BY priority;";
                            $result_g = $mysqli->query($query_g);
                            if($result_g) {
                                if ($result_g->num_rows > 0) {
                                    while ($row_g = $result_g->fetch_array(MYSQLI_ASSOC)) {
                                        $array_images[] = array("src"=>"gallery/".$row_g['image'],"thumb"=>"gallery/thumb/".$row_g['image']);
                                    }
                                }
                            }
                            $row_p['content'] = $array_images;
                        } else {
                            if($row_p['content']=='') continue;
                        }
                        $markers[] = $row_p;
                    }
                }
            }
            $row['markers'] = $markers;
            $rooms[] = $row;
            $array[$row['id']]=$row['name'];
        }

        $array2 = array();
        $array_id_rooms = array();
        if ($list_alt == '') {
            foreach ($rooms as $room) {
                array_push($array2,["id"=>$room['id'],"type"=>"room","hide"=>"0","name"=>$room['name']]);
            }
        } else {
            $list_alt_array = json_decode($list_alt, true);
            foreach ($list_alt_array as $item) {
                switch ($item['type']) {
                    case 'room':
                        if(array_key_exists($item['id'],$array)) {
                            array_push($array2, ["id" => $item['id'], "type" => "room", "hide" => $item['hide'], "name" => $array[$item['id']]]);
                        }
                        array_push($array_id_rooms,$item['id']);
                        break;
                    case 'category':
                        $childrens = array();
                        foreach ($item['children'] as $children) {
                            if ($children['type'] == "room") {
                                if(array_key_exists($children['id'],$array)) {
                                    array_push($childrens, ["id" => $children['id'], "type" => "room", "hide" => $children['hide'], "name" => $array[$children['id']]]);
                                }
                                array_push($array_id_rooms,$children['id']);
                            }
                        }
                        array_push($array2, ["id" => $item['id'], "type" => "category", "name" => $item['cat'], "childrens" => $childrens]);
                        break;
                }
            }
            foreach ($rooms as $room) {
                $id_room = $room['id'];
                if(!in_array($id_room,$array_id_rooms)) {
                    array_push($array2,["id"=>$room['id'],"type"=>"room","hide"=>"0","name"=>$room['name']]);
                }
            }
        }

        $mysqli->query("INSERT INTO svt_access_log(id_virtualtour,date_time) VALUES($id_virtualtour,NOW());");
        echo json_encode(array("status"=>"ok",
            "rooms"=>$rooms,
            "id_virtualtour"=>$id_virtualtour,
            "name_virtualtour"=>$name_virtualtour,
            "song"=>$song,
            "song_autoplay"=>$song_autoplay,
            "nadir_logo"=>$nadir_logo,
            "nadir_size"=>$nadir_size,
            "autorotate_inactivity"=>$autorotate_inactivity,
            "autorotate_speed"=>$autorotate_speed,
            "arrows_nav"=>$arrows_nav,
            "info_box"=>$info_box,
            "voice_commands"=>$voice_commands,
            "compass"=>$compass,
            "sameAzimuth"=>$sameAzimuth,
            "auto_show_slider"=>$auto_show_slider,
            "form_enable"=>$form_enable,
            "form_icon"=>$form_icon,
            "form_content"=>$form_content,
            "author"=>$author,
            "hfov"=>$hfov,
            "min_hfov"=>$min_hfov,
            "max_hfov"=>$max_hfov,
            "show_gallery"=>$show_gallery,
            "show_icons_toggle"=>$show_icons_toggle,
            "show_presentation"=>$show_presentation,
            "show_share"=>$show_share,
            "show_device_orientation"=>$show_device_orientation,
            "show_webvr"=>$show_webvr,
            "show_fullscreen"=>$show_fullscreen,
            "show_map"=>$show_map,
            "live_session"=>$live_session,
            "show_annotations"=>$show_annotations,
            "show_list_alt"=>$show_list_alt,
            "list_alt"=>$array2,
            "intro_desktop"=>$intro_desktop,
            "intro_mobile"=>$intro_mobile
        ));
    } else {
        echo json_encode(array("status"=>"invalid"));
    }
}