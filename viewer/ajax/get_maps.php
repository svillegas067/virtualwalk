<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$query = "SELECT * FROM svt_maps WHERE id_virtualtour=$id_virtualtour;";
$result = $mysqli->query($query);
$maps = array();
if($result) {
    if($result->num_rows>0) {
        while($row=$result->fetch_array(MYSQLI_ASSOC)) {
            $maps[] = $row;
        }
        echo json_encode(array("status"=>"ok","maps"=>$maps));
    } else {
        echo json_encode(array("status"=>"no map"));
    }
} else {
    echo json_encode(array("status"=>"error"));
}