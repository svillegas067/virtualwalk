<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
require_once("../../db/connection.php");
$form_data = array();
parse_str($_POST['form_data'], $form_data);

$id_virtualtour = $_POST['id_virtualtour'];
$id_room = $form_data['id_room'];
$title = $form_data['title'];
$title = str_replace("'","\'",$title);

if(isset($form_data['form_field_1'])) {
    $form_field_1 = $form_data['form_field_1'];
    $form_field_1 = strip_tags($form_field_1);
    $form_field_1 = str_replace("'","\'",$form_field_1);
} else {
    $form_field_1 = "";
}
if(isset($form_data['form_field_2'])) {
    $form_field_2 = $form_data['form_field_2'];
    $form_field_2 = strip_tags($form_field_2);
    $form_field_2 = str_replace("'","\'",$form_field_2);
} else {
    $form_field_2 = "";
}
if(isset($form_data['form_field_3'])) {
    $form_field_3 = $form_data['form_field_3'];
    $form_field_3 = strip_tags($form_field_3);
    $form_field_3 = str_replace("'","\'",$form_field_3);
} else {
    $form_field_3 = "";
}
if(isset($form_data['form_field_4'])) {
    $form_field_4 = $form_data['form_field_4'];
    $form_field_4 = strip_tags($form_field_4);
    $form_field_4 = str_replace("'","\'",$form_field_4);
} else {
    $form_field_4 = "";
}
if(isset($form_data['form_field_5'])) {
    $form_field_5 = $form_data['form_field_5'];
    $form_field_5 = strip_tags($form_field_5);
    $form_field_5 = str_replace("'","\'",$form_field_5);
} else {
    $form_field_5 = "";
}

$query = "INSERT INTO svt_forms_data(id_virtualtour,id_room,title,field1,field2,field3,field4,field5) VALUES($id_virtualtour,$id_room,'$title','$form_field_1','$form_field_2','$form_field_3','$form_field_4','$form_field_5');";
$result = $mysqli->query($query);
if($result) {
    echo json_encode(array("status"=>"ok"));
} else {
    echo json_encode(array("status"=>"error"));
}