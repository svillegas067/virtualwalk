(function ($) {
    'use strict';
    var panoramas = [];
    var pano_viewer, pano_viewer_vr, video_viewer, video_viewer_vr;
    var imgs = [];
    var imgs_loaded = [];
    var array_presentation = [];
    var len = 0;
    var $progress = $('#perc');
    var array_maps = [];
    var id_current_map = null;
    var map_selector_open = false;
    var html_map, html_logo;
    var v = 1; //increment only for caching problem, reload the images.
    var hfov = 100;
    var min_hfov = 60;
    var max_hfov = 100;
    var author = "";
    var name_virtual_tour = "";
    var id_virtual_tour;
    var song_bg = "";
    var nadir_logo = "";
    var nadir_size = "small";
    var audio_player = new Audio();
    var first_song_play = true;
    var song_autoplay = false;
    var song_is_playng = false;
    var autorotate_speed = 0;
    var autorotate_inactivity = 0;
    var arrows_nav = false;
    var info_box = '';
    var gallery_images = [];
    var sly;
    var controls_status = [];
    var vr_enabled = false;
    var wl = null;
    var interval_position = null;
    var goto_timeout = null;
    var current_id_panorama = null;
    var voice_commands_enable = 0;
    var show_compass = false;
    var show_gallery = false;
    var show_icons_toggle = false;
    var show_presentation = false;
    var show_share = false;
    var show_device_orientation = false;
    var show_webvr = false;
    var show_fullscreen = false;
    var show_map = 0;
    var show_live_session = false;
    var show_annotations = false;
    var show_list_alt = 0;
    var list_alt = '';
    var map_opened = false;
    var sameAzimuth = false;
    var interval_access_time_avg = null;
    var access_time_avg = 0;
    var access_time_id = 0;
    var form_lightbox = null;
    var auto_show_slider = 0;
    var form_enable = false;
    var form_icon = '';
    var form_content = '';
    var slider_index = 0;
    var current_panorama_type = 'image';
    var video_opened = false;
    var live_session_connected = false;
    var interval_live_session = null;
    var id_live_session = '';
    var call_session = null;
    var poi_open = false;
    var intro_desktop = '';
    var intro_mobile = '';

    $(document).bind("contextmenu",function(e){
        return false;
    });

    window.get_rooms = function (code) {
        $('.fa-circle-notch').show();
        $('#pbar').show();
        $progress.html("LOADING");
        document.getElementById("pbar").value = 20;
        $.ajax({
            url: "ajax/get_rooms.php",
            type: "POST",
            data: {
                code: code,
                mobile: window.is_mobile
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=='ok') {
                    document.getElementById("pbar").value = 30;
                    $('.loading').show();
                    id_virtual_tour = rsp.id_virtualtour;
                    name_virtual_tour = rsp.name_virtualtour;
                    author = rsp.author;
                    hfov = parseInt(rsp.hfov);
                    min_hfov = parseInt(rsp.min_hfov);
                    max_hfov = parseInt(rsp.max_hfov);
                    song_bg = rsp.song;
                    song_autoplay = parseInt(rsp.song_autoplay);
                    nadir_logo = rsp.nadir_logo;
                    nadir_size = rsp.nadir_size;
                    autorotate_speed = parseInt(rsp.autorotate_speed);
                    autorotate_inactivity = parseInt(rsp.autorotate_inactivity);
                    arrows_nav = parseInt(rsp.arrows_nav);
                    info_box = rsp.info_box;
                    voice_commands_enable = parseInt(rsp.voice_commands);
                    show_compass = parseInt(rsp.compass);
                    show_gallery = parseInt(rsp.show_gallery);
                    show_icons_toggle = parseInt(rsp.show_icons_toggle);
                    show_presentation = parseInt(rsp.show_presentation);
                    show_share = parseInt(rsp.show_share);
                    show_device_orientation = parseInt(rsp.show_device_orientation);
                    show_webvr = parseInt(rsp.show_webvr);
                    show_fullscreen = parseInt(rsp.show_fullscreen);
                    show_map = parseInt(rsp.show_map);
                    show_live_session = parseInt(rsp.live_session);
                    show_annotations = parseInt(rsp.show_annotations);
                    show_list_alt = parseInt(rsp.show_list_alt);
                    list_alt = rsp.list_alt;
                    sameAzimuth = parseInt(rsp.sameAzimuth);
                    auto_show_slider = parseInt(rsp.auto_show_slider);
                    form_enable = parseInt(rsp.form_enable);
                    form_icon = rsp.form_icon;
                    form_content = rsp.form_content;
                    intro_desktop = rsp.intro_desktop;
                    intro_mobile = rsp.intro_mobile;
                    if(window.is_mobile) {
                        if(intro_mobile!='') {
                            $('.intro_img img').attr('src','content/'+intro_mobile);
                        }
                    } else {
                        if(intro_desktop!='') {
                            $('.intro_img img').attr('src','content/'+intro_desktop);
                        }
                    }
                    jQuery.each(rsp.rooms, function(index, room) {
                        panoramas[index] = {};
                        panoramas[index].id = room.id;
                        panoramas[index].nome = room.name;
                        panoramas[index].type = room.type;
                        if(window.is_mobile) {
                            panoramas[index].panorama_image = "panoramas/mobile/"+room.panorama_image+"?v="+v;
                        } else {
                            panoramas[index].panorama_image = "panoramas/"+room.panorama_image+"?v="+v;
                        }
                        panoramas[index].panorama_video = "videos/"+room.panorama_video+"?v="+v;
                        panoramas[index].thumb_image = "panoramas/thumb/"+room.panorama_image+"?v="+v;
                        panoramas[index].northOffset = parseInt(room.northOffset);
                        panoramas[index].pitch = parseInt(room.pitch);
                        panoramas[index].yaw = parseInt(room.yaw);
                        panoramas[index].allow_pitch = parseInt(room.allow_pitch);
                        panoramas[index].min_pitch = parseInt(room.min_pitch);
                        panoramas[index].max_pitch = parseInt(room.max_pitch);
                        panoramas[index].id_map = parseInt(room.id_map);
                        panoramas[index].map_top = parseInt(room.map_top);
                        panoramas[index].map_left = parseInt(room.map_left);
                        panoramas[index].visible_list = parseInt(room.visible_list);
                        panoramas[index].song = room.song;
                        panoramas[index].annotation_title = room.annotation_title;
                        panoramas[index].annotation_description = room.annotation_description;
                        panoramas[index].hotSpots = [];
                        panoramas[index].hotSpots_vr = [];
                        if(nadir_logo!='') {
                            panoramas[index].hotSpots.push({
                                "type": "nadir",
                                "pitch": -90,
                                "yaw": 0,
                                "rotateX": 0,
                                "rotateZ": 0,
                                "scale": true,
                                "cssClass": "nadir-hotspot-"+nadir_size,
                                "createTooltipFunc": hotspot_nadir,
                                "createTooltipArgs": nadir_logo
                            });
                            panoramas[index].hotSpots_vr.push({
                                "type": "nadir",
                                "pitch": -90,
                                "yaw": 0,
                                "rotateX": 0,
                                "rotateZ": 0,
                                "scale": true,
                                "cssClass": "nadir-hotspot-small_vr",
                                "createTooltipFunc": hotspot_nadir,
                                "createTooltipArgs": nadir_logo
                            });
                        }
                        jQuery.each(room.markers, function(index_m, marker_m) {
                            switch(marker_m.type) {
                                case 'marker':
                                    panoramas[index].hotSpots.push({
                                        "id": marker_m.id,
                                        "type": marker_m.type,
                                        "pitch": parseInt(marker_m.pitch),
                                        "yaw": parseInt(marker_m.yaw),
                                        "rotateX": parseInt(marker_m.rotateX),
                                        "rotateZ": parseInt(marker_m.rotateZ),
                                        "size_scale": parseFloat(marker_m.size_scale),
                                        "cssClass": "custom-hotspot",
                                        "createTooltipFunc": hotspot,
                                        "createTooltipArgs": marker_m,
                                        "clickHandlerFunc": goto,
                                        "clickHandlerArgs": [marker_m.id_room_target,parseInt(marker_m.pitch),parseInt(marker_m.yaw)]
                                    });
                                    panoramas[index].hotSpots_vr.push({
                                        "id": marker_m.id,
                                        "type": marker_m.type,
                                        "pitch": parseInt(marker_m.pitch),
                                        "yaw": parseInt(marker_m.yaw),
                                        "rotateX": parseInt(marker_m.rotateX),
                                        "rotateZ": parseInt(marker_m.rotateZ),
                                        "size_scale": parseFloat(marker_m.size_scale),
                                        "cssClass": "custom-hotspot custom-hotspot_vr",
                                        "createTooltipFunc": hotspot,
                                        "createTooltipArgs": marker_m,
                                        "clickHandlerFunc": goto,
                                        "clickHandlerArgs": [marker_m.id_room_target,parseInt(marker_m.pitch),parseInt(marker_m.yaw)]
                                    });
                                    break;
                                case 'image':
                                case 'gallery':
                                case 'video':
                                case 'video360':
                                case 'audio':
                                case 'link':
                                case 'html':
                                case 'html_sc':
                                case 'form':
                                    panoramas[index].hotSpots.push({
                                        "id": marker_m.id,
                                        "type": marker_m.type,
                                        "pitch": parseInt(marker_m.pitch),
                                        "yaw": parseInt(marker_m.yaw),
                                        "rotateX": 0,
                                        "rotateZ": 0,
                                        "size_scale": parseFloat(marker_m.size_scale),
                                        "cssClass": "custom-hotspot-content",
                                        "createTooltipFunc": hotspot_content,
                                        "createTooltipArgs": marker_m,
                                        "clickHandlerFunc": view_content,
                                        "clickHandlerArgs": marker_m
                                    });
                                    break;
                                case 'download':
                                    panoramas[index].hotSpots.push({
                                        "id": marker_m.id,
                                        "type": marker_m.type,
                                        "pitch": parseInt(marker_m.pitch),
                                        "yaw": parseInt(marker_m.yaw),
                                        "rotateX": 0,
                                        "rotateZ": 0,
                                        "size_scale": parseFloat(marker_m.size_scale),
                                        "cssClass": "custom-hotspot-content",
                                        "createTooltipFunc": hotspot_content,
                                        "createTooltipArgs": marker_m,
                                        "clickHandlerFunc": set_poi_statistics,
                                        "clickHandlerArgs": marker_m.id
                                    });
                                    break;
                                case 'link_ext':
                                    panoramas[index].hotSpots.push({
                                        "id": marker_m.id,
                                        "type": marker_m.type,
                                        "pitch": parseInt(marker_m.pitch),
                                        "yaw": parseInt(marker_m.yaw),
                                        "rotateX": 0,
                                        "rotateZ": 0,
                                        "size_scale": parseFloat(marker_m.size_scale),
                                        "cssClass": "custom-hotspot-content",
                                        "createTooltipFunc": hotspot_content,
                                        "createTooltipArgs": marker_m,
                                        "clickHandlerFunc": set_poi_statistics,
                                        "clickHandlerArgs": marker_m.id
                                    });
                                    break;
                            }
                        });
                    });
                    for(var i=0; i < panoramas.length; i++) {
                        var id = panoramas[i].id;
                        var img = panoramas[i].panorama_image;
                        var thumb = panoramas[i].thumb_image;
                        var nome = panoramas[i].nome.toUpperCase();
                        var visible_list = panoramas[i].visible_list;
                        imgs.push(img);
                        if(visible_list) {
                            $('.slidee').append('<li data-index_id="'+i+'" data-id="'+id+'" class="disabled pointer_list pointer_list_'+id+'"><img src="'+thumb+'"><span class="noselect"><i class="fas fa-spin fa-circle-notch"></i> '+nome+'</span></li>');
                        }
                    }
                    len = imgs.length;
                    get_maps();
                }
            }
        });
    }

    window.check_password_vt = function () {
        var password = $('#vt_password').val();
        if(password!='') {
            $.ajax({
                url: "ajax/check_password_vt.php",
                type: "POST",
                data: {
                    code: window.code,
                    password: password
                },
                async: false,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=='ok') {
                        $('.fa-circle-notch').show();
                        $('#pbar').show();
                        $('.protect').hide();
                        get_rooms(window.code);
                    } else {
                        $('#vt_password').val('');
                    }
                }
            });
        }
    }

    window.start_vt = function () {
        $('#icon_play').hide();
        if(password_protected) {
            show_password_input();
        } else {
            get_rooms(code);
        }
    }

    window.show_password_input = function () {
        $('.fa-circle-notch').hide();
        $('#pbar').hide();
        $('.protect').show();
    }

    window.show_icon_play = function () {
        $('.fa-circle-notch').hide();
        $('#pbar').hide();
        $('#icon_play').show();
    }

    function IsNaN(o) {
        return typeof(o) === 'number' && isNaN(o);
    }

    window.zoom_map = function () {
        if($(".map").hasClass("map_zoomed")) {
            $(".map").removeClass('map_zoomed');
            $('.map_zoom_control i').addClass('fa-expand-alt').removeClass('fa-compress-alt');
        } else {
            $(".map").addClass('map_zoomed');
            $('.map_zoom_control i').removeClass('fa-expand-alt').addClass('fa-compress-alt');
        }
    }

    function hexToRgb(hex) {
        var hex=hex.replace('#', '');
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;
        return r + "," + g + "," + b;
    }

    function loading_config() {
        if(window.logo!='') {
            if(window.link_logo!='') {
                html_logo = "<a href='"+window.link_logo+"' target='_blank'><img draggable=\"false\" src=\"content/"+logo+"\"></a>";
            } else {
                html_logo = "<img draggable=\"false\" src=\"content/"+logo+"\">";
            }
        }
        if(array_maps.length>0) {
            html_map = '';
            for(var i=0; i < array_maps.length; i++) {
                html_map += "<div class='all_maps map_"+array_maps[i].id+"'>";
                html_map += "<img class='map_image' draggable=\"false\" src=\"maps/"+array_maps[i].map+"\">";
                html_map += "<span class='noselect'>"+array_maps[i].name+"</span>";
                html_map += '<div onclick="open_map_selector();" class="map_selector_control">\n' +
                    '    <i class="fas fa-layer-group"></i>\n' +
                    '</div>';
                html_map += '<div onclick="zoom_map();" class="map_zoom_control">\n' +
                    '    <i class="fas fa-expand-alt"></i>\n' +
                    '</div>';
                html_map += "</div>";
            }
            html_map += "<div class='map_selector'>\n" +
                "  <ul>\n";
            for(var i=0; i < array_maps.length; i++) {
                html_map += "    <li onclick='change_map("+array_maps[i].id+");'><a class='noselect'>"+array_maps[i].name+"</a></li>\n";
            }
            html_map += "  </ul>\n" +
                "</div>";
            for(var i=0; i < panoramas.length; i++) {
                var bg_color = '#000000';
                var point_size = 20;
                if(!IsNaN(panoramas[i].map_top)) {
                    for(var k=0; k < array_maps.length; k++) {
                        if(panoramas[i].id_map==array_maps[k].id) {
                            bg_color = array_maps[k].point_color;
                            point_size = array_maps[k].point_size;
                        }
                    }
                    var scale = point_size/20;
                    var rgb = hexToRgb(bg_color);
                    html_map += "<div data-scale='"+scale+"' style='transform: rotate(0deg) scale("+scale+");top:"+(panoramas[i].map_top+(point_size/2))+"px;left:"+(panoramas[i].map_left+(point_size/2))+"px;' class='disabled pointer pointer_map_"+panoramas[i].id_map+" pointer_"+panoramas[i].id+"'><i style='margin-top:10px;font-size:21px !important;vertical-align:top;' class=\"fas fa-spin fa-circle-notch\"></i><div style='background: rgb("+rgb+");background: linear-gradient(-45deg, rgba("+rgb+",0) 10%, rgba("+rgb+",1) 100%);' class=\"view_direction__arrow\"></div><div style='background: "+bg_color+";' title=\""+panoramas[i].nome+"\" data-id='"+panoramas[i].id+"' class=\"view_direction__center\"></div></div>";
                }
            }
        }
        if(array_maps.length>0) {
            var image_map = array_maps[0].map;
            var id_map_image = panoramas[0].id_map;
            if(!isNaN(id_map_image)) {
                for(var i=0; i < array_maps.length; i++) {
                    if(array_maps[i].id==id_map_image) {
                        image_map = array_maps[i].map;
                    }
                }
            }
            var image_map_load = new Image();
            $(image_map_load).on('load',function () {
                preload_first_image();
            }).attr('src','maps/'+image_map);
        } else {
            preload_first_image();
        }
    }

    $(document).on("mousedown touchstart",function(e){
        $('.intro_img').css('display','none');
        var map = $('.map');
        if (!map.is(e.target) && map.has(e.target).length === 0) {
            close_map_selector();
            try {
                if($(".map").hasClass("map_zoomed")) {
                    $(".map").removeClass('map_zoomed');
                    $('.map_zoom_control i').addClass('fa-expand-alt').removeClass('fa-compress-alt');
                }
            } catch(e) {}
        }
        var menu = $('.menu_controls');
        if (!menu.is(e.target) && menu.has(e.target).length === 0) {
            close_menu_controls();
        }
    });

    document.addEventListener('keyup', event => {
        $('.intro_img').css('display','none');
        if(!live_session_connected) {
            if (event.code === 'Space') {
                if(!poi_open) {
                    $('.pnlm-pointer.hotspot_hover').trigger('click');
                    $('.pnlm-pointer.hotspot_hover a').trigger('click');
                }
            }
            if (event.code === "Escape") {
                poi_open = false;
                $('.pnlm-container').focus();
                $('.pnlm-container').trigger('click');
            }
        }
    });

    window.change_map = function(id) {
        $('.all_maps').hide();
        $('.pointer').hide();
        $('.map_'+id).show();
        $('.pointer_map_'+id).show();
        close_map_selector();
    }

    window.open_map_selector = function () {
        if((array_maps.length>1) && (map_selector_open==false)) {
            $('.map_image').addClass('darker_img');
            $('.pointer').css('visibility','hidden');
            $('.all_maps span').hide();
            $('.map_selector_control').hide();
            $('.map_selector').css('display','flex');
            $('.map_zoom_control').hide();
            map_selector_open = true;
        }
    }

    window.close_map_selector = function () {
        if(map_selector_open) {
            $('.map_image').removeClass('darker_img');
            $('.pointer').css('visibility', 'visible');
            $('.all_maps span').show();
            $('.map_selector_control').show();
            $('.map_selector').hide();
            $('.map_zoom_control').show();
            map_selector_open = false;
        }
    }

    function preload_first_image() {
        var img = panoramas[0].panorama_image;
        document.getElementById("pbar").value = 50;
        var first_image = new Image();
        $(first_image).on('load',function () {
            document.getElementById("pbar").value = 100;
            $progress.html("INITIALIZING");
            initialize();
        }).attr('src',img);
    }

    function preload_images() {
        $.preload(imgs, {
            each: function (img_loaded) {
                var id_load = -1;
                var icon = '';
                for(var i=0; i < panoramas.length; i++) {
                    var id = panoramas[i].id;
                    var img = panoramas[i].panorama_image;
                    if(img==img_loaded) {
                        id_load=id;
                    }
                }
                if(id_load>0) {
                    imgs_loaded.push(id_load);
                    $('.marker_img_'+id_load).removeClass('fas fa-spin fa-circle-notch');
                    $('.marker_img_'+id_load).parent().removeClass('disabled');
                    $('.marker_img_'+id_load).each(function () {
                        var icon = $(this)[0].getAttribute('data-icon');
                        $(this).addClass(icon);
                    });
                    $('.pointer_list_'+id_load+' i').remove();
                    $('.pointer_list_'+id_load).removeClass('disabled');
                    $('.pointer_'+id_load+' i').remove();
                    $('.pointer_'+id_load).removeClass('disabled');
                    $(".arrows_nav").find("[data-roomtarget='" + id_load + "']").removeClass('disabled');
                    $('.list_alt_'+id_load).removeClass('disabled');
                }
            },
            all: function () {
                for(var i=0; i < panoramas.length; i++) {
                    var id = panoramas[i].id;
                    if(imgs_loaded.indexOf(id) !== -1) {
                        imgs_loaded.push(id);
                    }
                    $('.marker_img_'+id).removeClass('fas fa-spin fa-circle-notch');
                    $('.marker_img_'+id).parent().removeClass('disabled');
                    $('.marker_img_'+id).each(function () {
                        var icon = $(this)[0].getAttribute('data-icon');
                        $(this).addClass(icon);
                    });
                    $('.pointer_list_'+id+' i').remove();
                    $('.pointer_list_'+id).removeClass('disabled');
                    $('.pointer_'+id+' i').remove();
                    $('.pointer_'+id).removeClass('disabled');
                    $(".arrows_nav").find("[data-roomtarget='" + id + "']").removeClass('disabled');
                    $('.list_alt_'+id).removeClass('disabled');
                }
            }
        });
    }

    function add_custom_controls() {
        if (window.DeviceOrientationEvent && location.protocol == 'https:' && navigator.userAgent.toLowerCase().indexOf('mobi') >= 0) {
            $('.orient_control').show();
        } else {
            $('.orient_control').hide();
        }
        if(song_bg!='') {
            $('.song_control').show();
        } else {
            $('.song_control').hide();
            for(var i=0; i < panoramas.length; i++) {
                var song = panoramas[i].song;
                if(song!='') {
                    $('.song_control').show();
                }
            }
        }
        var info_control = false;
        var voice_control = false;
        var gallery_control = false;
        if((info_box!='') && (info_box!=null) && (info_box!='<p><br></p>')) {
            info_control = true;
        }
        if(voice_commands_enable>0) {
            if(peer_id=='') {
                voice_control = true;
                initialize_speech();
            }
        }
        if(gallery_images.length>0) {
            if(show_gallery) {
                gallery_control = true;
            }
        }
        if((info_control) && (voice_control) && (gallery_control)) {
            $('.info_control').show();
            $('.info_control').css('left','6px');
            $('.gallery_control').show();
            $('.gallery_control').css('left','52px');
            $('#skitt-ui').css('left','98px');
        } else if((info_control) && (gallery_control)) {
            $('.info_control').show();
            $('.info_control').css('left','6px');
            $('.gallery_control').show();
            $('.gallery_control').css('left','52px');
        } else if((info_control) && (voice_control)) {
            $('.info_control').show();
            $('.info_control').css('left','6px');
            $('.gallery_control').hide();
            $('#skitt-ui').css('left','52px');
        } else if((voice_control) && (gallery_control)) {
            $('.info_control').hide();
            $('.gallery_control').show();
            $('.gallery_control').css('left','6px');
            $('#skitt-ui').css('left','52px');
        } else if(info_control) {
            $('.info_control').show();
            $('.info_control').css('left','6px');
            $('.gallery_control').hide();
        } else if(voice_control) {
            $('.info_control').hide();
            $('.gallery_control').hide();
            $('#skitt-ui').css('left','6px');
        } else if(gallery_control) {
            $('.info_control').hide();
            $('.gallery_control').show();
            $('.gallery_control').css('left','6px');
        }
        if(array_presentation.length>0) {
            $('.presentation_control').show();
        } else {
            $('.presentation_control').hide();
        }
        if(form_enable) {
            try {
                form_content = JSON.parse(form_content);
                var title = form_content[0].title;
                $('#mform_icon').addClass(form_icon);
                $('#mform_name').html(title);
                $('.form_control').show();
            } catch (e) {}
        } else {
            $('.form_control').hide();
        }
        $('.tooltip').tooltipster({
            theme: 'tooltipster-borderless'
        });
        $('.pnlm-orientation-button').hide();
        if(!show_compass) {
            $('.fb_dialog').css('right','4px');
        }
        if(!show_icons_toggle) {
            $('.icons_control').addClass('hidden');
        }
        if(!show_presentation) {
            $('.presentation_control').addClass('hidden');
        }
        if(!show_share) {
            $('.share_control').addClass('hidden');
        }
        if(!show_device_orientation) {
            $('.orient_control').addClass('hidden');
        }
        if(!show_webvr) {
            $('.vr_control').addClass('hidden');
        }
        if(!show_fullscreen) {
            $('.fullscreen_control').addClass('hidden');
            $('.map_control').css('right','14px');
        }
        if(array_maps.length>0) {
            switch (show_map) {
                case 0:
                    $('.map_control').addClass('hidden');
                    break;
                case 1:
                case 2:
                case 3:
                    $('.map_control').show();
                    if(controls_status['map']) {
                        $('.map_control').addClass('active_control');
                        $('.map_control i').removeClass('icon-map_off').addClass('icon-map_on');
                    } else {
                        $('.map_control').removeClass('active_control');
                        $('.map_control i').removeClass('icon-map_on').addClass('icon-map_off');
                    }
                    break;
            }
        }
        if(auto_show_slider==2) {
            $('.list_control').addClass('hidden');
        }
        if (location.protocol == 'https:') {
            if(show_live_session) {
                if(peer_id!='') {
                    $('.live_control').addClass('hidden');
                }
            } else {
                $('.live_control').addClass('hidden');
            }
        } else {
            $('.live_control').addClass('hidden');
        }
        var all_menu_hidden = true;
        $('.dropdown p').each(function () {
            if($(this).is(':visible')) {
                all_menu_hidden = false;
            }
        });
        if(all_menu_hidden) {
            $('.menu_controls').addClass('hidden');
            $('.song_control').css('left','8px');
        }
        switch (show_list_alt) {
            case 0:
                $('.list_alt_menu').addClass('hidden');
                break;
            case 1:
            case 2:
                if(all_menu_hidden) {
                    $('.list_alt_menu').css('left','3px');
                    $('.song_control').css('left','45px');
                } else {
                    $('.list_alt_menu').css('left','40px');
                    $('.song_control').css('left','82px');
                }
                var id_open_cat = 0;
                for(var i=0;i<list_alt.length;i++) {
                    var id = list_alt[i].id;
                    var name = list_alt[i].name;
                    var type = list_alt[i].type;
                    var hide = parseInt(list_alt[i].hide);
                    switch (type) {
                        case 'room':
                            if(hide==0) {
                                if(id==panoramas[0].id) {
                                    var icon = 'fa-home active';
                                } else {
                                    var icon = 'fa-circle';
                                }
                                $('.list_alt_menu .dropdown').append('<p class="disabled list_alt_'+id+'" onclick="goto(\'\',['+id+',null,null]);"><i class="fa '+icon+'"></i>&nbsp;&nbsp;&nbsp;'+name+'</p>');
                            }
                            break;
                        case 'category':
                            $('.list_alt_menu .dropdown').append('<p class="cat cat_'+id+'" onclick="open_cat_list_alt(\''+id+'\')"><i class="fas fa-chevron-circle-right"></i>&nbsp;&nbsp;&nbsp;'+name+'</p>');
                            var childrens = list_alt[i].childrens;
                            for(var k=0;k<childrens.length;k++) {
                                if(childrens[k]['hide']==0) {
                                    if(childrens[k]['id']==panoramas[0].id) {
                                        var icon = 'fa-home active';
                                        id_open_cat = id;
                                    } else {
                                        var icon = 'fa-circle';
                                    }
                                    $('.list_alt_menu .dropdown').append('<p style="margin-left: 15px;display: none" data-cat="'+id+'" class="disabled children list_alt_'+childrens[k]['id']+' cat_parent_'+id+'" onclick="goto(\'\',['+childrens[k]['id']+',null,null]);"><i class="fa '+icon+'"></i>&nbsp;&nbsp;&nbsp;'+childrens[k]['name']+'</p>');
                                }
                            }
                            break;
                    }
                }
                $('.list_alt_menu').show();
                if(id_open_cat!=0) {
                    open_cat_list_alt(id_open_cat);
                }
                if(show_list_alt==2) click_list_alt_menu();
                break;
        }
    }

    window.open_cat_list_alt = function(id) {
        $('.list_alt_menu .dropdown .children').hide();
        if(id==0) {
            $('.cat i').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
        } else if($('.cat_'+id+' i').hasClass('fa-chevron-circle-right')) {
            $('.cat_parent_'+id).show();
            $('.cat_'+id+' i').removeClass('fa-chevron-circle-right').addClass('fa-chevron-circle-down');
            $(".cat i:not('.cat_"+id+" i')").removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
        } else {
            $('.cat_'+id+' i').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
        }
    }

    function initialize() {
        controls_status['fullscreen']=false;
        controls_status['orient']=false;
        if(song_autoplay) {
            controls_status['song']=true;
        } else {
            controls_status['song']=false;
            for(var i=0; i < panoramas.length; i++) {
                var song = panoramas[i].song;
                if(song!='') {
                    controls_status['song']=true;
                }
            }
        }
        controls_status['map']=false;
        controls_status['icons']=true;
        controls_status['list']=false;
        controls_status['share']=false;
        controls_status['hide']=false;
        controls_status['presentation']=false;
        current_id_panorama = panoramas[0].id;

        if(panoramas[0].song!='') {
            audio_player.src = "content/"+panoramas[0].song;
        } else if(song_bg!='') {
            audio_player.src = "content/"+song_bg;
        } else {
            audio_player.src = "";
        }
        if (typeof audio_player.loop == 'boolean') {
            audio_player.loop = true;
        } else {
            audio_player.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
            }, false);
        }
        audio_player.load();
        audio_player.addEventListener('playing', function () {
            first_song_play = false;
        });
        if(song_autoplay) {
            $('.song_control').addClass('active_control');
            $('.song_control i').addClass('fa-volume-down').removeClass('fa-volume-mute');
            audio_player.play().catch(function(error) {
                $('body').on('click touchend',function (e) {
                    if(e.which) {
                        if(first_song_play) {
                            audio_player.play();
                            $('.song_control').addClass('active_control');
                            $('.song_control i').addClass('fa-volume-down').removeClass('fa-volume-mute');
                            first_song_play = false;
                        }
                    }
                });
            });
        } else {
            audio_player.pause();
            audio_player.volume = 0;
            first_song_play = false;
            $('.song_control').removeClass('active_control');
            $('.song_control i').addClass('fa-volume-mute').removeClass('fa-volume-down');
        }

        if(array_maps.length>0) {
            $('.map').append(html_map);
            if(array_maps.length>1) {
                $('.map_selector_control').show();
            } else {
                $('.map_selector_control').hide();
            }
            $('.view_direction__center').tooltipster({
                theme: 'tooltipster-borderless',
                side: 'bottom'
            });
            $('.all_maps').hide();
            $('.pointer').hide();
            id_current_map = panoramas[0].id_map;
            if(!isNaN(id_current_map)) {
                $('.map_'+id_current_map).show();
                $('.pointer_map_'+id_current_map).show();
            } else {
                $('.map_'+array_maps[0].id).show();
                $('.pointer_map_'+array_maps[0].id).show();
            }
            jQuery.each(imgs_loaded, function(index, id_load) {
                $('.pointer_'+id_load+' i').remove();
                $('.pointer_'+id_load).removeClass('disabled');
            });
            $('.view_direction__center').click(function () {
                var id_room_target = $(this).data('id');
                goto('',[id_room_target,null,null]);
            });
        }
        $('.view_direction__arrow').hide();
        initialize_room(0,false,false);
        setTimeout(function () {
            $('.loading').animate({
                opacity: 0
            }, { duration: 250, queue: false });
            $('.loading').css('z-index',0);
            $('.loading').hide();
            $('#background_loading').hide();
            if(panoramas[0].type=='video') {
                $('#video_viewer').animate({
                    opacity: 1
                }, { duration: 250, queue: false });
                $('#video_viewer').css('z-index',10);
            } else {
                $('#panorama_viewer').animate({
                    opacity: 1
                }, { duration: 250, queue: false });
                $('#panorama_viewer').css('z-index',10);
            }
            $('.header_vt').show();
            var annotation_title = panoramas[0].annotation_title;
            var annotation_description = panoramas[0].annotation_description;
            if((annotation_title!='') && (annotation_description!='') && (show_annotations)) {
                $('.annotation_title').html(annotation_title.toUpperCase());
                $('.annotation_description').html(annotation_description);
                $('.annotation').fadeIn();
            } else {
                $('.annotation').hide();
            }
            if(array_maps.length>0) {
                switch (show_map) {
                    case 0:
                        controls_status['map']=false;
                        $('.map').hide();
                        break;
                    case 1:
                        if(window.is_mobile) {
                            $('.map').hide();
                            controls_status['map']=false;
                        } else {
                            $('.map').show();
                            controls_status['map']=true;
                        }
                        break;
                    case 2:
                        $('.map').hide();
                        controls_status['map']=false;
                        break;
                    case 3:
                        $('.map').show();
                        controls_status['map']=true;
                        break;
                }
            } else {
                $('.map').hide();
                controls_status['map']=false;
            }
            adjust_ratio_hfov();
            $('.menu_controls').show();
            add_custom_controls();
            $(document).trigger('resize');
            if(logo!='') {
                $('.logo').append(html_logo);
                $('.map').css('top','89px');
                $('.logo').show();
            } else {
                $('.map').css('top','45px');
            }
            $('.fullscreen_control').show();
            switch(arrows_nav) {
                case 0:
                    $('.arrows_nav').hide();
                    break;
                case 1:
                    $('.arrows_nav').show();
                    break;
                case 2:
                    if(window.is_mobile) {
                        $('.arrows_nav').hide();
                    } else {
                        $('.arrows_nav').show();
                    }
                    break;
            }
            if (window.DeviceOrientationEvent && location.protocol == 'https:' && navigator.userAgent.toLowerCase().indexOf('mobi') >= 0) {
                $('.vr_control').show();
            } else {
                $('.vr_control').hide();
            }
            $('.pointer_'+panoramas[0].id).animate({
                opacity: 1
            }, { duration: 250, queue: false });
            $('.pointer_list_'+panoramas[0].id).addClass('active');
            setTimeout(function () {
                sly = new Sly('.frame', {
                    horizontal: 1,
                    itemNav: 'centered',
                    smart: 1,
                    mouseDragging: 1,
                    touchDragging: 1,
                    releaseSwing: 1,
                    scrollBy: 1,
                    speed: 300,
                    elasticBounds: 1,
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1,
                    startAt: 0,
                });
                sly.init();
                $('.pointer_list').on('click',function () {
                    var id_room_target = parseInt($(this).data('id'));
                    goto('',[id_room_target,null,null]);
                });
                $('#list_right').on('click',function () {
                    slider_index++;
                    if(slider_index>panoramas.length-1) slider_index = 0;
                    if(!panoramas[slider_index].visible_list) {
                        $('#list_right').trigger('click');
                        return;
                    }
                    var id_room_target = panoramas[slider_index].id;
                    goto('',[id_room_target,null,null]);
                });
                $('#list_left').on('click',function () {
                    slider_index--;
                    if(slider_index<0) slider_index = panoramas.length-1;
                    if(!panoramas[slider_index].visible_list) {
                        $('#list_left').trigger('click');
                        return;
                    }
                    var id_room_target = panoramas[slider_index].id;
                    goto('',[id_room_target,null,null]);
                });
                switch(auto_show_slider) {
                    case 0:
                        $('.list_control').show();
                        controls_status['list']=true;
                        break;
                    case 1:
                        $('.list_control').show();
                        var f_h = parseInt($('.frame').height());
                        $('.frame').css('z-index',999);
                        $('.frame').css('opacity',1);
                        $('.list_control').addClass('active_control');
                        $('.list_control').css('bottom',f_h+'px');
                        $('.info_control').css('bottom',(f_h+4)+'px');
                        $('.gallery_control').css('bottom',(f_h+4)+'px');
                        $('#skitt-ui').css('bottom',(f_h+4)+'px');
                        $('.pnlm-compass').css('bottom',(f_h+4)+'px');
                        $('.fb_dialog').css('bottom',(f_h+4)+'px');
                        $('.list_control i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                        controls_status['list']=true;
                        break;
                    case 2:
                        $('.list_control').addClass('hidden');
                        controls_status['list']=false;
                        break;
                }
                $("body").floatingSocialShare({
                    place: "content-left",
                    counter: false,
                    twitter_counter: false,
                    buttons: [
                        "mail", "facebook", "linkedin",
                        "twitter", "vk", "telegram",
                        "viber", "whatsapp"
                    ],
                    title: name_virtual_tour,
                    url: window.location.href,
                    text: {
                        'default': 'share with ',
                        'facebook': 'share with facebook',
                        'twitter': 'tweet'
                    },
                    text_title_case: false,
                    description: $('meta[name="description"]').attr("content"),
                    media: $('meta[property="og:image"]').attr("content"),
                    target: true,
                    popup: true,
                    popup_width: 800,
                    popup_height: 600
                });
                if(window.is_mobile) {
                    var link_whatsapp = $('.whatsapp').attr('href');
                    link_whatsapp = link_whatsapp.replace('web.whatsapp','api.whatsapp');
                    $('.whatsapp').attr('href',link_whatsapp);
                }
                $('.fb_dialog').css('opacity',1);
                if(window.peer_id!='') {
                    init_peer();
                } else {
                    if(window.is_mobile) {
                        if(intro_mobile!='') {
                            $('.intro_img').css('display','flex');
                        }
                    } else {
                        if(intro_desktop!='') {
                            $('.intro_img').css('display','flex');
                        }
                    }
                }
                preload_images();
            },500);
        },2000);
    }

    window.toggle_fullscreen = function () {
        if($('.fullscreen_control').hasClass('active_control')) {
            $('.fullscreen_control').removeClass('active_control');
            controls_status['fullscreen']=false;
        } else {
            $('.fullscreen_control').addClass('active_control');
            controls_status['fullscreen']=true;
        }
        if(current_panorama_type=='image') {
            pano_viewer.toggleFullscreen();
        } else {
            video_viewer.pnlmViewer.toggleFullscreen();
        }
        window.scrollTo(0, 0);
    }

    window.close_live = function () {
        $.confirm({
            useBootstrap: false,
            closeIcon: false,
            type: 'red',
            typeAnimated: true,
            title: 'Live Session',
            content: 'Are you sure you want to end this live session? The link generated will be invalidated.',
            buttons: {
                end_call: {
                    text: 'End Call',
                    btnClass: 'btn-red',
                    action: function(){
                        try {
                            window.stream_sender.getTracks().forEach(function(track) { track.stop(); })
                        } catch (e) {}
                        try {
                            call_session.close();
                        } catch (e) {}
                        $('#webcam_my').hide();
                        peer.destroy();
                        live_session_connected = false;
                        clearInterval(interval_live_session);
                        $('.live_call').hide();
                        $('.live_control').removeClass('active_control');
                        exit_sender_viewer();
                    }
                },
                cancel: function () {

                },
            }
        });
    }

    window.close_live_receiver = function () {
        $.confirm({
            useBootstrap: false,
            closeIcon: false,
            type: 'red',
            typeAnimated: true,
            title: 'Live Session',
            content: 'Are you sure you want to end this live session?',
            buttons: {
                end_call: {
                    text: 'End Call',
                    btnClass: 'btn-red',
                    action: function(){
                        try {
                            window.stream_sender.getTracks().forEach(function(track) { track.stop(); })
                        } catch (e) {}
                        try {
                            call_session.close();
                        } catch (e) {}
                        $('#webcam_my').hide();
                        peer.destroy();
                        $('.live_call').hide();
                    }
                },
                cancel: function () {

                },
            }
        });
    }

    window.toggle_live = function () {
        if($('.live_control').hasClass('active_control')) {
            close_live();
        } else {
            $('.live_control').addClass('active_control');
            close_menu_controls();
            close_list_alt_menu();
            init_peer();
        }
    }

    window.toggle_orient = function () {
        if($('.orient_control').hasClass('active_control')) {
            $('.orient_control').removeClass('active_control');
            $('.orient_control .fa-circle').removeClass('active').addClass('not_active');
            controls_status['orient']=false;
            if(current_panorama_type=='image') {
                pano_viewer.stopOrientation();
            } else {
                video_viewer.pnlmViewer.stopOrientation();
            }
        } else {
            $('.orient_control').addClass('active_control');
            $('.orient_control .fa-circle').removeClass('not_active').addClass('active');
            controls_status['orient']=true;
            if(current_panorama_type=='image') {
                pano_viewer.startOrientation();
            } else {
                video_viewer.pnlmViewer.startOrientation();
            }
            close_menu_controls();
        }
    }

    window.open_gallery = function () {
        $('.gallery_control').lightGallery({
            dynamic: true,
            thumbnail: true,
            download: false,
            preload: 3,
            zoom: true,
            hash: false,
            autoplay: false,
            dynamicEl: gallery_images
        });
    }

    window.toggle_map = function () {
        if($('.map_control').hasClass('active_control')) {
            $('.map').hide();
            $('.map_control').removeClass('active_control');
            $('.map_control i').removeClass('icon-map_on').addClass('icon-map_off');
            controls_status['map']=false;
        } else {
            $('.map').show();
            $('.map_control').addClass('active_control');
            $('.map_control i').removeClass('icon-map_off').addClass('icon-map_on');
            controls_status['map']=true;
        }
    }

    window.toggle_list = function () {
        if($('.list_control').hasClass('active_control')) {
            $('.frame').css('z-index',0);
            $('.frame').css('opacity',0);
            $('.list_control').removeClass('active_control');
            $('.list_control').css('bottom','0px');
            $('.info_control').css('bottom','4px');
            $('.gallery_control').css('bottom','4px');
            $('#skitt-ui').css('bottom','4px');
            $('.pnlm-compass').css('bottom','4px');
            $('.fb_dialog').css('bottom','4px');
            $('.list_control i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            controls_status['list']=false;
        } else {
            var f_h = parseInt($('.frame').height());
            $('.frame').css('z-index',999);
            $('.frame').css('opacity',1);
            $('.list_control').addClass('active_control');
            $('.list_control').css('bottom',f_h+'px');
            $('.info_control').css('bottom',(f_h+4)+'px');
            $('.gallery_control').css('bottom',(f_h+4)+'px');
            $('#skitt-ui').css('bottom',(f_h+4)+'px');
            $('.pnlm-compass').css('bottom',(f_h+4)+'px');
            $('.fb_dialog').css('bottom',(f_h+4)+'px');
            $('.list_control i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            controls_status['list']=true;
        }
    }

    window.toggle_share = function () {
        if($('.share_control').hasClass('active_control')) {
            $('#floatingSocialShare').css('z-index',0);
            $('#floatingSocialShare').hide();
            $('.share_control').removeClass('active_control');
            $('.share_control .fa-circle').removeClass('active').addClass('not_active');
            controls_status['share']=false;
        } else {
            $('#floatingSocialShare').css('z-index',999);
            $('#floatingSocialShare').show();
            $('.share_control').addClass('active_control');
            $('.share_control .fa-circle').removeClass('not_active').addClass('active');
            controls_status['share']=true;
        }
    }

    window.toggle_icons = function () {
        if($('.icons_control').hasClass('active_control')) {
            $('.custom-hotspot').css('opacity',0);
            $('.custom-hotspot-content').css('opacity',0);
            $('.icons_control').removeClass('active_control');
            $('.icons_control .fa-circle').removeClass('active').addClass('not_active');
            controls_status['icons']=false;
        } else {
            $('.custom-hotspot').css('opacity',1);
            $('.custom-hotspot-content').css('opacity',1);
            $('.icons_control').addClass('active_control');
            $('.icons_control .fa-circle').removeClass('not_active').addClass('active');
            controls_status['icons']=true;
        }
    }

    function audio_isPlaying() {
        return !audio_player.paused && !audio_player.ended && 0 < audio_player.currentTime;
    }

    window.toggle_song = function () {
        if($('.song_control').hasClass('active_control')) {
            audio_player.volume = 0;
            $('.song_control').removeClass('active_control');
            $('.song_control i').removeClass('fa-volume-down').addClass('fa-volume-mute');
            controls_status['song']=false;
        } else {
            audio_player.volume = 1;
            if(!audio_isPlaying()) audio_player.play();
            $('.song_control').addClass('active_control');
            $('.song_control i').addClass('fa-volume-down').removeClass('fa-volume-mute');
            controls_status['song']=true;
        }
    }

    window.view_info_box = function () {
        poi_open = true;
        info_box = '<div class="bootstrap-iso">'+info_box+'</div>';
        $.fancybox.open({
            src  : info_box,
            type : 'html',
            touch: false
        });
    }

    window.view_form = function() {
        poi_open = true;
        var html_content = '';
        var content = form_content;
        try {
            var title = content[0].title;
            var button = content[0].button;
            if(button=='') button = 'SUBMIT';
            var description = content[0].description;
            html_content += '<div><form method="post" action="#" class="form_main" style="text-align: center;">'
            if(title!='') {
                html_content += '<h2 style="margin-bottom: 5px">'+title.toUpperCase()+'</h2>';
            }
            if(description!='') {
                html_content += '<p>'+description+'</p><br>';
            }
            for(var i=1;i<=5;i++) {
                if(!('type' in content[i])) content[i]['type']='text';
                if(content[i]['enabled']) {
                    if(content[i]['required']) {
                        html_content += '<b>'+content[i]['label']+' *</b><br>';
                        html_content += '<input style="width: 100%;text-align: center;height: 30px;margin-bottom: 5px" required id="form_field_'+i+'" name="form_field_'+i+'" type="'+content[i]['type']+'" />';
                    } else {
                        html_content += '<b>'+content[i]['label']+'</b><br>';
                        html_content += '<input style="width: 100%;text-align: center;height: 30px;margin-bottom: 5px" id="form_field_'+i+'" name="form_field_'+i+'" type="'+content[i]['type']+'" />';
                    }
                    html_content += '<br>';
                }
            }
            html_content += '<input type="hidden" name="id_room" value="0" />';
            html_content += '<input type="hidden" name="title" value="'+title+'" />';
            html_content += '<button type="submit" style="margin-top: 10px">'+button+'</button>';
            html_content += '</form></div>';
            form_lightbox = $.fancybox.open({
                src  : html_content,
                type : 'html'
            });
            $(".form_main").submit(function(e){
                var form_data = $(this).serialize();
                confirm_main_form(form_data);
                e.preventDefault();
            });
        } catch (e) {}
    }

    $(document).on('afterClose.fb', function( e, instance, slide ) {
        poi_open = false;
        if(!live_session_connected) {
            if(video_opened) {
                if(controls_status['song']) {
                    audio_player.volume = 1;
                }
                video_opened = false;
            }
        } else {
            try {
                peer_conn.send({type:'close_content'});
            } catch (e) {}
        }
    });

    window.open_video = function (id) {
        video_opened = true;
        audio_player.volume = 0;
        if(live_session_connected) {
            try {
                peer_conn.send({type:'view_video',id:id});
            } catch (e) {}
        }
    }

    function view_content(hotSpotDiv, args) {
        poi_open = true;
        if(live_session_connected) {
            try {
                peer_conn.send({type:'view_content',args:args});
            } catch (e) {}
        }
        try {
            if(args.content.substr(args.content.length - 4)=='.pdf') {
                args.type = 'link';
            }
        } catch (e) {}
        switch(args.type) {
            case 'image':
                if(args.title==null) args.title = '';
                if(args.description==null) args.description = '';
                $.fancybox.open({
                    src  : args.content,
                    type : 'image',
                    opts : {
                        caption : '<b>'+args.title+'</b><br><i>'+args.description+'</i>',
                    }
                });
                break;
            case 'gallery':
                $('.hotspot_'+args.id).lightGallery({
                    dynamic: true,
                    thumbnail: true,
                    download: false,
                    preload: 3,
                    zoom: true,
                    hash: false,
                    autoplay: false,
                    dynamicEl: args.content
                });
                break;
            case 'link':
                $.fancybox.open({
                    src  : args.content,
                    type : 'iframe'
                });
                break;
            case 'html':
            case 'html_sc':
                $.fancybox.open({
                    src  : args.content,
                    type : 'html'
                });
                break;
            case 'audio':
                var html_content = '';
                html_content += '<div><audio id="audio_poi" controls>\n' +
                    '  <source src="'+args.content+'" type="audio/mpeg">\n' +
                    'Your browser does not support the audio element.\n' +
                    '</audio></div>';
                $.fancybox.open({
                    src: html_content,
                    type: 'html',
                    touch: false,
                    beforeClose: function(instance,slide) {
                        if(!live_session_connected) {
                            if(controls_status['song']) {
                                audio_player.volume = 1;
                            }
                        }
                    }
                });
                document.getElementById('audio_poi').addEventListener('play', function () {
                    audio_player.volume = 0;
                });
                break;
            case 'video360':
                var html_content = '';
                html_content += '<div style="padding:32px;width:calc(100% - 20px);height:calc(100% - 20px);"><deo-video loop width="100%" height="100%" format="mono" angle="360" id="video360">' +
                    '    <source src="'+args.content+'" />' +
                    '</deo-video></div>';
                $.fancybox.open({
                    src  : html_content,
                    type : 'html',
                    touch : false,
                    beforeClose: function(instance,slide) {
                        $('.deo-close-button').trigger('click');
                        if(!live_session_connected) {
                            if(controls_status['song']) {
                                audio_player.volume = 1;
                            }
                        }
                    }
                });
                DEO.initialize(document.getElementById('video360'));
                document.getElementById('video360').addEventListener('play', function () {
                    audio_player.volume = 0;
                });
                document.getElementById('video360').addEventListener('end', function () {
                    if(!live_session_connected) {
                        if(controls_status['song']) {
                            audio_player.volume = 1;
                        }
                    }
                });
                break;
            case 'form':
                var html_content = '';
                try {
                    var content = JSON.parse(args.content);
                    var title = content[0].title;
                    var button = content[0].button;
                    if(button=='') button = 'SUBMIT';
                    var description = content[0].description;
                    html_content += '<div><form method="post" action="#" class="form_poi" style="text-align: center;">'
                    if(title!='') {
                        html_content += '<h2 style="margin-bottom: 5px">'+title.toUpperCase()+'</h2>';
                    }
                    if(description!='') {
                        html_content += '<p>'+description+'</p><br>';
                    }
                    for(var i=1;i<=5;i++) {
                        if(!('type' in content[i])) content[i]['type']='text';
                        if(content[i]['enabled']) {
                            if(content[i]['required']) {
                                html_content += '<b>'+content[i]['label']+' *</b><br>';
                                html_content += '<input style="width: 100%;text-align: center;height: 30px;margin-bottom: 5px" required id="form_field_'+i+'" name="form_field_'+i+'" type="'+content[i]['type']+'" />';
                            } else {
                                html_content += '<b>'+content[i]['label']+'</b><br>';
                                html_content += '<input style="width: 100%;text-align: center;height: 30px;margin-bottom: 5px" id="form_field_'+i+'" name="form_field_'+i+'" type="'+content[i]['type']+'" />';
                            }
                            html_content += '<br>';
                        }
                    }
                    html_content += '<input type="hidden" name="id_room" value="'+args.id_room+'" />';
                    html_content += '<input type="hidden" name="title" value="'+title+'" />';
                    html_content += '<button type="submit" style="margin-top: 10px">'+button+'</button>';
                    html_content += '</form></div>';
                    form_lightbox = $.fancybox.open({
                        src  : html_content,
                        type : 'html'
                    });
                    $(".form_poi").submit(function(e){
                        var form_data = $(this).serialize();
                        confirm_poi_form(form_data);
                        e.preventDefault();
                    });
                } catch (e) {}
                break;
        }

        $.ajax({
            url: "ajax/set_statistics.php",
            type: "POST",
            data: {
                type: 'poi',
                id: args.id
            },
            async: true
        });
    }

    window.confirm_poi_form = function(form_data) {
        var btn_c = $('.form_poi button').html();
        $('.form_poi button').addClass("disabled");
        $('.form_poi button').html("<i class='fas fa-circle-notch fa-spin'></i>");
        $.ajax({
            url: "ajax/store_form_data.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtual_tour,
                form_data: form_data
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=='ok') {
                    $('.form_poi button').addClass("button_success_form");
                    $('.form_poi button').html("<i class='fas fa-check'></i>");
                    setTimeout(function () {
                        $('.form_poi button').removeClass("disabled");
                        $('.form_poi button').removeClass("button_success_form");
                        form_lightbox.close();
                    },1000);
                } else {
                    $('.form_poi button').html(btn_c);
                    $('.form_poi button').removeClass("disabled");
                }
            }
        });
    }

    window.confirm_main_form = function(form_data) {
        var btn_c = $('.form_poi button').html();
        $('.form_main button').addClass("disabled");
        $('.form_main button').html("<i class='fas fa-circle-notch fa-spin'></i>");
        $.ajax({
            url: "ajax/store_form_data.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtual_tour,
                form_data: form_data
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.status=='ok') {
                    $('.form_main button').addClass("button_success_form");
                    $('.form_main button').html("<i class='fas fa-check'></i>");
                    setTimeout(function () {
                        $('.form_main button').removeClass("disabled");
                        $('.form_main button').removeClass("button_success_form");
                        form_lightbox.close();
                    },1000);
                } else {
                    $('.form_main button').html(btn_c);
                    $('.form_main button').removeClass("disabled");
                }
            }
        });
    }

    window.set_poi_statistics = function(hotSpotDiv,id) {
        $.ajax({
            url: "ajax/set_statistics.php",
            type: "POST",
            data: {
                type: 'poi',
                id: id
            },
            async: true
        });
    }

    function hotspot_nadir(hotSpotDiv, args) {
        if(vr_enabled) {
            hotSpotDiv.classList.add('nadir-hotspot-small_vr');
        }
        hotSpotDiv.classList.add('noselect');
        hotSpotDiv.style = "background-image:url(../viewer/content/"+args+");background-size:cover;";
    }

    function hotspot_content(hotSpotDiv, args) {
        hotSpotDiv.classList.add('noselect');
        hotSpotDiv.classList.add('hotspot_'+args.id);
        var style_t = parseInt(args.style);
        switch (style_t) {
            case 0:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                switch (args.type) {
                    case 'download':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.download = '';
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'link_ext':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.target = '_blank';
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'video':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.id = "video_"+args.id;
                        a.onclick = function() {
                            open_video("video_"+args.id);
                        };
                        a.setAttribute('data-fancybox','');
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    default:
                        hotSpotDiv.appendChild(i);
                        break;
                }
                break;
            case 1:
                var img = document.createElement('img');
                img.src = 'icons/'+args.img_icon_library;
                img.style = "width:50px;margin: 0 auto;vertical-align:middle;";
                switch (args.type) {
                    case 'download':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.download = '';
                        a.appendChild(img);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'link_ext':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.target = '_blank';
                        a.appendChild(img);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'video':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.id = "video_"+args.id;
                        a.onclick = function() {
                            open_video("video_"+args.id);
                        };
                        a.setAttribute('data-fancybox','');
                        a.appendChild(img);
                        hotSpotDiv.appendChild(a);
                        break;
                    default:
                        hotSpotDiv.appendChild(img);
                        break;
                }
                break;
            case 2:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                var span = document.createElement('span');
                span.innerHTML = '&nbsp;'+args.label.toUpperCase();
                span.style = "vertical-align: text-top;color:"+args.color;
                switch (args.type) {
                    case 'download':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.download = '';
                        a.appendChild(i);
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'link_ext':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.target = '_blank';
                        a.appendChild(i);
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'video':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.id = "video_"+args.id;
                        a.onclick = function() {
                            open_video("video_"+args.id);
                        };
                        a.setAttribute('data-fancybox','');
                        a.appendChild(i);
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    default:
                        hotSpotDiv.appendChild(i);
                        hotSpotDiv.appendChild(span);
                        break;
                }
                break;
            case 3:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.label.toUpperCase()+'&nbsp;';
                span.style = "vertical-align: text-top;color:"+args.color;
                var i = document.createElement('i');
                i.className = args.icon;
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                switch (args.type) {
                    case 'download':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.download = '';
                        a.appendChild(span);
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'link_ext':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.target = '_blank';
                        a.appendChild(span);
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'video':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.id = "video_"+args.id;
                        a.onclick = function() {
                            open_video("video_"+args.id);
                        };
                        a.setAttribute('data-fancybox','');
                        a.appendChild(span);
                        a.appendChild(i);
                        hotSpotDiv.appendChild(a);
                        break;
                    default:
                        hotSpotDiv.appendChild(span);
                        hotSpotDiv.appendChild(i);
                        break;
                }
                break;
            case 4:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.label.toUpperCase();
                span.style = "font-size:14px;vertical-align: text-top;color:"+args.color;
                switch (args.type) {
                    case 'download':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.download = '';
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'link_ext':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.target = '_blank';
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    case 'video':
                        var a = document.createElement('a');
                        a.style = 'text-decoration:none;';
                        a.href = args.content;
                        a.id = "video_"+args.id;
                        a.onclick = function() {
                            open_video("video_"+args.id);
                        };
                        a.setAttribute('data-fancybox','');
                        a.appendChild(span);
                        hotSpotDiv.appendChild(a);
                        break;
                    default:
                        hotSpotDiv.appendChild(span);
                        break;
                }
                break;
        }
    }

    function hotspot(hotSpotDiv, args) {
        hotSpotDiv.classList.add('custom-tooltip');
        hotSpotDiv.classList.add('noselect');
        hotSpotDiv.classList.add('marker_'+args.id);
        var markers_show_room_t = parseInt(args.show_room);
        if(vr_enabled) markers_show_room_t = 0;
        switch (markers_show_room_t) {
            case 0:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.setAttribute('data-icon', args.icon);
                if(imgs_loaded.indexOf(args.id_room_target) !== -1) {
                    i.className = args.icon+" marker_img_"+args.id_room_target;
                } else {
                    i.className = "fas fa-spin fa-circle-notch marker_img_"+args.id_room_target;
                }
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                hotSpotDiv.title = args.name_room_target.toUpperCase();
                hotSpotDiv.classList.add('tooltip_marker_'+args.id_room_target);
                try {
                    $('.tooltip_marker_'+args.id_room_target).tooltipster('destroy');
                } catch (e) {}
                $('.tooltip_marker_'+args.id_room_target).tooltipster({
                    arrow: false,
                    hideOnClick: true,
                    touchDevices: false,
                    timer: 2000
                });
                break;
            case 1:
                hotSpotDiv.style.background = args.background;
                var i = document.createElement('i');
                i.setAttribute('data-icon', args.icon);
                if(imgs_loaded.indexOf(args.id_room_target) !== -1) {
                    i.className = args.icon+" marker_img_"+args.id_room_target;
                } else {
                    i.className = "fas fa-spin fa-circle-notch marker_img_"+args.id_room_target;
                }
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                var span = document.createElement('span');
                span.innerHTML = '&nbsp;'+args.name_room_target.toUpperCase();
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
            case 2:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.name_room_target.toUpperCase()+'&nbsp;';
                span.style = "vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                var i = document.createElement('i');
                i.setAttribute('data-icon', args.icon);
                if(imgs_loaded.indexOf(args.id_room_target) !== -1) {
                    i.className = args.icon+" marker_img_"+args.id_room_target;
                } else {
                    i.className = "fas fa-spin fa-circle-notch marker_img_"+args.id_room_target;
                }
                i.style = "margin: 0 auto;vertical-align:middle;font-size:24px;color:"+args.color;
                hotSpotDiv.appendChild(i);
                break;
            case 3:
                hotSpotDiv.style.background = args.background;
                var span = document.createElement('span');
                span.innerHTML = args.name_room_target.toUpperCase();
                span.classList.add("marker_img_"+args.id_room_target);
                span.style = "font-size:14px;vertical-align: text-top;color:"+args.color;
                hotSpotDiv.appendChild(span);
                break;
            case 4:
                var img = document.createElement('img');
                img.src = 'icons/'+args.img_icon_library;
                img.classList.add("marker_img_"+args.id_room_target);
                img.style = "width:50px";
                hotSpotDiv.appendChild(img);
                hotSpotDiv.title = args.name_room_target.toUpperCase();
                hotSpotDiv.classList.add('tooltip_marker_'+args.id_room_target);
                try {
                    $('.tooltip_marker_'+args.id_room_target).tooltipster('destroy');
                } catch (e) {}
                $('.tooltip_marker_'+args.id_room_target).tooltipster({
                    arrow: false,
                    hideOnClick: true,
                    touchDevices: false,
                    timer: 2000
                });
                break;
        }
        if(imgs_loaded.indexOf(args.id_room_target) == -1) {
            hotSpotDiv.classList.add('disabled');
        }
    }

    function initialize_room(id,cb,click_m) {
        var len = panoramas.length;
        var prev_panorama = panoramas[(id+len-1)%len];
        var next_panorama = panoramas[(id+1)%len];
        $('#prev_arrow').attr('onclick','goto("",['+prev_panorama.id+',null,null]);');
        $('#prev_arrow').attr('data-roomtarget',prev_panorama.id);
        $('#prev_arrow').attr('title',prev_panorama.nome);
        $('#next_arrow').attr('onclick','goto("",['+next_panorama.id+',null,null]);');
        $('#next_arrow').attr('data-roomtarget',next_panorama.id);
        $('#next_arrow').attr('title',next_panorama.nome);
        $(".arrows").addClass('disabled');
        if(imgs_loaded.indexOf(prev_panorama.id) !== -1) {
            $(".arrows_nav").find("[data-roomtarget='" + prev_panorama.id + "']").removeClass('disabled');
        }
        if(imgs_loaded.indexOf(next_panorama.id) !== -1) {
            $(".arrows_nav").find("[data-roomtarget='" + next_panorama.id + "']").removeClass('disabled');
        }
        try {
            $('#prev_arrow').tooltipster('destroy');
        } catch (e) {}
        $('#prev_arrow').tooltipster({
            theme: 'tooltipster-borderless',
            side: 'right'
        });
        try {
            $('#next_arrow').tooltipster('destroy');
        } catch (e) {}
        $('#next_arrow').tooltipster({
            theme: 'tooltipster-borderless',
            side: 'left'
        });
        $('.name_vt').html(name_virtual_tour+" - "+panoramas[id].nome);
        if(author!='') {
            $('.author_vt').html('by '+author);
        }
        if(panoramas[id].allow_pitch==1) {
            var minPitch = parseInt(panoramas[id].min_pitch)-34;
            var maxPitch = parseInt(panoramas[id].max_pitch)+34;
            var pitch = parseInt(panoramas[id].pitch);
        } else {
            var minPitch = 0;
            var maxPitch = 0;
            var pitch = 0;
        }
        if(vr_enabled) {
            var orientationOnByDefault = true;
            var min_hfov_f = 110;
            var max_hfov_f = 110;
            var hfov_f = 110;
            var draggable = false;
            var autorotate_speed_f = 0;
            var workingYaw = parseInt(panoramas[id].yaw);
        } else {
            if(controls_status['orient']) {
                var orientationOnByDefault = true;
            } else {
                var orientationOnByDefault = false;
            }
            var min_hfov_f = min_hfov;
            var max_hfov_f = max_hfov;
            var hfov_f = hfov;
            var draggable = true;
            var autorotate_speed_f = autorotate_speed;
        }

        if(sameAzimuth && click_m) {
            if(current_panorama_type=='image') {
                var current_yaw = parseInt(pano_viewer.getYaw());
                var current_north = parseInt(pano_viewer.getNorthOffset());
            } else {
                var current_yaw = parseInt(video_viewer.pnlmViewer.getYaw());
                var current_north = parseInt(video_viewer.pnlmViewer.getNorthOffset());
            }
            var workingYaw = current_yaw + (current_north || 0) - (parseInt(panoramas[id].northOffset) || 0);
            pitch = 0;
        } else {
            var workingYaw = parseInt(panoramas[id].yaw);
        }

        try {
            pano_viewer.destroy();
        } catch (e) {}
        try {
            video_viewer.pnlmViewer.destroy();
            video_viewer.dispose();
            video_viewer = null;
        } catch (e) {}

        if(window.innerHeight > window.innerWidth){
            if(window.innerWidth<500) {
                var touchPanSpeedCoeffFactor = 1.5;
                var friction = 0.08;
            } else {
                var touchPanSpeedCoeffFactor = 1;
                var friction = 0.2;
            }
        } else {
            var touchPanSpeedCoeffFactor = 1;
            var friction = 0.1;
        }

        if(panoramas[id].type=='video') {
            if(vr_enabled) {
                $('#div_panoramas').append('<video id="video_viewer" class="video-js vjs-default-skin vjs-big-play-centered" style="display: none;position: absolute;top: 0;left: 0;width: 50%;height: 100vh;opacity: 0" muted preload="none" crossorigin="anonymous"><source src="'+panoramas[id].panorama_video+'" type="video/mp4"/></video>');
            } else {
                $('#div_panoramas').append('<video id="video_viewer" class="video-js vjs-default-skin vjs-big-play-centered" style="display: none;width: 100%;height: 100vh;opacity: 0" muted preload="none" crossorigin="anonymous"><source src="'+panoramas[id].panorama_video+'" type="video/mp4"/></video>');
            }
            current_panorama_type = 'video';
            $('#video_viewer').show();
            $('#panorama_viewer').css('opacity',0);
            $('#panorama_viewer').css('z-index',0);
            video_viewer = videojs('video_viewer', {
                loop: true,
                autoload: true,
                plugins: {
                    pannellum: {
                        "id_room": panoramas[id].id,
                        "autoLoad": true,
                        "showZoomCtrl": false,
                        "showFullscreenCtrl": false,
                        "orientationOnByDefault": orientationOnByDefault,
                        "draggable": draggable,
                        "autoRotate": autorotate_speed_f,
                        "autoRotateStopDelay": 10,
                        "autoRotateInactivityDelay": autorotate_inactivity,
                        "friction": friction,
                        "touchPanSpeedCoeffFactor": touchPanSpeedCoeffFactor,
                        "compass": true,
                        "northOffset": parseInt(panoramas[id].northOffset),
                        "pitch": pitch,
                        "yaw": workingYaw,
                        "hfov": hfov_f,
                        "minHfov": min_hfov_f,
                        "maxHfov" : max_hfov_f,
                        "minPitch": minPitch,
                        "maxPitch" : maxPitch,
                        "hotSpots": panoramas[id].hotSpots,
                    }
                }
            });
            video_viewer.load();
            video_viewer.on('ready', function() {
                video_viewer.play();
                video_viewer.pnlmViewer.on('load',function () {
                    player_initialized(cb,id);
                });
            });
        } else {
            current_panorama_type = 'image';
            $('#video_viewer').css('opacity',0);
            $('#video_viewer').css('z-index',0);
            pano_viewer = pannellum.viewer('panorama_viewer', {
                "id_room": panoramas[id].id,
                "type": "equirectangular",
                "panorama": panoramas[id].panorama_image,
                "autoLoad": true,
                "showZoomCtrl": false,
                "showFullscreenCtrl": false,
                "orientationOnByDefault": orientationOnByDefault,
                "draggable": draggable,
                "autoRotate": autorotate_speed_f,
                "autoRotateStopDelay": 10,
                "autoRotateInactivityDelay": autorotate_inactivity,
                "friction": friction,
                "touchPanSpeedCoeffFactor": touchPanSpeedCoeffFactor,
                "compass": true,
                "northOffset": parseInt(panoramas[id].northOffset),
                "pitch": pitch,
                "yaw": workingYaw,
                "hfov": hfov_f,
                "minHfov": min_hfov_f,
                "maxHfov" : max_hfov_f,
                "minPitch": minPitch,
                "maxPitch" : maxPitch,
                "hotSpots": panoramas[id].hotSpots,
            });
            pano_viewer.on('load',function () {
                player_initialized(cb,id);
            });
        }

        if(vr_enabled) {
            try {
                pano_viewer_vr.destroy();
            } catch (e) {}
            try {
                video_viewer_vr.pnlmViewer.destroy();
                video_viewer_vr.dispose();
                video_viewer_vr = null;
            } catch (e) {}
            if(panoramas[id].type=='video') {
                $('#div_panoramas').append('<video id="video_viewer_vr" class="video-js vjs-default-skin vjs-big-play-centered" style="display: none;position: absolute;width: 50%;top: 0;left: 50%;height: 100vh;opacity: 0" muted preload="none" crossorigin="anonymous"><source src="'+panoramas[id].panorama_video+'" type="video/mp4"/></video>');
                $('#video_viewer_vr').show();
                video_viewer_vr = videojs('video_viewer_vr', {
                    loop: true,
                    autoload: true,
                    plugins: {
                        pannellum: {
                            "id_room": panoramas[id].id,
                            "autoLoad": true,
                            "showZoomCtrl": false,
                            "showFullscreenCtrl": false,
                            "orientationOnByDefault": orientationOnByDefault,
                            "draggable": draggable,
                            "autoRotate": autorotate_speed_f,
                            "autoRotateStopDelay": 10,
                            "autoRotateInactivityDelay": autorotate_inactivity,
                            "friction": friction,
                            "touchPanSpeedCoeffFactor": touchPanSpeedCoeffFactor,
                            "compass": true,
                            "northOffset": parseInt(panoramas[id].northOffset),
                            "pitch": pitch,
                            "yaw": workingYaw,
                            "hfov": hfov_f,
                            "minHfov": min_hfov_f,
                            "maxHfov" : max_hfov_f,
                            "minPitch": minPitch,
                            "maxPitch" : maxPitch,
                            "hotSpots": panoramas[id].hotSpots_vr,
                        }
                    }
                });
                video_viewer_vr.load();
                video_viewer_vr.on('ready', function() {
                    video_viewer_vr.play();
                    video_viewer_vr.pnlmViewer.on('load',function () {
                        $('#video_viewer_vr').animate({
                            opacity: 1
                        }, { duration: 250, queue: false });
                        $('#video_viewer_vr').css('z-index',10);
                        $('#background_pano').fadeOut();
                        $('#background_pano_vr').fadeOut();
                        $('.pnlm-controls-container').hide();
                        $('.map').hide();
                        $('#floatingSocialShare').hide();
                        $('.custom-hotspot-content').css('opacity',0);
                        $('.pnlm-orientation-button').hide();
                        if(show_compass) {
                            $('.pnlm-compass').show();
                        } else {
                            $('.pnlm-compass').hide();
                        }
                        clearInterval(interval_position);
                        interval_position = setInterval(function () {
                            check_vr_pos();
                        },250);
                        setTimeout(function () {
                            $('.loading_vr').fadeOut();
                        },500);
                    });
                });
            } else {
                pano_viewer_vr = pannellum.viewer('panorama_viewer_vr', {
                    "id_room": panoramas[id].id,
                    "type": "equirectangular",
                    "panorama": panoramas[id].panorama_image,
                    "autoLoad": true,
                    "showZoomCtrl": false,
                    "showFullscreenCtrl": false,
                    "orientationOnByDefault": orientationOnByDefault,
                    "draggable": draggable,
                    "autoRotate": autorotate_speed_f,
                    "autoRotateStopDelay": 10,
                    "autoRotateInactivityDelay": autorotate_inactivity,
                    "compass": true,
                    "northOffset": panoramas[id].northOffset,
                    "pitch": pitch,
                    "yaw": workingYaw,
                    "hfov": hfov_f,
                    "minHfov": min_hfov_f,
                    "maxHfov" : max_hfov_f,
                    "minPitch": minPitch,
                    "maxPitch" : maxPitch,
                    "hotSpots": panoramas[id].hotSpots_vr,
                });
                pano_viewer_vr.on('load',function () {
                    $('#panorama_viewer_vr').animate({
                        opacity: 1
                    }, { duration: 250, queue: false });
                    $('#panorama_viewer_vr').css('z-index',10);
                    $('#background_pano').fadeOut();
                    $('#background_pano_vr').fadeOut();
                    $('.pnlm-controls-container').hide();
                    $('.map').hide();
                    $('#floatingSocialShare').hide();
                    $('.custom-hotspot-content').css('opacity',0);
                    $('.pnlm-orientation-button').hide();
                    if(show_compass) {
                        $('.pnlm-compass').show();
                    } else {
                        $('.pnlm-compass').hide();
                    }
                    clearInterval(interval_position);
                    interval_position = setInterval(function () {
                        check_vr_pos();
                    },250);
                    setTimeout(function () {
                        $('.loading_vr').fadeOut();
                    },500);
                });
            }
        }
    }

    function player_initialized(cb,id) {
        if(cb) {
            if(current_panorama_type=='image') {
                $('#panorama_viewer').animate({
                    opacity: 1
                }, { duration: 250, queue: false });
                $('#panorama_viewer').css('z-index',10);
            } else {
                $('#video_viewer').animate({
                    opacity: 1
                }, { duration: 250, queue: false });
                $('#video_viewer').css('z-index',10);
            }
            if(array_maps.length>0) {
                if(controls_status['map']) {
                    $('.all_maps').hide();
                    $('.pointer').hide();
                    var id_map_target = panoramas[id].id_map;
                    if(!isNaN(id_map_target)) {
                        id_current_map = id_map_target;
                        $('.map_'+id_map_target).show();
                        $('.pointer_map_'+id_map_target).show();
                    } else {
                        $('.map_'+id_current_map).show();
                        $('.pointer_map_'+id_current_map).show();
                    }
                }
            }
            slider_index = id;
            try {
                sly.toCenter(id);
            } catch (e) {}
            var annotation_title = panoramas[id].annotation_title;
            var annotation_description = panoramas[id].annotation_description;
            if((annotation_title!='') && (annotation_description!='') && (show_annotations)) {
                $('.annotation_title').html(annotation_title.toUpperCase());
                $('.annotation_description').html(annotation_description);
                $('.annotation').fadeIn();
            } else {
                $('.annotation').hide();
            }
            var new_audio_src = '';
            if(panoramas[id].song!='') {
                new_audio_src = "content/"+panoramas[id].song;
            } else if(song_bg!='') {
                new_audio_src = "content/"+song_bg;
            }
            if(audio_player.src.toString().indexOf(new_audio_src) === -1) {
                audio_player.src = new_audio_src;
                if(controls_status['song']) {
                    audio_player.load();
                    audio_player.play();
                }
            }
        }
        poi_open = false;
        if (!controls_status['icons']) {
            $('.custom-hotspot').css('opacity',0);
            $('.custom-hotspot-content').css('opacity',0);
        }
        setTimeout(function () {
            if(current_panorama_type=='image') {
                pano_viewer.resize();
            } else {
                video_viewer.pnlmViewer.resize();
            }
            setTimeout(function () {
                if (controls_status['icons']) {
                    $('.custom-hotspot').css('opacity',1);
                    if(vr_enabled) {
                        $('.custom-hotspot-content').css('opacity',0);
                    } else {
                        $('.custom-hotspot-content').css('opacity',1);
                    }
                }
            },50);
        },100);
        if(show_compass) {
            $('.pnlm-compass').show();
        } else {
            $('.pnlm-compass').hide();
        }
        if(controls_status['presentation']) {
            if(current_panorama_type=='image') {
                pano_viewer.stopAutoRotate();
            } else {
                video_viewer.pnlmViewer.stopAutoRotate();
            }
            $('.pnlm-compass').hide();
        }
        if(!vr_enabled) {
            $('#background_pano').fadeOut(200);
            $('#loading_pano').css('opacity',0);
            $('#loading_pano').hide();
            $('.pointer_'+panoramas[id].id+' .view_direction__arrow').show();
            if($('.list_control').hasClass('active_control')) {
                var f_h = parseInt($('.frame').height());
                $('.pnlm-compass').css('bottom',(f_h+4)+'px');
            }
            if(!live_session_connected) {
                clearInterval(interval_position);
                interval_position = setInterval(function () {
                    check_pano_pos();
                },500);
            }
        }
        $('.pnlm-orientation-button').hide();
        $('.pnlm-compass').attr('onclick','set_initial_pos('+id+');');
        try {
            if($(".map").hasClass("map_zoomed")) {
                $(".map").removeClass('map_zoomed');
            }
        } catch(e) {}
        $.ajax({
            url: "ajax/set_statistics.php",
            type: "POST",
            data: {
                type: 'room',
                id: panoramas[id].id
            },
            async: true
        });
        clearInterval(interval_access_time_avg);
        access_time_avg = 0;
        access_time_id = panoramas[id].id;
        interval_access_time_avg = setInterval(function () {
            access_time_avg = access_time_avg + 1;
        }, 1000);
        $('.pnlm-container').focus();
        $('.pnlm-container').trigger('click');
        if(live_session_connected) {
            clearInterval(interval_live_session);
            interval_live_session = setInterval(function () {
                if(current_panorama_type=='image') {
                    var yaw = parseInt(pano_viewer.getYaw());
                    var pitch = parseInt(pano_viewer.getPitch());
                    var hfov = parseInt(pano_viewer.getHfov());
                } else {
                    var yaw = parseInt(video_viewer.pnlmViewer.getYaw());
                    var pitch = parseInt(video_viewer.pnlmViewer.getPitch());
                    var hfov = parseInt(video_viewer.pnlmViewer.getHfov());
                }
                try {
                    peer_conn.send({type:'lookAt',yaw:yaw,pitch:pitch,hfov:hfov});
                } catch (e) {}
            },1000);
        }
    }

    window.set_initial_pos = function(id) {
        if(peer_id=='') {
            if(!vr_enabled) {
                if(current_panorama_type=='image') {
                    pano_viewer.lookAt(parseInt(panoramas[id].pitch),parseInt(panoramas[id].yaw),parseInt(hfov));
                } else {
                    video_viewer.pnlmViewer.lookAt(parseInt(panoramas[id].pitch),parseInt(panoramas[id].yaw),parseInt(hfov));
                }
            }
        }
    }

    function check_vr_pos() {
        if(current_panorama_type=='image') {
            var yaw = parseInt(pano_viewer_vr.getYaw());
            var pitch = parseInt(pano_viewer_vr.getPitch());
        } else {
            var yaw = parseInt(video_viewer_vr.pnlmViewer.getYaw());
            var pitch = parseInt(video_viewer_vr.pnlmViewer.getPitch());
        }
        var index = get_id_viewer(current_id_panorama);
        var in_marker = false;
        jQuery.each(panoramas[index].hotSpots_vr, function(index, hotspot_vr) {
            var h_yaw = hotspot_vr.yaw;
            var h_pitch = hotspot_vr.pitch;
            if(((yaw<=h_yaw+5) && (yaw>=h_yaw-5)) && ((pitch<=h_pitch+5) && (pitch>=h_pitch-5))) {
                in_marker = true;
                $('.cursor_vr').addClass('cursor_vr_active');
                $('.cursor_vr').addClass('fa-pulse');
                if(goto_timeout == null) {
                    goto_timeout = setTimeout(function () {
                        goto_timeout = null;
                        goto('',hotspot_vr.clickHandlerArgs)
                    },2000);
                }
            }
        });
        if(!in_marker) {
            clearTimeout(goto_timeout);
            goto_timeout = null;
            if(interval_position == null) {
                interval_position = setInterval(function () {
                    check_vr_pos();
                },250);
            }
            $('.cursor_vr').removeClass('cursor_vr_active');
            $('.cursor_vr').removeClass('fa-pulse');
        }
    }

    function check_pano_pos() {
        if(current_panorama_type=='image') {
            var yaw = parseInt(pano_viewer.getYaw());
            var pitch = parseInt(pano_viewer.getPitch());
        } else {
            var yaw = parseInt(video_viewer.pnlmViewer.getYaw());
            var pitch = parseInt(video_viewer.pnlmViewer.getPitch());
        }
        var index = get_id_viewer(current_id_panorama);
        $('.pnlm-pointer').removeClass('hotspot_hover');
        var array_coord = [];
        jQuery.each(panoramas[index].hotSpots, function(index, hotspot) {
            var h_yaw = hotspot.yaw;
            var h_pitch = hotspot.pitch;
            if(((yaw<=h_yaw+20) && (yaw>=h_yaw-20)) && ((pitch<=h_pitch+20) && (pitch>=h_pitch-20))) {
                array_coord.push({x:h_yaw,y:h_pitch,hotspot:hotspot});
            }
        });
        if(array_coord.length>0) {
            sortByDistance(array_coord, {x: yaw, y: pitch});
            if(array_coord[0].hotspot.type=='marker') {
                $('.marker_'+array_coord[0].hotspot.id).addClass('hotspot_hover');
            } else {
                $('.hotspot_'+array_coord[0].hotspot.id).addClass('hotspot_hover');
            }
        }
    }

    const distance = (coor1, coor2) => {
        const x = coor2.x - coor1.x;
        const y = coor2.y - coor1.y;
        return Math.sqrt((x*x) + (y*y));
    };
    const sortByDistance = (coordinates, point) => {
        const sorter = (a, b) => distance(a, point) - distance(b, point);
        coordinates.sort(sorter);
    };

    window.goto = function(hotSpotDiv, args) {
        if(live_session_connected) {
            clearInterval(interval_live_session);
            try {
                peer_conn.send({type:'goto',args:args});
            } catch (e) {}
        }
        current_id_panorama = args[0];
        if(args[1]!=null) {
            if(current_panorama_type=='image') {
                var c_hfov = parseInt(pano_viewer.getHfov());
                if(c_hfov>90) c_hfov=90;
                pano_viewer.lookAt(0,args[2],c_hfov,250,function () {
                    fade_background(current_id_panorama,true);
                });
            } else {
                var c_hfov = parseInt(video_viewer.pnlmViewer.getHfov());
                if(c_hfov>90) c_hfov=90;
                video_viewer.pnlmViewer.lookAt(0,args[2],c_hfov,250,function () {
                    try {
                        video_viewer.pause();
                    } catch (e) {}
                    fade_background(current_id_panorama,true);
                });
            }
        } else {
            fade_background(current_id_panorama,false);
        }

        if(access_time_id!=0) {
            clearInterval(interval_access_time_avg);
            $.ajax({
                url: "ajax/set_statistics.php",
                type: "POST",
                data: {
                    type: 'room_time',
                    id: access_time_id,
                    access_time_avg: access_time_avg
                },
                async: true
            });
            access_time_avg = 0;
        }
    }

    function fade_background(current_id_panorama,click_m) {
        var id = get_id_viewer(current_id_panorama);
        var canvas = $('canvas')[0];
        var dataURL = canvas.toDataURL('image/jpeg', 0.5);
        $('#background_pano').off('load');
        if(vr_enabled) {
            $('#background_pano_vr').off('load');
            $('#background_pano_vr').on('load',function () {
                $('#background_pano_vr').show();
                setTimeout(function () {
                    $('#panorama_viewer_vr').css('opacity',0);
                    $('#video_viewer_vr').css('opacity',0);
                },50);
            }).attr('src',dataURL);
        }
        $('#background_pano').on('load',function () {
            $('#background_pano').show();
            if(!vr_enabled) {
                $('#loading_pano').show();
                $('#loading_pano').css('opacity',0.8);
            }
            setTimeout(function () {
                $('#panorama_viewer').css('opacity',0);
                $('#video_viewer').css('opacity',0);
                initialize_room(id,true,click_m);
                $('.pointer').animate({
                    opacity: 0.4
                }, { duration: 250, queue: false });
                $('.pointer_'+current_id_panorama).animate({
                    opacity: 1
                }, { duration: 250, queue: false });
                $('.view_direction__arrow').hide();
                $('.pointer_list').removeClass('active');
                $('.pointer_list_'+current_id_panorama).addClass('active');
                $('.list_alt_menu .dropdown p i').removeClass('active');
                $('.list_alt_'+current_id_panorama+' i').addClass('active');
                if($('.list_alt_'+current_id_panorama).hasClass('children')) {
                    var id_cat = $('.list_alt_'+current_id_panorama).attr('data-cat');
                    $('.cat i').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
                    if($('.cat_'+id_cat+' i').hasClass('fa-chevron-circle-right')) {
                        open_cat_list_alt(id_cat);
                    }
                } else {
                    open_cat_list_alt(0);
                }
                adjust_ratio_hfov();
            },50);
        }).attr('src',dataURL);
    }

    function adjust_ratio_hfov() {
        if(!live_session_connected) {
            if(!vr_enabled) {
                var c_w = $(window).width();
                var c_h = $(window).height();
                var ratio_panorama = c_w / c_h;
                var ratio_hfov = 1.7771428571428571 / ratio_panorama;
                if(ratio_hfov<1) ratio_hfov=1;
                var min_hfov_t = min_hfov / ratio_hfov;
                var max_hfov_t = max_hfov / ratio_hfov;
                var hfov_t = hfov / ratio_hfov;
                if(current_panorama_type=='image') {
                    try {
                        pano_viewer.setHfovBounds([min_hfov_t,max_hfov_t]);
                        pano_viewer.setHfov(hfov_t,false);
                    } catch (e) {}
                } else {
                    try {
                        video_viewer.pnlmViewer.setHfovBounds([min_hfov_t,max_hfov_t]);
                        video_viewer.pnlmViewer.setHfov(hfov_t,false);
                    } catch (e) {}
                }
            }
        }

    }

    $(window).resize(function () {
        adjust_ratio_hfov();
        try {
            sly.reload();
        } catch (e) {}
        if(window.innerHeight > window.innerWidth){
            if(window.innerWidth<500) {
                var touchPanSpeedCoeffFactor = 2.5;
                var friction = 0.1;
            } else {
                var touchPanSpeedCoeffFactor = 1;
                var friction = 0.2;
            }
        } else {
            var touchPanSpeedCoeffFactor = 1;
            var friction = 0.1;
        }
        if(current_panorama_type=='image') {
            try {
                pano_viewer.setFriction(friction);
                pano_viewer.setTouchPanSpeedCoeffFactor(touchPanSpeedCoeffFactor);
            } catch (e) {}
        } else {
            try {
                video_viewer.pnlmViewer.setFriction(friction);
                video_viewer.pnlmViewer.setTouchPanSpeedCoeffFactor(touchPanSpeedCoeffFactor);
            } catch (e) {}
        }
        $('#skitt-ui').css('margin-left','0px');
        if(window.innerWidth<540) {
            var info_control = false;
            var voice_control = false;
            var gallery_control = false;
            if((info_box!='') && (info_box!=null) && (info_box!='<p><br></p>')) {
                info_control = true;
            }
            if(voice_commands_enable>0) {
                voice_control = true;
            }
            if(gallery_images.length>0) {
                if(show_gallery) {
                    gallery_control = true;
                }
            }
            if((info_control) && (voice_control) && (gallery_control)) {
                $('.gallery_control').css('margin-left','-10px');
                $('#skitt-ui').css('margin-left','-18px');
            } else if((info_control) && (gallery_control)) {
                $('.gallery_control').css('margin-left','-10px');
            } else if((info_control) && (voice_control)) {
                $('#skitt-ui').css('margin-left','-10px');
            } else if((voice_control) && (gallery_control)) {
                $('#skitt-ui').css('margin-left','-10px');
            }
        } else {
            $('.gallery_control').css('margin-left','0px');
            $('#skitt-ui').css('margin-left','0px');
        }
    });

    $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function() {
        var isFullScreen = document.fullScreen ||
            document.mozFullScreen ||
            document.webkitIsFullScreen || (document.msFullscreenElement != null);
        if (isFullScreen) {
            $('.fullscreen_control').addClass('active_control');
            controls_status['fullscreen']=true;
        } else {
            $('.fullscreen_control').removeClass('active_control');
            controls_status['fullscreen']=false;
        }
    });

    function get_gallery() {
        $.ajax({
            url: "ajax/get_gallery.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtual_tour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if(rsp.length>0) {
                    initialize_gallery(rsp);
                }
                loading_config();
            },
            error: function () {
                loading_config();
            }
        });
    }

    function initialize_gallery(array_images) {
        jQuery.each(array_images, function(index, image) {
            gallery_images.push({"src":"gallery/"+image,"thumb":"gallery/thumb/"+image});
        });
    }

    function get_maps() {
        $.ajax({
            url: "ajax/get_maps.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtual_tour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == 'ok') {
                    array_maps = rsp.maps;
                }
                get_presentation();
            },
            error: function () {
                get_presentation();
            }
        });
    }

    function get_presentation() {
        $.ajax({
            url: "ajax/get_presentation.php",
            type: "POST",
            data: {
                id_virtualtour: id_virtual_tour
            },
            async: true,
            success: function (json) {
                var rsp = JSON.parse(json);
                if (rsp.status == 'ok') {
                    array_presentation = rsp.presentation;
                }
                get_gallery();
            },
            error: function () {
                get_gallery();
            }
        });
    }

    window.start_presentation = function () {
        if(controls_status['song']) {
            song_is_playng = true;
        } else {
            song_is_playng = false;
        }
        if(controls_status['map']) {
            map_opened = true;
        } else {
            map_opened = false;
        }
        if(current_panorama_type=='image') {
            pano_viewer.stopAutoRotate();
        } else {
            video_viewer.pnlmViewer.stopAutoRotate();
        }
        controls_status['presentation']=true;
        $('.custom-hotspot').css('opacity',0);
        $('.custom-hotspot-content').css('opacity',0);
        $('#floatingSocialShare').css('z-index',0);
        $('#floatingSocialShare').hide();
        $('.frame').css('z-index',0);
        $('.frame').css('opacity',0);
        $('.list_control').removeClass('active_control');
        $('.list_control').css('bottom','0px');
        $('.list_control i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        $('.info_control').css('bottom','4px');
        $('.gallery_control').css('bottom','4px');
        $('#skitt-ui').css('bottom','4px');
        $('.pnlm-compass').css('bottom','4px');
        $('.fb_dialog').css('bottom','4px');
        $('.map').hide();
        $('.pnlm-controls').hide();
        $('#dialog').show();
        $('#dialog').css('z-index',999);
        $('.pnlm-dragfix').css('pointer-events','none');
        $('.controls_control').css('pointer-events','none');
        $('.arrows_nav').hide();
        $('.vr_control_right').hide();
        $('.fb_dialog').css('opacity',0);
        $('.info_control').css('opacity',0);
        $('.fullscreen_control').css('opacity',0);
        $('.gallery_control').css('opacity',0);
        $('.list_control').css('opacity',0);
        $('.map_control').css('opacity',0);
        $('.song_control').css('opacity',0);
        $('.fb_dialog').css('pointer-events','none');
        $('.info_control').css('pointer-events','none');
        $('.fullscreen_control').css('pointer-events','none');
        $('.gallery_control').css('pointer-events','none');
        $('.list_control').css('pointer-events','none');
        $('.map_control').css('pointer-events','none');
        $('.song_control').css('pointer-events','none');
        $('.logo').css('pointer-events','none');
        $('#btn_stop_presentation').show();
        $('.menu_controls').hide();
        $('.list_alt_menu').hide();
        if(voice_commands_enable>0) {
            try {
                SpeechKITT.hide();
                if (annyang) { annyang.pause(); }
            } catch (e) {}
        }
        controls_status['orient']=false;
        if(current_panorama_type=='image') {
            pano_viewer.stopOrientation();
        } else {
            video_viewer.pnlmViewer.stopOrientation();
        }
        audio_player.volume = 1;
        audio_player.currentTime = 0;
        if(!audio_isPlaying()) audio_player.play();
        controls_status['song']=true;
        controls_status['map']=false;
        controls_status['icons']=false;
        controls_status['list']=false;
        controls_status['share']=false;
        presentation_steps(0);
    }

    window.stop_presentation = function () {
        controls_status['presentation']=false;
        try {
            typed.stop();
            typed.destroy();
        } catch (e) {}
        $('.custom-hotspot').css('opacity',1);
        $('.custom-hotspot-content').css('opacity',1);
        $('.pnlm-compass').show();
        $('.controls_control').show();
        $('.presentation_control').show();
        $('#dialog').hide();
        $('#dialog').css('z-index',0);
        $('.fb_dialog').css('opacity',1);
        $('.info_control').css('opacity',1);
        $('.fullscreen_control').css('opacity',1);
        $('.gallery_control').css('opacity',1);
        $('.list_control').css('opacity',1);
        $('.map_control').css('opacity',1);
        $('.song_control').css('opacity',1);
        $('.fb_dialog').css('pointer-events','initial');
        $('.info_control').css('pointer-events','initial');
        $('.fullscreen_control').css('pointer-events','initial');
        $('.gallery_control').css('pointer-events','initial');
        $('.list_control').css('pointer-events','initial');
        $('.map_control').css('pointer-events','initial');
        $('.song_control').css('pointer-events','initial');
        $('.logo').css('pointer-events','initial');
        $('.pnlm-dragfix').css('pointer-events','initial');
        switch(arrows_nav) {
            case 0:
                $('.arrows_nav').hide();
                break;
            case 1:
                $('.arrows_nav').show();
                break;
            case 2:
                if(window.is_mobile) {
                    $('.arrows_nav').hide();
                } else {
                    $('.arrows_nav').show();
                }
                break;
        }
        if (window.DeviceOrientationEvent && location.protocol == 'https:' && navigator.userAgent.toLowerCase().indexOf('mobi') >= 0) {
            $('.vr_control').show();
        } else {
            $('.vr_control').hide();
        }
        if(voice_commands_enable>0) {
            try {
                SpeechKITT.show();
            } catch (e) {}
            if(voice_commands_enable==2) {
                try {
                    if (annyang) { annyang.resume(); }
                } catch (e) {}
            }
        }
        controls_status['orient']=false;
        if(show_map!=0) {
            if(map_opened) {
                $('.map').show();
                $('.map_control').addClass('active_control');
                $('.map_control i').removeClass('icon-map_off').addClass('icon-map_on');
                controls_status['map']=true;
            }
        }
        controls_status['icons']=true;
        controls_status['list']=false;
        controls_status['share']=false;
        if(song_is_playng) {
            controls_status['song']=true;
            $('.song_control').addClass('active_control');
            $('.song_control i').addClass('fa-volume-down').removeClass('fa-volume-mute');
            audio_player.volume = 1;
        } else {
            controls_status['song']=false;
            $('.song_control').removeClass('active_control');
            $('.song_control i').removeClass('fa-volume-down').addClass('fa-volume-mute');
            audio_player.volume = 0;
        }
        $('#btn_stop_presentation').hide();
        $('.menu_controls').show();
        $('.list_alt_menu').show();
    }

    function get_id_viewer(id) {
        for(var i=0; i<panoramas.length; i++) {
            if(id==panoramas[i].id) {
                return i;
            }
        }
    }

    function presentation_steps(index) {
        if(controls_status['presentation']) {
            if(typeof array_presentation[index] === 'undefined') {
                stop_presentation();
                return;
            }
            var action = array_presentation[index].action;
            var params = array_presentation[index].params;
            var sleep_ms = array_presentation[index].sleep;
            switch (action) {
                case 'goto':
                    goto('',[params,null,null]);
                    setTimeout(function () {
                        presentation_steps(index+1);
                    },sleep_ms+1000);
                    break;
                case 'type':
                    type(params,function () {
                        setTimeout(function () {
                            presentation_steps(index+1);
                        },sleep_ms);
                    });
                    break;
                case 'lookAt':
                    if(current_panorama_type=='image') {
                        pano_viewer.lookAt(parseInt(params[0]),parseInt(params[1]),parseInt(params[2]),parseInt(params[3]),function () {
                            setTimeout(function () {
                                presentation_steps(index+1);
                            },sleep_ms);
                        });
                    } else {
                        video_viewer.pnlmViewer.lookAt(parseInt(params[0]),parseInt(params[1]),parseInt(params[2]),parseInt(params[3]),function () {
                            setTimeout(function () {
                                presentation_steps(index+1);
                            },sleep_ms);
                        });
                    }
                    break;
            }
        }
    }

    var typed;
    function type ( stringArray, onComplete ) {
        onComplete = onComplete || function(){};
        typed = new Typed( "#typed", {
            strings: stringArray,
            typeSpeed: 50,
            showCursor: false,
            startDelay: 0,
            onComplete: onComplete
        });
    }

    function lock (orientation) {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        }
        try {
            screen.orientation.lock(orientation);
        } catch (e) {}
    }

    function unlock () {
        try {
            screen.orientation.unlock();
        } catch (e) {}
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }

    window.enable_vr = function () {
        if(controls_status['map']) {
            map_opened = true;
        } else {
            map_opened = false;
        }
        if ('wakeLock' in navigator) {
            try {
                wl = navigator.wakeLock.request('screen');
            } catch (e) {}
        }
        $('.loading_vr').css('display','flex');
        $('.arrows_nav').hide();
        $('#panorama_viewer').css('width','50%');
        $('#panorama_viewer_vr').show();
        $('#video_viewer').css('width','50%');
        $('#video_viewer_vr').show();
        $('#background_pano').css('width','50%');
        $('.fb_dialog').css('opacity',0);
        $('.fullscreen_control').css('opacity',0);
        $('.gallery_control').css('opacity',0);
        $('.list_control').css('opacity',0);
        $('.song_control').css('opacity',0);
        $('.map_control').css('opacity',0);
        $('.annotation').css('opacity',0);
        $('.annotation').css('pointer-events','none');
        $('.fb_dialog').css('pointer-events','none');
        $('.info_control').css('pointer-events','none');
        $('.fullscreen_control').css('pointer-events','none');
        $('.gallery_control').css('pointer-events','none');
        $('.list_control').css('pointer-events','none');
        $('.map_control').css('pointer-events','none');
        $('.song_control').css('pointer-events','none');
        $('.logo').css('pointer-events','none');
        $('.logo').hide();
        $('.info_control').hide();
        $('.cursor_vr').show();
        if(voice_commands_enable>0) {
            try {
                SpeechKITT.hide();
                if (annyang) { annyang.pause(); }
            } catch (e) {}
        }
        vr_enabled = true;
        $('.menu_controls').hide();
        $('.list_alt_menu').hide();
        $('.header_vt').css('width','50%');
        $('.header_vt_vr').show();
        $('#btn_stop_vr').show();
        $('#btn_stop_vr_2').show();
        setTimeout(function () {
            lock('landscape-primary');
            goto('',[current_id_panorama,null,null]);
        },250);
    }

    window.disable_vr = function () {
        if ('wakeLock' in navigator) {
            try {
                wl.release();
                wl=null;
            } catch (e) {}
        }
        if ('orientation' in screen) {
            try {
                screen.orientation.unlock();
            } catch (e) {}
        }
        $('.loading_vr').css('display','flex');
        if(current_panorama_type=='image') {
            $('#panorama_viewer').css('width','100%');
            $('#panorama_viewer_vr').hide();
        } else {
            $('#video_viewer').css('width','100%');
            $('#video_viewer_vr').hide();
        }
        $('#background_pano').css('width','100%');
        $('.cursor_vr').hide();
        $('.logo').show();
        $('.info_control').show();
        $('.fb_dialog').css('opacity',1);
        $('.fullscreen_control').css('opacity',1);
        $('.gallery_control').css('opacity',1);
        $('.list_control').css('opacity',1);
        $('.song_control').css('opacity',1);
        $('.map_control').css('opacity',1);
        $('.annotation').css('opacity',1);
        $('.annotation').css('pointer-events','initial');
        $('.fb_dialog').css('pointer-events','initial');
        $('.info_control').css('pointer-events','initial');
        $('.fullscreen_control').css('pointer-events','initial');
        $('.gallery_control').css('pointer-events','initial');
        $('.list_control').css('pointer-events','initial');
        $('.map_control').css('pointer-events','initial');
        $('.song_control').css('pointer-events','initial');
        $('.logo').css('pointer-events','initial');
        $('.pnlm-controls-container').show();
        if(voice_commands_enable>0) {
            try {
                SpeechKITT.show();
            } catch (e) {}
            if(voice_commands_enable==2) {
                try {
                    if (annyang) { annyang.resume(); }
                } catch (e) {}
            }
        }
        if(show_map!=0) {
            if(map_opened) {
                $('.map').show();
                $('.map_control').addClass('active_control');
                $('.map_control i').removeClass('icon-map_off').addClass('icon-map_on');
                controls_status['map']=true;
            }
        }
        controls_status['icons']=true;
        controls_status['list']=false;
        controls_status['share']=false;
        if(arrows_nav) {
            $('.arrows_nav').show();
        }
        if((info_box!='') && (info_box!=null) && (info_box!='<p><br></p>')) {
            $('.info_control').show();
        } else {
            $('.info_control').hide();
        }
        clearInterval(interval_position);
        if(current_panorama_type=='image') {
            try {
                pano_viewer_vr.off('load');
                pano_viewer_vr.destroy();
            } catch (e) {}
        } else {
            try {
                video_viewer_vr.pnlmViewer.destroy();
                video_viewer_vr.dispose();
            } catch (e) {}
        }
        vr_enabled = false;
        $('.menu_controls').show();
        $('.list_alt_menu').show();
        $('.header_vt').css('width','100%');
        $('.header_vt_vr').hide();
        $('#btn_stop_vr').hide();
        $('#btn_stop_vr_2').hide();
        goto('',[current_id_panorama,null,null]);
        setTimeout(function () {
            $('.loading_vr').hide();
            unlock();
        },1000);
    }

    window.initialize_speech = function () {
        if (annyang) {
            $.ajax({
                url: "ajax/get_voice_commands.php",
                type: "POST",
                data: {
                    id_virtualtour: id_virtual_tour
                },
                async: false,
                success: function (json) {
                    var rsp = JSON.parse(json);
                    if(rsp.status=='ok') {
                        var commands = [];
                        var voice_commands = rsp.voice_commands[0];
                        annyang.setLanguage(voice_commands.language);
                        commands.push({
                            phrase : voice_commands.help_cmd,
                            callback : function() {
                                annyang.pause();
                                SpeechKITT.setInstructionsText(voice_commands.help_msg_1);
                                setTimeout(function () {
                                    SpeechKITT.setInstructionsText(voice_commands.help_msg_2);
                                    setTimeout(function () {
                                        SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                        annyang.resume();
                                    },4000);
                                },4000);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.next_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.next_msg);
                                var len = panoramas.length;
                                var index = get_id_viewer(current_id_panorama);
                                var next_panorama = panoramas[(index+1)%len];
                                goto('',[next_panorama.id,null,null]);
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.prev_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.prev_msg);
                                var len = panoramas.length;
                                var index = get_id_viewer(current_id_panorama);
                                var prev_panorama = panoramas[(index+len-1)%len];
                                goto('',[prev_panorama.id,null,null]);
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.left_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.left_msg);
                                if(current_panorama_type=='image') {
                                    var yaw_s = pano_viewer.getYaw();
                                    var pitch_s = pano_viewer.getPitch();
                                    var hfov_s = pano_viewer.getHfov();
                                    yaw_s = yaw_s - 90;
                                    pano_viewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                } else {
                                    var yaw_s = video_viewer.pnlmViewer.getYaw();
                                    var pitch_s = video_viewer.pnlmViewer.getPitch();
                                    var hfov_s = video_viewer.pnlmViewer.getHfov();
                                    yaw_s = yaw_s - 90;
                                    video_viewer.pnlmViewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                }
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.right_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.right_msg);
                                if(current_panorama_type=='image') {
                                    var yaw_s = pano_viewer.getYaw();
                                    var pitch_s = pano_viewer.getPitch();
                                    var hfov_s = pano_viewer.getHfov();
                                    yaw_s = yaw_s + 90;
                                    pano_viewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                } else {
                                    var yaw_s = video_viewer.pnlmViewer.getYaw();
                                    var pitch_s = video_viewer.pnlmViewer.getPitch();
                                    var hfov_s = video_viewer.pnlmViewer.getHfov();
                                    yaw_s = yaw_s + 90;
                                    video_viewer.pnlmViewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                }
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.up_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.up_msg);
                                if(current_panorama_type=='image') {
                                    var yaw_s = pano_viewer.getYaw();
                                    var pitch_s = pano_viewer.getPitch();
                                    var hfov_s = pano_viewer.getHfov();
                                    pitch_s = pitch_s + 45;
                                    pano_viewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                } else {
                                    var yaw_s = video_viewer.pnlmViewer.getYaw();
                                    var pitch_s = video_viewer.pnlmViewer.getPitch();
                                    var hfov_s = video_viewer.pnlmViewer.getHfov();
                                    pitch_s = pitch_s + 45;
                                    video_viewer.pnlmViewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                }
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        commands.push({
                            phrase : voice_commands.down_cmd,
                            callback : function() {
                                annyang.pause();
                                $('#skitt-ui').addClass('ok');
                                $('#skitt-toggle-button').addClass('ok');
                                SpeechKITT.setInstructionsText(voice_commands.down_msg);
                                if(current_panorama_type=='image') {
                                    var yaw_s = pano_viewer.getYaw();
                                    var pitch_s = pano_viewer.getPitch();
                                    var hfov_s = pano_viewer.getHfov();
                                    pitch_s = pitch_s - 45;
                                    pano_viewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                } else {
                                    var yaw_s = video_viewer.pnlmViewer.getYaw();
                                    var pitch_s = video_viewer.pnlmViewer.getPitch();
                                    var hfov_s = video_viewer.pnlmViewer.getHfov();
                                    pitch_s = pitch_s - 45;
                                    video_viewer.pnlmViewer.lookAt(pitch_s,yaw_s,hfov_s,2000);
                                }
                                setTimeout(function () {
                                    $('#skitt-ui').removeClass('ok');
                                    $('#skitt-toggle-button').removeClass('ok');
                                    SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                    annyang.resume();
                                },1500);
                            }
                        });
                        annyang.addCommandsWithDynamicText(commands);
                        annyang.addCallback('resultNoMatch', function(userSaid, commandText, phrases) {
                            annyang.pause();
                            $('#skitt-ui').addClass('error');
                            $('#skitt-toggle-button').addClass('error');
                            SpeechKITT.setInstructionsText(voice_commands.error_msg);
                            setTimeout(function () {
                                $('#skitt-ui').removeClass('error');
                                $('#skitt-toggle-button').removeClass('error');
                                SpeechKITT.setInstructionsText(voice_commands.listening_msg);
                                annyang.resume();
                            },3000);
                        });
                        SpeechKITT.annyang();
                        SpeechKITT.setStartCommand(function() {
                            annyang.start({ autoRestart: true, continuous: true, paused: true });
                        });
                        SpeechKITT.setStylesheet('vendor/SpeechKITT/themes/flat.css?v=15');
                        SpeechKITT.setInstructionsText(voice_commands.initial_msg);
                        SpeechKITT.render();
                        if(voice_commands_enable==2) {
                            annyang.resume();
                        }
                    }
                }
            });
        }
    }

    window.click_menu_controls = function () {
        $('.menu_controls .dropdown')[0].classList.toggle('down');
        $('.menu_controls .arrow')[0].classList.toggle('gone');
        if ($('.menu_controls .dropdown')[0].classList.contains('down')) {
            close_list_alt_menu();
            setTimeout(function() {
                $('.menu_controls .dropdown')[0].style.overflow = 'visible';
            }, 100)
        } else {
            $('.menu_controls .dropdown')[0].style.overflow = 'hidden';
        }
    }

    window.click_list_alt_menu = function () {
        $('.list_alt_menu .dropdown')[0].classList.toggle('down');
        $('.list_alt_menu .arrow')[0].classList.toggle('gone');
        if ($('.list_alt_menu .dropdown')[0].classList.contains('down')) {
            $('.list_alt_menu .title i').removeClass('far').addClass('fas');
            setTimeout(function() {
                $('.list_alt_menu .dropdown')[0].style.overflow = 'visible';
            }, 100)
        } else {
            $('.list_alt_menu .title i').removeClass('fas').addClass('far');
            $('.list_alt_menu .dropdown')[0].style.overflow = 'hidden';
        }
    }

    window.close_menu_controls = function () {
        $('.menu_controls .dropdown')[0].classList.remove('down');
        $('.menu_controls .arrow')[0].classList.remove('gone');
        $('.menu_controls .dropdown')[0].style.overflow = 'hidden';
    }

    window.close_list_alt_menu = function () {
        $('.list_alt_menu .dropdown')[0].classList.remove('down');
        $('.list_alt_menu .arrow')[0].classList.remove('gone');
        $('.list_alt_menu .dropdown')[0].style.overflow = 'hidden';
        $('.list_alt_menu .title i').removeClass('fas').addClass('far');
    }

    window.init_peer = function() {
        if(peer_id=='') {
            init_sender_viewer();
            $('.live_status span').html('initializing ...');
            $('#btn_live_end').hide();
            $('#btn_link_session').hide();
            $('#btn_live_status').css('color','black');
            $('.live_call').show();
            $(".video_my_wrapper").draggable();
            $(".video_remote_wrapper").draggable();
            window.peer = new Peer(null,{
                host: 'svtpeerjs.simpledemo.it',
                port: '443',
                path: '/svt',
                secure: true,
                debug: 2
            });
            peer.on('open', function(id) {
                init_sender();
                id_live_session = id;
                $('.live_status span').html('awaiting connection ...');
                $('#btn_live_end').show();
                $('#btn_link_session').show();
                $('#btn_live_status').css('color','orange');
                open_live_link_modal();
            });
            peer.on('error', function (err) {
                if(err.toString().indexOf('concurrent') !== -1) {
                    setTimeout(function () {
                        init_peer();
                    },2000);
                } else if(err.toString().indexOf('not get an ID') !== -1) {
                    setTimeout(function () {
                        init_peer();
                    },2000);
                } else {
                    alert(err);
                    try {
                        window.stream_sender.getTracks().forEach(function(track) { track.stop(); })
                    } catch (e) {}
                    try {
                        call_session.close();
                    } catch (e) {}
                    $('#webcam_my').hide();
                    peer.destroy();
                    live_session_connected = false;
                    clearInterval(interval_live_session);
                    $('.live_call').hide();
                    $('.live_control').removeClass('active_control');
                    exit_sender_viewer();
                }
            });
        } else {
            init_receiver_viewer();
            $('.live_status').css('width','120px');
            $('.live_status').css('left','calc(50% - 60px)');
            $('.live_call').show();
            $(".video_my_wrapper").draggable();
            $(".video_remote_wrapper").draggable();
            window.peer = new Peer(null,{
                host: 'svtpeerjs.simpledemo.it',
                port: '443',
                path: '/svt',
                secure: true,
                debug: 2
            });
            peer.on('open', function(id) {
                $('.live_status span').html('connecting ...');
                $('#btn_live_status').css('color','orange');
                init_receiver();
            });
            peer.on('error', function (err) {
                if(err.toString().indexOf('concurrent') !== -1) {
                    setTimeout(function () {
                        init_peer();
                    },2000);
                } else if(err.toString().indexOf('not get an ID') !== -1) {
                    setTimeout(function () {
                        init_peer();
                    },2000);
                } else if(err.toString().indexOf('Lost connection') !== -1) {
                    setTimeout(function () {
                        init_peer();
                    },2000);
                } else if(err.toString().indexOf('not connect to peer') !== -1) {
                    $('.live_status span').html('invalid session');
                    $('#btn_live_status').css('color','red');
                    setTimeout(function () {
                        try {
                            window.stream_sender.getTracks().forEach(function(track) { track.stop(); })
                        } catch (e) {}
                        try {
                            call_session.close();
                        } catch (e) {}
                        $('#webcam_my').hide();
                        peer.destroy();
                        $('.live_call').hide();
                        exit_receiver_viewer();
                    },5000);
                }
            });
        }
    }

    window.open_live_link_modal = function () {
        var link_live = window.url_vt+"index.php?code="+window.code+"&peer_id="+id_live_session;
        var link_live_share = link_live.replace("&","%26");
        var html_live = '<div class="modal_live_link">' +
            '<span style="margin: 0 auto;">Send this link to the person you want to invite</span><br><br>' +
            '<textarea id="live_link" rows="3" style="width:100%;padding:5px;text-align:center" readonly>'+link_live+'</textarea><br>'+
            '<i data-clipboard-target="#live_link" class="fas fa-clipboard"></i> <a target="_blank" href="mailto:?body='+link_live_share+'"><i class="fas fa-envelope"></i></a> <a target="_blank" href="https://web.whatsapp.com/send?text='+link_live_share+'"><i class="fab fa-whatsapp"></i></a>'+
            '</div>';
        $.fancybox.open({
            clickSlide: false,
            clickOutside: false,
            smallBtn: false,
            touch: false,
            src  : html_live,
            type : 'html',
            afterShow : function () {
                new ClipboardJS('.modal_live_link i');
            }
        });
    }

    function init_sender() {
        var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        getUserMedia({video: true, audio: true}, function(stream) {
            $('#webcam_my').show();
            window.webcam_my.srcObject = stream;
            window.webcam_my.volume = 0;
            window.stream_sender = stream;
            peer.on('call', function(call) {
                call_session = call;
                call.answer(stream);
                call.on('stream', function(remoteStream) {
                    $('#webcam_remote').show();
                    window.webcam_remote.srcObject = remoteStream;
                });
            });
        }, function(err) {
            console.log('Failed to get local stream' ,err);
        });
        window.peer.on('connection', function(conn) {
            if (window.peer_conn && window.peer_conn.open) {
                conn.on('open', function() {
                    setTimeout(function() { conn.close(); }, 500);
                });
                return;
            }
            window.peer_conn = conn;
            window.peer_conn.on('data', function(data) {
                switch(data.type) {
                    case 'chat':
                        if(!$('.floating-chat').hasClass('expand')) {
                            $('.floating-chat').addClass('blink');
                        }
                        receiveMessage(data.message);
                        break;
                }
            });
            window.peer_conn.on('close', function() {
                live_session_connected = false;
                clearInterval(interval_live_session);
                $('.live_status span').html('awaiting connection ...');
                $('#webcam_remote').hide();
                $('#btn_live_status').css('color','orange');
                window.peer_conn = null;
            });
            $.fancybox.close(true);
            setTimeout(function() {
                live_chat.addClass('enter');
                live_chat.click(openLiveChat);
                $('#webcam_remote').show();
                $('.live_status span').html('connected');
                $('#btn_live_status').css('color','green');
                live_session_connected = true;
                goto('',[panoramas[0].id,null,null]);
            }, 1000);
        });
    }

    function init_receiver() {
        window.peer_conn = window.peer.connect(peer_id, {
            reliable: true
        });
        window.peer_conn.on('open', function() {
            var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            getUserMedia({video: true, audio: true}, function(stream) {
                $('#webcam_my').show();
                window.webcam_remote.srcObject = stream;
                window.webcam_remote.volume = 0;
                window.stream_sender = stream;
                var call = peer.call(peer_id, stream);
                call_session = call;
                call.on('stream', function(remoteStream) {
                    $('#webcam_remote').show();
                    window.webcam_my.srcObject = remoteStream;
                });
            }, function(err) {
                console.log('Failed to get local stream' ,err);
            });
            setTimeout(function() {
                live_chat.addClass('enter');
                live_chat.click(openLiveChat);
                $('.live_status span').html('connected');
                $('#btn_live_status').css('color','green');
                $('#btn_live_end').attr('onclick','close_live_receiver()');
                $('#btn_live_end').show();
            }, 1000);
        });
        window.peer_conn.on('data', function(data) {
            switch(data.type) {
                case 'chat':
                    if(!$('.floating-chat').hasClass('expand')) {
                        $('.floating-chat').addClass('blink');
                    }
                    receiveMessage(data.message);
                    break;
                case 'goto':
                    goto('',data.args);
                    break;
                case'lookAt':
                    if(current_panorama_type=='image') {
                        pano_viewer.lookAt(parseInt(data.pitch),parseInt(data.yaw),parseInt(data.hfov));
                    } else {
                        video_viewer.pnlmViewer.lookAt(parseInt(data.pitch),parseInt(data.yaw),parseInt(data.hfov));
                    }
                    break;
                case 'view_content':
                    view_content('',data.args);
                    break;
                case 'close_content':
                    $.fancybox.close(true);
                    break;
                case 'view_video':
                    $('#'+data.id).trigger('click');
                    break;
            }
        });
        window.peer_conn.on('disconnected', function () {
            console.log('Connection lost. Please reconnect');
        });
        window.peer_conn.on('close', function() {
            console.log('Connection closed');
            $('#btn_live_end').hide();
            $('.live_status span').html('connection closed');
            $('#btn_live_status').css('color','red');
            setTimeout(function () {
                try {
                    window.stream_sender.getTracks().forEach(function(track) { track.stop(); })
                } catch (e) {}
                try {
                    call_session.close();
                } catch (e) {}
                $('#webcam_my').hide();
                peer.destroy();
                $('.live_call').hide();
                exit_receiver_viewer();
            },2000);
        });
        window.peer_conn.on('error', function (err) {
            console.log(err);
        });
    }

    function init_receiver_viewer() {
        $('.msg_lock').show();
        $('#div_panoramas').css('pointer-events','none');
        $('.frame').css('pointer-events','none');
        $('.annotation').addClass('hidden');
        $('.fb_dialog').addClass('hidden');
        $('.info_control').addClass('hidden');
        $('.gallery_control').addClass('hidden');
        $('.list_control').addClass('hidden');
        $('.song_control').addClass('hidden');
        $('.arrows_nav').addClass('hidden');
        audio_player.volume = 0;
        try {
            audio_player.pause();
        } catch (e) {}
        controls_status['song']=false;
        $('.song_control').removeClass('active_control');
        $('.song_control i').removeClass('fa-volume-down').addClass('fa-volume-mute');
    }

    function exit_receiver_viewer() {
        $('#div_panoramas').css('pointer-events','initial');
        $('.frame').css('pointer-events','initial');
        $('.fb_dialog').removeClass('hidden');
        $('.info_control').removeClass('hidden');
        $('.gallery_control').removeClass('hidden');
        $('.list_control').removeClass('hidden');
        $('.song_control').removeClass('hidden');
        $('.arrows_nav').removeClass('hidden');
        $('.annotation').removeClass('hidden');
        $('.msg_lock').hide();
    }

    function init_sender_viewer() {
        $('.menu_controls').css('pointer-events','none');
        $('.annotation').addClass('hidden');
        $('.fb_dialog').addClass('hidden');
        $('.info_control').addClass('hidden');
        $('.gallery_control').addClass('hidden');
        $('.list_control').addClass('hidden');
        $('.song_control').addClass('hidden');
        audio_player.volume = 0;
        try {
            audio_player.pause();
        } catch (e) {}
        $('.song_control').removeClass('active_control');
        $('.song_control i').removeClass('fa-volume-down').addClass('fa-volume-mute');
        controls_status['song']=false;
        if(voice_commands_enable>0) {
            try {
                SpeechKITT.hide();
                if (annyang) { annyang.pause(); }
            } catch (e) {}
        }
    }

    function exit_sender_viewer() {
        $('.menu_controls').css('pointer-events','initial');
        $('.fb_dialog').removeClass('hidden');
        $('.info_control').removeClass('hidden');
        $('.gallery_control').removeClass('hidden');
        $('.list_control').removeClass('hidden');
        $('.song_control').removeClass('hidden');
        $('.annotation').removeClass('hidden');
    }

    function openLiveChat() {
        $('.floating-chat').removeClass('blink');
        var messages = live_chat.find('.messages');
        var textInput = live_chat.find('.text-box');
        live_chat.find('>i').hide();
        live_chat.addClass('expand');
        live_chat.find('.chat').addClass('enter');
        textInput.keydown(onChatEnter).prop("disabled", false).focus();
        live_chat.off('click', openLiveChat);
        live_chat.find('.header button').click(closeLiveChat);
        live_chat.find('#sendMessage').click(sendNewMessage);
        messages.scrollTop(messages.prop("scrollHeight"));
    }

    function closeLiveChat() {
        live_chat.find('.chat').removeClass('enter').hide();
        live_chat.find('>i').show();
        live_chat.removeClass('expand');
        live_chat.find('.header button').off('click', closeLiveChat);
        live_chat.find('#sendMessage').off('click', sendNewMessage);
        live_chat.find('.text-box').off('keydown', onChatEnter).prop("disabled", true).blur();
        setTimeout(function() {
            live_chat.find('.chat').removeClass('enter').show()
            live_chat.click(openLiveChat);
        }, 500);
    }

    function sendNewMessage() {
        var userInput = $('.text-box');
        var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');
        if (!newMessage) return;
        var newMessage_s = urlify(newMessage);
        var messagesContainer = $('.messages');
        messagesContainer.append([
            '<li class="self">',
            newMessage_s,
            '</li>'
        ].join(''));
        userInput.html('');
        userInput.focus();
        messagesContainer.finish().animate({
            scrollTop: messagesContainer.prop("scrollHeight")
        }, 250);
        try {
            peer_conn.send({type:'chat',message:newMessage});
        } catch (e) {}
    }

    function receiveMessage(message) {
        var message = message.replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');
        var message_s = urlify(message);
        var messagesContainer = $('.messages');
        messagesContainer.append([
            '<li class="other">',
            message_s,
            '</li>'
        ].join(''));
        messagesContainer.finish().animate({
            scrollTop: messagesContainer.prop("scrollHeight")
        }, 250);
    }

    function onChatEnter(event) {
        if (event.keyCode == 13) {
            sendNewMessage();
            event.preventDefault();
        }
    }

    function urlify(text) {
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return text.replace(urlRegex, function(url) {
            return '<a style="color: white;text-decoration: underline;" target="_blank" href="' + url + '">' + url + '</a>';
        })
    }

})(jQuery);