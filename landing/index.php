<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
session_start();
require_once("../db/connection.php");
$v = time();
if((isset($_GET['furl'])) || (isset($_GET['code']))) {
    if(isset($_GET['furl'])) {
        $furl = $_GET['furl'];
        $query = "SELECT v.html_landing,v.code,v.name as name_virtualtour,v.background_image,v.description,u.expire_plan_date FROM svt_virtualtours AS v
                JOIN svt_users AS u ON u.id=v.id_user
                WHERE v.friendly_url = '$furl' AND v.active=1 LIMIT 1;";
    }
    if(isset($_GET['code'])) {
        $code = $_GET['code'];
        $query = "SELECT v.html_landing,v.code,v.name as name_virtualtour,v.background_image,v.description,u.expire_plan_date FROM svt_virtualtours AS v
                JOIN svt_users AS u ON u.id=v.id_user
                WHERE v.code = '$code' AND v.active=1 LIMIT 1;";
    }
    $result = $mysqli->query($query);
    if($result) {
        if ($result->num_rows == 1) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if($row['expire_plan_date']!=null) {
                if (new DateTime() > new DateTime($row['expire_plan_date'])) {
                    die("Expired link");
                }
            }
            $code = $row['code'];
            $name_virtualtour = strtoupper($row['name_virtualtour']);
            $background_image = $row['background_image'];
            $description = $row['description'];
            $html_landing = $row['html_landing'];
        } else {
            die("Invalid link");
        }
    } else {
        die("Invalid link");
    }
} else {
    die("Invalid link");
}
$currentPath = $_SERVER['PHP_SELF'];
$pathInfo = pathinfo($currentPath);
$hostName = $_SERVER['HTTP_HOST'];
$protocol = strtolower(substr($_SERVER["REQUEST_SCHEME"],0,5))=='https'?'https://':'http://';
$url = $protocol.$hostName.$pathInfo['dirname']."/";
$url = str_replace("/landing/","/viewer/",$url);

$iframe_html = "<iframe allowfullscreen allow=\"gyroscope; accelerometer; xr; microphone *\" width=\"100%\" height=\"100%\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"".$url."index.php?code=$code\"></iframe>";
$html_landing = str_replace("<img style=\"width: 100%;\" src=\"snippets/preview/vt_preview.jpg\">",$iframe_html,$html_landing);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $name_virtualtour; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta property="og:title" content="<?php echo $name_virtualtour; ?>">
    <?php if($background_image!='') : ?>
    <meta property="og:image" content="<?php echo $url."content/".$background_image; ?>" />
    <?php endif; ?>
    <?php if($description!='') : ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <meta property="og:description" content="<?php echo $description; ?>" />
    <?php endif; ?>
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="../backend/vendor/keditor/plugins/bootstrap-3.4.1/css/bootstrap.min.css" data-type="keditor-style" />
</head>
<body>
    <style>
        body {
            overflow-x: hidden;
        }
        .row {
            padding: 15px;
        }
    </style>
    <?php echo $html_landing; ?>
</body>
</html>
