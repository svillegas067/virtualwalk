CREATE TABLE IF NOT EXISTS `svt_plans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `n_virtual_tours` int(11) NOT NULL DEFAULT '-1',
  `n_rooms` int(11) NOT NULL DEFAULT '-1',
  `n_markers` int(11) NOT NULL DEFAULT '-1',
  `n_pois` int(11) NOT NULL DEFAULT '-1',
  `days` int(11) NOT NULL DEFAULT '-1',
  `create_landing` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `svt_plans` (`id`, `name`) VALUES(1, 'Unlimited');

CREATE TABLE IF NOT EXISTS `svt_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` varchar(50) DEFAULT 'customer',
  `id_plan` bigint(20) DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `forgot_code` varchar(16) DEFAULT NULL,
  `registration_date` timestamp DEFAULT CURRENT_TIMESTAMP,
  `expire_plan_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `svt_users` (`id`, `username`, `password`, `role`) VALUES(1, '<USER_NAME>', MD5(<PASSWORD>), 'administrator');

CREATE TABLE IF NOT EXISTS `svt_virtualtours` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `date_created` date NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `hfov` int(11) NOT NULL DEFAULT '100',
  `min_hfov` int(11) NOT NULL DEFAULT '60',
  `max_hfov` int(11) NOT NULL DEFAULT '100',
  `song` varchar(50) DEFAULT NULL,
  `song_autoplay` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(50) DEFAULT NULL,
  `link_logo` varchar(250) DEFAULT NULL,
  `nadir_logo` varchar(50) DEFAULT NULL,
  `nadir_size` varchar(25) NOT NULL DEFAULT 'small',
  `autorotate_speed` int(11) NOT NULL DEFAULT '0',
  `autorotate_inactivity` int(11) NOT NULL DEFAULT '0',
  `markers_icon` varchar(50) NOT NULL DEFAULT 'fas fa-chevron-circle-up',
  `markers_background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)',
  `markers_color` varchar(25) NOT NULL DEFAULT '#000000',
  `markers_show_room` tinyint(1) NOT NULL DEFAULT '1',
  `pois_icon` varchar(50) NOT NULL DEFAULT 'fas fa-info-circle',
  `pois_background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)',
  `pois_color` varchar(25) NOT NULL DEFAULT '#000000',
  `pois_style` tinyint(1) NOT NULL DEFAULT '0',
  `arrows_nav` tinyint(1) NOT NULL DEFAULT '1',
  `info_box` longtext,
  `password` varchar(200) DEFAULT NULL,
  `voice_commands` tinyint(1) NOT NULL DEFAULT '0',
  `compass` tinyint(1) NOT NULL DEFAULT '1',
  `background_image` varchar(50) DEFAULT NULL,
  `auto_start` tinyint(1) NOT NULL DEFAULT '1',
  `description` text DEFAULT NULL,
  `ga_tracking_id` varchar(50) DEFAULT NULL,
  `friendly_url` varchar(200) DEFAULT NULL,
  `compress_jpg` int(11) NOT NULL DEFAULT '90',
  `max_width_compress` int(11) NOT NULL DEFAULT '8192',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sameAzimuth` tinyint(1) NOT NULL DEFAULT '0',
  `auto_show_slider` tinyint(1) NOT NULL DEFAULT '0',
  `form_enable` tinyint(1) NOT NULL DEFAULT '0',
  `form_icon` varchar(50) NOT NULL DEFAULT 'fas fa-file-signature',
  `form_content` text DEFAULT NULL,
  `html_landing` longtext,
  `fb_messenger` tinyint(1) NOT NULL DEFAULT '0',
  `fb_page_id` varchar(100) DEFAULT NULL,
  `show_info` tinyint(1) NOT NULL DEFAULT '1',
  `show_gallery` tinyint(1) NOT NULL DEFAULT '1',
  `show_icons_toggle` tinyint(1) NOT NULL DEFAULT '1',
  `show_presentation` tinyint(1) NOT NULL DEFAULT '1',
  `show_main_form` tinyint(1) NOT NULL DEFAULT '1',
  `show_share` tinyint(1) NOT NULL DEFAULT '1',
  `show_device_orientation` tinyint(1) NOT NULL DEFAULT '1',
  `show_webvr` tinyint(1) NOT NULL DEFAULT '1',
  `show_map` tinyint(1) NOT NULL DEFAULT '1',
  `show_fullscreen` tinyint(1) NOT NULL DEFAULT '1',
  `show_audio` tinyint(1) NOT NULL DEFAULT '1',
  `live_session` tinyint(1) NOT NULL DEFAULT '0',
  `show_annotations` tinyint(1) NOT NULL DEFAULT '1',
  `show_list_alt` tinyint(1) NOT NULL DEFAULT '0',
  `list_alt` text,
  `intro_desktop` varchar(50) DEFAULT NULL,
  `intro_mobile` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `svt_virtualtours_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `svt_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_rooms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` enum('image','video') DEFAULT 'image',
  `panorama_image` varchar(100) DEFAULT NULL,
  `northOffset` int(11) NOT NULL DEFAULT '0',
  `pitch` int(11) NOT NULL DEFAULT '0',
  `yaw` int(11) NOT NULL DEFAULT '0',
  `id_map` bigint(20) unsigned DEFAULT NULL,
  `map_top` int(11) DEFAULT NULL,
  `map_left` int(11) DEFAULT NULL,
  `allow_pitch` tinyint(1) NOT NULL DEFAULT '1',
  `min_pitch` int(11) NOT NULL DEFAULT '-90',
  `max_pitch` int(11) NOT NULL DEFAULT '90',
  `priority` int(11) NOT NULL DEFAULT '0',
  `access_count` bigint(20) NOT NULL DEFAULT '0',
  `visible_list` tinyint(1) NOT NULL DEFAULT '1',
  `song` varchar(50) DEFAULT NULL,
  `annotation_title` varchar(100) DEFAULT NULL,
  `annotation_description` text,
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_rooms_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_markers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_room` bigint(20) unsigned DEFAULT NULL,
  `pitch` int(11) DEFAULT NULL,
  `yaw` int(11) DEFAULT NULL,
  `rotateX` int(11) NOT NULL DEFAULT '0',
  `rotateZ` int(11) NOT NULL DEFAULT '0',
  `id_room_target` bigint(20) unsigned DEFAULT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'fas fa-chevron-circle-up',
  `id_icon_library` bigint(20) unsigned NOT NULL DEFAULT '0',
  `background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)',
  `color` varchar(25) NOT NULL DEFAULT '#000000',
  `size_scale` float NOT NULL DEFAULT '1',
  `show_room` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  KEY `id_room_target` (`id_room_target`),
  CONSTRAINT `svt_markers_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `svt_markers_ibfk_2` FOREIGN KEY (`id_room_target`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_pois` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_room` bigint(20) unsigned DEFAULT NULL,
  `pitch` int(11) DEFAULT NULL,
  `yaw` int(11) DEFAULT NULL,
  `type` enum('image','video','link','link_ext','html','html_sc','download','form','video360','audio','gallery') DEFAULT NULL,
  `style` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(50) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `id_icon_library` bigint(20) unsigned NOT NULL DEFAULT '0',
  `background` varchar(25) NOT NULL DEFAULT 'rgba(255,255,255,0.7)',
  `color` varchar(25) NOT NULL DEFAULT '#000000',
  `size_scale` float NOT NULL DEFAULT '1',
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `content` longtext,
  `access_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  CONSTRAINT `svt_pois_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_access_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_access_log_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_presentations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
  `id_room` bigint(20) unsigned DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `params` text,
  `sleep` int(11) NOT NULL DEFAULT '0',
  `priority_1` int(11) DEFAULT NULL,
  `priority_2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_virtual_tour` (`id_virtualtour`),
  KEY `id_room` (`id_room`),
  CONSTRAINT `svt_presentations_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `svt_presentations_ibfk_2` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_gallery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_gallery_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_maps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned NOT NULL,
  `map` varchar(200) DEFAULT NULL,
  `point_color` varchar(25) NOT NULL DEFAULT '#005eff',
  `name` varchar(200) DEFAULT NULL,
  `point_size` int(11) NOT NULL DEFAULT '20',
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_maps_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_voice_commands` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) NOT NULL DEFAULT 'en-US',
  `initial_msg` varchar(200) NOT NULL DEFAULT 'Listening ... Say HELP for command list',
  `listening_msg` varchar(200) NOT NULL DEFAULT 'Listening ...',
  `next_cmd` varchar(200) NOT NULL DEFAULT 'next',
  `next_msg` varchar(200) NOT NULL DEFAULT 'Ok, going to next room',
  `prev_cmd` varchar(200) NOT NULL DEFAULT 'prev',
  `prev_msg` varchar(200) NOT NULL DEFAULT 'Ok, going to previous room',
  `left_cmd` varchar(200) NOT NULL DEFAULT 'left',
  `left_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking left',
  `right_cmd` varchar(200) NOT NULL DEFAULT 'right',
  `right_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking right',
  `up_cmd` varchar(200) NOT NULL DEFAULT 'up',
  `up_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking up',
  `down_cmd` varchar(200) NOT NULL DEFAULT 'down',
  `down_msg` varchar(200) NOT NULL DEFAULT 'Ok, looking down',
  `help_cmd` varchar(200) NOT NULL DEFAULT 'help',
  `help_msg_1` varchar(200) NOT NULL DEFAULT 'Say NEXT / PREVIOUS to navigate between rooms',
  `help_msg_2` varchar(200) NOT NULL DEFAULT 'Say LEFT / RIGHT / UP / DOWN to look around',
  `error_msg` varchar(200) NOT NULL DEFAULT 'I do not understand, repeat please...',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `svt_voice_commands` (`id`) VALUES(1);

CREATE TABLE IF NOT EXISTS `svt_settings` (
  `id` int(11) NOT NULL DEFAULT '0',
  `purchase_code` varchar(250) DEFAULT NULL,
  `license` varchar(250) DEFAULT NULL,
  `name` varchar(200) DEFAULT 'Simple Virtual Tour',
  `logo` varchar(50) DEFAULT NULL,
  `background` varchar(50) DEFAULT NULL,
  `smtp_server` varchar(100) DEFAULT NULL,
  `smtp_auth` tinyint(1) NOT NULL DEFAULT '0',
  `smtp_username` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `smtp_secure` enum('none','ssl','tls') DEFAULT NULL,
  `smtp_port` int(11) DEFAULT NULL,
  `smtp_from_email` varchar(100) DEFAULT NULL,
  `smtp_from_name` varchar(100) DEFAULT NULL,
  `smtp_valid` tinyint(1) NOT NULL DEFAULT '0',
  `enable_registration` tinyint(1) NOT NULL DEFAULT '0',
  `default_id_plan` bigint(20) unsigned DEFAULT '1',
  `furl_blacklist` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `svt_settings` (`id`) VALUES(1);

CREATE TABLE IF NOT EXISTS `svt_icons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_icon_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_rooms_access_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_room` bigint(20) unsigned DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  CONSTRAINT `svt_rooms_access_log_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `svt_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_forms_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_virtualtour` bigint(20) unsigned NOT NULL,
  `id_room` bigint(20) unsigned DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `field1` text,
  `field2` text,
  `field3` text,
  `field4` text,
  `field5` text,
  PRIMARY KEY (`id`),
  KEY `id_virtualtour` (`id_virtualtour`),
  CONSTRAINT `svt_forms_data_ibfk_1` FOREIGN KEY (`id_virtualtour`) REFERENCES `svt_virtualtours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `svt_poi_gallery` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_poi` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_poi` (`id_poi`),
  CONSTRAINT `svt_poi_gallery_ibfk_1` FOREIGN KEY (`id_poi`) REFERENCES `svt_pois` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;