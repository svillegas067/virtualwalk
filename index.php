<?php
if(!file_exists("config/config.inc.php")) {
    header("Location: install/start.php");
} else {
    header("Location: backend/login.php");
}